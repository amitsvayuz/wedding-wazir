/*
     Project: BooSign | Responsive Bootstrap One Page HTML Template
     Author : Alex Ashihmin
 */

'use strict';

/*
 Header State after Scroll
 */
$(window).on('scroll load', function () {
    var e = $(window).scrollTop(), t = $('#header').height();
    e >= t ? $('#header').addClass('navbar-small') : $('#header').removeClass('navbar-small');
});

$(document).ready(function () {

    /*
     Show Page Container
     */
    $('#page-container').addClass('in');

    /*
        Masonry
     */

    if($('.masonry-container').length > 0) {
        $('.masonry-container').imagesLoaded( function () {
            $('.masonry-container').masonry({
                columnWidth: '.item',
                itemSelector: '.item'
            });
        });
    }

    /*
        Back to top button
     */
    if ($('#back-to-top').length) {
        var scrollTrigger = 800,
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    /*
     Change Color Theme
     */
    $('[data-click="theme-settings-expand"]').on("click", function () {
        var e = ".theme-settings", t = "active";
        $(e).hasClass(t) ? $(e).removeClass(t) : $(e).addClass(t)
    });
    $(".theme-list [data-theme]").on("click", function () {
        var e = "assets/css/theme/" + $(this).attr("data-theme") + ".css";
        $("#theme").attr("href", e), $(".theme-list [data-theme]").not(this).closest("li").removeClass("active"), $(this).closest("li").addClass("active")
    });
});