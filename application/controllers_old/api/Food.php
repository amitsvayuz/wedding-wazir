<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Food extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
      
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/ke
        $this->load->model('admin_model');
        $this->load->model('calender_model');
        $this->load->model('inquiry_model');
    }

        public function foodenvite_get()
	{
	foreach (getallheaders() as $name => $value) 
           {
          if($name=='session_id')
             {
          $user_id=$value;
             }
          }
		$data['userinfo'] = $this->admin_model->user($user_id);	
		// $data['numslot']=$this->calender_model->row_food();
		 //print_r( $data['userinfo']);die;
		
			        $date =$this->get("date");
			   
				$new = new DateTime($date);
				$start=$new->format("Y-m-d");
				    
				
					$arrSchedule =array(
			
		                         'sp_id'                => $user_id,
		                         'title'    			=> $this->get('name'),
					 'type'    				=> 'FOD',
					 'start'     			=> $start,
					 'gemail'      			=> $this->get('gmail'),
					 'desc'      			=> $this->get('desc'),
					 'calendar_user'     	=> strip_tags($data['userinfo'][0]->user_name),
					 'created_by'      		=> strip_tags($data['userinfo'][0]->user_email),
					 'venue'     	=> $this->get('venue')
					 
                    );

					$tableSlot="ww_cal_slot";
		
					$this->calender_model->insert_food($tableSlot,$arrSchedule);
					
						$data['userinfo'] = $this->admin_model->user($user_id);			
						$to = $this->input->post("gemail");
						$base_url=base_url();
						$subject = 'Food invite from '.strip_tags($data['userinfo'][0]->user_name);
						$message = '<html><body>';
						$message = '<div style="overflow-x:auto;">';
						$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
						$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
						
						$curText = htmlentities($this->input->post("desc"));           
						if (($curText) != '') {
							$message .= "<tr><td><strong>Other Content:</strong> </td><td>" . $curText . "</td></tr>";
						}
						$message .= "</table>";
						$message .= "</div>";
						$message .= "</body></html>";
						
						$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
						$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
						$headers .= "CC: susan@example.com\r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

						
						if(mail($to, $subject, $message, $headers))
						{
							//$data['sendvenuesucees']="Message is Successfully";
							 $this->response([
                                                                   'status' => true,
                                                                   'result' =>'food invitation successfully'
                                                                         ], REST_Controller::HTTP_OK);
							
						}
						else
						{
						$this->response([
                                                                   'status' => false,
                                                                   'result' =>'oops something wrong'
                                                                         ], REST_Controller::HTTP_OK);
						
						}
						
					
			
		}
		
 public function getAvailable_get()
	{
	foreach (getallheaders() as $name => $value) 
           {
          if($name=='session_id')
             {
          $sess_id=$value;
             }
           }
           $dte='2016-05-27 00:00:00';
           $data['events']=$this->admin_model->get_data('ww_cal_slot',array('sp_id'=>$sess_id,'start'=>$dte,'type'=>'EVT','booking'=>'Available'));
          for($i=0;$i<count($data['events']);$i++){
          $venue_id='venue'.$i;
          $result[$venue_id]=$data['events'][$i]->venue;
           
          } 
          if(count($result)==0){
          
                                                              $this->response([
                                                                   'status' => false,
                                                                   'result' =>'oops something wrong'
                                                                         ], REST_Controller::HTTP_OK);
          }else{
                                                               $this->response([
                                                                   'status' => true,
                                                                   'result' =>$result
                                                                         ], REST_Controller::HTTP_OK);
          }
          
      	}
      	
      	
      	public function sendproposal_get()
	{
		
		foreach (getallheaders() as $name => $value) 
           {  
                  if($name=='session_id')
                 {
                  $user_id=$value;
                 }
           }
		$data['userinfo'] = $this->admin_model->user($user_id);
		$customer_name = $this->get('customer_name');
		$email = $this->get('email');
		$contact_number = $this->get('contact_number');
		$message1 = $this->get('message');
		//$img = $this->get('photos');
		$selimg = $this->get('selimg');
		$proposalarray = array(
		                       'proposal_id'		=>rand(),
							   'sp_id'      		=>$user_id,
							   'sp_name'    		=>$data['userinfo'][0]->user_name,
							   'sp_email'   		=>$data['userinfo'][0]->user_email,
							   'customer_name'    	=>$customer_name,
							   'customer_email'    	=>$email,
							   'contact_number'    	=>$contact_number,
							   'message'    	    =>$message1,
							   'image'    			=>json_encode($selimg),
							   'send_date'			=>strtotime(date('Y-m-d')),
							   'status'  			=>0
							  );
		$table="ww_customer_proposal";
		$response=$this->admin_model->send_proposal($table,$proposalarray);
		//die;
        /********* send mail customer_name *****************/
        if($response==true):


		$to = $email;
                $base_url=base_url();
		$subject = 'Customer Proposal Request';
		$message = '<html><body>';
		$message = '<div style="overflow-x:auto;">';
		$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr style='background: #eee;'><td><strong>Customer Name:</strong> </td><td>" . strip_tags($customer_name) . "</td></tr>";
		$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($email) . "</td></tr>";
		$message .= "<tr><td><strong>Contact Number:</strong> </td><td>" . strip_tags($contact_number) . "</td></tr>";
		$selimg = $this->get('selimg');
		if (($selimg) != '') {
		
			for($i=0;$i<count($selimg);$i++)
			{
				if($i%2==0)
				{
					//http://localhost/weddingwazir/assets/uploadplanner/photography/145890672312524354_967731819973040_3803816411973390791_n.jpg
					
					$message .= '<tr width="300px">';
					$message .= '<td width="150px"><img width="100px" height="50px" border-style="dotted" src="'.$base_url.'assets/uploadplanner/photography/'.$selimg[$i].'" alt="proposal image" /></td>';	
					
				}
				else
				{
					
					$message .= '<td width="150px"><img width="100px" height="50px" border-style="dotted" src="'.$base_url.'assets/uploadplanner/photography/'.$selimg[$i].'" alt="proposal image" /></td>';	
					$message .= '</tr>';
				}
				
			}
			
		}
		$curText = htmlentities($message1);           
		if (($curText) != '') {
			$message .= "<tr><td><strong>Other Content:</strong> </td><td>" . $curText . "</td></tr>";
		}
		$message .= "</table>";
		$message .= "</div>";
		$message .= "</body></html>";
		

		
		$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		//$headers .= "CC: susan@example.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        
		if(mail($to, $subject, $message, $headers))
		{
			
                   $this->response([
                                    'status' => true,
                                    'result' =>'proposal successfully send'
                                   ], REST_Controller::HTTP_OK);
               
			
		}else{
		
		$this->response([
                                    'status' => false,
                                    'result' =>'oops something wrong'
                                   ], REST_Controller::HTTP_OK);
		}
      	       // $data="Message is Successfully";
		//$data = base64_encode($data);
		//redirect('Photographer/list_proposal?set='.$data);
		endif;
				
       /********  end send mail *********************/		
		
	}
	
	
	public function addenquery_chk_post()
	{
		
		foreach (getallheaders() as $name => $value) 
           {  
                  if($name=='session_id')
                 {
                  $sess_id=$value;
                 }
           }
           
           $date_enq = $this->input->post('date_enq');
	   $venue_id = $this->input->post('venue_id');
	   $dte='2016-05-27 00:00:00';
	   $type='EVT';
           $data=$this->admin_model->get_data('ww_cal_slot',array('sp_id'=>$sess_id,'start'=>$date_enq,'venue'=>$venue_id,'type'=>$type));
          // print_r($data);
           
           if(count($data)==0)
		{
			
                   $this->response([
                                    'status' => false,
                                    'message' =>'oops koi slot ni bna'
                                   ], REST_Controller::HTTP_OK);
               
			
		}else{
		
		$this->response([
                                    'status' => true,
                                    'result' =>$data
                                   ], REST_Controller::HTTP_OK);
		}
           }
           
           public function addenquery_post()
	    {
		
		foreach (getallheaders() as $name => $value) 
           {  
                  if($name=='session_id')
                 {
                  $sess_id=$value;
                 }
           }
           
           $date_enq = $this->input->post('date_enq');
	   $venue_id = $this->input->post('venue_id');
	   $slot_id = $this->input->post('slot_id');
	   $cust_name = $this->input->post('cust_name');
	   $cust_email = $this->input->post('cust_email');
	   $contact_no = $this->input->post('contact_no');
	   $event_desc = $this->input->post('event_desc');
	   $guest_range = $this->input->post('guest_range');
	   
	   $inquiryarray = array(
								   'inq_id'=>'INQ'.rand(),
								   'sp_id' =>$sess_id,
								   'name' =>trim($cust_name),
								   'email' =>trim($cust_email),
								   'created_date' =>strtotime($date_enq),
								   'guest_range' =>trim($guest_range),
								   'decs' =>trim($event_desc),
								   'status' =>0,
								   'venue' =>$venue_id,
								   'slot_id' =>$slot_id,
								   'contact_num' =>$contact_no
								   
				
				);
				
				
				$data=$this->inquiry_model->insert_inquiry_services($inquiryarray);
	   
	   $dte='2016-05-27 00:00:00';
          // $data['events']=$this->admin_model->get_data('ww_cal_slot',array('sp_id'=>$sess_id,'start'=>$date_enq,'venue'=>$venue_id));
          // print_r($data);
           
           if($data==0)
		{
			
                   $this->response([
                                    'status' => false,
                                    'message' =>'oops something wrong'
                                   ], REST_Controller::HTTP_OK);
               
			
		}else{
		
		$this->response([
                                    'status' => true,
                                    'message' =>'enquiry successfully send........'
                                   ], REST_Controller::HTTP_OK);
		}
           }
	
	

}
?>
