<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Dateevnt extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
      
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
     //   $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
      //  $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
       // $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/ke
        $this->load->model('admin_model');
        $this->load->model('calender_model');
        $this->load->model('inquiry_model');
    }

     public function vi_post()
           {
       	foreach (getallheaders() as $name => $value) 
           {  
                  if($name=='session_id')
                 {
                  $sess_id=$value;
                 }
           }
         //  $as=array('mww'=>$sess_id);
                
           $venue_id=$this->input->post('venue_id');
           $dte=$this->input->post('cal_date');
                $table='ww_cal_slot';
		$this->load->model('admin_model');

                $data=$this->admin_model->get_data('ww_cal_slot',array('sp_id'=>$sess_id,'venue'=>$venue_id,'start'=>$dte));
                $da=count($data);
                $asl=array('mww'=>$data);
         
        if($da==0){
           $this->response([
                                         'status' => false,
                                         'mes' =>'no slot available'
                                         ], REST_Controller::HTTP_OK);
       }else{
         
        $this->response([
                                         'status' => true,
                                         'result' =>$data
                                         ], REST_Controller::HTTP_OK);
         }
         
         }
       public function enquires_post(){
       
         foreach (getallheaders() as $name => $value) 
          {  
               if($name=='session_id')
               {
                $sess_id=$value;
              }
           }
             $venue_id=$this->input->post('venue_id');
             $slot_id=$this->input->post('slot_id');
            $data=$this->admin_model->get_data('ww_inquiry',array('sp_id'=>$sess_id,'venue'=>$venue_id,'slot_id'=> $slot_id));
         
            $da=count($data);
             
              if($da==0){
              $this->response([
                                         'status' => false,
                                         'mes' =>'no enquires still'
                                         ], REST_Controller::HTTP_OK);
                         }else{
         
             $this->response([
                                         'status' => true,
                                         'result' =>$data
                                         ], REST_Controller::HTTP_OK);
         }
         
         }
          public function portfolio_get()
          {
       
            foreach (getallheaders() as $name => $value) 
            {  
               if($name=='session_id')
               {
                $sess_id=$value;
               }
            }
          
            $data=$this->admin_model->get_data('ww_portfolio',array('sp_id'=>$sess_id));
            
             $da=count($data);
             
              if($da==0){
              $this->response([
                                         'status' => false,
                                         'mes' =>'no album created till now'
                                         ], REST_Controller::HTTP_OK);
                         }else{
         
             $this->response([
                                         'status' => true,
                                         'result' =>$data
                                         ], REST_Controller::HTTP_OK);
         }
             
           }
           
           
        public function albumb_post()
          {
           $album_id=$this->input->post('album_id');
           $data=$this->admin_model->get_data('ww_portfolio_images',array('album_id'=>$album_id));
           for($i=0;$i<count($data);$i++){
           $data[$i]->image_name='http://192.168.10.121:80/weddingwazir/uploads/'.$data[$i]->image_name;
           
           
           }
           $da=count($data);
             
              if($da==0){
              $this->response([
                                         'status' => false,
                                         'mes' =>'no album created till now'
                                         ], REST_Controller::HTTP_OK);
                         }else{
         
             $this->response([
                                         'status' => true,
                                         'result' =>$data
                                         ], REST_Controller::HTTP_OK);
         }
          
          }
          
}
?>
