<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
   class Login extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
      
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/ke
        $this->load->model('admin_model');
    }

    public function splogin_post()
    {
	   
		
			$username =$this->input->post('user_name');//$this->get('user_name');//input->post("name");
			$password = md5($this->input->post('password'));//input->post("email"));
			
			$result = $this->admin_model->authenticate($username,$password);
                       if(count($result)==0)
                         {
                         
                           $this->response([
                                         'status' =>false,
                                         'message' =>'user id or password missmatch'
                                         ], REST_Controller::HTTP_OK);

                          }else{
			// Create session array
			$sess_array = array(
			'id' => $result->id,
			'username' => $result->user_name,
			'password' => $result->password,
			'role' => $result->role_type,
			'status' => $result->status,
			'logged_in' => TRUE
			);
			// Add user value in session
			$this->session->set_userdata($sess_array);
		        $user_id = $this->session->userdata('id');
	      
                $table='ww_admin_login';
		$this->load->model('admin_model');
		$data['result']=$this->admin_model->select_single_data($table,$user_id);
                $data['venue_result']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$user_id));
                $data['property_result']=$this->admin_model->get_data('ww_venue_property',array('user_id'=>$user_id));
                $data['prop_result']=$this->admin_model->get_data('city_setting',array('proName'=>$data['venue_result'][0]->prop_name));

              if(count($data['result'])==0){
                 $this->response([
                                         'status' => false,
                                         'message' =>"session Expired"
                                         ], REST_Controller::HTTP_OK);
                    }else{

                $final_result=[
                      'message'=>'login successfull',
                      'sessionID'=>$user_id,
                      'full_name'=>$data['result'][0]->user_name,
                      'gender'=>$data['result'][0]->gender,
                      'dob'=>$data['result'][0]->d_o_b,
                      'email'=>$data['result'][0]->user_email,
                      'contact'=>$data['result'][0]->contact_no,
                      'avtar_url'=>$data['result'][0]->pro_pic,
                      'sp_type'=>$data['result'][0]->role_type,
                      'website_gplushurl'=>$data['result'][0]->gp_url,
                      'property_detail'=>[
                                           'company_name'=>$data['result'][0]->company_name,
                                           'brand_name'=>$data['prop_result'][0]->name,  
                                           'property_adrs'=>$data['property_result'][0]->propertyAddr,
                                           'property_name'=>$data['property_result'][0]->propertyName             
                                            ],
                      'location'=>[
                                   'country'=>$data['result'][0]->country,
                                   'state'=>$data['result'][0]->state,
			           'city'=>$data['result'][0]->city,
			           'street'=>$data['result'][0]->street,
			           'zip'=>$data['result'][0]->zip_code
                                  ]



                     ];
             
                 $this->response([
                                         'status' =>TRUE,
                                         'result' =>$final_result
                                         ], REST_Controller::HTTP_OK);
                }
              }
  }


      public function venue_get()
      {

          foreach (getallheaders() as $name => $value) 
          {
          if($name=='session_id')
             {
          $sess_id=$value;
             }
          }

         if($sess_id!='')
               {
	        $table='ww_admin_login';
		$this->load->model('admin_model');
                $data=$this->admin_model->get_data('ww_venue',array('sp_id'=>$sess_id));

                  if(count($data)==0)
                   {
                    $this->response([
                                         'status' => false,
                                         'message' =>"session Expired or missmatch"
                                         ], REST_Controller::HTTP_OK);
                    }else

                          {
                 $this->response([
                                         'status' => true,
                                         'result' =>$data
                                         ], REST_Controller::HTTP_OK);
                           }
               }
		else
               {
              
                 $this->response([
                                         'status' => false,
                                         'message' =>"does't get your session"
                                         ], REST_Controller::HTTP_OK);  

               }
 
 }
  public function available_post()
      {

          foreach (getallheaders() as $name => $value) 
          {
          if($name=='session_id')
             {
         $sess_id=$value;
             }
          }
        
        $dtea =$this->input->post('check');
        $data['events']=$this->admin_model->get_data('ww_cal_slot',array('sp_id'=>$sess_id,'start'=>$dtea,'type'=>'EVT','booking'=>'Available'));
        $data['venue']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$sess_id));
        $cnt=count($data['events']); 
        $cntvn=count($data['venue']); 
        
        $k=0;
             for($i=0;$i<count($data['venue']);$i++)
             {
             for($j=0;$j<count($data['events']);$j++){
             if(($data['venue'][$i]->v_id)==($data['events'][$j]->venue))
               {
               
               $data['available_venue'][$k]=$data['venue'][$i];
               $k++;
               break;
               }else{
               
                    }
             
                 
          }
             }
       if( $cnt==0){
         $this->response([
                                         'status' => false,
                                         'message' =>'no event found at this date'
                                        //'result' =>$data['available_venue']
                                         ], REST_Controller::HTTP_OK);}
                                         
                                         else{
                                          $this->response([
                                         'status' => true,
                                         'result' =>$data['available_venue']
                                         ], REST_Controller::HTTP_OK);
                                         
                                         }
                                         
                                         
                              }
                              
                              
      public function vcvgb()
      {

          //foreach (getallheaders() as $name => $value) 
        //  {
        //  if($name=='session_id')
          //   {
         // $sess_id=$value;
         //    }
        //  }
      
        
          $dtea =$this->input->post('check');
          $data['events']=$this->admin_model->get_data('ww_cal_slot',array('sp_id'=>'Ko45N6GUym','start'=>$dtea,'type'=>'EVT','booking'=>'Available'));
         $data['venue']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$sess_id));
                                         
          
             $k=0;
             for($i=0;$i<count($data['venue']);$i++)
             {
             for($j=0;$j<count($data['events']);$j++){
             if(($data['venue'][$i]->v_id)==($data['events'][$j]->venue))
               {
               
               $data['available_venue'][$k]=$data['venue'][$i];
               $k++;
               break;
               }else{
               
                    }
             
                 
          }
             }
        if(count($data['available_venue'])==0){
         
              $this->response([
                                         'status' => false,
                                         'message' =>'no venue available'
                                        //'result' =>$data['available_venue']
                                         ], REST_Controller::HTTP_OK);
                                         
           }else{
                       $this->response([
                                         'status' => true,
                                       //  'message' =>$data['available_venue']
                                  
                                     'result' =>$data['available_venue']
                                         ], REST_Controller::HTTP_OK);
           }
          
                           
    }           
          
      

       public function datevnt_post()
         {

          foreach (getallheaders() as $name => $value) 
          {
          if($name=='session_id')
             {
          $sess_id=$value;
             }
          }
           
          $venue_id=$this->input->post('venue_id');
          $mnth_yr=$this->input->post('mont_yr');
          

         if($sess_id!='')
               {
	        $table='ww_cal_slot';
		$this->load->model('admin_model');

                $data['events']=$this->admin_model->get_data('ww_cal_slot',array('sp_id'=>$sess_id,'venue'=>$venue_id));
                
                
                   for($n=0;$n<count($data['events']);$n++){

                    $data['events'][$n]->end=substr($data['events'][$n]->end, 0, -9); 
                    $data['events'][$n]->start=substr($data['events'][$n]->start, 0, -9);  
                    $pieces = explode("-", $data['events'][$n]->end);
                    $start = explode("-", $data['events'][$n]->start);
                  switch($pieces[1]){
                            case 01:
                             $pieces[1]='January';
                            break;
                             case 02:
                             $pieces[1]='February';
                            break;
                             case 03:
                             $pieces[1]='March';
                            break;
                             case 04:
                             $pieces[1]='April';
                            break;
                             case 05:
                             $pieces[1]='May';
                            break;
                             case 06:
                             $pieces[1]='June';
                            break;
                             case 07:
                             $pieces[1]='July';
                            break;
                             case 23:
                             $pieces[1]='August';
                            break;
                             case 33:
                             $pieces[1]='September';
                            break;
                             case 10:
                             $pieces[1]='October';
                            break;
                             case 11:
                             $pieces[1]='November';
                            break;
                             default:
                             $pieces[1]='December';
                            
                           
                           }
                    $data['events'][$n]->end=$pieces[2].'-'.$pieces[1].'-'.$pieces[0];
                    switch($start[1]){
                            case 01:
                             $start[1]='January';
                            break;
                             case 02:
                             $start[1]='February';
                            break;
                             case 03:
                             $start[1]='March';
                            break;
                             case 04:
                             $start[1]='April';
                            break;
                             case 05:
                             $start[1]='May';
                            break;
                             case 06:
                             $start[1]='June';
                            break;
                             case 07:
                             $start[1]='July';
                            break;
                             case 23:
                             $start[1]='August';
                            break;
                             case 33:
                             $start[1]='September';
                            break;
                             case 10:
                             $start[1]='October';
                            break;
                             case 11:
                             $start[1]='November';
                            break;
                             default:
                             $start[1]='December';
                            
                           
                           }
                   $data['events'][$n]->start=$start[2].'-'.$start[1].'-'.$start[0];
                   
                   }
                  //print_r($data['events']);die;
                
                $data['enquires']=$this->admin_model->get_data('ww_inquiry',array('sp_id'=>$sess_id,'venue'=>$venue_id));//'702034314'));
                
                //$data['spacial_day']=$this->admin_model->get_data('ww_cal_slot',array('sp_id'=>                        $sess_id,'type'=>'SPD'));
                	
               for($i=0;$i<count($data['enquires']);$i++)
                     {
                     $yr=getdate($data['enquires'][$i]->created_date);
                     $data['enquires'][$i]->created_date=$yr['mday'].'-'.$yr['month'].'-'.$yr['year'];
                     }
             // print_r($data['enquires']);die;
               for($j=0;$j<31;$j++){
                      $dte=($j+1).'-'.$mnth_yr;
                     $count=0;
                     $is_specialday=false;
                    for($k=0;$k<count($data['enquires']);$k++)
                     {  
                      if(($data['enquires'][$k]->created_date)===$dte) {
                          $count++;
                        
                          //echo $dte;
                          //echo 'yes increase the count';
                         // echo $data['enquires'][$k]->created_date;
                  
                      }else{
                             $dataresult[$j]=0;
                                  }
                                  for($s=0;$s<count($data['events']);$s++){
                    
                                if(($data['events'][$s]->start)===$dte){//&&(($data['events'][$s]->type)=='SPD')){
                        //  echo 'yes';//$data['events'][$s]->type;
                           // $dataresult[$dte]=array("enquires_count"=>$count,"is_spacialday"=>$data['events'][$s]->type);  
                           if(($data['events'][$s]->type)=='SPD'){
                                 $is_specialday=true;
                                break; }
                                 
                             }else{
                           // echo $data['events'][$s]->type;
                             //echo 'no';
                          //  $dataresult[$dte]= array("enquires_count"=>$count,"is_spacialday"=>$data['events'][$s]->type);    
                                 $is_specialday=false;
                                 }
                     }
                 
                 //$dataresult[$dte]=$count;
                   $dataresult[$j]= array("dte"=>$dte,"enquires_count"=>$count,"is_spacialday"=>$is_specialday); 
                 
                     }         

                                    }//echo print_r($dataresult);
                    

                  if(count($dataresult)==0)
                   {
                    $this->response([
                                         'status' => false,
                                         'message' =>"session Expired or missmatch"
                                         ], REST_Controller::HTTP_OK);
                    }else

                          {
                    $this->response([
                                         'status' => true,
                                         'result' =>$dataresult
                                         ], REST_Controller::HTTP_OK);
                           }
               }
		else
               {
              
                    $this->response([
                                         'status' => false,
                                         'message' =>"does't get your session"
                                         ], REST_Controller::HTTP_OK);  

               }
 
 }
 
 
              public function view_post()
           {
          foreach (getallheaders() as $name => $value) 
           {
          if($name=='session_id')
             {
          $sess_id=$value;
             }
          }
           
          $venue_id=$this->input->post('venue_id');
         $dte=$this->input->post('cal_date');
         
         
         if($dte===27-May-2016){
           $this->response([
                                         'status' => true,
                                         'message' =>'you r right charliii'
                                         ], REST_Controller::HTTP_OK);
         }else{
         
           $this->response([
                                         'status' => false,
                                         'message' =>'no....glat h'
                                         ], REST_Controller::HTTP_OK);
         }
         
         }
         
 ///for on date click in calendarview.which show slot info and enquires of related slot 

        public function dateview_post()
           {
          foreach (getallheaders() as $name => $value) 
           {
          if($name=='session_id')
             {
          $sess_id=$value;
             }
          }
           
          $venue_id=$this->input->post('venue_id');
         $dte=$this->input->post('cal_date');

         if($sess_id!='')
               {
	        $table='ww_cal_slot';
		$this->load->model('admin_model');

                $data['events']=$this->admin_model->get_data('ww_cal_slot',array('sp_id'=>$sess_id,'venue'=>$venue_id,'type'=>'EVT'));
                if(count($data['events'])==0){
                
                 $this->response([
                                         'status' => false,
                                         'message' =>'no event found..'
                                         ], REST_Controller::HTTP_OK);
                }
                else
                {
                for($n=0;$n<count($data['events']);$n++){

                    $data['events'][$n]->end=substr($data['events'][$n]->end, 0, -9); 
                    $data['events'][$n]->start=substr($data['events'][$n]->start, 0, -9);  
                    $pieces = explode("-", $data['events'][$n]->end);
                    $start = explode("-", $data['events'][$n]->start);
                  switch($pieces[1]){
                            case 01:
                             $pieces[1]='January';
                            break;
                             case 02:
                             $pieces[1]='February';
                            break;
                             case 03:
                             $pieces[1]='March';
                            break;
                             case 04:
                             $pieces[1]='April';
                            break;
                             case 05:
                             $pieces[1]='May';
                            break;
                             case 06:
                             $pieces[1]='June';
                            break;
                             case 07:
                             $pieces[1]='July';
                            break;
                             case 23:
                             $pieces[1]='August';
                            break;
                             case 33:
                             $pieces[1]='September';
                            break;
                             case 10:
                             $pieces[1]='October';
                            break;
                             case 11:
                             $pieces[1]='November';
                            break;
                             default:
                             $pieces[1]='December';
                            
                           
                           }
                    $data['events'][$n]->end=$pieces[2].'-'.$pieces[1].'-'.$pieces[0];

                    switch($start[1]){
                            case 01:
                             $start[1]='January';
                            break;
                             case 02:
                             $start[1]='February';
                            break;
                             case 03:
                             $start[1]='March';
                            break;
                             case 04:
                             $start[1]='April';
                            break;
                             case 05:
                             $start[1]='May';
                            break;
                             case 06:
                             $start[1]='June';
                            break;
                             case 07:
                             $start[1]='July';
                            break;
                             case 23:
                             $start[1]='August';
                            break;
                             case 33:
                             $start[1]='September';
                            break;
                             case 10:
                             $start[1]='October';
                            break;
                             case 11:
                             $start[1]='November';
                            break;
                             default:
                             $start[1]='December';
                            
                           
                           }
                   $data['events'][$n]->start=$start[2].'-'.$start[1].'-'.$start[0];
                     
                   
                   }

                    //  print_r($data['events']);die;
                       $data['enquires']=$this->admin_model->get_data('ww_inquiry',array('sp_id'=>$sess_id,'venue'=>$venue_id));
                	
               for($i=0;$i<count($data['enquires']);$i++)
                     {
                     $yr=getdate($data['enquires'][$i]->created_date);
                     $data['enquires'][$i]->created_date=$yr['mday'].'-'.$yr['month'].'-'.$yr['year'];
                     }

                     
                     //$dte='27-May-2016';
                         $a=0;
                       for($s=0;$s<count($data['enquires']);$s++){
                    
                           if(($data['enquires'][$s]->created_date)===$dte){
                              $arry_name='enquires'.$a;  
                             $dataaaa[$a]=$data['enquires'][$s];
                             $a++;
                             }else{
                                 $is_specialday=false;
                                 }
                  
             }
                    //  print_r($data['enquires']);die;
                        $b=0;
                        for($s=0;$s<count($data['events']);$s++){
                    
                           if(($data['events'][$s]->start)===$dte){
                              $arry_name='event'.$b;  
                             $dataaa[$b]=$data['events'][$s];
                               $b++;
                             }else{
                                 $is_specialday=false;
                                 }
                  
             }

                $result=['event'=>$dataaa,'enquires'=>$dataaaa];
                
                    if(count($dataaa)==0){
                            
                            $this->response([
                                         'status' => false,
                                       //  'message' =>'no event created..'
                                         ], REST_Controller::HTTP_OK);

                      }else{

                             $this->response([
                                         'status' => true,
                                        // 'message' =>'no event found..'
                                        'result' =>$result
                                         ], REST_Controller::HTTP_OK);
                            }
                    
                
                }
                
             
                
                   
                           
               }
		else
               {
              
                    $this->response([
                                         'status' => false,
                                       //  'message' =>"does't get your session"
                                         ], REST_Controller::HTTP_OK);  

               }
 
 }

             public function add_food()
	        {
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);	
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('gemail','Email','required');
		
		
		if ($this->form_validation->run() == false)
		{
		 $data['title'] = "Food Invite Day";	
		 $data['numslot']=$this->calender_model->row_food();	
		// $this->load->view('backend/add_food_invite',$data);
		}
		else
		{
				$date =$this->input->post("date");
				$new = new DateTime($date);
				$start=$new->format("Y-m-d");
				
					$arrSchedule =array(
			
		                         'sp_id'                => $user_id,
		                         'title'    			=> $this->input->post("name"),
					 'type'    				=> 'FOD',
					 'start'     			=> $start,
					 'gemail'      			=> $this->input->post("gemail"),
					 'desc'      			=> $this->input->post("desc"),
					 'calendar_user'     	=> strip_tags($data['userinfo'][0]->user_name),
					 'created_by'      		=> strip_tags($data['userinfo'][0]->user_email),
					 'venue'     	=> $this->input->post("venue")
					 
                    );

					$tableSlot="ww_cal_slot";
		
					$this->calender_model->insert_food($tableSlot,$arrSchedule);
					
						$data['userinfo'] = $this->admin_model->user($user_id);			
						$to = $this->input->post("gemail");
						$base_url=base_url();
						$subject = 'Food invite from '.strip_tags($data['userinfo'][0]->user_name);
						$message = '<html><body>';
						$message = '<div style="overflow-x:auto;">';
						$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
						$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
						
						$curText = htmlentities($this->input->post("desc"));           
						if (($curText) != '') {
							$message .= "<tr><td><strong>Other Content:</strong> </td><td>" . $curText . "</td></tr>";
						}
						$message .= "</table>";
						$message .= "</div>";
						$message .= "</body></html>";
						
						$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
						$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
						$headers .= "CC: susan@example.com\r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

						
						if(mail($to, $subject, $message, $headers))
						{
							$data['sendvenuesucees']="Message is Successfully";
							
						}	
					
			
		}
	}

}
?>
