<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venue_setting_cntlr extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Venue_setting_model');
	}

	public function venue_setting_home1(){
		$data['title']="Venue Setting";
		$data['result1']=$this->Venue_setting_model->get_venue(1);
		$data['result2']=$this->Venue_setting_model->get_venue(2);
		$data['result3']=$this->Venue_setting_model->get_venue(3);
		$data['result4']=$this->Venue_setting_model->get_venue(4);
		$data['result5']=$this->Venue_setting_model->get_venue(5);
		$data['result6']=$this->Venue_setting_model->get_venue(6);
		$data['result7']=$this->Venue_setting_model->get_venue(7);
		$data['result8']=$this->Venue_setting_model->get_venue(8);
		$data['result9']=$this->Venue_setting_model->get_venue(9);
		$data['result10']=$this->Venue_setting_model->get_venue(10);
		$this->load->view('backend/venue_setting_home',$data);
	}

	public function insert_venue()
	{
		$idfr = $this->uri->segment(3);
		$names= $this->input->post('namee');
		$descs= $this->input->post('desc');
		$result = $this->Venue_setting_model->inser_venue($idfr,$names,$descs);
		if($result=1){
		$data = "Successful anchor add";
		$data = base64_encode($data);
		redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
		}else{
        $data = "Something is wrong";
		$data = base64_encode($data);
		redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
		}
	} 

	public function add_venue()
	{
		$data['title']="Venue Setting";
		$this->load->view('backend/venue_settings',$data);
	}
	
	public function delete_venue(){
		$idfr = $this->uri->segment(4);
		$id=$this->uri->segment(3);
		$result=$this->Venue_setting_model->delete_row($id,$idfr);
		if($result=1){
		$data = "Successful anchor deleted";
		$data = base64_encode($data);
		redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
		}else{
        $data = "Something is wrong";
		$data = base64_encode($data);
		redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
		}
	}
	
	public function edit_venue()
	{
		$idfr = $this->uri->segment(4);
		$ide=$this->uri->segment(3);
		$data['idfru']=$idfr;
		$data['id']=$ide;
		$data['title']="VENUE SETTING";
		$data['result']=$this->Venue_setting_model->get_venueid($ide,$idfr);
		
		$this->load->view('backend/venue_setting_edit',$data);
	}
	public function venue_update()
	{
		 $id = $this->uri->segment(3);
		 $idfr = $this->uri->segment(4);
	         $names= $this->input->post('namee');
	         $descs= $this->input->post('desc');
                 $result = $this->Venue_setting_model->venue_update($id,$idfr,$names,$descs);
  

   if($result=1){
   $data = "Successful anchor updated";
					$data = base64_encode($data);
					redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
   }else{
   
        $data = "Something is wrong";
					 $data = base64_encode($data);
					
					redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
    }
   }

  public function venue_view(){
	 $ide = $this->uri->segment(3);
	 $idfr = $this->uri->segment(4);
	 $data['idfru']=$idfr;
		$data['id']=$ide;
	 $data['title']="View";
	 $data['result']=$this->Venue_setting_model->get_venueid($ide,$idfr);
	$this->load->view('backend/venue_setting_view',$data);
} 
}
?>
