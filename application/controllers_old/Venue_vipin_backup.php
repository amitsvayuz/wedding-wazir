<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venue extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Venue_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
	}
	public function index()
	{
		//$data['title']="Sign In";
		//$this->load->view('backend/login',$data);
	}
/****************Venue Manage****************/	
	public function add_venue()
	{
		$this->form_validation->set_rules('company_city', 'Company City', 'required');
		$this->form_validation->set_rules('company_name', 'Company Name', 'required');
		$this->form_validation->set_rules('venue_name', 'Venue Name', 'required');
		$this->form_validation->set_rules('venueaddr', 'Venue Description', 'required');
		$this->form_validation->set_rules('nature', 'Venue Nature', 'required');
		$this->form_validation->set_rules('floor', 'Venue Floor', 'required');

		if ($this->form_validation->run() == FALSE)
		{  
			$data['title']="Add Venue";
			$this->load->view('backend/add_venue',$data);
		}
		else
		{
			
			$venuearray = array(
					 'v_id'=>rand(),
					 'sp_id'=>rand(),
					 'v_city'=>$this->input->post('company_city'),
					 'v_name'=>$this->input->post('venue_name'),
					 'v_company'=>$this->input->post('company_name'),
					 'v_addr'=>$this->input->post('venueaddr'),
					 'v_nature'=>$this->input->post('nature'),
					 'v_type'=>$this->input->post('atype'),
					 'v_type_value'=>json_encode($this->input->post('venuetype')),
					 'v_dinein'=>json_encode($this->input->post('dinein')),
					 'v_outside_details'=>$this->input->post('outsidedetails'),
					 'v_floor'=>$this->input->post('floor'),
					 'v_lift'=>$this->input->post('lift'),
					 'v_lighting'=>$this->input->post('lighting'),
					 'v_fencing'=>$this->input->post('fencing'),
					 'v_dresser'=>$this->input->post('dresser'),
					 'v_power_backup'=>$this->input->post('powerbackup'),
					 'v_power_desc'=>$this->input->post('powerdetails'),
					 'v_rain_backup'=>$this->input->post('rainbackup'),
					 'v_rain_desc'=>$this->input->post('raindetails'),
					 'v_fire_safety'=>$this->input->post('firebackup'),
					 'v_fire_safety_desc'=>$this->input->post('firedetails'),
					 'v_suite_type'=>$this->input->post('suitetype'),
					 'v_washroom'=>$this->input->post('washroom'),
					 'v_ext_permission'=>$this->input->post('extpermistion'),
					 'v_rest_limit'=>$this->input->post('restlimit'),
					 
					 
					 'v_valet_parking'=>$this->input->post('valetparking'),
					 'v_valet_facility'=>json_encode($this->input->post('valetparkingfacility')),
					 'v_gustroomsuite'=>$this->input->post('gustroomsuite'),
					 'v_accommodate'=>$this->input->post('addaccommodate'),
					 'v_licencebar'=>$this->input->post('licencebar'),
					 'v_licencebar_late'=>$this->input->post('licencebarlate'),
					 'v_airport_pickup'=>$this->input->post('airportpickup'),
					 'v_bridal_suite'=>$this->input->post('bridalsuite'),
					 'v_wedding_event_list'=>json_encode($this->input->post('weddinglist')),
					 'v_dj'=>$this->input->post('dj'),
					 'v_dol'=>$this->input->post('dol'),
					 'v_shenai'=>$this->input->post('shenai'),
					 'v_band'=>$this->input->post('band'),
					 'v_stage'=>$this->input->post('stage'),
					 'v_decor'=>$this->input->post('decor'),
					 'v_florist'=>$this->input->post('florist'),
					 'v_light'=>$this->input->post('light'),
					 
					 'v_event'=>$this->input->post('event'),
					 'v_photographer'=>$this->input->post('photography'),
					 'v_videographer'=>$this->input->post('videography'),
					 'v_ghodi'=>$this->input->post('ghodi'),
					 'v_palki'=>$this->input->post('palki'),
					 'v_artist'=>$this->input->post('artist'),
					 'v_status'=>0
					 );
		$tablename ='ww_venue';
		
		$this->Venue_model->add_venue($tablename,$venuearray);
		}
	}
	
	public function venue_update()
	{
		$id=$this->uri->segment(3);
		$venuearray = array(
		             'v_id'=>rand(),
					 'sp_id'=>rand(),
					 'v_city'=>$this->input->post('company_city'),
					 'v_name'=>$this->input->post('venue_name'),
					 'v_company'=>$this->input->post('company_name'),
					 'v_addr'=>$this->input->post('venueaddr'),
					 'v_nature'=>$this->input->post('nature'),
					 'v_type'=>$this->input->post('atype'),
					 'v_type_value'=>json_encode($this->input->post('venuetype')),
					 'v_outside_details'=>$this->input->post('outsidedetails'),
					 'v_floor'=>$this->input->post('floor'),
					 'v_lift'=>$this->input->post('lift'),
					 'v_lighting'=>$this->input->post('lighting'),
					 'v_fencing'=>$this->input->post('fencing'),
					 'v_dresser'=>$this->input->post('dresser'),
					 'v_power_backup'=>$this->input->post('powerbackup'),
					 'v_power_desc'=>$this->input->post('powerdetails'),
					 'v_rain_backup'=>$this->input->post('rainbackup'),
					 'v_rain_desc'=>$this->input->post('raindetails'),
					 'v_fire_safety'=>$this->input->post('firebackup'),
					 'v_fire_safety_desc'=>$this->input->post('firedetails'),
					 'v_suite_type'=>$this->input->post('suitetype'),
					 'v_washroom'=>$this->input->post('washroom'),
					 'v_ext_permission'=>$this->input->post('extpermistion'),
					 'v_rest_limit'=>$this->input->post('restlimit'),
					 'v_valet_parking'=>$this->input->post('valetparking'),
					 'v_valet_facility'=>json_encode($this->input->post('valetparkingfacility')),
					 'v_gustroomsuite'=>$this->input->post('gustroomsuite'),
					 'v_accommodate'=>$this->input->post('addaccommodate'),
					 'v_licencebar'=>$this->input->post('licencebar'),
					 'v_licencebar_late'=>$this->input->post('licencebarlate'),
					 'v_airport_pickup'=>$this->input->post('airportpickup'),
					 'v_bridal_suite'=>$this->input->post('bridalsuite'),
					 'v_wedding_event_list'=>json_encode($this->input->post('weddinglist')),
					 'v_dj'=>$this->input->post('dj'),
					 'v_dol'=>$this->input->post('dol'),
					 'v_shenai'=>$this->input->post('shenai'),
					 'v_band'=>$this->input->post('band'),
					 'v_stage'=>$this->input->post('stage'),
					 'v_decor'=>$this->input->post('decor'),
					 'v_florist'=>$this->input->post('florist'),
					 'v_light'=>$this->input->post('light'),
					 
					 'v_event'=>$this->input->post('event'),
					 'v_photographer'=>$this->input->post('photography'),
					 'v_videographer'=>$this->input->post('videography'),
					 'v_ghodi'=>$this->input->post('ghodi'),
					 'v_palki'=>$this->input->post('palki'),
					 'v_artist'=>$this->input->post('artist'),
					 'v_status'=>0
		             );
		$tablename ='ww_venue';
		$this->Venue_model->venue_update($tablename,$venuearray,$id);
	}
	
	public function venue_manager()
	{
		$data['title']="Venue Manage";
		$data['vcresult'] = $this->Venue_model->get_venue();
		$this->load->view('backend/add_venue_manager',$data);
	}
	
	public function add_venue_manager()
	{
		$data['title']="Add Venue";
		$this->load->view('backend/add_venue',$data);
	}
	
	public function venue_edit_get()
	{
		$id=$this->uri->segment(3);
		$data['editdata']=$this->Venue_model->venue_edit_get($id);
		$data['title']="Edit Venue";
		$this->load->view('backend/add_venue',$data);
		
	}
	
	public function venue_delete()
	{
	     $id=$this->uri->segment(3);
	     $this->Venue_model->venue_delete($id);
		 
	}
/***************** Venue Manager Ends here******************/
}
