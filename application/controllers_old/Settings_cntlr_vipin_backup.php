<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_cntlr extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Settings_model');
	}
	
	public function setting_insert()
	{
		 $idfr = $this->uri->segment(3);
		//$idfr=$_GET['id'];
		$names= $this->input->post('namee');
		$descs= $this->input->post('desc');
		$result = $this->Settings_model->inser_setting($idfr,$names,$descs);		
		if($result=1){
		$data = "Successful anchor add";
		$data = base64_encode($data);
		redirect('Cms/settings?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Cms/settings?set='.$data);
		}
	} 

	public function add_settings()
	{
		$data['title']="SETTING";
		$this->load->view('backend/settings',$data);
	}
	
	public function delete_setting(){
		$idfr = $this->uri->segment(4);
		$id=$this->uri->segment(3);
		$result=$this->Settings_model->delete_row($id,$idfr);
		if($result=1){
		$data = "Successful anchor deleted";
		$data = base64_encode($data);
		redirect('Cms/settings?set='.$data);
		}else{
        $data = "Something is wrong";
		$data = base64_encode($data);
		redirect('Cms/settings?set='.$data);
		}
	}
	
	public function edit_settings()
	{
		$idfr = $this->uri->segment(4);
		$ide=$this->uri->segment(3);
		$data['idfru']=$idfr;
		$data['id']=$ide;
		$data['title']="EDIT SETTING";
		$data['result']=$this->Settings_model->get_viewid($ide,$idfr);
		
		$this->load->view('backend/settings_edit',$data);
	}
	public function setting_update()
	{
		 $id = $this->uri->segment(3);
		 $idfr = $this->uri->segment(4);
		$names= $this->input->post('namee');
		$descs= $this->input->post('desc');
		$result = $this->Settings_model->inser_update($id,$idfr,$names,$descs);
		if($result=1){
			$data = "Successful anchor updated";
			$data = base64_encode($data);
			redirect('Cms/settings?set='.$data);
		}else{
          $data = "Something is wrong";
		  $data = base64_encode($data);		
		redirect('Cms/settings?set='.$data);
    }
   }

  public function setting_view(){
	 $ide = $this->uri->segment(3);
	 $idfr = $this->uri->segment(4);
	 $data['idfru']=$idfr;
	 $data['id']=$ide;
	 $data['title']="View";
	 $data['result']=$this->Settings_model->get_viewid($ide,$idfr);
	 $this->load->view('backend/setting_view',$data);
	} 
	
		
	#--------city setting----->
	public function city_setting(){
		
		 $data['title']="City Setting";
		 $this->load->view('backend/city_setting',$data);
	}
	public function add_city(){
		
		 $data['title']="Add_City";
		 $this->load->view('backend/add_city',$data);
	}
	public function edit_city(){
		 $id=$_GET['id'];
		 $data['title']="Edit_City";
		 $data['result']=$this->Settings_model->get_city($id);
		 $this->load->view('backend/add_city',$data);
	}
	
	public function view_city(){
		 $data['title']="Edit_City";
		 $data['result']=$this->Settings_model->get_citya();
		 $this->load->view('backend/city_setting',$data);
	}
	
	public function city_insert(){
		$name= $this->input->post('namee');
		$descr= $this->input->post('desc');
		$result = $this->Settings_model->insert_city($name,$descr);
	    if($result=1){
		$data = "Successful anchor add";
				$data = base64_encode($data);
				redirect('Settings_cntlr/view_city?set='.$data);
		}else{
	   	$data = "Something is wrong";
	    $data = base64_encode($data);
		redirect('Settings_cntlr/view_city?set='.$data);
	   }
	}
	
	public function city_update(){
	    $id=$_GET['id'];
		$name= $this->input->post('namee');
		$descr= $this->input->post('desc');
		$result = $this->Settings_model->update_city($id,$name,$descr);
	    if($result=1){
		$data = "Successful anchor udated";
		$data = base64_encode($data);
		redirect('Settings_cntlr/view_city?set='.$data);
		}else{
	   	$data = "Something is wrong";
		$data = base64_encode($data);
		redirect('Settings_cntlr/view_city?set='.$data);
	   }
	}
	
	public function delete_city(){
			$id=$_GET['id'];
			$result=$this->Settings_model->delete1_city($id);
			if($result=1){
			$data = "Successful anchor deleted";
			$data = base64_encode($data);
			redirect('Settings_cntlr/view_city?set='.$data);
			}else{
	     	$data = "Something is wrong";
			 $data = base64_encode($data);
			redirect('Settings_cntlr/view_city?set='.$data);
	   }
	}
	
	public function view_citybyid(){
		 $id=$_GET['id'];
		 $data['title']="City_Detail";
		 $data['result']=$this->Settings_model->get_city($id);
		 $this->load->view('backend/view_city',$data);
	}

}
	?>
