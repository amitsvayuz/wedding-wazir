<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Decorator extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model'); 
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		error_reporting(0);
		
		if(!$this->session->userdata('id')){
			redirect('registration');
		}
	}
	
	public function index()
	{
		$data['title']="Photographer Addition";
		$this->load->view('backend/add_decorator_images',$data);
	}

	  public function add_decorator_services()
	{
		$data['title']="Service 's Addition";
		if(!empty($_POST)){
		    $services = implode(',',$_POST[services]);
			$other_services = implode(',',$_POST[other_services]);
			
			$additionaldata = array(
				'services' => $services,
				'other_services' => $other_services,
				'user_id' => $this->session->userdata('id')
			);
			$user_id = $this->session->userdata('id');
			$services_details = $this->admin_model->get_decorator_services($user_id);
			if(!empty($services_details)){
				$id = $this->admin_model->update_decorator_services($user_id , $additionaldata);
				$this->session->set_flashdata('message',"Decorator services has been updated successfully" );
			}else{
				$id = $this->admin_model->add_decorator_services($additionaldata);
				$this->session->set_flashdata('message',"Decorator services has been added successfully" );
			}
			redirect('decorator/list_decorator_services');
		}	
		$this->load ->view('backend/add_decorator_services',$data);
	}
	
	public function edit_decorator_services()
	{
		$data['title']="Service 's Eddition";
		$id = $_GET['id'];
		$user_id = $this->session->userdata('id');
		if(!empty($_POST)){
		    $services = implode(',',$_POST[services]);
			$other_services = implode(',',$_POST[other_services]);
			
			$additionaldata = array(
				'services' => $services,
				'other_services' => $other_services,
				'user_id' => $this->session->userdata('id')
			);
			$user_id = $this->session->userdata('id');
			
			$id = $this->admin_model->update_decorator_services($user_id , $additionaldata);
			$this->session->set_flashdata('message',"Decorator services has been updated successfully" );
			
			redirect('decorator/list_decorator_services');
		}	
		$data['services_details'] = $this->admin_model->get_decorator_services($user_id);
		$this->load ->view('backend/edit_decorator_services',$data);
	}


       public function admin_edit_decorator_services()
	{
		$data['title']="Service 's Eddition";
		
		if(!empty($_POST)){
		    $services = implode(',',$_POST[services]);
			$other_services = implode(',',$_POST[other_services]);
		    $user_id = $this->input->post(hidden_user_id);
		   $id = $this->input->post(hidden_id);
		   	$additionaldata = array(
				'services' =>  $services,
				'other_services' => $other_services
			);
			$id = $this->admin_model->update_decoratorservices($user_id , $additionaldata);
			$this->session->set_flashdata('message',"Decorator services has been updated successfully" );
			redirect('sp_manager/profile/?id='.base64_encode($user_id));
		}	
	}


	public function list_decorator_services()
	{
		$data['title']="Decorator Services Listings";
		$user_id = $this->session->userdata('id');
		$data['decorator_details'] = $this->admin_model->list_deco_services($user_id);
		$this->load->view('backend/list_decorator_services',$data);
	}
	
	 public function list_services_delete()
	{
		$id = $_GET['id'];
		$data['title']="Decorator Service 's List";
		$this->admin_model->list_decorator_services_delete($id);
		$this->session->set_flashdata('message',"Decorator services has been deleted successfully" );
		redirect('decorator/list_decorator_services');
	}
	
	 public function list_decorator_images()
	{
		$data['title']="Decorator Images Listings";
		$user_id = $this->session->userdata('id');
		$data['decorator_details'] = $this->admin_model->list_decorator_images($user_id);
		$this->load->view('backend/list_decorator_images',$data);
	}
	
	 public function admin_list_decorator_title_images()
	{
		$data['title']="Decorator Images Listings";
		$user_id = $_GET['id'];
		$data['decorator_details'] = $this->admin_model->list_decorator_images($user_id);
		$this->load->view('backend/list_decorator_title_images',$data);
	}
	
	 public function admin_list_decorator_images()
	{
		$data['title']="Decorator Images Listings";
		$id = $_GET['id'];
		$user_id = $_GET['user_id'];
		$data['decorator_details'] = $this->admin_model->list_decorator_admin_images($user_id , $id);
		$this->load->view('backend/admin_list_decorator_images',$data);
	}
	
	
	 public function remove_images()
	{   
		$id = $_GET['id'];
		$url = $_GET['url'];
		
		$record_id = $this->admin_model->get_last_decorator_record($id);
		$rec_id = $this->admin_model->delete_last_decorator_record($record_id->id);
		if(!empty($rec_id)){
			$this->session->set_flashdata("message","Uploaded photographs removed successfully.");
		}
		$user_id = $this->session->userdata('id');
		$images = array();
		$images = explode(',',$record_id->images);
		//print_r($images);
		//print_r($record_id);die("/");
		
		for($i=0;$i<sizeof($images);$i++){
			$x = $url.'assets/uploads/photography/'.$user_id."/".$images[$i];
			unlink($url.'assets/uploads/photography/'.$user_id."/".$images[$i]);
			echo $x;
		}
		redirect("decorator/image_upload_decorator");
	}
	
		public function image_upload_decorator()
	{
		$data['title']="Decorator Multiple Image Upload";
		define ("MAX_SIZE","9000"); // 2MB MAX file size
		function getExtension($str)
		{
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
		}
		// Valid image formats 
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
		if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
		{
	    $hidden = $_POST['hidden'];
		$image = array();
		$photography = "photography";
		//$user_id = $this->session->userdata('id');
		$dirname = "assets/uploaddecorator/".$photography."/".$user_id."/";
		
		if(is_dir($dirname)){
		$file_path = $dirname;
		}else{
		$file_path = mkdir($dirname,0777);
		$file_path = $dirname;
		}
		
		
	foreach ($_FILES['photos']['name'] as $name => $value)
		{
		$filename = stripslashes($_FILES['photos']['name'][$name]);
		
		$size=filesize($_FILES['photos']['tmp_name'][$name]);
		//Convert extension into a lower case format
		$ext = getExtension($filename);
		$ext = strtolower($ext);
		//File extension check
		if(in_array($ext,$valid_formats))
		{
		//File size check
		if ($size < (MAX_SIZE*1024))
		{
		$image_name=time().$filename;
		$image[] = $image_name;
		echo "<img src='".$hidden.$file_path.$image_name."' class='imgList'>";
		//echo $file_path;
		//echo $user_id;
		$newname=$file_path.$image_name;

		//Moving file to uploads folder
		if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname))
		{
		$time=time();
		//Insert upload image files names into user_uploads table
		//mysql_query("INSERT INTO user_uploads(image_name,user_id_fk,created) VALUES('$image_name','$session_id','$time')");
		}
		else
		{
		echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>'; }
		}

		else
		{
		echo '<span class="imgList">You have exceeded the size limit!</span>';
		}

		}

		else
		{
		echo '<span class="imgList">Unknown extension!</span>';
		}

		} //foreach end
		
		$image_multiple_name = implode(',',$image);
		$user_id = $this->session->userdata('id');
		$upload_multiple = array(
		'images' => $image_multiple_name,
		'date'   => date('Y-m-d H:i:s'),
		'user_id' => $user_id
		);
		 $this->admin_model->decorator_tags($upload_multiple);
		 //echo 1;
        die;
		} 
		
      $this->load ->view('backend/add_decorator_images',$data); 
	}
	
	public function image_uploaddecorator()
	{    
	    $user_id = $this->session->userdata('id');;
		$upload_id = $this->admin_model->get_decorator_tags($user_id);
		
		
		$title = $_POST['title'];
		$location = $_POST['location'];
		$type_arrangement_tags = implode(',',$_POST['tags']);
		$add_multiple_images = array(
		'title' => $title,
		'location' => $location,
		'tags' => $type_arrangement_tags,
		);
		
		$this->admin_model->add_decorator_tags($add_multiple_images , $upload_id->id);
		$this->session->set_flashdata('message',"Portfolio for decorator has been added successfully");
		redirect('decorator/image_upload_decorator');
		
	}
	
	public function decorator_customer_proposal()
	{
		
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$customer_name = $this->input->post('customer_name');
		$email = $this->input->post('email');
		$contact_number = $this->input->post('contact_number');
		$message1 = $this->input->post('message');
		$img = $this->input->post('photos');
		$selimg = $this->input->post('selimg');
		$proposalarray = array(
		                       'proposal_id'		=>rand(),
							   'sp_id'      		=>$user_id,
							   'sp_name'    		=>$data['userinfo'][0]->user_name,
							   'sp_email'   		=>$data['userinfo'][0]->user_email,
							   'customer_name'    	=>$customer_name,
							   'customer_email'    	=>$email,
							   'contact_number'    	=>$contact_number,
							   'message'    	    =>$message1,
							   'image'    			=>json_encode($selimg),
							   'send_date'			=>strtotime(date('Y-m-d')),
							   'status'  			=>0
							  );
		$table="ww_customer_proposal";
		$response=$this->admin_model->send_proposal($table,$proposalarray);
		//die;
        /********* send mail customer_name *****************/
        if($response==true):

		$to = 'vipin.vayuz@gmail.com';
        $base_url=base_url();
		$subject = 'Customer Proposal Request';
		$message = '<html><body>';
		$message = '<div style="overflow-x:auto;">';
		$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr style='background: #eee;'><td><strong>Customer Name:</strong> </td><td>" . strip_tags($customer_name) . "</td></tr>";
		$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($email) . "</td></tr>";
		$message .= "<tr><td><strong>Contact Number:</strong> </td><td>" . strip_tags($contact_number) . "</td></tr>";
		$selimg = $this->input->post('selimg');
		if (($selimg) != '') {
		
			for($i=0;$i<count($selimg);$i++)
			{
				if($i%2==0)
				{
					
					$message .= '<tr width="300px">';
					$message .= '<td width="150px"><img width="100px" height="50px" border-style="dotted" src="'.$base_url.'assets/uploaddecorator/photography/'.$selimg[$i].'" alt="proposal image" /></td>';	
					
				}
				else
				{
					
					$message .= '<td width="150px"><img width="100px" height="50px" border-style="dotted" src="'.$base_url.'assets/uploaddecorator/photography/'.$selimg[$i].'" alt="proposal image" /></td>';	
					$message .= '</tr>';
				}
				
			}
			
		}
		$curText = htmlentities($message1);           
		if (($curText) != '') {
			$message .= "<tr><td><strong>Other Content:</strong> </td><td>" . $curText . "</td></tr>";
		}
		$message .= "</table>";
		$message .= "</div>";
		$message .= "</body></html>";
		
		
		
		$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		//$headers .= "CC: susan@example.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        
		if(mail($to, $subject, $message, $headers))
		{
			$data['sendvenuesucees']="Message is Successfully";
			
		}	
		$data="Message is Successfully";
		$data = base64_encode($data);
		redirect('Photographer/list_proposal?set='.$data);
		endif;
				
       /********  end send mail *********************/		
		
	}
}
