<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lead extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('lead_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		error_reporting(0);
		
	}
	
/**************Lead Listing starts here*******************/	
	public function index()
	{
	    $data['title']="Lead's List";
	    //for admin
	    if($this->session->userdata('role')=='admin'){
		$data['leads_results'] = $this->lead_model->leads();
		 }
		 //for service provider
		 else {
       $result= $this->admin_model->get_data('ww_lead_module_user',array('sp_id'=>$this->session->userdata('id'),'status'=>1));
       		 
		 $results = $this->lead_model->leads();		 
			foreach($result as $res){
				$idlead[]=$res->lead_id;						
			}
			foreach($results as $res){
				if(in_array($res->lead_id,$idlead)){
				
				$data['leads_results'][]=$res;
				}			
			}
		 }
		// echo "<pre>";
		 //print_r($data['leads_results']);
		 //exit;
		$this->load->view('backend/lists_lead',$data);
	}
/**************Lead Listing Ends here*******************/	

/**************Lead Share starts here*******************/		
	public function share_lead()
	{
	        $data['title']="Lead's Share";
	        $user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$type = $this->uri->segment(4);
		$data['id'] = $this->uri->segment(3);
		$data['leads_provider'] = $this->lead_model->leads_provider($type);
		$hidden_id = $this->input->post('hidden_id');
		$data['leads_results'] = $this->lead_model->leads_details($hidden_id);	
		
		
		if(!empty($_POST)){
			/***********Update lead with sp id************/
		 $sp=implode(',',$this->input->post('service_provider'));
		$detail=array('sp_id'=>$sp);
		$where=array('lead_id'=>$this->input->post('hidden_id'));

		$table='ww_lead_module';
 		$this->admin_model->update_data($table,$detail,$where);	
		
			$service_provider = $this->input->post('service_provider');
			foreach($service_provider as $key=>$val){
				$details=array('sp_id'=>$val,
								  'lead_id'=>$this->input->post('hidden_id')
									);
		

		$table='ww_lead_module_user';
 		$this->admin_model->add_data($details,$table);
				$emails = $this->lead_model->leads_email_details($val);
				$email_id[] = $emails->user_email;
				
				$to = $emails->user_email;
				$base_url=base_url();
				$subject = 'Lead Management Request';
				$message = '<html><body>';
				$message = '<div style="overflow-x:auto;">';
				$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
				$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
				$message .= "<tr style='background: #eee;'><td><strong>Customer Name:</strong> </td><td>" . strip_tags($data['leads_results']->Name) . "</td></tr>";
				$message .= "<tr><td><strong>Budget:</strong> </td><td>" . strip_tags($data['leads_results']->Budget) . "</td></tr>";
				$message .= "<tr><td><strong>Response:</strong> </td><td>" . strip_tags($data['leads_results']->Response) . "</td></tr>";
				$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($data['leads_results']->Email_Id) . "</td></tr>";
				$message .= "<tr><td><strong>Contact Number:</strong> </td><td>" . strip_tags($data['leads_results']->Contact_Num) . "</td></tr>";
				
				$message .= "</table>";
				$message .= "</div>";
				$message .= "</body></html>";
				
				
				$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
				$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
				//$headers .= "CC: susan@example.com\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

				
				if(mail($to, $subject, $message, $headers))
				{
					$data['sendvenuesucees']="Message is Successfully";
					
				}	
				$data="Message is Successfully";
		}
		
		//$data = base64_encode($data);
		$this->session->set_flashdata('message',"Leads details are send successfully.");
		redirect('lead');
		
		
			
		}
		
	    $this->load->view('backend/share_lead',$data);
	}
	
/**************Lead Share Ends here*******************/		
	
	/**************Create Lead starts here*******************/	
	public function create_lead()
	{
	    $data['title']="Add Lead's";
		if(!empty($_POST)){
			
		    $type= $this->input->post('type');
			$customer_name= $this->input->post('customer_name');
			$email= $this->input->post('email');
			$contact_no= $this->input->post('contact_no');
			$budget= $this->input->post('budget');
			$lid='LED'.rand(1,50000000);
			$leadArray = array(
			'lead_id'=>$lid,
			'Type' => $type,
			'Name' => $customer_name,
			'Contact_Num' => $contact_no,
			'Email_Id' => $email,
			'Budget' => $budget,
			'event'=>$this->input->post('event'),
			'description'=>$this->input->post('description'),
			'created_date' => date('Y-m-d H:i:s')
			);
		    $data['leads_results'] = $this->lead_model->add_custom_leads($leadArray);	
			redirect('lead');
			
		}
		
		$this->load->view('backend/add_custom_lead',$data);
	}
/**************create Lead Ends here*******************/	

/**************View Lead starts here*******************/	
	public function lead_view()
	{
		$data['title'] = 'Leads View';
		
	    $id =$this->uri->segment(3);
		$data['leads_details'] = $this->lead_model->leads_details($id);
		$data['id'] = $this->uri->segment(3);
			   
	    $data['leads_provider'] = $this->lead_model->leads_provider($data['leads_details']->Type);
		
		$hidden_id = $this->input->post('hidden_id');
		
		$data['leads_results'] = $this->lead_model->get_custom_leads($id);
		$data['leads_status'] = $this->admin_model->get_data('ww_lead_module_user',array('lead_id'=>$id));
		
		$data['user_data'] = $this->admin_model->get_data_all('ww_admin_login');	
			
		$this->load->view('backend/view_custom_lead',$data);
	}
	
/**************View lead ends here*******************/	

/**************Lead Deletion starts here*******************/		
	public function lead_delete()
	{
	    $id = base64_decode($_GET['id']);
		$this->lead_model->leads_delete($id);
		redirect('lead');
	}
	
/**************Lead Deletion Ends here*******************/		
}