<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('blog_model');
		$this->load->model('admin_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		error_reporting(0);
		
		
	}
	
	public function index()
	{
	    $data['title']="Blog Addition";
		$this->load->view('backend/blog',$data);
	}
	
	/*
	public function add_blogger()
	{
	    $data['title']="Blog Addition";
		
		if(!empty($_POST)){
			$blogger_name = $this->input->post('blogger_name');
			$email = $this->input->post('email');
			$mobile_no = $this->input->post('mobile_no');
			$user_id = $this->session->userdata('id');
			$blog_array = array(
			 'name' => $blogger_name,
			 'email' => $email,
			 'mobile' =>  $mobile_no,
			 'role'  => 'blogger',
			 'user_id' => $user_id,
			 'created_date' => date('Y-m-d H:i:s')
		 );
		$id = $this->blog_model->insert_blogger($blog_array);
		if(!empty($id)){
			 redirect('blog/list_blogger');	
		}
       	}
		
		$this->load->view('backend/add_blogger',$data);
	}
	*/
	
	public function edit_blogger()
	{   
	    $data['title']="Blog's Eddition";
		$id = $_GET['id'];
		$user_id = $this->session->userdata('id');
		
        if(!empty($_POST)){
			$blogger_name = $this->input->post('blogger_name');
			$email = $this->input->post('email');
			$mobile_no = $this->input->post('mobile_no');
			$id = $this->input->post('hidden_id');
			$user_id = $this->session->userdata('id');
			$blog_array = array(
			 'name' => $blogger_name,
			 'email' => $email,
			 'mobile' =>  $mobile_no,
			 'role'  => 'blogger',
			 'user_id' => $user_id,
			 'updated_date' => date('Y-m-d H:i:s')
		 );
		$id = $this->blog_model->update_blogger($id , $blog_array);	
		redirect('blog/list_blogger');
		}
		
		$data['blogger_info'] = $this->blog_model->get_blogger_details($id);
		$this->load ->view('backend/edit_blogger',$data);
	}
	
	function list_blogger(){
		$data['title']="Blogger Lists";
		$data['blog_result'] = $this->blog_model->get_blogger();
		$this->load->view('backend/list_blogger',$data);
	}

	public function add_blogger()
	{
		$data['title'] ="Create Blogger";
		
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$this->form_validation->set_rules('blogger_name','blogger_name','required');
		$this->form_validation->set_rules('email','email','required');
		$this->form_validation->set_rules('mobile_no','mobile_no','required');
		
		if ($this->form_validation->run() == false)
		{
		 $this->load->view('backend/add_blogger',$data);
		}else{
		
			
			$email    = strtolower($this->input->post('email'));
			
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
			$password = substr( str_shuffle( $chars ), 0, 8 );

			$this->load->helper('string');
			$login_id= substr($role,0,3).random_string('alnum',10).substr($user_name,0,3);
			$email = strtolower($this->input->post('email'));
			$role = strtolower($this->input->post('role'));
	        $blogger_name = $this->input->post('blogger_name');
			$email = $this->input->post('email');
			$mobile_no = $this->input->post('mobile_no');
			$user_id = $this->session->userdata('id');
			$blog_array = array(
			 'id' => $login_id,
			 'name' => $blogger_name,
			 'email' => $email,
			 'mobile' =>  $mobile_no,
			 'role'  => 'blogger',
			 'password' => md5($password),
			 'status' => '0',
			 'user_id' => $user_id,
			 'created_date' => date('Y-m-d H:i:s')
		    );
		    $id = $this->blog_model->insert_blogger($blog_array);
		   
			$link = base_url().'blog/user_activation/?user_id='.$login_id;
			
			
			
			
			if(!$id):

		$to = 'ankit.vayuz@gmail.com';
        $base_url=base_url();
			
		$subject = 'User Addition';
		$message = '<html><body>';
		$message = '<div style="overflow-x:auto;">';
		$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr style='background: #eee;'><td><strong>Email :</strong> </td><td>" . strip_tags($email) . "</td></tr>";
		$message .= "<tr><td><strong>Password :</strong> </td><td>" . strip_tags($password) . "</td></tr>";
		$message .= '<tr><td><strong>Activation Link :</strong> </td><td><a href="'.$link.'">Activate Account</a></td></tr>';
		
		$message .= "</table>";
		$message .= "</div>";
		$message .= "</body></html>";
		
		
		
		$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		//$headers .= "CC: susan@example.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        
		if(mail($to, $subject, $message, $headers))
		{
			$data['sendvenuesucees']="Message is Successfully";
			
		}	
	    $data="Message is Successfully";
		$data = base64_encode($data);
		$this->session->set_flashdata('message'," Blogger has been added successfully" );
		redirect('blog/list_blogger');	
		endif;
		
		}
		
		$this->load->view('backend/add_blogger',$data);
	}
	
       public function user_activation() {
	    $data['title']="User Login";
		$data['userid'] = $_GET['user_id'];
		$this->load->view('backend/activation_view',$data);
	}
	
	public function change_password() {
	    $data['title']="User Login";
		$password = md5($_POST['password']);
		$id = $_POST['hidden'];
		$passArray = array(
		'password'=> $password);
		$pass = $this->blog_model->activate_account($passArray , $id);
		$this->session->set_flashdata('message',"Your password has been reset." );
		$this->load->view('backend/blog_login');
	}

	public function check_old_password() {
	    $old_password = md5($this->input->post('old_password'));
	    $user_id = $this->input->post('user_id');
		$result = $this->blog_model->get_password($old_password,$user_id);
		if(empty($result)){
			echo 1;
			die;
		}else{
			echo 0;
			die;
		}
		$this->load->view('backend/activation_view',$data);
	}
	
	public function add_blog()
	{   
	if(!empty($_POST)){
		
		$title = $_POST['blog_title'];
		$allow_like = $_POST['allow_like'];
		if(empty($allow_like)){
			$allow_like = 'off';
		}
		$allow_comments = $_POST['allow_comments'];
		if(empty($allow_comments)){
			$allow_comments = 'off';
		}
		$allow_sharing = $_POST['allow_sharing'];
		if(empty($allow_sharing)){
			$allow_sharing = 'off';
		}
		
		$published_date = $_POST['published_date'];
		$description = $_POST['description'];
		$keywords = implode(',',$_POST['keywords']);
        $user_id = $this->session->userdata('id');
		
		 
	   $fileName = $_FILES["file1"]["name"]; // The file name
	   $file_name =  time().$fileName;
	   $fileTmpLoc = $_FILES["file1"]["tmp_name"]; // File in the PHP tmp folder
	   $fileType = $_FILES["file1"]["type"]; // The type of file it is
	   $fileSize = $_FILES["file1"]["size"]; // File size in bytes
	   $fileErrorMsg = $_FILES["file1"]["error"]; // 0 for false... and 1 for true
	    
		$dirname = "uploads/";
		
		
		
        move_uploaded_file($fileTmpLoc, "$dirname/$file_name");
		
		$file_type = array('image/png',
		'image/gif',
		'image/jpeg',
		'image/pjpeg',
		'text/plain',
		'text/html',
		'application/x-zip-compressed',
		'application/pdf',
		'application/msword');
	  
		 $image_array = array(
		 'title' => $title,
		 'description' => $description,
		 'keywords' =>  $keywords,
		 'published_date' =>  $published_date,
		 'allow_like' =>  $allow_like,
		 'allow_comments' =>  $allow_comments,
		 'allow_sharing' =>  $allow_sharing,
		  'image' =>  $file_name,
		  'user_id' => $user_id,
		  'created_date' => date('Y-m-d H:i:s')
		 );
		//print_r($image_array);die;
		$id = $this->blog_model->insert_blog($image_array);
	   redirect('blog/list_blogs');
	}
	
	}
	
	function list_blogs(){
		$data[blog_result] = $this->blog_model->get_blogs();
		$this->load->view('backend/list_blogs',$data);
	}
	
	public function edit_blog()
	{   
	    $data['title']="Blog's Eddition";
		$id = $_GET['id'];
		$user_id = $this->session->userdata('id');
		if(!empty($_POST)){
			
		$published_date = date('Y-m-d',$_POST['published_date']);
		$allow_like = $_POST['allow_like'];
		$allow_comments = $_POST['allow_comments'];
		$allow_sharing = $_POST['allow_sharing'];
		$title = $_POST['blog_title'];
		$id = $_POST['hidden'];
		$description = $_POST['description'];
		$keywords = implode(',',$_POST['keywords']);
	    
		if(empty($allow_like)){
			$allow_like = 'off';
		}
		$allow_comments = $_POST['allow_comments'];
		if(empty($allow_comments)){
			$allow_comments = 'off';
		}
		$allow_sharing = $_POST['allow_sharing'];
		if(empty($allow_sharing)){
			$allow_sharing = 'off';
		}
			
	if(!empty($_FILES["file1"]["name"])){
	   $fileName = $_FILES["file1"]["name"]; // The file name
	   $file_name =  time().$fileName;
	   $fileTmpLoc = $_FILES["file1"]["tmp_name"]; // File in the PHP tmp folder
	   $fileType = $_FILES["file1"]["type"]; // The type of file it is
	   $fileSize = $_FILES["file1"]["size"]; // File size in bytes
	   $fileErrorMsg = $_FILES["file1"]["error"]; // 0 for false... and 1 for true
	    
		$dirname = "uploads/";
        move_uploaded_file($fileTmpLoc, "$dirname/$file_name");
		$additionaldata = array(
				'title' => $title,
				'user_id' => $this->session->userdata('id'),
				'image' => $file_name,
				'published_date' => $published_date,
				'allow_like' => $allow_like,
				'allow_comments' => $allow_comments,
				'allow_sharing' => $allow_sharing,
				'keywords' => $keywords,
				'update_date' => date('Y-m-d H:i:s')
			);
		}else{
			$additionaldata = array(
				'title' => $title,
				'user_id' => $this->session->userdata('id'),
				'published_date' => $published_date,
				'allow_like' => $allow_like,
				'allow_comments' => $allow_comments,
				'allow_sharing' => $allow_sharing,
				'keywords' => $keywords,
				'update_date' => date('Y-m-d H:i:s')
			);
		}
	  
			$id = $this->blog_model->update_blog($id , $additionaldata);
			$this->session->set_flashdata('message',"Blog has been updated successfully" );
			
			redirect('blog/list_blogs');
		}	
		$data['blog_info'] = $this->blog_model->get_blog_details($id);
		$this->load ->view('backend/editblog',$data);
	}
	
	function delete_blog(){
		$id = $_GET['id'];
		$data[blog_result] = $this->blog_model->deleteblogs($id);
		$this->session->set_flashdata('message',"Blog has been deleted successfully" );
		redirect('blog/list_blogs');
	}

	function delete_blogger(){
		$id = $_GET['id'];
		$data[blog_result] = $this->blog_model->deleteblogger($id);
		$this->session->set_flashdata('message',"Blogger has been deleted successfully" );
		redirect('blog/list_blogger');
	}
	
	function check_duplicate_title(){
		$user_id = $this->session->userdata('id');
		$blog_title = $this->input->post('blog_title');
		$result = $this->blog_model->duplicate_title($blog_title , $user_id);
		if(!empty($result)){
			echo 1;
		}
	}
}