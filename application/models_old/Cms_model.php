<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cms_model extends CI_model
{
	
	/******Registration Starts here*******/	
	public function inser_cms($page_title,$page_heading,$page_desc1, $statuss,$created_date)
	{
	$arrcms=array(
	  'page_title'=>$page_title,
	  'page_heading'=>$page_heading,
	  'page_desc1'=>$page_desc1,
	  'status'=>0,
	  'create_date'=>strtotime($created_date),		  
	);
          return $this->db->insert('ww_cms',$arrcms);
	}
	
	public function get_cms(){
		$q = $this->db->get("ww_cms");
		 if($q->num_rows() > 0)
			{
				return $q->result();
			}
		 return array();
	}
	
	public function delete_row($id){
	$this->db->where('id', $id);
	return $this->db->delete('ww_cms'); 
	}
	
	public function update_info($id,$page_title,$page_heading,$page_desc1){
	$arrcms=array(
	  'page_title'=>$page_title,
	  'page_heading'=>$page_heading,
	  'page_desc1'=>$page_desc1
	  
	   );
	$this->db->where('id', $id);
	return $this->db->update('ww_cms', $arrcms);
    }
	
    public function get_csm1($id){
	$this->db->where('id', $id);
    $q = $this->db->get('ww_cms');
    return $q->result();
    }
}
?>
