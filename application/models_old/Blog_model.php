<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_Model extends CI_Model
{
   // Photographer by Ankit Singh
   
	public function insert_blog($additional_data)
	{
		$this->db->insert('ww_blog',$additional_data);
		return $this->db->insert_id();
	}

	 public function get_blogs() {
		$this->db->select('*');
		$this->db->where('user_id',$this->session->userdata('id'));
		$this->db->from('ww_blog');
		$query = $this->db->get();
		return $query->result();
	} 
	
       public function insert_blogger($additional_data)
	{
		$this->db->insert('ww_blogger',$additional_data);
		return $this->db->insert_id();
	}
        
       public function activate_account($statusArray , $id)
	{
	    $this->db->where('id',$id);
	    $this->db->update('ww_blogger',$statusArray);
	}
        
        public function get_blogger() {
		$this->db->select('*');
		$this->db->from('ww_blogger');
		$query = $this->db->get();
		return $query->result();
	} 
        
        public function get_password($old_password,$user_id) {
		$this->db->select('*');
		$this->db->where('password',$old_password);
		$this->db->where('id',$user_id);
		$this->db->from('ww_blogger');
		$query = $this->db->get();
		return $query->row();
	} 

        public function get_blogger_details($id) {
	    $this->db->select('*');
		$this->db->where('id',$id);
		$this->db->from('ww_blogger');
		$query = $this->db->get();
		return $query->row();
	}

	public function get_blog_details($id) {
	    $this->db->select('*');
		$this->db->where('id',$id);
		$this->db->from('ww_blog');
		$query = $this->db->get();
		return $query->row();
	}

       public function deleteblogger($id) {
		$this->db->where('id',$id);
	    $this->db->delete('ww_blogger'); //echo $this->db->last_query();die;
	}
	
	public function update_blogger($id , $catData) {
		$this->db->where('id',$id);
	    $this->db->update('ww_blogger',$catData); //echo $this->db->last_query();die;
	}

       public function duplicate_title($blog_title , $user_id) {
	    $this->db->select('*');
		$this->db->where('title',$blog_title);
		$this->db->where('user_id',$user_id);
		$this->db->from('ww_blog');
		$query = $this->db->get();
		return $query->row();
	}
	public function update_blog($id , $catData) {
		$this->db->where('id',$id);
	    $this->db->update('ww_blog',$catData); //echo $this->db->last_query();die;
	}
	public function deleteblogs($id) {
		$this->db->where('id',$id);
	    $this->db->delete('ww_blog'); //echo $this->db->last_query();die;
	}
}
?>
