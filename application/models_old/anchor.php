<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Anchor extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
        $this->output->cache(0);
		
	}
	
	public function add_profile()
	{
		$anchor_name = $this->input->post('anchor-name');
		$profile_dsc = $this->input->post('profile-dsc');
	
		$profile_pic = $this->input->post('image1');
		$hidden_flag = $this->input->post('hidden-flag');
		$chanel_name = $this->input->post('chanel-name');
		$profile_graph = $this->input->post('profile-graph');
		$twitter_graph = $this->input->post('twitter-graph');
		
		$name_id = mb_substr($anchor_name, 0, 3);
		$this->load->helper('string');
        $anchor_id =random_string('alnum',6).'_anch_'.random_string('alnum',4).'_'.$name_id;
		
		
		 
    ///=============upload Image ==================///
    /* Create the config for upload library */
      /* (pretty self-explanatory) */
      //$config['upload_path'] = './assets/upload/';
      $config['upload_path']=realpath(APPPATH . '../assets/upload/');	  /* NB! create this dir! */
	 // chmod($config['upload_path'], 0755);
      $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
      $config['max_size']  = '2048';
      $config['max_width']  = '5000';
      $config['max_height']  = '5000';
      /* Load the upload library */
      $this->load->library('upload', $config);
 
      /* Create the config for image library */
      /* (pretty self-explanatory) */
      $configThumb = array();
      $configThumb['image_library'] = 'gd2';
      $configThumb['source_image'] = '';
      $configThumb['create_thumb'] = TRUE;
      $configThumb['maintain_ratio'] = TRUE;
      /* Set the height and width or thumbs */
      /* Do not worry - CI is pretty smart in resizing */
      /* It will create the largest thumb that can fit in those dimensions */
      /* Thumbs will be saved in same upload dir but with a _thumb suffix */
      /* e.g. 'image.jpg' thumb would be called 'image_thumb.jpg' */
      $configThumb['width'] = 140;
      $configThumb['height'] = 210;
      /* Load the image library */
      $this->load->library('image_lib');
    $file=array();
    $file_thumb=array();
				 /* We have 5 files to upload
       * If you want more - change the 6 below as needed
       */
      for($i = 1; $i < 4; $i++) {
        /* Handle the file upload */
        $upload = $this->upload->do_upload('image'.$i);
        /* File failed to upload - continue */
        if($upload === FALSE) continue;
        /* Get the data about the file */
        $data = $this->upload->data();
$file[$i] = $data['file_name'];
$file_thumb[$i] = $data['raw_name'].'_thumb'.$data['file_ext'];

 
        $uploadedFiles[$i] = $data;
        /* If the file is an image - create a thumbnail */
        if($data['is_image'] == 1) {
          $configThumb['source_image'] = $data['full_path'];
          $this->image_lib->initialize($configThumb);
          $this->image_lib->resize();
		  if($this->image_lib->resize())
		  { 
	        $status = true;
           $msg = $data['file_name'];
		   
          }
		  else
		  {
           $status = false;
		   echo 'error';
          }
        }
      }
	 
				
    ///=============upload end here==================////
		
		
		
		$this->load->model('anchor_model');
		$result = $this->anchor_model->add_anchor($anchor_id, $anchor_name, $profile_dsc, $file['1'], $file_thumb['1'], $hidden_flag, $chanel_name, $file['2'], $file_thumb['2'], $file['3'], $file_thumb['3']);
	    if($result==TRUE)
	            {
					$data = "Successful anchor add";
					$data = base64_encode($data);
					redirect('admin/anchors?set='.$data);
				}
				else
				{
					
					$data = "Something is wrong";
					 $data = base64_encode($data);
					
					redirect('admin/anchors?set='.$data);
				}
		
	
	
	}
	public function edit_profile()
	{
		$anchor_id = $this->input->post('anchor-id');
		$anchor_name = $this->input->post('anchor-name');
		$profile_dsc = $this->input->post('profile-dsc');
	
		$profile_pic = $this->input->post('profile-pic');
		$hidden_flag = $this->input->post('hidden-flag');
		$chanel_name = $this->input->post('chanel-name');
		$profile_graph = $this->input->post('profile-graph');
		$twitter_graph = $this->input->post('twitter-graph');
		 
    ///=============upload Image ==================///
    /* Create the config for upload library */
      /* (pretty self-explanatory) */
      $config['upload_path'] = './assets/upload/'; /* NB! create this dir! */
      $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
      $config['max_size']  = '2048';
      $config['max_width']  = '5000';
      $config['max_height']  = '5000';
      /* Load the upload library */
      $this->load->library('upload', $config);
 
      /* Create the config for image library */
      /* (pretty self-explanatory) */
      $configThumb = array();
      $configThumb['image_library'] = 'gd2';
      $configThumb['source_image'] = '';
      $configThumb['create_thumb'] = TRUE;
      $configThumb['maintain_ratio'] = TRUE;
      /* Set the height and width or thumbs */
      /* Do not worry - CI is pretty smart in resizing */
      /* It will create the largest thumb that can fit in those dimensions */
      /* Thumbs will be saved in same upload dir but with a _thumb suffix */
      /* e.g. 'image.jpg' thumb would be called 'image_thumb.jpg' */
      $configThumb['width'] = 140;
      $configThumb['height'] = 210;
      /* Load the image library */
      $this->load->library('image_lib');
    $file=array();
    $file_thumb=array();
				 /* We have 5 files to upload
       * If you want more - change the 6 below as needed
       */
      for($i = 1; $i < 4; $i++) {
        /* Handle the file upload */
        $upload = $this->upload->do_upload('image'.$i);
        /* File failed to upload - continue */
        if($upload === FALSE) continue;
        /* Get the data about the file */
        $data = $this->upload->data();
$file[$i] = $data['file_name'];
$file_thumb[$i] = $data['raw_name'].'_thumb'.$data['file_ext'];

 
        $uploadedFiles[$i] = $data;
        /* If the file is an image - create a thumbnail */
        if($data['is_image'] == 1) {
          $configThumb['source_image'] = $data['full_path'];
          $this->image_lib->initialize($configThumb);
          $this->image_lib->resize();
        }
      }
				
    ///=============upload end here==================////
		
		
		
		$this->load->model('anchor_model');
		
		
		$data['resultchannel'] = $this->anchor_model->select_single_anchor($anchor_id);
        $chanelresult=$data['resultchannel'];
        $pro=$data['resultchannel'][0]->profile_pic;
        $proumb=$data['resultchannel'][0]->profile_thumb;
        $prog=$data['resultchannel'][0]->profile_graph;
        $proumb=$data['resultchannel'][0]->profile_graph_thumb;
		$twi=$data['resultchannel'][0]->twitter_graph;
        $twiumb=$data['resultchannel'][0]->twitter_thumb;
        if($file['1']==''){$file['1']=$pro;}
        if($file['2']==''){$file['2']=$prog;}
		if($file['3']==''){$file['3']=$twi;}
        if($file_thumb['1']==''){$file_thumb['1']=$proumb;}
        if($file_thumb['2']==''){$file_thumb['2']=$proumb;}
		if($file_thumb['3']==''){$file_thumb['3']=$twiumb;}
		
		
		$result = $this->anchor_model->edit_anchor($anchor_id, $anchor_name, $profile_dsc, $file['1'], $file_thumb['1'], $hidden_flag, $chanel_name, $file['2'], $file_thumb['2'], $file['3'], $file_thumb['3']);
	if($result==TRUE)
	            {
					$data = "Successful anchor update";
					$data = base64_encode($data);
					redirect('admin/anchors?set='.$data);
				}
				else
				{
					
					$data = "Something is wrong";
					 $data = base64_encode($data);
					
					redirect('admin/anchors?set='.$data);
				}
		
	
	
	}
}

?>
