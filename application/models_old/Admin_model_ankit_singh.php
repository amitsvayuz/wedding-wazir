<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Model extends CI_Model
{
   // Photographer by Ankit Singh
   
	public function registration($additional_data)
	{
		$this->db->insert('ww_admin_login',$additional_data);
		return 1;
	}

	public function emailexists($email)
	{
		$this->db->where('user_email', $email);
		$query = $this->db->get('WW_admin_login');
		if( $query->num_rows() > 0 ){ return 1; } else { return 0; }
	}
	
	public function add_photographer($catData) {
		$this->db->insert('ww_photographer',$catData);
		return $this->db->insert_id();
	}
	
	public function list_photographer_services_delete($id) {
		$this->db->where('id', $id);
		$this->db->delete('ww_photographer_services');
	}
	
	public function listphotographer() {
		$this->db->select('*');
		$this->db->where('role_type',photographer);
		$this->db->from('WW_admin_login');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_names_role($role) {
		$this->db->select('*');
		$this->db->where('role_type',$role);
		$this->db->from('WW_admin_login');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function list_photographer_images($id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('user_id',$id);
		$this->db->from('ww_sp_photograph');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function list_planner_images($id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('user_id',$id);
		$this->db->from('ww_planner_portfolio');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function list_decorator_images($id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('user_id',$id);
		$this->db->from('ww_decorator_portfolio');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function list_photographer_videos($id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('user_id',$id);
		$this->db->from('ww_sp_videos');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function list_photographer_services($id) {
		$this->db->select('*');
		$this->db->from('ww_photographer_services');
		$this->db->where('user_id', $id);
		$query = $this->db->get();
		return $query->row();
	}
	
	public function get_photographer_services($id) {
		$this->db->select('*');
		$this->db->from('ww_photographer_services');
		$this->db->where('user_id', $id);
		$query = $this->db->get();
		return $query->row();
	}
	
	public function edit_listphotographer($id) {
		$this->db->select('*');
		$this->db->from('ww_photographer');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->row();
	}
	
	public function update_photographer($id , $catData) {
		$this->db->where('id',$id);
	    $this->db->update('ww_photographer',$catData); //echo $this->db->last_query();die;
	}
	
	public function update_photographer_services($user_id , $additionaldata) { 
	    $this->db->where('user_id',$user_id);
	    $this->db->update('ww_photographer_services',$additionaldata);
	}
	
	public function authenticate($username,$password) {
		$this->db->select('*');
		$this->db->from('ww_admin_login');
		$this->db->where('user_email',$username);
		$this->db->where('password',$password);
		$query = $this->db->get();
		return $query->row();
	}
	
	public function add_photographer_services($additionaldata) {
		$this->db->insert('ww_photographer_services',$additionaldata);
		return $this->db->insert_id();
	}
	
	public function photographer_tags($additionaldata) {
		$this->db->insert('ww_sp_photograph',$additionaldata);
		return $this->db->insert_id();
	}
	
	public function get_photographer_tags($id) {
		$this->db->select('*');
		$this->db->where('user_id',$id);
		$this->db->order_by('date','desc');
		$query=$this->db->get('ww_sp_photograph');
		return $query->row();
	}
	
	public function add_photographertags($additionaldata,$id) {
		$this->db->where('id',$id);
		$this->db->update('ww_sp_photograph',$additionaldata);
	}
	
	public function add_video_tags($additionaldata) {
		$this->db->insert('ww_sp_videos',$additionaldata);
	}
	
	public function add_cinema_tags($additionaldata) {
		$this->db->insert('ww_sp_cinema',$additionaldata);
	}
	
	/********Select all Service Providers*************/
	
	public function select_all_serviceprovider()
	{
		$this->db->select('*');
		$this->db->from('WW_admin_login');
		$this->db->where('role_type', 'planner');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function select_all_decorator()
	{
		$this->db->select('*');
		$this->db->from('WW_admin_login');
		$this->db->where('role_type', 'decorater');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function select_all_venuemanager()
	{
		$this->db->select('*');
		$this->db->from('ww_venue');
		//$this->db->where('role_type', 'venue');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function select_single_data($table,$id)
	{
	  $this->db->select('*');
	  $this->db->from($table);
	  $this->db->where('id', $id);
	  $query = $this->db->get();
	  return $query->result();
	}
	
	public function update_data($table,$content,$where)
	{
	
		$this->db->where($where);      
		$result = $this->db->update($table,$content);
		if(($result==1))
		{
		 return true;
		}
		else
		{
		return false;
		}
	}
	
	public function deletephotographer($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_admin_login');
	}
	
	public function deletedecorator($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_admin_login');
	}
	
	public function list_planner_services($id) {
		$this->db->select('*');
		$this->db->where('user_id', $id);
		$query = $this->db->get('ww_planner_services');
		return $query->row();
	}
	
	public function list_planner_arrangements() {
		$this->db->select('*');
		$query = $this->db->get('ww_planner_arrangements');
		return $query->result();
	}
	
	public function list_planner_events() {
		$this->db->select('*');
		$query = $this->db->get('ww_event');
		return $query->result();
	}
	
	public function add_planner_services($additionaldata) {
		$this->db->insert('ww_planner_services',$additionaldata);
		return $this->db->insert_id();
	}
	
	public function get_planner_services($id , $user_id) {
		$this->db->select('*');
		$this->db->where('user_id', $user_id);
		$this->db->where('id', $id);
		$query = $this->db->get('ww_planner_services');
		return $query->row();
	}
	
	public function update_planner_services($id , $user_id , $additionaldata) { 
	    $this->db->where('id',$id);
	   // $this->db->where('user_id',$user_id);
	    $this->db->update('ww_planner_services',$additionaldata);
	}
	
	public function list_planner_services_delete($id) {
		$this->db->where('id', $id);
		$this->db->delete('ww_planner_services');
	}
	
	public function planner_tags($additionaldata) {
		$this->db->insert('ww_planner_portfolio',$additionaldata);
		return $this->db->insert_id();
	}
	
	public function get_planner_tags($id) {
		$this->db->select('*');
		$this->db->where('user_id',$id);
		$this->db->order_by('date','desc');
		$query=$this->db->get('ww_planner_portfolio');
		return $query->row();
	}
	
	public function add_planner_tags($additionaldata,$id) {
		$this->db->where('id',$id);
		$this->db->update('ww_planner_portfolio',$additionaldata);
	}
	
	public function get_password($email) {
		$this->db->select('*');
		$this->db->where('user_email', $email);
		$query = $this->db->get('ww_admin_login');
		return $query->row();
	}
	
	public function update_password($new_password , $email) {
		$this->db->where('user_email',$email);
		$this->db->update('ww_admin_login',$new_password);
		return 1;
	}
	
	public function current_password($current_password) {
		$user_id = $this->session->userdata('id');
		$this->db->where('id', $user_id);
		$this->db->where('password', $current_password);
		$query = $this->db->get('ww_admin_login');
		if( $query->num_rows() > 0 ){ return 1; } else { return 0; }
	}
	
	public function change_password($passArray , $id) {
		$this->db->where('id',$id);
		$this->db->update('ww_admin_login',$passArray);
	}
	
	public function get_last_record($id) {
		$this->db->select('*');
		$this->db->where('user_id', $id);
		$this->db->order_by("date","desc");
		$query = $this->db->get('ww_sp_photograph');
		return $query->row();
	}
	
	public function delete_last_record($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_sp_photograph');
	}
	
	public function video_insert($additionaldata) {
		$this->db->insert('ww_sp_videos',$additionaldata);
		return $this->db->insert_id();
	}
	
	public function get_last_decorator_record($id) {
		$this->db->select('*');
		$this->db->where('user_id', $id);
		$this->db->order_by("date","desc");
		$query = $this->db->get('ww_decorator_portfolio');
		return $query->row();
	}
	
	public function get_last_planner_record($id) {
		$this->db->select('*');
		$this->db->where('user_id', $id);
		$this->db->order_by("date","desc");
		$query = $this->db->get('ww_planner_portfolio');
		return $query->row();
	}
	
	public function delete_last_planner_record($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_planner_portfolio');
	}
	
	public function delete_last_decorator_record($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_decorator_portfolio');
	}
	
	public function decorator_tags($additionaldata) {
		$this->db->insert('ww_decorator_portfolio',$additionaldata);
		return $this->db->insert_id();
	}
	
	public function get_decorator_tags($id) {
		$this->db->select('*');
		$this->db->where('user_id',$id);
		$this->db->order_by('date','desc');
		$query=$this->db->get('ww_decorator_portfolio');
		return $query->row();
	}
	
	public function add_decorator_tags($additionaldata,$id) {
		$this->db->where('id',$id);
		$this->db->update('ww_decorator_portfolio',$additionaldata);
	}
	
	/*********Funtiona by Manoj***/
	
}
?>
