<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Venue_model extends CI_model
{
	/******Venue manager Starts here*********/	

	public function add_venue($table,$companyarray)
	{
		$result = $this->db->insert($table,$companyarray);
		if($result==TRUE):
		$data = "Successful add venue";
		$data = base64_encode($data);
		redirect('Venue/venue_manager?set='.$data);
		else:
		$data = "Something is wrong";
		$data = base64_encode($data);
		redirect('Venue/venue_manager?set='.$data);
		endif;
	}

	public function venue_update($tablename,$venuearray,$id)
	{
		//$this->db->where('v_id', $id);
		$result = $this->db->update($tablename,$venuearray,"v_id=$id");
		if($result==TRUE):
		$data = "Successful update venue";
		$data = base64_encode($data);
		redirect('Venue/venue_manager?set='.$data);
		else:
		$data = "Something is wrong";
		$data = base64_encode($data);
		redirect('Venue/venue_manager?set='.$data);
		endif;
	}
	
	public function get_venue()
	{
		$result= $this->db->get('ww_venue');
		if($result->num_rows() > 0):
		return $result->result();
		endif;
	}

	public function venue_delete($id)
	{
		$this->db->where('v_id', $id);
		$result =$this->db->delete('ww_venue');
		if($result==TRUE):
		$data = "Successful delete venue";
		$data = base64_encode($data);
		redirect('Venue/venue_manager?set='.$data);
		else:
		$data = "Something is wrong";
		$data = base64_encode($data);
		redirect('Venue/venue_manager?set='.$data);
		endif;
	}

	public function venue_edit_get($id)
	{
		$this->db->where('v_id', $id);
		$result =$this->db->get('ww_venue');
		if($result->num_rows() > 0):
		return $result->result();
		endif;
	}
}
?>
