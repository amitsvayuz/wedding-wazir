<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Venue_setting_model extends CI_model
{
	

/****************get city***********************/	
		public function get_city()
		{
			$result= $this->db->get('city_setting');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}

       /****************get_pro***********************/	
		public function get_pro()
		{
			$result= $this->db->get('ww_venue_property');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
	
	
	/***************get_events*********************/
	public function get_events()
	{
		$result= $this->db->get('ww_event');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
	}
/***************add property*********************/

public function add_property($propertyarray)
{
	$table='ww_venue_property';
    $result = $this->db->insert($table,$propertyarray);
    if($result=true)
			{
				$data = "Property added successfully";
				$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
				redirect('Venue_setting_cntlr/list_property');
			}
			else
			{
				$data = "Something is wrong";
				$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
				redirect('Venue_setting_cntlr/list_property');
			
			}
	
}

/***************update property*********************/

public function update_property($propertyarray,$id)
{
	
	$table='ww_venue_property';
    $result = $this->db->update($table,$propertyarray,"pro_id=$id");
    if($result==TRUE ):
	$data = "Property updated successfully";
	$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
	redirect('Venue_setting_cntlr/list_property');
	else:
	$data = "Something is wrong";
	$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
	redirect('Venue_setting_cntlr/add_property');
	endif;
	
}

/*************** delete property*********************/

public function delete_property($id)
{
	$table='ww_venue_property';
    $result = $this->db->delete($table,"pro_id=$id");
    if($result==TRUE ):
	$data = "Property deleted successfully  ";
	$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
	redirect('Venue_setting_cntlr/list_property');
	else:
	$data = "Something is wrong";
	$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
	redirect('Venue_setting_cntlr/add_property');
	endif;
	
}

/****************get property***********************/	
		public function get_property()
		{
			$result= $this->db->get('ww_venue_property');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		
		/****************get venue company***********************/	
		public function get_venue_company()
		{
			$result= $this->db->get('ww_admin_login');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		
		/****************get company by ajax***********************/	
		public function sel_ajax_company($table,$id)
		{
			$result = $this->db->get_where($table, array('city_id' => $id));
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		
		/****************get_company_uniqueid***********************/	
		public function get_company_uniqueid($id)
		{
			$result = $this->db->get_where('ww_company', array('c_id' => $id));
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		/****************get brand by ajax***********************/	
		public function get_brand_ajax($table,$id)
		{
			$result = $this->db->get_where($table, array('proName' => $id));
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		
		/****************get city by ajax***********************/	
		public function get_city_ajax($table,$id)
		{
			$result = $this->db->get_where($table, array('city_id' => $id));
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		
		/****************get location by ajax***********************/	
		public function get_location_ajax($table,$id)
		{
			$result = $this->db->get_where($table, array('city_id' => $id));
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		/****************get property by ajax***********************/	
		public function get_property_ajax($table,$id)
		{
			$result = $this->db->get_where($table, array('location' => $id));
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		/****************get belt***********************/	
		public function get_beltt()
		{
			$result= $this->db->get('ww_venue_belt');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		
		/****************get_propertyUnigue***********************/	
		public function get_propertyUnigue($id)
		{
			$result = $this->db->get_where('ww_venue_property', array('pro_id' => $id));
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}

                /****************get_company***********************/	
		public function get_company()
		{
			$result = $this->db->get('ww_admin_login');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
              
		
		/****************get_sitting***********************/	
		public function get_sitting($id)
		{
			$result = $this->db->get_where('ww_sitting_arrangement', array('p_id' => $id));
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		/****************get_parking***********************/	
		public function get_parking($id)
		{
			$result = $this->db->get_where('ww_parking', array('p_id' => $id));
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		
		/****************get_keyDist***********************/	
		public function get_keyDist($id)
		{
			$result = $this->db->get_where('ww_venue_key_distance', array('p_id' => $id));
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}



}
?>
