<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Planner extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model'); 
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		//error_reporting(0);
		
		if(!$this->session->userdata('id')){
			redirect('registration');
		}
	}
	
	public function index()
	{
		$data['title']="Photographer Addition";
		$this->load->view('backend/list_photographer',$data);
	}

	 public function add_planner_services()
	{
		$data['title']="Planner Service 's Addition";
		if(!empty($_POST)){
		    $events = implode(',',$_POST[events]);
			$arrangements  = implode(',',$_POST[arrangements]);
			$property_preferences = implode(',',$_POST[property_preferences]);
			$venue_preferences  = $_POST[venue_preferences];
			
		   
			$additionaldata = array(
				'events' => $events,
				'user_id' => $this->session->userdata('id'),
				'arrangements' => $arrangements,
				'property_preference' => $property_preferences,
				'venue_preference' => $venue_preferences
			);
			$user_id = $this->session->userdata('id'); 
			$planner_services = $this->admin_model->get_planner_services($user_id);
			if(!empty($planner_services)){
				$id = $this->admin_model->update_planner_services($user_id , $additionaldata);
				$this->session->set_flashdata('message',"Planner Services has been updated successfully" );
			}else{
				$id = $this->admin_model->add_planner_services($additionaldata);
				$this->session->set_flashdata('message',"Planner Services has been added successfully" );
			}
			redirect('planner/list_planner_services');
		}
		$data['events_arrangements'] = $this->admin_model->list_planner_arrangements();
		$data['events_details'] = $this->admin_model->list_planner_events();
		$this->load ->view('backend/add_planner_services',$data);
    }
	
	 public function edit_planner_services()
	{
		$data['title']="Planner Service's Edition";
		$id = $_GET['id'];
		$user_id = $this->session->userdata('id');
		  if(!empty($_POST)){
		    $id = $_POST['hidden_id'];
		    $events = implode(',',$_POST[events]);
			$arrangements  = implode(',',$_POST[arrangements]);
			$property_preferences = implode(',',$_POST[property_preferences]);
			$venue_preferences  = $_POST[venue_preferences];
			
		   
			$additionaldata = array(
				'events' => $events,
				'user_id' => $this->session->userdata('id'),
				'arrangements' => $arrangements,
				'property_preference' => $property_preferences,
				'venue_preference' => $venue_preferences
			);
			
			$user_id = $this->session->userdata('id'); 
			$update_id = $this->admin_model->update_planner_services($id , $user_id , $additionaldata);
			$this->session->set_flashdata('message',"Planner Services has been updated successfully" );
			redirect('planner/list_planner_services');
		}
		$data['planner_services'] = $this->admin_model->get_planner_services($id , $user_id);
		$data['events_arrangements'] = $this->admin_model->list_planner_arrangements();
		$data['events_details'] = $this->admin_model->list_planner_events();
		$this->load ->view('backend/edit_planner_services',$data);
    }
	
	 public function admin_edit_planner_services()
	{
		$data['title']="Planner Service 's Edition";
		$user_id = $this->input->post(hidden_user_id);
		$id = $this->input->post(hidden_service_id);
		if(!empty($_POST)){
		    $events = implode(',',$_POST[events]);
			$arrangements  = implode(',',$_POST[arrangements]);
			$property_preferences = implode(',',$_POST[property_preferences]);
			$venue_preferences  = $_POST[venue_preferences];
			  
			$additionaldata = array(
				'events' => $events,
				'user_id' =>$user_id,
				'arrangements' => $arrangements,
				'property_preference' => $property_preferences,
				'venue_preference' => $venue_preferences
			);
			$data['result'] = $this->admin_model->select_planner_services($user_id);
            if(empty($data['result'])){
				$id = $this->admin_model->add_planner_services($additionaldata);
				$this->session->set_flashdata('message',"Planner Services has been updated successfully" );
			}else{
				$update_id = $this->admin_model->update_planner_services($id , $user_id , $additionaldata);
				$this->session->set_flashdata('message',"Planner Services has been added successfully" );
			}		
			redirect('sp_manager/planner_profile/?id='.base64_encode($user_id));
		}
    }
	
	
	public function detail_planner_services()
	{
        $table='ww_admin_login';
		$this->load->model('admin_model');
		$user_id = $this->session->userdata('id');
		$data['result']=$this->admin_model->select_single_data($table,$user_id);
		$data['title']="User Profile";       

        $data['title']="Service 's Eddition";
		$id = $_GET['id'];
		$data['services_details'] = $this->admin_model->get_planner_services($id  , $user_id);
		$this->load ->view('backend/detail_planner_services',$data);
	}

	 public function list_planner_services()
	{
		$data['title']="Planner Service 's List";
		$data['list_details'] = $this->admin_model->list_planner_services($this->session->userdata('id'));
		$data['events_arrangements'] = $this->admin_model->list_planner_arrangements();
		$data['events_details'] = $this->admin_model->list_planner_events();
		$this->load ->view('backend/list_planner_services',$data);
	}
	
	 public function list_planner_services_delete()
	{
		$id = $_GET['id'];
		$data['title']="Planner Service 's List";
		$data['list_details'] = $this->admin_model->list_planner_services_delete($id);
		$this->session->set_flashdata('message','Deleted successfully.');
		redirect('planner/add_planner_services');
	}
	
	 public function admin_list_planner_images()
	{
		$data['title']="Planner Images Listings";
		$user_id = $_GET['user_id'];
		$id = $_GET['id'];
		$data['photographer_details'] = $this->admin_model->list_planner_images($id , $user_id);
		//print_r($data['photographer_details']);die;
		$this->load->view('backend/admin_list_planner_images',$data);
	}
	
	 public function admin_list_planner_title_images()
	{
		$data['title']="Planner Images Listings";
		$user_id = $_GET['id'];
		$data['photographer_details'] = $this->admin_model->list_planner_title_images($user_id);
		//print_r($data['photographer_details']);die;
		$this->load->view('backend/admin_list_planner_title_images',$data);
	}
	
	 public function list_planner_images()
	{
		$data['title']="Images Listings";
		$user_id = $this->session->userdata('id');
		if($this->session->userdata('role')=="venue"){
			$data['venue_id']=$this->uri->segment(3);	
			$data['album_details'] = $this->admin_model->get_data('ww_portfolio',array('sp_id'=>$user_id,'venue_id'=>$data['venue_id']));	
		}
		else {
			$data['album_details'] = $this->admin_model->get_data('ww_portfolio',array('sp_id'=>$user_id));		
		}
		$data['photographer_details'] = $this->admin_model->list_planner_title_images($user_id);
		$this->load->view('backend/list_planner_images',$data);
	}
	
	 public function remove_images()
	{   
		$id = $_GET['id'];
		$url = $_GET['url'];
		
		$record_id = $this->admin_model->get_last_planner_record($id);
		$rec_id = $this->admin_model->delete_last_planner_record($record_id->id);
		if(!empty($rec_id)){
			$this->session->set_flashdata("message","Uploaded photographs removed successfully.");
		}
		$user_id = $this->session->userdata('id');
		$images = array();
		$images = explode(',',$record_id->images);
		//print_r($images);
		//print_r($record_id);die("/");
		
		for($i=0;$i<sizeof($images);$i++){
			$x = $url.'assets/uploads/photography/'.$user_id."/".$images[$i];
			unlink($url.'assets/uploads/photography/'.$user_id."/".$images[$i]);
			echo $x;
		}
		redirect("planner/image_upload_planner");
	}
	
		public function image_upload_planner()
	{
		$data['title']="Planner Multiple Image Upload";
		define ("MAX_SIZE","9000"); // 2MB MAX file size
		function getExtension($str)
		{
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
		}
		// Valid image formats 
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
		if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
		{
	    $hidden = $_POST['hidden'];
		$image = array();
		$photography = "photography";
		//$user_id = $this->session->userdata('id');
		$dirname = "assets/uploadplanner/".$photography."/".$user_id."/";
		
		if(is_dir($dirname)){
		$file_path = $dirname;
		}else{
		$file_path = mkdir($dirname,0777);
		$file_path = $dirname;
		}
		
		
	foreach ($_FILES['photos']['name'] as $name => $value)
		{
		$filename = stripslashes($_FILES['photos']['name'][$name]);
		
		$size=filesize($_FILES['photos']['tmp_name'][$name]);
		//Convert extension into a lower case format
		$ext = getExtension($filename);
		$ext = strtolower($ext);
		//File extension check
		if(in_array($ext,$valid_formats))
		{
		//File size check
		if ($size < (MAX_SIZE*1024))
		{
		$image_name=time().$filename;
		$image[] = $image_name;
		echo "<img src='".$hidden.$file_path.$image_name."' class='imgList'>";
		//echo $file_path;
		//echo $user_id;
		$newname=$file_path.$image_name;

		//Moving file to uploads folder
		if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname))
		{
		$time=time();
		//Insert upload image files names into user_uploads table
		//mysql_query("INSERT INTO user_uploads(image_name,user_id_fk,created) VALUES('$image_name','$session_id','$time')");
		}
		else
		{
		echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>'; }
		}

		else
		{
		echo '<span class="imgList">You have exceeded the size limit!</span>';
		}

		}

		else
		{
		echo '<span class="imgList">Unknown extension!</span>';
		}

		} //foreach end
		
		$image_multiple_name = implode(',',$image);
		$user_id = $this->session->userdata('id');
		$upload_multiple = array(
		'images' => $image_multiple_name,
		'date'   => date('Y-m-d H:i:s'),
		'user_id' => $user_id
		);
		 $this->admin_model->planner_tags($upload_multiple);
		 //echo 1;
        die;
		} 
		
      $this->load ->view('backend/planner_images',$data); 
	}
	
	public function image_uploadplanner()
	{    
	    $user_id = $this->session->userdata('id');;
		$upload_id = $this->admin_model->get_planner_tags($user_id);
		//echo $this->session->userdata('id');
		//print_r($upload_id->id);die('/');
		
		$title = $_POST['title'];
		$location = $_POST['location'];
		$type_arrangement_tags = implode(',',$_POST['arrangement_tags']);
		$event_tags = implode(',',$_POST['event_tags']);
		$add_multiple_images = array(
		'title' => $title,
		'location' => $location,
		'event_tags' => $event_tags,
		'type_of_arrangement_tags' => $type_arrangement_tags,
		);
		$this->admin_model->add_planner_tags($add_multiple_images , $upload_id->id);
		$this->session->set_flashdata('message',"Portfolio added successfully");
		redirect('planner/image_upload_planner');
		
	}
	
	public function planner_customer_proposal()
	{
		
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$customer_name = $this->input->post('customer_name');
		$email = $this->input->post('email');
		$contact_number = $this->input->post('contact_number');
		$message1 = $this->input->post('message');
		$img = $this->input->post('photos');
		$selimg = $this->input->post('selimg');
		$proposalarray = array(
		                       'proposal_id'		=>rand(),
							   'sp_id'      		=>$user_id,
							   'sp_name'    		=>$data['userinfo'][0]->user_name,
							   'sp_email'   		=>$data['userinfo'][0]->user_email,
							   'customer_name'    	=>$customer_name,
							   'customer_email'    	=>$email,
							   'contact_number'    	=>$contact_number,
							   'message'    	    =>$message1,
							   'image'    			=>json_encode($selimg),
							   'send_date'			=>strtotime(date('Y-m-d')),
							   'status'  			=>0
							  );
		$table="ww_customer_proposal";
		$response=$this->admin_model->send_proposal($table,$proposalarray);
		//die;
        /********* send mail customer_name *****************/
        if($response==true):


		$to = $email;
        $base_url=base_url();
		$subject = 'Customer Proposal Request';
		$message = '<html><body>';
		$message = '<div style="overflow-x:auto;">';
		$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr style='background: #eee;'><td><strong>Customer Name:</strong> </td><td>" . strip_tags($customer_name) . "</td></tr>";
		$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($email) . "</td></tr>";
		$message .= "<tr><td><strong>Contact Number:</strong> </td><td>" . strip_tags($contact_number) . "</td></tr>";
		$selimg = $this->input->post('selimg');
		if (($selimg) != '') {
		
			for($i=0;$i<count($selimg);$i++)
			{
				if($i%2==0)
				{
					//http://localhost/weddingwazir/assets/uploadplanner/photography/145890672312524354_967731819973040_3803816411973390791_n.jpg
					
					$message .= '<tr width="300px">';
					$message .= '<td width="150px"><img width="100px" height="50px" border-style="dotted" src="'.$base_url.'assets/uploadplanner/photography/'.$selimg[$i].'" alt="proposal image" /></td>';	
					
				}
				else
				{
					
					$message .= '<td width="150px"><img width="100px" height="50px" border-style="dotted" src="'.$base_url.'assets/uploadplanner/photography/'.$selimg[$i].'" alt="proposal image" /></td>';	
					$message .= '</tr>';
				}
				
			}
			
		}
		$curText = htmlentities($message1);           
		if (($curText) != '') {
			$message .= "<tr><td><strong>Other Content:</strong> </td><td>" . $curText . "</td></tr>";
		}
		$message .= "</table>";
		$message .= "</div>";
		$message .= "</body></html>";
		

		
		$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		//$headers .= "CC: susan@example.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        
		if(mail($to, $subject, $message, $headers))
		{
			$data['sendvenuesucees']="Message is Successfully";
			
		}	
      	$data="Message is Successfully";
		$data = base64_encode($data);
		redirect('Photographer/list_proposal?set='.$data);
		endif;
				
       /********  end send mail *********************/		
		
	}
/****************Portfolio*****************/
	public function add_portfolio(){
		$data['title']="Portfolio";
		if($this->session->userdata('role')=='venue'){
			$data['venue_id']=$this->uri->segment(3);		
		}
		else {
			$data['venue_id']='';		
		}
		
		
		if($_POST){
			//echo"<pre>";
			//print_r($_POST);
			//die;
			$user=$this->session->userdata('id');
			
			$role=$this->session->userdata('role');
			$album_id='ALB'.rand().$role;
			 $album_title=$this->input->post('title');
			 $location=$this->input->post('location');
			  $desc=$this->input->post('desc');	
			 $vid=$this->input->post('vid');
			  $data=array('album_id'=>$album_id,
							  'sp_id'=>$user,
							  'album_title'=>$album_title,
							  'location'=>$location,
							  'description'=>$desc,
							  'tags'=>$this->input->post('tags'),	
							  'status'=>1,
							  'venue_id'=>$vid		  
			  );
			  $table='ww_portfolio';
			  $this->admin_model->add_data($data,$table);
			  $this->upload_portfolio($album_id);
			
		
		}
		else {
		$this->load ->view('backend/portfolio',$data);
		}
			
	}
	public function upload_portfolio($album_id){
		$data['title']="Portfolio";
		$res=$this->admin_model->get_data('ww_portfolio',array('album_id'=>$album_id));
		$data['vid']=$res[0]->venue_id;		
		$data['album_id']=$album_id;
		$this->load ->view('backend/upload_portfolio',$data);
			
	}
	public function upload(){
			
		  $album_id=$_POST['album'];
		// $user=$this->session->userdata('id');
		 //print_r($_FILES);
			//mkdir('./uploads/'. $album_id );
			if(!empty($_FILES['file']['name'][0])){
				               /** $name_array = array();
                $count = count($_FILES['file']['size']);
                foreach($_FILES as $key=>$value){
                for($s=0; $s<=$count-1; $s++) {
                $_FILES['file']['name']=$value['name'][$s];
                $_FILES['file']['type']    = $value['type'][$s];
                $_FILES['file']['tmp_name'] = $value['tmp_name'][$s];
                $_FILES['file']['error']       = $value['error'][$s];
                $_FILES['file']['size']    = $value['size'][$s]; 
	}}**/
					//	print_r($_FILES);
						
						   
							$config['upload_path'] = './uploads';
							$config['allowed_types'] = 'gif|jpg|png';
							$config['max_size']	= '1000000';
							/*$config['max_width']  = '10240000';
							$config['max_height']  = '7680000';*/
							$config['file_name']="prof_".rand(1,5000);
							
							
								$this->load->library('upload');
								
								$this->upload->initialize($config);
								
								if ( ! $this->upload->do_upload('file'))
									{
									$error = $this->upload->display_errors();
							//echo "here";
										$this->session->set_flashdata('ppic_error',$error);
						
										
										redirect(admin_url().'admin/add_question');
									}
									else
									{
										$data = $this->upload->data();
										$qimg=$data['file_name'];
							
									}
						
						}
						$image_id='IMG'.rand();
						$cdate=strtotime(date('Y-m-d'));
						
						$update_data=array('image_id'=>$image_id,
												 'image_name'=>$qimg,
												 'album_id'=>$album_id,
												 'sp_id'=>$this->session->userdata('id'),
												 'created_date'=>$cdate,
												 'status'=>1				
						);
						$table='ww_portfolio_images';
						 $this->admin_model->add_data($update_data,$table);
						 $this->session->set_flashdata(array('message'=>'Portfolio added'));
						// $this->add_portfolio();
						 
						
echo json_encode(1);
			
	
}
 public function delete_pic()
			{
		               $id=$_POST['id'];
		              
		                $img=$_POST['name'];
	                  $this->db->where('image_id',$id);
						$da=	$this->db->delete('ww_portfolio_images'); 
							unlink('./uploads/'.$img);
							if($da==true){
								echo 1;
								die;							
							}
							else {
							echo 0;
							die;							
							}
							$this->session->set_flashdata(array('message'=>'Image Deleted'));
							
							
							//redirect('planner/list_planner_images');
		
      	}
/***************Portfolio modal*****************/
public function select_album(){
							$id=$_POST['id'];
								$res=$this->admin_model->get_data('ww_portfolio_images',array('album_id'=>$id));
			if(!empty($res)){			
			foreach($res as $pic){				
			$alb = '<div style="margin-left:0px !improtant;"  class="span2">';
			$alb.='<div class="item">';
			$alb.='<a class="fancybox-button" data-rel="fancybox-button" title="Metronic Tablet Preview" href="'.base_url().'uploads/'.$pic->image_name.'">';
			$alb.='<div class="zoom">';
			$alb.='<img style="height:120px;width:300px;" src="'.base_url().'uploads/'.$pic->image_name.'" alt="Photo" />';							
			$alb.='<div class="zoom-icon"></div></div></a>';
			$alb.='<div class="details">';
			$alb.='<a href="javascript:void(0)"  class="save1" rel="'.$pic->image_id.'" id="'.$pic->image_name.'" title="Delete"><i class="icon-remove"></i></a>';		
			//$alb.='<a href="#" onClick="var a=confirm("Are you sure to Active this!");if(a){ deleted('.$pic->image_id.');}else{return false;}" title="active" class="icon"><i class="icon-remove"></i></a>';
			$alb.='</div></div></div>'; 
		echo $alb;
			}			
		}


}
public function edit_portfolio(){
		$data['title']="Edit Portfolio";
		$album_id=$this->uri->segment(3);
		if($_POST){
			//echo"<pre>";
			//print_r($_POST);
			//die;
			
			 $album_title=$this->input->post('title');
			 $location=$this->input->post('location');
			  $desc=$this->input->post('desc');	
			 $vid=$this->input->post('vid');
			  $data=array(
							  'album_title'=>$album_title,
							  'location'=>$location,
							  'description'=>$desc,
							  'tags'=>$this->input->post('tags'),	
							  
			  );
			  $where=array('album_id'=>$album_id);
			  $table='ww_portfolio';
			  $this->admin_model->update_data($table,$data,$where);
			  $this->upload_portfolio($album_id);
			
		
		}
		else {
			$data['details']=$this->admin_model->get_data('ww_portfolio',array('album_id'=>$album_id));
		$this->load ->view('backend/portfolio',$data);
		}
			
	}

}
