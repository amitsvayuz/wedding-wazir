<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
     + Class name Calendar  Controller
     + Open Event Managenet Function create_event();
     + Open Event Managenet Function add_slot();
	 + Open Event Managenet Function update_slot();
     + Open Event Managenet Function show calendar();
	 + Open Event Managenet Function add_schedule();
	 + Open Event Managenet Function check_date();
	 + Open Event Managenet Function check_daterange();
    */


class Calender extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('calender_model');
		error_reporting(1);

		if(!$this->session->userdata('id')){
			redirect('registration');
		}
	}




	public function index()
	{

	    $data['title']="Calendar";
		$data['two_slots'] = $this->admin_model->select_two_slots();
		$data['three_slots'] = $this->admin_model->select_three_slots();
		$data['venues_name'] = $this->admin_model->select_venues($this->session->userdata('id'));
		$data['numslot1']=$this->calender_model->row_slot();
		$data['numslot2']=$this->calender_model->row_schedule();
		$data['numslot3']=$this->calender_model->row_special();
		$data['numslot4']=$this->calender_model->row_food();
		$this->load->view('backend/calendar',$data);

	}




    public function create_event()
    {

		 $id=$_GET['id'];
		 if(isset($id)):
		 $data['title'] = "Update Event";
		 else:
		 $data['title'] = "Events";
		 endif;
		 $data['slotresult']=$this->calender_model->get_slot($id);
		 $data['numslot1']=$this->calender_model->row_slot();

		 if($this->session->userdata('role')=='venue'){
			$data['venue']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$this->session->userdata('id')));
		}
         $this->load->view('backend/add_events',$data);

    }

	public function add_slot()
	{
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$slotLevel=$this->input->post("slotLevel");
		if($slotLevel == 'date'):
		$this->form_validation->set_rules('date','Date','required');
		else:
		$this->form_validation->set_rules('startdate','Start Date','required');
		$this->form_validation->set_rules('enddate','End Date','required');
		endif;
		$this->form_validation->set_rules('slotTitle','Event Title','required');

		if ($this->form_validation->run() == false)
		{
		 $data['title'] = "Events";
		 $data['numslot']=$this->calender_model->row_slot();
		 $this->load->view('backend/add_events',$data);
		}
		else
		{


				if($slotLevel == 'date'):
				$date =$this->input->post("date");
				$new = new DateTime($date);
				$start=$new->format("Y-m-d");
				$end =$new->format("Y-m-d");
				$ftime=$this->input->post("fromTime");
				$ttime=$this->input->post("toTime");

				$resp=$this->check_date($start,$ftime,$ttime);

				if($resp > 0)
				{
					$data['title'] = "Events";
					$data['resp']='This date and time always booked';
					$data['numslot']=$this->calender_model->row_slot();
					$this->load->view('backend/add_events',$data);
				}
                else
                {
				    $arrSlot =array(
							     'sp_id'                => $user_id,
								 'slotLevel' 		=> $this->input->post("slotLevel"),
								 'fromTime'  		=> $this->input->post("fromTime"),
								 'toTime'    		=> $this->input->post("toTime"),
								 'start'     		=> $start,
								 'end'       		=> $end,
								 'location'  		=> $this->input->post("location"),
								 'desc'      		=> $this->input->post("desc"),
								 'backgroundColor'  		=> $this->input->post("eventcolor"),
								 'type'  			=> 'EVT',

								 'reminder'  		=> $this->input->post("reminder"),

								 'booking'   		=> $this->input->post("booking"),
								 'title'     		=> $this->input->post("slotTitle"),
								 'calendar_user'  => strip_tags($data['userinfo'][0]->user_name),
					          'created_by'  	=> strip_tags($data['userinfo'][0]->user_email),
					          'venue'				=>$this->input->post("venue")
								);

					$tableSlot="ww_cal_slot";

					$this->calender_model->insert_slot($tableSlot,$arrSlot);

				}
				endif;
				if($slotLevel == 'month'):
				$startdate = $this->input->post("startdate");
				$enddate = $this->input->post("enddate");
				$startd=$startdate;
				$endd =$enddate;
				$ftime=$this->input->post("fromTime");
				$ttime=$this->input->post("toTime");

				$resp=$this->check_daterange($startd,$endd,$ftime,$ttime);
				if($resp > 0):
					$data['title'] = "Events";
					$data['resp']='This date range allready booked';
					$data['numslot']=$this->calender_model->row_slot();
					$this->load->view('backend/add_events',$data);
                else:

					echo $begin = new DateTime($startd);
					echo $end = new DateTime($endd);

					$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);

					foreach($daterange as $daterow):

					$arrSlot =array(
							     'sp_id'                => $user_id,
								 'slotLevel' 		=> $this->input->post("slotLevel"),
								 'fromTime'  		=> $this->input->post("fromTime"),
								 'toTime'    		=> $this->input->post("toTime"),
								 'start'     		=> $daterow->format("Y-m-d"),
								 'end'       		=> $daterow->format("Y-m-d"),
								 'location'  		=> $this->input->post("location"),
								 'desc'      		=> $this->input->post("desc"),
								 'backgroundColor'  => $this->input->post("eventcolor"),
								 'type'  			=> 'EVT',

								 'reminder'  		=> $this->input->post("reminder"),

								 'booking'   		=> $this->input->post("booking"),
								 'title'     		=> $this->input->post("slotTitle"),
								 'calendar_user'  => strip_tags($data['userinfo'][0]->user_name),
					          'created_by'   	=> strip_tags($data['userinfo'][0]->user_email),
								 'venue'				=>$this->input->post("venue")
								);

					$tableSlot="ww_cal_slot2";
					$this->db->insert($tableSlot,$arrSlot);

					endforeach;
					$data = "Successful add slot";
					$data = base64_encode($data);
					redirect('calender/create_event?set='.$data);
				endif;
				endif;


		}


	}


	public function update_slot()
	{
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$id=$this->input->post("id");
		$slotLevel=$this->input->post("slotLevel");
		if($slotLevel == 'date'):
		$this->form_validation->set_rules('date','Date','required');
		else:
		$this->form_validation->set_rules('startdate','Start Date','required');
		$this->form_validation->set_rules('enddate','End Date','required');
		endif;
		$this->form_validation->set_rules('slotTitle','Slot Title','required');

		if ($this->form_validation->run() == false)
		{
		 $data['title'] = "Events";
		 $data['numslot']=$this->calender_model->row_slot();
		 $this->load->view('backend/add_events',$data);
		}
		else
		{

			    $date =$this->input->post("date");
				$startdate = $this->input->post("startdate");
				$enddate = $this->input->post("enddate");

				if($date):
				$begin = new DateTime($date);
				$start=$begin->format("Y-m-d");
				$end =$begin->format("Y-m-d");
				$ftime=$this->input->post("fromTime");
				$ttime=$this->input->post("toTime");


				    $arrSlot =array(
							     'sp_id'                => $user_id,
								 'slotLevel' => $this->input->post("slotLevel"),
								 'fromTime'  => $this->input->post("fromTime"),
								 'toTime'    => $this->input->post("toTime"),
								 'start'     => $start,
								 'end'       => $end,
								 'booking'   => $this->input->post("booking"),
								 'title'     => $this->input->post("slotTitle")
								);

					$tableSlot="ww_cal_slot";

					$this->calender_model->update_slot($tableSlot,$arrSlot,$id);


				endif;
				if($startdate):
				$start=$startdate;
				$end =$enddate;
				$ftime=$this->input->post("fromTime");
				$ttime=$this->input->post("toTime");


					$begin = new DateTime($start);
					$end = new DateTime($end);

					$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);

					foreach($daterange as $date):

					$arrSlot =array(
							     'sp_id'                => $user_id,
								 'slotLevel' => $this->input->post("slotLevel"),
								 'fromTime'  => $this->input->post("fromTime"),
								 'toTime'    => $this->input->post("toTime"),
								 'start'     => $date->format("Y-m-d"),
								 'end'       => $date->format("Y-m-d"),
								 'booking'   => $this->input->post("booking"),
								 'title'     => $this->input->post("slotTitle")

								);

					$tableSlot="ww_cal_slot";
					$this->db->where('id',$id);
					$this->db->update($tableSlot,$arrSlot);
					endforeach;
					$data = "Successful update slot";
					$data = base64_encode($data);
					redirect('calender/create_event?set='.$data);

				endif;


		}


	}
	public function create_schedule()
    {

		 $id=$this->uri->segment(3);
		 if(isset($id)):
		 $data['title'] = "Update Schedule";
		 else:
		 $data['title'] = "Schedule";
		 endif;
		 $data['slotresult']=$this->calender_model->get_schedule($id);
		 $data['numslot2']=$this->calender_model->row_schedule();
		  if($this->session->userdata('role')=='venue'){
			$data['venue']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$this->session->userdata('id')));
			}
         $this->load->view('backend/add_schedule',$data);

    }

	public function add_schedule()
	{
		$id=$this->uri->segment(3);
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$this->form_validation->set_rules('schedule','Title','required');
		$this->form_validation->set_rules('date','Date','required');
		$this->form_validation->set_rules('fromTime','From Time','required');
		$this->form_validation->set_rules('toTime','To Time','required');
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('gemail','Email','required');
		$this->form_validation->set_rules('desc','Description','required');

		if ($this->form_validation->run() == false)
		{
		 $id=$this->uri->segment(3);
		 $data['title'] = "Schedule";
		 $data['slotresult']=$this->calender_model->get_schedule($id);
		 $data['numslot']=$this->calender_model->row_schedule();
		 $this->load->view('backend/add_schedule',$data);
		}
		else
		{
				$date =$this->input->post("date");
				$new = new DateTime($date);
				$start=$new->format("Y-m-d");
				$ftime = $this->input->post("fromTime");
				$ttime   = $this->input->post("toTime");
				$resp=$this->check_date($start,$ftime,$ttime);

				if($resp > 0)
				{
					$data['title'] = "Schedule";
					$data['resp']='This time always booked';
					$id=$this->uri->segment(3);

		            $data['slotresult']=$this->calender_model->get_schedule($id);
					$data['numslot']=$this->calender_model->row_schedule();
					$this->load->view('backend/add_schedule',$data);
				}
				else
				{
					$arrSchedule =array(

		             'sp_id'                => $user_id,
		             'title'    			=> $this->input->post("schedule"),
                     'name'    				=> $this->input->post("name"),
                     'gemail'  				=> $this->input->post("gemail"),
					 'fromTime'  			=> $ftime,
					 'toTime'    			=> $ttime,
					 'type'    				=> 'SCD',
					 'start'     			=> $start,
					 'desc'      			=> $this->input->post("desc"),
					 'calendar_user'     => $this->input->post("cal_name"),
					 'venue'     			=> $this->input->post("venue"),
					 'calendar_user'     => strip_tags($data['userinfo'][0]->user_name),
					 'created_by'      	=> strip_tags($data['userinfo'][0]->user_email)

                    );

					$tableSlot="ww_cal_slot";

					$this->calender_model->insert_schedule($tableSlot,$arrSchedule);



						$to = $this->input->post("gemail");
						$base_url=base_url();
						$subject = 'Meeting confirmation from '.strip_tags($data['userinfo'][0]->user_name);
						$message = '<html><body>';
						$message = '<div style="overflow-x:auto;">';
						$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
						$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';

						$curText = htmlentities($this->input->post("desc"));
						if (($curText) != '') {
							$message .= "<tr><td><strong>Other Content:</strong> </td><td>" . $curText . "</td></tr>";
						}
						$message .= "</table>";
						$message .= "</div>";
						$message .= "</body></html>";

						$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
						$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
						$headers .= "CC: susan@example.com\r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


						if(mail($to, $subject, $message, $headers))
						{
							$data['sendvenuesucees']="Message is Successfully";

						}

				}
		}
	}

	public function update_schedule()
	{
		$id=$this->input->post("id");
		$date =$this->input->post("date");
				$new = new DateTime($date);
				$start=$new->format("Y-m-d");
				$ftime = $this->input->post("fromTime");
				$ttime   = $this->input->post("toTime");
				$resp=$this->check_date($start,$ftime,$ttime);

					$arrSchedule =array(

		             'sp_id'                => $user_id,
		             'title'    			=> $this->input->post("schedule"),
                     'name'    				=> $this->input->post("name"),
                     'gemail'  				=> $this->input->post("gemail"),
					 'fromTime'  			=> $ftime,
					 'toTime'    			=> $ttime,
					 'type'    				=> 'SCD',
					 'start'     			=> $start,
					 'desc'      			=> $this->input->post("desc"),
					 'calendar_user'     	=> $this->input->post("cal_name"),
					 'calendar_user'     	=> strip_tags($data['userinfo'][0]->user_name),
					 'created_by'      		=> strip_tags($data['userinfo'][0]->user_email),
					 'venue'     	=> $this->input->post("venue")
                    );

					$tableSlot="ww_cal_slot";

					$this->calender_model->update_schedule($tableSlot,$arrSchedule,$id);


	}

	public function delete_schedule()
	{
		$id=$this->uri->segment(3);
		$tableSlot="ww_cal_slot";
		$this->calender_model->delete_schedule($tableSlot,$id);
	}

	public function create_special()
    {

		 $id=$this->uri->segment(3);
		 if(isset($id)):
		 $data['title'] = "Update Special Day";
		 else:
		 $data['title'] = "Special Day";
		 endif;
		 $data['slotresult']=$this->calender_model->get_special($id);
		 $data['numslot3']=$this->calender_model->row_special();
         $this->load->view('backend/add_special',$data);

    }

	public function add_special()
	{
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$this->form_validation->set_rules('special','Title','required');
		$this->form_validation->set_rules('desc','Description','required');

		if ($this->form_validation->run() == false)
		{
		 $data['title'] = "Special";
		 $data['numslot']=$this->calender_model->row_special();
		 $this->load->view('backend/add_special',$data);
		}
		else
		{
				$date =$this->input->post("date");
				$new = new DateTime($date);
				$start=$new->format("Y-m-d");

					$arrSchedule =array(

		             'sp_id'                => $user_id,
		             'title'    			=> $this->input->post("special"),
					 'type'    				=> 'SPD',
					 'start'     			=> $start,
					 'desc'      			=> $this->input->post("desc"),
					 'calendar_user'     	=> strip_tags($data['userinfo'][0]->user_name),
					 'created_by'      		=> strip_tags($data['userinfo'][0]->user_email)

                    );

					$tableSlot="ww_cal_slot";

					$this->calender_model->insert_special($tableSlot,$arrSchedule);

		}
	}

	public function update_special()
	{
		$id = $this->input->post("id");
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$this->form_validation->set_rules('special','Title','required');
		$this->form_validation->set_rules('desc','Description','required');

		if ($this->form_validation->run() == false)
		{
		 $data['title'] = "Special";
		 $data['numslot']=$this->calender_model->row_special();
		 $this->load->view('backend/add_special',$data);
		}
		else
		{
				$date =$this->input->post("date");
				$new = new DateTime($date);
				$start=$new->format("Y-m-d");

					$arrSchedule =array(

		             'sp_id'                => $user_id,
		             'title'    			=> $this->input->post("special"),
					 'type'    				=> 'SPD',
					 'start'     			=> $start,
					 'desc'      			=> $this->input->post("desc"),
					 'calendar_user'     	=> strip_tags($data['userinfo'][0]->user_name),
					 'created_by'      		=> strip_tags($data['userinfo'][0]->user_email)

                    );

					$tableSlot="ww_cal_slot";

					$this->calender_model->update_special($tableSlot,$arrSchedule,$id);

		}
	}

	public function delete_special()
	{
		$id = $this->uri->segment(3);
		$tableSlot="ww_cal_slot";
		$this->calender_model->delete_special($tableSlot,$id);
	}

	public function create_food()
    {

		 $id=$this->uri->segment(3);
		 if(isset($id)):
		 $data['title'] = "Update Food Invite Day";
		 else:
		 $data['title'] = "Food Invite Day";
		 endif;
		 $data['slotresult']=$this->calender_model->get_goodById($id);
		  if($this->session->userdata('role')=='venue'){
			$data['venue']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$this->session->userdata('id')));
		}


		 $data['numslot4']=$this->calender_model->row_food();
         $this->load->view('backend/add_food_invite',$data);

    }

	public function add_food()
	{
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('gemail','Email','required');


		if ($this->form_validation->run() == false)
		{
		 $data['title'] = "Food Invite Day";
		 $data['numslot']=$this->calender_model->row_food();
		 $this->load->view('backend/add_food_invite',$data);
		}
		else
		{
				$date =$this->input->post("date");
				$new = new DateTime($date);
				$start=$new->format("Y-m-d");

					$arrSchedule =array(

		             'sp_id'                => $user_id,
		             'title'    			=> $this->input->post("name"),
					 'type'    				=> 'FOD',
					 'start'     			=> $start,
					 'gemail'      			=> $this->input->post("gemail"),
					 'desc'      			=> $this->input->post("desc"),
					'calendar_user'     	=> strip_tags($data['userinfo'][0]->user_name),
					'created_by'      		=> strip_tags($data['userinfo'][0]->user_email),
					 'venue'     	=> $this->input->post("venue")

                    );

					$tableSlot="ww_cal_slot";

					$this->calender_model->insert_food($tableSlot,$arrSchedule);

						$data['userinfo'] = $this->admin_model->user($user_id);
						$to = $this->input->post("gemail");
						$base_url=base_url();
						$subject = 'Food invite from '.strip_tags($data['userinfo'][0]->user_name);
						$message = '<html><body>';
						$message = '<div style="overflow-x:auto;">';
						$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
						$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';

						$curText = htmlentities($this->input->post("desc"));
						if (($curText) != '') {
							$message .= "<tr><td><strong>Other Content:</strong> </td><td>" . $curText . "</td></tr>";
						}
						$message .= "</table>";
						$message .= "</div>";
						$message .= "</body></html>";

						$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
						$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
						$headers .= "CC: susan@example.com\r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


						if(mail($to, $subject, $message, $headers))
						{
							$data['sendvenuesucees']="Message is Successfully";

						}


		}
	}


	public function update_food()
	{
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$id = $this->input->post("id");
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('gemail','Email','required');


		if ($this->form_validation->run() == false)
		{
		 $data['title'] = "Food Invite Day";
		 $data['numslot']=$this->calender_model->row_food();
		 $this->load->view('backend/add_food_invite',$data);
		}
		else
		{
				$date =$this->input->post("date");
				$new = new DateTime($date);
				$start=$new->format("Y-m-d");

					$arrSchedule =array(

		             'sp_id'                => $user_id,
		             'title'    			=> $this->input->post("name"),
					 'type'    				=> 'FOD',
					 'start'     			=> $start,
					 'gemail'      			=> $this->input->post("gemail"),
					 'desc'      			=> $this->input->post("desc"),
					 'calendar_user'     	=> strip_tags($data['userinfo'][0]->user_name),
					 'created_by'      		=> strip_tags($data['userinfo'][0]->user_email),
					  'venue'     	=> $this->input->post("venue")

                    );

					$tableSlot="ww_cal_slot";

					$this->calender_model->update_food($tableSlot,$arrSchedule,$id);

		}
	}


	public function delete_food()
	{
		$id = $this->uri->segment(3);
		$tableSlot="ww_cal_slot";
		$this->calender_model->delete_food($tableSlot,$id);
	}


	public function check_date($start,$ftime,$ttime)
	{

		$tableSlot2="ww_cal_slot";
	    $checkback = $this->calender_model->check_date($tableSlot2,$start,$ftime,$ttime);
		return $checkback;

	}


	public function check_daterange($startd,$endd,$ftime,$ttime)
	{

		$tableSlot1="ww_cal_slot";
	    $res1=$this->calender_model->check_daterange($tableSlot1,$startd,$endd,$ftime,$ttime);
		return $res1;

	}


	function date_range($first, $end, $step = '+1 day', $output_format = 'd/m/Y' )
	{

						$dates = array();
						$current = strtotime($first);
						$last = strtotime($last);

						while( $current <= $last ) {

							$dates[] = date($output_format, $current);
							$current = strtotime($step, $current);
						}

						return $dates;
	}

	public function getYear($pdate) {
        $date = DateTime::createFromFormat("Y-m-d", $pdate);
        return $date->format("Y");
    }

    public function getMonth($pdate) {
        $date = DateTime::createFromFormat("Y-m-d", $pdate);
        return $date->format("m");
    }

    public function getDay($pdate) {
        $date = DateTime::createFromFormat("Y-m-d", $pdate);
        return $date->format("d");
    }
}
