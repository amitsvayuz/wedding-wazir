<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General_setting extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Setting_model');
		$this->load->library('form_validation');
		$this->load->library('googlemaps');
		error_reporting(0);
		
	}
	public function index()
	{
		$data['result']=$this->Setting_model->get_city();
		$this->load->view('backend/add_company',$data);
	}
/****************General_setting****************/	
	public function setting_company()
	{
      $data['title']="Setting Company";
	  $data['result']=$this->Setting_model->get_city();
      $this->load->view('backend/add_company',$data);
	}
	public function add_city()
	{
		
		//$this->form_validation->set_rules('city_name', 'Brand name', 'required');
		$this->form_validation->set_rules('company_name', 'City Name', 'required');
		$this->form_validation->set_rules('company_desc', 'City Description', 'required');

		if ($this->form_validation->run() == FALSE)
		{  
	        $data['title']="Add City";
		$data['result']=$this->Setting_model->get_city();
                $data['resultCity']=$this->Setting_model->fetch_city();
		$this->load->view('backend/add_company',$data);
		}
		else
		{
			$companyarray = array(
		             'c_id'=>rand(),
					 'city_id'=>$this->input->post('city_name'),
					 'company_name'=>$this->input->post('company_name'),
					 'company_addr'=>$this->input->post('company_desc')
		             );
		    $tablename ='ww_company';
		
			$this->Setting_model->add_company($tablename,$companyarray);
			
		}
	}
	public function list_city()
	{
		$data['title']=" City";
		
		$data['resultCity']=$this->Setting_model->get_city();
                $data['resultFetch']=$this->Setting_model->fetch_city();
                $data['result']=$this->Setting_model->get_company();
		$this->load->view('backend/list_company',$data);
	}
	
	public function edit_city()
	{
		$data['title']=" Edit City";
		$id = $this->uri->segment(3);
		$data['resultCompanyId']=$this->Setting_model->get_companyById($id);
                $data['resultCity']=$this->Setting_model->fetch_city();
		$data['result']=$this->Setting_model->get_city();
		$this->load->view('backend/add_company',$data);
	}
	
	public function update_city()
	{
		$id = $this->uri->segment(3);
		$companyarray = array(
					 'city_id'=>$this->input->post('city_name'),
					 'company_name'=>$this->input->post('company_name'),
					 'company_addr'=>$this->input->post('company_desc')
		             );
		    $tablename ='ww_company';
		
			$this->Setting_model->update_company($tablename,$companyarray,$id);
	}
	
	public function delete_city()
	{
		$id = $this->uri->segment(3);
		$this->Setting_model->delete_company($id);
	}
	
	//  start brand
	
	public function list_brand()
	{
		$data['title']="Brand";
		$data['result']=$this->Setting_model->get_city();
		$data['resultPro']=$this->Setting_model->get_property();
		$this->load->view('backend/brand_setting',$data);
	}
	
	
	public function add_brand()
	{
		$data['title']="Add Brand";
		
		$data['resultPro']=$this->Setting_model->get_property();
		$this->load->view('backend/add_brand',$data);
	}
	
	
	public function edit_brand()
	{
		
		$data['title']="Update Brand";
                
		$data['resultPro']=$this->Setting_model->get_property();
		$data['result']=$this->Setting_model->get_cityById($id);
		$this->load->view('backend/add_brand',$data);
	}
	
	
	public function adding_Brand()
	{
		$this->form_validation->set_rules('cityName', 'Brand name', 'required');
		$this->form_validation->set_rules('cityDesc', 'Brand Description', 'required');
		if ($this->form_validation->run() == FALSE)
		{  
	        $data['title']="Add Brand";
            
			$data['resultPro']=$this->Setting_model->get_property();
			$this->load->view('backend/add_brand',$data);
		}
		else
		{
			$cityarray = array(
			         'proName'=>$this->input->post('proName'),
					 'name'=>$this->input->post('cityName'),
					 'description'=>$this->input->post('cityDesc')
		             );
		    $tablename ='city_setting';
		
			$this->Setting_model->add_brand($tablename,$cityarray);
			
		}
		
	}
		public function brand_update()
		{
			$id = $this->uri->segment(3);
			$cityarray = array(
			         'proName'=>$this->input->post('proName'),
					 'name'=>$this->input->post('cityName'),
					 'description'=>$this->input->post('cityDesc')
		             );
		    $tablename ="city_setting";
		
			$this->Setting_model->brand_update($tablename,$cityarray,$id);
			
		}
		
		public function delete_brand()
		{
			$id = $this->uri->segment(3);
			$this->Setting_model->delete_brand($id);
			
		}

      /*****Fun ctions by Vipin Yadav******////////
	

public function add_belt()
	{
		$data['title']="Add Belt";
		if(!empty($_POST)){
		$beltData = array(
		'beltName' => $this->input->post('belt_name'),
		'beltDesc'  => $this->input->post('belt_desc')
		);
		$this->Setting_model->addBelt($beltData);	
		//$this->session->set_flashdata('message','Belt added successfully.');
		//redirect('General_setting/list_belt');
		}
		
		$this->load->view('backend/add_belt',$data);
	}
	
	public function list_belt()
	{
		$data['title']="List Belt";
		$data['result']=$this->Setting_model->get_venue_belt();
       	$this->load->view('backend/list_belt',$data);
	}
	
	public function delete_belt()
	{
		$id = $this->uri->segment(3);
		$this->Setting_model->delete_belt($id);
		$this->session->set_flashdata('message','Belt Deleted successfully.');
		redirect('General_setting/list_belt');
	}
	
	public function edit_belt()
	{
		$id = $this->uri->segment(3);
		$data['title']="Update Belt";
		if(!empty($_POST)){
			$id = $this->input->post('hidden_id');
			$belt_Data = array(
			'beltName' => $this->input->post('belt_name'),
			'beltDesc' => $this->input->post('belt_desc')
			);
		$this->Setting_model->updateBelt($belt_Data,$id);	
		$this->session->set_flashdata('message','Belt Updated Successfully.');
		redirect('General_setting/list_belt');
		}
		$data['result']=$this->Setting_model->get_beltById($id);
		$this->load->view('backend/edit_belt',$data);
	}
	
	/********list location**********/
	
	public function location()
	{
		$data['title']="List Location";
		
		//print_r($data);die;
		$data['resultCom']=$this->Setting_model->get_company();
		$data['result']=$this->Setting_model->get_location();
       	$this->load->view('backend/list_location',$data);
	}
	/********add view location**********/
	
	public function add_view()
	{
		$data['title']="add Location";
		 $config['center'] = '37.4419, -122.1419';
		    $config['zoom'] = 'auto';
		    $config['places'] = TRUE;
		    $config['placesAutocompleteInputID'] = 'myPlaceTextBox';
		    $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
		    $config['placesAutocompleteOnChange'] = 'alert(\'You selected a place\');';
		    $this->googlemaps->initialize($config);
		    $data['map'] = $this->googlemaps->create_map();
			$id=$this->uri->segment(3);
		  $data['resultLoc']=$this->Setting_model->getById_location($id);
		  $data['resultCity']=$this->Setting_model->get_company();
		//$data['result']=$this->Setting_model->get_location();
       	$this->load->view('backend/add_location',$data);
	}
	/********add location**********/
	public function add_location()
	{
		$this->form_validation->set_rules('locName', 'Name', 'required');
		$this->form_validation->set_rules('locCity', 'City', 'required');
		$this->form_validation->set_rules('locDesc', 'Description', 'required');
		if ($this->form_validation->run() == FALSE)
		{  
	        $data['title']="Add Brand";
			$config['center'] = '37.4419, -122.1419';
		    $config['zoom'] = 'auto';
		    $config['places'] = TRUE;
		    $config['placesAutocompleteInputID'] = 'myPlaceTextBox';
		    $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
		    $config['placesAutocompleteOnChange'] = 'alert(\'You selected a place\');';
		    $this->googlemaps->initialize($config);
		    $data['map'] = $this->googlemaps->create_map();
                        
			$data['resultCity']=$this->Setting_model->get_company();
			$this->load->view('backend/add_location',$data);
		}
		else
		{
			$locarray = array(
			         'loc_id'=>'LOC'.rand(),
			         'city_id'=>$this->input->post('locCity'),
					 'location'=>$this->input->post('locName'),
					 'loc_desc'=>$this->input->post('locDesc')
		             );
					
		    $tablename ='ww_location';
		
			$this->Setting_model->add_location($tablename,$locarray);
			
		}
	}
	/********update location**********/
	public function update_location()
	{
		  $id=$this->uri->segment(3);
		  $locarray = array(
			
			         'city_id'=>$this->input->post('locCity'),
					 'location'=>$this->input->post('locName'),
					 'loc_desc'=>$this->input->post('locDesc')
		             );
					
		    $tablename ='ww_location';
		
			$this->Setting_model->update_location($tablename,$locarray,$id);
	}
	
	/********delete location**********/
	public function delete_location()
	{
		    $id=$this->uri->segment(3);		
		    $tablename ='ww_location';
			$this->Setting_model->delete_location($tablename,$id);
	}
	
}

