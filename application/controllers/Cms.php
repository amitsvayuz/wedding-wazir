<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('cms_model');
        $this->load->model('settings_model');
		$this->load->library('form_validation');
	}
	public function index()
	{
		//$data['title']="CMS";
		//$this->load->view('backend/cms',$data);
	}

	
	public function cms_insert()
	{
		$page_title= $this->input->post('page_title');
		$page_heading= $this->input->post('page_heading');
		$page_desc1=$this->input->post('dsc1');
		
		$status=$this->input->post('page_status');
		$created_date=$this->input->post('page_date');
		$result = $this->cms_model->inser_cms($page_title,$page_heading,$page_desc1,$status,$created_date);
		 if($result=1){
			$data = "CMS add successful ";
			$data = base64_encode($data);
			redirect('Cms/cms1?set='.$data);
			}else{
			$data = "Something is wrong";
			$data = base64_encode($data);
			redirect('Cms/cms1?set='.$data);
	   }
	}

	public function cms_delete(){
		 $id=$_GET['id'];
		 $result=$this->cms_model->delete_row($id);
		 if($result=1){
			$data = "CMS deleted Successful ";
			$data = base64_encode($data);
			redirect('Cms/cms1?set='.$data);
		}else{
			$data = "Something is wrong";
			$data = base64_encode($data);
			redirect('Cms/cms1?set='.$data);
		}
	}

	public function cms_edit(){
		$id=$_GET['id'];
		$page_title= $this->input->post('page_title');
		$page_heading= $this->input->post('page_heading');
		$page_desc1=$this->input->post('page_desc1');
		$result = $this->cms_model->update_info($id,$page_title,$page_heading,$page_desc1);
		
		 if($result=1){
			$data = "Successful  updated";
			$data = base64_encode($data);
			redirect('Cms/cms1?set='.$data);
		   }else{
		   
				$data = "Something is wrong";
				$data = base64_encode($data);
				redirect('Cms/cms1?set='.$data);
		   }
	}

	public function cms1()
	{
		$data['title']="CMS";
		$data['result']=$this->cms_model->get_cms();
		$this->load->view('backend/cms_home',$data);
	}
	
	/****************cms view****************/
	public function cms_home()
	{
		$data['title']="CMS";
		$data['result']=$this->cms_model->get_cms();
		$this->load->view('backend/cms',$data);
	}
	
	/****************cms_view page****************/
	public function cms_view()
	{
		$id=$_GET['id'];
		$data['title']="CMS";
	    $data['result']=$this->cms_model->get_csm1($id);
		$this->load->view('backend/cms_view',$data);
	}
		/****************cms update info page****************/
		
    public function cms_edit1(){
		 $id=$_GET['id'];
		$data['title']="CMS_Edit";
		$data['result']=$this->cms_model->get_csm1($id);
		$this->load->view('backend/cms',$data);
	}
	
	/****************for settings page****************/
	public function settings(){
		$data['title']="SETTINGS";
		$data['result1']=$this->settings_model->get_view(1);
		$data['result2']=$this->settings_model->get_view(2);
		$data['result3']=$this->settings_model->get_view(3);
		$data['result4']=$this->settings_model->get_view(4);
		$data['result5']=$this->settings_model->get_view(5);
		$data['result6']=$this->settings_model->get_view(6);
		$data['result7']=$this->settings_model->get_view(7);
		$data['result8']=$this->settings_model->get_view(8);
		$this->load->view('backend/settings_home',$data);
	}
}

