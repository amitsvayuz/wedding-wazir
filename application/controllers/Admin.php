<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		
		error_reporting(0);
		
		if(!$this->session->userdata('id')){
			redirect('registration');
		}
	}
	

       // default page load index() function 
	public function index()
	{
	    $data['title']="Sign In";
		$this->load->view('backend/login',$data);
	}
	
	
	
/****************Dashboard view function****************/	
	public function dashboard()
	{
		$data['title']="Dashboard";
		$this->load->view('backend/index',$data);
	}
	
	/****************Forgot password view function****************/	
	public function forgetpassword()
	{
		$data['title']="Forget Password";
		$this->load->view('backend/forgetpassword',$data);
	}
	
	
	
	/******Functions done by Manoj********/
        /****************All customer view function****************/	
	public function customer()
	{
		$data['title']="Customer ";
		$data['result']=$this->admin_model->select_all_customer();
		$this->load->view('backend/customerlisting',$data);
	}
	
        /****************Add customer function****************/
	public function add_customer()
	{
		$data['title']="Add Customer";
		$this->load->view('backend/add_customer',$data);
		
	}
	
       /****************Edit customer  function****************/
	public function edit_customer()
	{
      $id=$_GET['id'];	
       $customer_id=base64_decode($id);	

		$data['title']="Edit Customer";
		$data['result']=$this->admin_model->select_single_customer($customer_id);
		$this->load->view('backend/add_customer',$data);
		
	}
	/****************Add customer details function****************/
	public function add_customer_details()
	{
		 $idfr = $this->uri->segment(3);
		//$idfr=$_GET['id'];
		$name= $this->input->post('name');
		$email= $this->input->post('email');
		$contact= $this->input->post('contact');
		$pass= $this->input->post('password');
		$dob= $this->input->post('dob');
		$dateofbirth=strtotime($dob);
		$password=md5($pass);
		$created_date=date('d/m/y');
		$this->load->helper('string');
        $customer_id =random_string('alnum',6).'_Cust_'.random_string('alnum',4).'_'.substr($name,0,3);
		$detail=array(
					  'customer_id'=>$customer_id,
					  'customer_name'=>$name,
					  'customer_email'=>$email,
					  'customer_contact'=>$contact,
					  'password'=>$password,
					  'customer_dob'=>$dateofbirth,
					  'status'=>1,
					  'creation_date'=>strtotime($created_date),
					   );
	 
	  
	    $table="ww_customer";
		$result = $this->admin_model->add_data($detail,$table);		
		if($result=1){
		$data = "Successful Customer add";
		$data = base64_encode($data);
		redirect('Admin/customer?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/add_customer?set='.$data);
		}
	}
	
/****************Edit customer details function****************/
	public function edit_customer_details()
	{
		$idfr = $this->uri->segment(3);
		$id=$_GET['id'];
		$customer_id=base64_decode($id);
		$name= $this->input->post('name');
		$email= $this->input->post('email');
		$contact= $this->input->post('contact');
		$pass= $this->input->post('password');
		$dob= $this->input->post('dob');
		$dateofbirth=strtotime($dob);
		$password=md5($pass);
		
		$detail=array(
					  'customer_name'=>$name,
					  'customer_email'=>$email,
					  'customer_contact'=>$contact,
					  'password'=>$password,
					  'customer_dob'=>$dateofbirth
					   );
		$where=array('customer_id'=>$customer_id);
		$table="ww_customer";
		$result = $this->admin_model->update_data($table,$detail,$where);		
		if($result=1){
		$data = "Successful Customer Updated";
		$data = base64_encode($data);
		redirect('Admin/customer?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/edit_customer?set='.$data);
		}
	}
	
        /****************Deactive user function****************/
	 public function deactive_user()
	   {
		$id=$this->input->post('id');
		$deactive_customer=array('status'=>0);
		$result = $this->admin_model->update_user_status($deactive_customer , $id);
	   }  
	
	  /****************Active user function****************/
	public function active_user()
	{
		$id=$this->input->post('id');
		$deactive_customer=array('status'=>1);
		$result = $this->admin_model->update_user_status($deactive_customer , $id);
	}  
	
        /****************Deactive customer function****************/
	public function deactive_customer()
	{
		$id=$this->input->post('id');
		$deactive_customer=array('status'=>0);
		$this->db->where('customer_id',$id);
		$this->db->update('ww_customer',$deactive_customer); 
	}  
	
        /****************Active customer function****************/   
	public function active_customer()
	{
		$id=$this->input->post('id');
		$active_customer=array('status'=>1);
		$this->db->where('customer_id',$id);
		$this->db->update('ww_customer',$active_customer);  
	}
	
        /****************Delete customer function****************/	   
	public function delete_customer()
	{
		$id=base64_decode($_GET['del']);
		$this->db->where('customer_id',$id);
		$this->db->delete('ww_customer'); 
		$data="Customer Deleted Successfuly";
		$data=base64_encode($data);
		redirect('Admin/customer?set='.$data);
		
	}
	
        /********************Recharge History view***********************/	
		public function recharge_history()
	{
                $user_id = $this->session->userdata('id');
		$data['title']="Recharge History";
		$this->load->model('admin_model');
		$data['sp'] = $this->admin_model->select_all_sp();
		$data['result'] = $this->admin_model->select_recharge_history($user_id);
		$this->load->view('backend/recharge_history',$data);
		
	}
	/********************Recharge Setting view***********************/	
		public function recharge_setting()
	{
		$data['title']="Recharge Setting";
		$this->load->model('admin_model');
		$data['result'] = $this->admin_model->select_all_rchrgsttng();
	
		$this->load->view('backend/recharge_setting',$data);
		
	}
		public function add_recharge_setting()
	{
		$data['title']="Add Recharge Setting";
		
	
		$this->load->view('backend/add_recharge_setting',$data);
		
	}

        /****************Edit recharge setting function****************/
	public function edit_recharge_setting()
	{
		 $id=$_GET['id'];	
        $setting_id=base64_decode($id);
		$data['title']="Edit Recharge Setting";
		$data['result']=$this->admin_model->select_single_recharge_setting($setting_id);
	
		$this->load->view('backend/add_recharge_setting',$data);
		
	}

        /***************Add recharge setting details function****************/
	public function add_recharge_setting_details()
	{
			 $idfr = $this->uri->segment(3);
		//$idfr=$_GET['id'];
		$name= $this->input->post('setting_name');
	
		$percentage= $this->input->post('percentage');
		$created_date=date('Y-m-d');
		
		$detail=array(
	  
	  'setting_name'=>$name,
	  'percentage'=>$percentage,
	  'status'=>1,
	  'creation_date'=>strtotime($created_date)
	   );
	   
	  
	   $table="ww_recharge_setting";
		$result = $this->admin_model->add_data($detail,$table);		
		if($result=1){
		$data = "Successful Setting add";
		$data = base64_encode($data);
		redirect('Admin/recharge_setting?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/add_recharge_setting?set='.$data);
		}
		
	}
	
        /****************Edit recharge setting details function****************/
	public function edit_recharge_setting_details()
	{
			 $idfr = $this->uri->segment(3);
		$id=$_GET['id'];
		 $setting_id=base64_decode($id);
	
		$name= $this->input->post('setting_name');
	
		$percentage= $this->input->post('percentage');
		
		$detail=array(
	 
	  
	  'setting_name'=>$name,
	  'percentage'=>$percentage,
	  
	  
	   );
	 $where=array('id'=>$setting_id);
	  
	   $table="ww_recharge_setting";
		$result = $this->admin_model->update_data($table,$detail,$where);		
		if($result=1){
		$data = "Successful Setting Updated";
		$data = base64_encode($data);
		redirect('Admin/recharge_setting?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/edit_recharge_setting?set='.$data);
		}
	}
		/***********deactivate rchrgsttng********************/
	
		 public function deactive_rchrgsttng()
		   {
		  // $this->authAdLogin();
			   
			$id=$this->input->post('id');
			$deactive_customer=array('status'=>0);
			$this->db->where('id',$id);
			$this->db->update('ww_recharge_setting',$deactive_customer); 
			
		   }  
	   
	   /***********activate rchrgsttng********************/
	
		 public function active_rchrgsttng()
		   {
		 //  $this->authAdLogin();
			   
			$id=$this->input->post('id');
			$active_customer=array('status'=>1);
			$this->db->where('id',$id);
			$this->db->update('ww_recharge_setting',$active_customer);  
			
		   }
   /********************Delete recharge setting***********************/	
   public function delete_rchrgsttng()
	{
		$id=base64_decode($_GET['del']);
	    $this->db->where('id',$id);
		$this->db->delete('ww_recharge_setting'); 
		$data="Setting Deleted Successfuly";
		$data=base64_encode($data);
		redirect('Admin/recharge_setting?set='.$data);
		
	}
/********************Cupons And Deals***********************/	
    public function cupon()
	{
		$data['title']="Coupons And Deals";
		$this->load->model('admin_model');
		$data['result'] = $this->admin_model->select_all_cupon();
	
		$this->load->view('backend/cupons',$data);
		
	}	
	/********************Add Cupons And Deals***********************/	
	public function add_cupon()
	{
		$data['title']="Add Cupons";
		$this->load->model('admin_model');
		//$data['result'] = $this->admin_model->select_all_rchrgsttng();
	
		$this->load->view('backend/add_cupons',$data);
		
	}	
	/********************Edit Cupons And Deals***********************/	
	public function edit_cupon()
	{
		$id=$_GET['id'];
		 $cupon_id=base64_decode($id);
		$data['title']="Edit Cupons";
		$this->load->model('admin_model');
		$data['result'] = $this->admin_model->select_single_cupon($cupon_id);
		$this->load->view('backend/add_cupons',$data);
		
	}	
	/********************Add Cupons details***********************/	
	public function add_cupon_details()
	{
			 $idfr = $this->uri->segment(3);
		//$idfr=$_GET['id'];
		$promo= $this->input->post('promocode');
	$type= $this->input->post('type');
	$remark=$this->input->post('remark');
		$amount= $this->input->post('amount');
		$sdate= $this->input->post('sdate');
$edate= $this->input->post('edate');
		//$created_date=date('d/m/y');
		
		$detail=array(
	  
	  'promocode'=>$promo,
	  'discount_type'=>$type,
		'discount_amount'=>$amount,
	  'status'=>1,
	  'remark'=>$remark,
	  'start_date'=>strtotime($sdate),
	  'end_date'=>strtotime($edate)
	   );
	   
	  
	   $table="ww_cupons";
		$result = $this->admin_model->add_data($detail,$table);		
		if($result=1){
		$data = "Successful Cupon add";
		$data = base64_encode($data);
		redirect('Admin/cupon?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/add_cupon?set='.$data);
		}
		
	}
	
        /********************Edit Cupons details***********************/	
	public function edit_cupon_details()
	{
			 $idfr = $this->uri->segment(3);
		$id=$_GET['id'];
		 $cupon_id=base64_decode($id);
	$promo= $this->input->post('promocode');
	$type= $this->input->post('type');
	$remark=$this->input->post('remark');
		$amount= $this->input->post('amount');
		$sdate= $this->input->post('sdate');
$edate= $this->input->post('edate');
		
		$detail=array(
	 
	 'promocode'=>$promo,
	  'discount_type'=>$type,
		'discount_amount'=>$amount,
	  'remark'=>$remark,
	  'start_date'=>strtotime($sdate),
	  'end_date'=>strtotime($edate)
	  
	  
	   );
	 $where=array('id'=>$cupon_id);
	  
	   $table="ww_cupons";
		$result = $this->admin_model->update_data($table,$detail,$where);		
		if($result=1){
		$data = "Successful Cupon Updated";
		$data = base64_encode($data);
		redirect('Admin/cupon?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/edit_cupon?set='.$data);
		}
	}
		/***********deactivate cupon********************/
	
		 public function deactive_cupon()
		   {
		  // $this->authAdLogin();
			   
			$id=$this->input->post('id');
			$deactive_cupon=array('status'=>0);
			$this->db->where('id',$id);
			$this->db->update('ww_cupons',$deactive_cupon); 
			
		   }  
	   
	   /***********activate coupon********************/
	
		 public function active_cupon()
		   {
		 //  $this->authAdLogin();
			   
			$id=$this->input->post('id');
			$active_cupon=array('status'=>1);
			$this->db->where('id',$id);
			$this->db->update('ww_cupons',$active_cupon);  
			
		   }
		   public function delete_cupon()
	{
		 $id=base64_decode($_GET['del']);
	 $this->db->where('id',$id);
							$this->db->delete('ww_cupons'); 
							$data="Cupon Deleted Successfuly";
							$data=base64_encode($data);
							
							redirect('Admin/cupon?set='.$data);
		
	}
	
	/******End of Functions done by Manoj Singh ******/
	
	
	/******Functions done by Ankit Singh ******/
        
        /******Add SP User ******/
        
		public function create_user()
	{
		$data['title'] ="Create User";
		$data['role'] = @$_GET['role'];
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
                $data['result'] = $this->admin_model->select_all_rchrgsttng();
		$this->form_validation->set_rules('role','role','required');
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('contact_no','contact_no','required');
		$this->form_validation->set_rules('email','email','required');
		
		if ($this->form_validation->run() == false)
		{
		 $this->load->view('backend/add_service_provider',$data);
		}else{
			$username = strtolower($this->input->post('username'));
			$email    = strtolower($this->input->post('email'));
			
                         $recharge = strtolower($this->input->post('recharge'));

			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
			$password = substr( str_shuffle( $chars ), 0, 8 );

			$contact_no = $this->input->post('contact_no');
			$companyname = strtolower($this->input->post('companyname'));
			if(empty($companyname)){
				$companyname = $username;
			}
			$this->load->helper('string');
			$login_id= substr($role,0,3).random_string('alnum',10).substr($user_name,0,3);
			$email = strtolower($this->input->post('email'));
			$role = strtolower($this->input->post('role'));
	
		    $additional_data = array(
				'id' => $login_id,
				'user_name' => $username,
				'user_email'  => $email,
				'password'  => md5($password),
				'contact_no'  => $contact_no,
                                'subscription_value'  => $recharge,
				'role_type' => $this->input->post('role'),
				'create_date' => date('Y-m-d'),
				'company_name' => $companyname
			 );
			$id = $this->admin_model->registration($additional_data);
			$link = base_url().'registration/user_activation/?user_id='.$login_id;
			
			
			
			
			if($id):

		$to = 'ankit.vayuz@gmail.com';
        $base_url=base_url();
			
		$subject = 'Account  Created';
		$message = '<html><body>';
		$message = '<div style="overflow-x:auto;">';
		$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr style='background: #eee;'><td><strong>Email :</strong> </td><td>" . strip_tags($email) . "</td></tr>";
		$message .= "<tr><td><strong>Password :</strong> </td><td>" . strip_tags($password) . "</td></tr>";
		$message .= '<tr><td><strong>Activation Link :</strong> </td><td><a href="'.$link.'">Activate Account</a></td></tr>';
		
		$message .= "</table>";
		$message .= "</div>";
		$message .= "</body></html>";
		
		
		
		$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		//$headers .= "CC: susan@example.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        
		if(mail($to, $subject, $message, $headers))
		{
			$data['sendvenuesucees']="Message is Successfully";
			
		}	
	    $data="Message is Successfully";
		$data = base64_encode($data);
		$this->session->set_flashdata('message'," ".$this->input->post('role')." has been added successfully" );
		redirect('admin/create_user');	
		endif;
		
		}	
	}
	
        /******Change password SP user ******/
	public function change_password() {
	    $data['title']="Change Password";
		if(!empty($_POST)){
			$password = md5($this->input->post('password'));
			$user_role = $this->input->post('user_role');
			$sp_id = $this->input->post('sp_id');
			$passArray = array(
			'password'=> $password);
			$pass = $this->admin_model->change_password($passArray , $sp_id);
			
			$this->session->set_flashdata('message',"Password changed successfully." );
			if($user_role == 'photographer'){
					redirect('sp_manager/photographer_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'planner'){
					redirect('sp_manager/planner_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'decorator'){
					redirect('sp_manager/profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'venue'){
					redirect('sp_manager/venue_profile?id='.base64_encode($sp_id)); 
				}
		}
	}
	
	/******Check password SP user ******/
	public function check_password() {
	    $data['title']="Change Password";
		$current_password = md5($this->input->post('curr_pass'));
		$user_id = $this->input->post('user_id');
		$detail = $this->admin_model->current_password($current_password , $user_id);
		if(empty($detail)){
			echo 1;
			die;
		}
	}
	
	/********Unique Email Check of SP*******/
	public function email_exists()
	{
		$email = $this->input->post('email');
		$is_unique = $this->admin_model->emailexists($email);
		if($is_unique){
			echo 1;die;
		}else{
			echo 0;die;
		}
	}
	
        /******Populate calendar ******/
	public function calendar()
	{
	    $data['title']="Calendar";
		$data['two_slots'] = $this->admin_model->select_two_slots();
		$data['three_slots'] = $this->admin_model->select_three_slots();
		$data['venues_name'] = $this->admin_model->select_venues($this->session->userdata('id'));
		$this->load->view('backend/calendar',$data);
	}
	
        /******Populate events in calendar ******/
	public function events()
	{
		$json = array();
                $id = $this->session->userdata('id');
                $data['events'] = $this->admin_model->select_events($id);
                
                echo json_encode($data['events']); 
		
	}
		
        /******Add event in calendar ******/
	public function add_events()
	{
		$title = $_POST['title'];
		$start = $_POST['start'];
		$description = $_POST['description'];
		$all = $_POST['allDay'];
		
		$events_full = $_POST['events_full'];
		if(!empty($events_full) && $events_full!='none'){
			$events_slots = $events_full;
		}
		
		$events_second_slots = $_POST['events_second_slots'];
		if(!empty($events_second_slots) && $events_full == 'none') {
			$events_slots = $events_second_slots;
		}
		
		$events_third_slots = $_POST['events_third_slots'];
		if(!empty($events_third_slots) && $events_second_slots=='none' &&  $events_full=='none'){
			$events_slots = $events_third_slots;
		}
		
		$events_venues = $_POST['venues'];
		$merge_venue = $_POST['merge_venue'];
		if(!empty($merge_venue) && $merge_venue != 'none'){
			$events_venues = $_POST['venues'].','.$_POST['merge_venue'];
		}
		
	
		$user_id = $this->session->userdata('id');
		
		if(!empty($events_venues)){
		$event_array = array(
			'title' => $title,
			'start' => $start,
			'description' => $description,
			'user_id' =>$user_id,
			'venues' =>$events_venues,
			'allday' =>$all,
			'events_slots' =>$events_slots
		);	
		}else{
			$event_array = array(
			'title' => $title,
			'start' => $start,
			'description' => $description,
			'user_id' =>$user_id,
			'allday' =>$all,
			'events_slots' =>$events_slots
		);		
		}
	 
	  $slots = $this->admin_model->event_slots($events_slots,$start,$user_id);
	 
	  if($slots){
		  
		 echo "You have already booked event for this slot.";  
		  die;
	  }else{
		 $id = $this->admin_model->event_insert($event_array);
	  if(!empty($id)){
	  echo "Event added successfully.";
	  }
	  die;  
	  }
	 
	 }
	 
	 /*****************Venue Details***************/
	 public function venue_details(){
			$data['title']="Venue Details";	 
	 		$data['venue']=$this->admin_model->get_data('ww_admin_login',array('role_type'=>'venue'));
	 		$this->load->view('backend/all_venue_listing',$data);
	 
	 }
	 public function venue_related_details(){
			$data['title']="Venue Details";	 
			$data['company']=$this->uri->segment(4);
			$data['Brand_id']=$this->uri->segment(3);
	 		//$data['company']=$this->admin_model->get_data('ww_admin_login',array('id'=>$data['sp_id']));
	 		//$data['brandresult']=$this->admin_model->get_data('city_setting',array('proName'=>$data['sp_id']));
	 		//$data['venueresult']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$data['sp_id']));
	 		//$data['cityresult']=$this->admin_model->get_data_all('ww_company');	
	 		$data['cityresult']=$this->admin_model->get_data_distinct('cityName','ww_venue_property',array('brandName'=>$data['Brand_id']));
	 		$data['propertyresult']=$this->admin_model->get_data('ww_venue_property',array('brandName'=>$data['Brand_id']));
			$data['city']=$this->admin_model->get_data_all('ww_city');
	 		$this->load->view('backend/venue_related_details',$data);
	 
	 }
	  public function venue_brand(){
			$id=$_POST['id'];
			$res=$this->admin_model->get_data('city_setting',array('proName'=>$id));
			//print_r($res);
			$brnd='';
			foreach($res as $bres){
				$prop=$this->admin_model->get_data('ww_venue_property',array('brandName'=>$bres->id));
				
			$brnd.='<div class="tiles">
					<div class="tile double selected bg-blue">
						<div class="corner"></div>
						<div class="check"></div>';
			$brnd.='<a href="'.base_url().'admin/venue_related_details/'.$bres->id.'/'.$id.'">';
			$brnd.='<div class="tile-body">';
			$brnd.='<h4>'.ucfirst($bres->name).'</h4>';
			$brnd.='<p>'.ucfirst($bres->description).'</p>
							
						</div>
						<div class="tile-object">
							<div class="name">
								<a href="'.base_url().'General_setting/edit_brand/'.$bres->id.'" class="name">
								<i class="icon-edit"></i>
							</a>
							</div>
							<div class="number">
							Property-'.sizeof($prop).'
							</div>
						</div>
					</div>
					</div>';
				echo $brnd;
			}
	  
	  }
	  public function venue_prop(){
		$id=$_POST['id'];
		$res=$this->admin_model->get_data('ww_venue_property',array('cityName'=>$id)); 
		$prop='';
		$prop.='<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>Property</th>
											<th class="hidden-480">Address</th>
											<th class="hidden-480">Venue</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>';
									 foreach($res as $pro){
										$prop.='<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>';
										$prop.='<td>'.$pro->propertyName.'</td>';
										$prop.='<td class="hidden-480"><a href="javascript:void">'.$pro->propertyAddr.'&nbsp;&nbsp;'.$pro->pincode.'</a></td>';
										$prop.='<td class="hidden-480"><a href="#myModal2" id="'.$pro->pro_id.'" data-toggle="modal" title="'.$pro->pro_id.'"  class="btn mini green-stripe venu"><i class="icon-eye-open">View Venue</i></a></td>';
										$prop.='<td class="hidden-480"><a class="btn mini blue-stripe" title="Edit Property" href="'.base_url().'Venue_setting_cntlr/property_edit/'.$pro->pro_id.'"><i class="icon-edit">Edit Property</i></td>	
										</tr>';
									 }
									
									$prop.='</tbody>
								</table>';
									
									$prop.='<script>
									$(function(){
									
									$(".venu").click(function(){
										//alert("mannu");
									//alert($(this).attr("id"));
									var id = $(this).attr("title");
									 $.ajax({type:"post",
												url:"'.base_url().'admin/getvenue",
												data:{id2:id},
												success:function(res){
																	 //alert(res);
																	  $("#venuediv").html(res);
																	 }
											  });

									 });
									});
									</script>';
								echo $prop;
	  }
	 public function getvenue(){
		$id=$_POST['id2'];
		$prop='';
		$res=$this->admin_model->get_data('ww_venue',array('pvName'=>$id));  
		$prop.='<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											
											<th class="hidden-480">Venue Name</th>
											<th class="hidden-480">Nature</th>
											<th class="hidden-480">Capacity</th>
											<th>Action</th>
											
										</tr>
									</thead>
									<tbody>';
									 foreach($res as $ven){
										$prop.='<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>';
										$prop.='<td>'.$ven->v_name.'</td>';
										$prop.='<td class="hidden-480">'.$ven->v_nature.'</td>';
										$prop.='<td class="hidden-480">'.$ven->sittingCapecity.'</td>';
										$prop.='<td class="hidden-480"><a class="btn mini blue-stripe" title="Edit Venue" href="'.base_url().'Venue/venue_edit_get/'.$ven->v_id.'"><i class="icon-edit">Edit Venue</i></a>
										&nbsp;&nbsp;<a onclick="confirm("Are You Sure Delete?");" class="btn mini red-stripe" title="Delete Venue" href="'.base_url().'Venue/venue_delete/'.$ven->v_id.'"><i class="icon-trash">Delete Venue</i></a>
										</td>	
										</tr>';
									 }
									
									$prop.='</tbody>
								</table>';
								echo $prop;
		
	 }
        
/**********************Venue Details***************************/	
        
	
        }
