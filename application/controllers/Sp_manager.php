<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sp_Manager extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		error_reporting(0);
		
		if(!$this->session->userdata('id')){
			redirect('registration');
		}
	}
	
	public function index()
	{
		$data['title']="Sign In";
		$this->load->view('backend/login',$data);
	}
	
	
	public function dashboard()
	{
		$data['title']="Dashboard";
		$this->load->view('backend/index',$data);
	}
	
		/******Spmanager Listing Here*******/
	public function spmanager()
	{
		$data['title']="Wedding Planner";
		$this->load->model('admin_model');
		$data['result'] = $this->admin_model->select_all_serviceprovider();
		$this->load->view('backend/spmanager',$data);
	}
	
	  
	
	public function venue_manager()
	{
		$data['title']="Venue Manager";
		$this->load->model('admin_model');
		$data['venue_details'] = $this->admin_model->select_all_venuemanager();
		$this->load->view('backend/list_venue_manager',$data);
	}
	
	 /******Decorator starts here*******/
	public function decorator()
	{
		$data['title']="Decorator";
		$this->load->model('admin_model');
		$data['result'] = $this->admin_model->select_all_decorator();
		$this->load->view('backend/decorator',$data);
	}
	
	public function profile()
	{
		$user_id=$this->session->userdata('id');
                $getid=base64_decode($_GET['id']);

                if($getid):
                $id=$getid;
                else:
                $id=$user_id;
                endif;
		$table='ww_admin_login';
		$this->load->model('admin_model');
		$data['result']=$this->admin_model->select_single_data($table,$id);
		$data['business_result']=$this->admin_model->get_decorator_services($id);
		$data['title']="User Profile";
		$this->load->view('backend/decorator_profile',$data);
	}
	
	public function venue_profile()
	{      
	        
		$user_id=$this->session->userdata('id');
                $getid=base64_decode($_GET['id']);

                if($getid):
                $id=$getid;
                else:
                $id=$user_id;
                endif;

		$table='ww_admin_login';
		$this->load->model('admin_model');
                $tableVenue="ww_venue";
                $data['resultVenue']=$this->admin_model->select_single_venue($tableVenue,$id);

                $tableBrand="city_setting";
                $data['resultBrand']=$this->admin_model->select_single_brand($tableBrand,$data['resultVenue'][0]->brand_name);

                $tableBelt="ww_venue_belt";
                $data['resultBelt']=$this->admin_model->select_single_belt($tableBelt,$data['resultVenue'][0]->belt);

                $tableProperty="ww_venue_property";
                $data['resultProperty']=$this->admin_model->select_single_property($tableProperty,$data['resultVenue'][0]->pvName);
           	$data['pendingleads']=$this->admin_model->get_data('ww_lead_module_user',array('sp_id'=>$user_id,'status'=>Null));
           	$data['acceptedleads']=$this->admin_model->get_data('ww_lead_module_user',array('sp_id'=>$user_id,'status'=>1));
  			  $data['leaddetails']=$this->admin_model->get_data('ww_lead_module',array('sp_id'=>$user_id));         
		$data['result']=$this->admin_model->select_single_data($table,$id);
          
               
		$data['business_result']=$this->admin_model->select_photographer_services($id);
		$data['title']="Profile";
		$this->load->view('backend/venue_profile',$data);
	}
	
	public function photographer_profile()
	{
		$user_id=$this->session->userdata('id');
                $getid=base64_decode($_GET['id']);

                if($getid):
                $id=$getid;
                else:
                $id=$user_id;
                endif;
		
		$table='ww_admin_login';
		$this->load->model('admin_model');
		$data['result']=$this->admin_model->select_single_data($table,$id);
		$data['business_result']=$this->admin_model->select_photographer_services($id);
		$data['events_details'] = $this->admin_model->list_planner_events();
		$data['title']="User Profile";
		$this->load->view('backend/photographer_profile',$data);
	}

	
	public function planner_profile()
	{
		$user_id=$this->session->userdata('id');
                $getid=base64_decode($_GET['id']);

                if($getid):
                $id=$getid;
                else:
                $id=$user_id;
                endif;

		$table='ww_admin_login';
		$this->load->model('admin_model');
		$data['result']=$this->admin_model->select_single_data($table,$id);
		$data['business_result']=$this->admin_model->select_planner_services($id);
		$data['events_details'] = $this->admin_model->list_planner_events();
		$data['events_arrangements'] = $this->admin_model->list_planner_arrangements();
		$data['title']="User Profile";
		$this->load->view('backend/planner_profile',$data);
	}
	
	/*****Sp Profile*******/
	public function update_user_detail()
	{
	   // $encoded_data = base64_encode(file_get_contents($_FILES['tmp_name']));
		$user_name = $this->input->post('user_name');
		$gender = $this->input->post('gender');
		$dob = $this->input->post('dob');
		//$experience = $this->input->post('experience');
		$mobile = $this->input->post('mobile');
		$user_role = $this->input->post('user_role');
		$contact = $this->input->post('contact');
		$sp_id = $this->input->post('sp_id');
		$date_of_birth=$this->input->post('dob');
		
		mkdir('./uploads/'.$sp_id); 
        if(!empty($_FILES['pro_pic']['name'])){ 
                         
			$config['upload_path'] = './uploads/'.$sp_id; 
			$config['allowed_types'] = 'gif|jpg|png'; 
			$config['file_name']="prof_".rand(1,5000); 
                        $this->load->library('upload');    
			$this->upload->initialize($config); 
			if ( ! $this->upload->do_upload('pro_pic')) 
				{ 
				$error = $this->upload->display_errors(); 
				$this->session->set_flashdata('ppic_error',$error); 
				redirect(base_url().'sp_manager/pofile'); 
				} 
				else 
				{ 
				$data = $this->upload->data(); 
			    $prof_pic=$data['file_name']; 
				$content=array('user_name'=>$user_name,
				'gender'=> $gender,
				'd_o_b'=> $date_of_birth,
				'mob_no'=> $mobile,
				'contact_no'=> $contact,
				'pro_pic'=> $prof_pic,
				'gender' => $gender,
				'update_date' => date('Y-m-d')
				);
				$table='ww_admin_login';
				$where= array('id'=>$sp_id);	
		
				$result = $this->admin_model->update_data($table,$content,$where);
				if($user_role == 'photographer'){
					redirect('sp_manager/photographer_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'planner'){
					redirect('sp_manager/planner_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'decorator'){
					redirect('sp_manager/profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'venue'){
					redirect('sp_manager/venue_profile?id='.base64_encode($sp_id)); 
				}
				} 
                         
        }
					
		$this->load->library('form_validation');
		$this->form_validation->set_rules('user_name', 'Name', 'required|trim');
	    $this->form_validation->set_rules('mobile', 'Mobile', 'required|trim');
	
		if ($this->form_validation->run() == FALSE)
		{
			redirect('sp_manager/profile');
		}
		else
		{
			$content=array('user_name'=>$user_name,
			'gender'=> $gender,
			'd_o_b'=> $date_of_birth,
			'mob_no'=> $mobile,
			'contact_no'=> $contact,
			'gender' => $gender,
			'update_date' => date('Y-m-d')
			);
			$table='ww_admin_login';
			$where= array('id'=>$sp_id);	
		
		$result = $this->admin_model->update_data($table,$content,$where);
	                        if($result==TRUE)
	                        {
					$data = "User Details Updated";
					$data = base64_encode($data);
					if($user_role == 'photographer'){
					redirect('sp_manager/photographer_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'planner'){
					redirect('sp_manager/planner_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'decorator'){
					redirect('sp_manager/profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'venue'){
					redirect('sp_manager/venue_profile?id='.base64_encode($sp_id)); 
				}
				}
				else
				{
					$data = "Something is wrong";
					$data = base64_encode($data);
					if($user_role == 'photographer'){
					redirect('sp_manager/photographer_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'planner'){
					redirect('sp_manager/planner_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'decorator'){
					redirect('sp_manager/profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'venue'){
					redirect('sp_manager/venue_profile?id='.base64_encode($sp_id)); 
				}
				}
		}			
}

		public function update_company_detail()
	{
		$company_name = $this->input->post('company_name');
		$company_description = $this->input->post('company_description');
		$count_of_event = $this->input->post('count_of_event');
		$last_event_date = $this->input->post('last_event_date');
		$budget = $this->input->post('budget');
		$country = $this->input->post('country');
		$state = $this->input->post('state');
		$city = $this->input->post('city');
		$street = $this->input->post('street');
		$zip_code = $this->input->post('zip_code');
		$lattitude = $this->input->post('lattitude');
		$longitude = $this->input->post('longitude');
		$landmark = $this->input->post('landmark');
		$fb_url = $this->input->post('fb_url');
		$gp_url = $this->input->post('gp_url');
		$sp_id = $this->input->post('hidden');
		$user_role = $this->input->post('user_role');
		$this->load->library('form_validation');
 
		$this->form_validation->set_rules('company_name', 'Company Name', 'required|trim');
	
		if ($this->form_validation->run() == FALSE)
		{
			redirect('sp_manager/profile');
		}
		else
		{
			$content=array(	'company_name'=>$company_name,
			'company_description'=>$company_description,
			'country'=>$country,
			'state'=>$state,
			'city'=>$city,
			'street'=>$street,
			'zip_code'=>$zip_code,
			'landmark'=>$landmark,
			'fb_url'=>$fb_url,
			'gp_url'=>$gp_url,
			);
			$table='ww_admin_login';
			$where= array('id'=>$sp_id);	
		
		$result = $this->admin_model->update_data($table,$content,$where);
	    if($result==TRUE)
	            {
					$data = "Comapny Details Updated";
					$data = base64_encode($data);
					//redirect('sp_manager/profile?set='.$data.'&id='.base64_encode($sp_id));
					$this->session->set_flashdata('message','User company updated successfully.');
				if($user_role == 'photographer'){
					redirect('sp_manager/photographer_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'planner'){
					redirect('sp_manager/planner_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'decorator'){
					redirect('sp_manager/profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'venue'){
					redirect('sp_manager/venue_profile?id='.base64_encode($sp_id)); 
				}
				}
				else
				{
				$data = "Something is wrong";
				$data = base64_encode($data);
				$this->session->set_flashdata('message','User company updated successfully.');
				if($user_role == 'photographer'){
					redirect('sp_manager/photographer_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'planner'){
					redirect('sp_manager/planner_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'decorator'){
					redirect('sp_manager/profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'venue'){
					redirect('sp_manager/venue_profile?id='.base64_encode($sp_id)); 
				}
				}
		}			
		
		// end validation sp
	}
	
	/********Delete Decorator*******/
	public function delete_decorator()
	{
		$id = $_GET['id'];
		$this->admin_model->deletedecorator(base64_decode($id));
		redirect('sp_manager/decorator');
	}
     
	public function delete_planner()
	{
		$id = $_GET['id'];
		$this->admin_model->delete_last_planner_record(base64_decode($id));
		redirect('sp_manager/spmanager');
	}
	
	
	public function delete_venue()
	{
		$id = $_GET['id'];
		$this->admin_model->deletevenue(base64_decode($id));
		redirect('sp_manager/venue_manager');
	}
	
	public function get_role_names()
	{
		$role = $this->input->post('role');
		$data['result'] = $this->admin_model->get_names_role($role);
		
		foreach($data['result'] as $resu){ ?> 
			$html .= <option value="'".<?php echo $resu->user_name ;?>."'"><?php echo  $resu->user_name;?></option>;
		<?php
		}
		$html = '<option value="">Photographer Name</option>';
		echo $html;
		die;
		
	}

	public function customer_proposal() 
	{
	   
		$user_id = $this->session->userdata('id');
	    
		if($this->session->userdata('role') == 'venue'){
			$data['title']="Venue Customer Proposal";
			$data['images_result'] = $this->admin_model->list_planner_title_images($user_id);
			$data['venue']=$this->admin_model->get_data_all('ww_venue');
			$data['album_details'] = $this->admin_model->list_album($user_id);
			$this->load->view('backend/planner_customer_proposal',$data);
		}
		if($this->session->userdata('role') == 'photographer'){
			$data['title']="Photographer Customer Proposal";
			$data['images_result'] = $this->admin_model->list_photographer_title_images($user_id);
			$this->load->view('backend/photographer_customer_proposal',$data);
		}
		if($this->session->userdata('role') == 'planner'){
			$data['title']="Planner Customer Proposal";
			$data['images_result'] = $this->admin_model->list_planner_title_images($user_id);
			$data['album_details'] = $this->admin_model->list_album($user_id);
			$this->load->view('backend/planner_customer_proposal',$data);
		}
		if($this->session->userdata('role') == 'decorator'){
			$data['title']="Decorator Customer Proposal";
			$data['images_result'] = $this->admin_model->list_decorator_images($user_id);
			$this->load->view('backend/decorator_customer_proposal',$data);
		}
		
		
	}
	
	/********Recharge History*******/
	
	public function recharge_history()
	{
		$res ='';
		$rechargearr = array('sp_id'=>$_POST['id'],
		                   'transaction_type'=>$_POST['remark'],
						   'amount'=>$_POST['amount'],
						   'recharge_date'=>strtotime(date('Y-m-d'))
						  );
		$table = "ww_recharge_history";
		$responce = $this->admin_model->recharge_history($table,$rechargearr);
		if($responce == true):
		$res.='Recharge successfully';
		echo $res;
		else :
		$res.='Recharge failure';
		echo $res;;
		endif;
	}
}
