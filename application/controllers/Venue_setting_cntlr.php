<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venue_setting_cntlr extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Venue_setting_model');
		$this->load->model('Setting_model');
		error_reporting(0);
	}

	public function venue_setting_home1(){
		$data['title']="Venue Setting";
		$data['result1']=$this->Venue_setting_model->get_venue(1);
		$data['result2']=$this->Venue_setting_model->get_venue(2);
		$data['result3']=$this->Venue_setting_model->get_venue(3);
		$data['result4']=$this->Venue_setting_model->get_venue(4);
		$data['result5']=$this->Venue_setting_model->get_venue(5);
		$data['result6']=$this->Venue_setting_model->get_venue(6);
		$data['result7']=$this->Venue_setting_model->get_venue(7);
		$data['result8']=$this->Venue_setting_model->get_venue(8);
		$data['result9']=$this->Venue_setting_model->get_venue(9);
		$data['result10']=$this->Venue_setting_model->get_venue(10);
		$this->load->view('backend/venue_setting_home',$data);
	}

	public function insert_venue()
	{
		$idfr = $this->uri->segment(3);
		$names= $this->input->post('namee');
		$descs= $this->input->post('desc');
		$result = $this->Venue_setting_model->inser_venue($idfr,$names,$descs);
		if($result=1){
		$data = "Successful add";
		$data = base64_encode($data);
		redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
		}else{
        $data = "Something is wrong";
		$data = base64_encode($data);
		redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
		}
	} 

	public function add_venue()
	{
		$data['title']="Venue Setting";
		$this->load->view('backend/venue_settings',$data);
	}
	
	public function delete_venue(){
		$idfr = $this->uri->segment(4);
		$id=$this->uri->segment(3);
		$result=$this->Venue_setting_model->delete_row($id,$idfr);
		if($result=1){
		$data = "Successful deleted";
		$data = base64_encode($data);
		redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
		}else{
        $data = "Something is wrong";
		$data = base64_encode($data);
		redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
		}
	}
	
	public function edit_venue()
	{
		$idfr = $this->uri->segment(4);
		$ide=$this->uri->segment(3);
		$data['idfru']=$idfr;
		$data['id']=$ide;
		$data['title']="VENUE SETTING";
		$data['result']=$this->Venue_setting_model->get_venueid($ide,$idfr);
		
		$this->load->view('backend/venue_setting_edit',$data);
	}
	public function venue_update()
	{
		 $id = $this->uri->segment(3);
		 $idfr = $this->uri->segment(4);
	         $names= $this->input->post('namee');
	         $descs= $this->input->post('desc');
                 $result = $this->Venue_setting_model->venue_update($id,$idfr,$names,$descs);
  

   if($result=1){
   $data = "Successful  updated";
					$data = base64_encode($data);
					redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
   }else{
   
        $data = "Something is wrong";
					 $data = base64_encode($data);
					
					redirect('Venue_setting_cntlr/venue_setting_home1?set='.$data);
    }
   }

  public function venue_view(){
	 $ide = $this->uri->segment(3);
	 $idfr = $this->uri->segment(4);
	 $data['idfru']=$idfr;
		$data['id']=$ide;
	 $data['title']="View";
	 $data['result']=$this->Venue_setting_model->get_venueid($ide,$idfr);
	$this->load->view('backend/venue_setting_view',$data);
}
 /*
  *
  *property listing--->
  *
  */
  
    public function sel_ajax_company()
	{
		$id=$_REQUEST['city_id'];
		$table="ww_company";
		$data['result']=$this->Venue_setting_model->sel_ajax_company($table,$id);
		if(isset($data['result'])):
	    $html=" ";
		$html.="<option value=''>select company</option>";
		foreach($data['result'] as $rowcompany):
		$html.="<option value='".$rowcompany->c_id."'>".$rowcompany->company_name."</option>";
		endforeach;
		echo $html;
		else:
			echo $html ="<option value=''>select company</option>";
		endif;
	} 
	
	public function list_property()
	{
		$data['title']="List Property";
		$data['resultLoc']=$this->Setting_model->get_location();
        $data['result']=$this->Venue_setting_model->get_property();
		$this->load->view('backend/list_property',$data);
	} 
	
	public function property()
	{
		$data['title']="Property";
        $data['resultLoc']=$this->Setting_model->get_location();
		$this->load->view('backend/add_property',$data);
	}
	
	public function add_Property()
	{
		$this->form_validation->set_rules('location', 'Lcation', 'required');
		$this->form_validation->set_rules('propertyName', 'Property', 'required');
		$this->form_validation->set_rules('propertyAddr', 'Address', 'required');
		$this->form_validation->set_rules('numFloor', 'Floor', 'required');	

		if ($this->form_validation->run() == FALSE)
		{  
			$data['title']="Add Venue";
			$data['resultLoc']=$this->Setting_model->get_location();
			$this->load->view('backend/add_property',$data);
		}
		else
		{ 
			$pro_id='PRO'.rand();
			/************** key distance *********************/
			$kd_name =$_REQUEST['mytext'];
		    $ked_value   =$_REQUEST['mytextval'];
			
			/************* sitting arrangement **************/
			$s_name =$_REQUEST['mytexts'];
		    $s_capacity  =$_REQUEST['mytextsval'];
			/************* parking arrangement **************/
			$p_name =$_REQUEST['mytextp'];
			$p_type =$_REQUEST['mytextptype'];
			$p_carcap =$_REQUEST['mytextpc'];
			$p_twocap =$_REQUEST['mytextptc'];
			$vp = $this->input->post('valetFacility');
			$vparry=array();
			if(isset($vp))
			{
				$vparry[]=$vp;
			}
			else
			{
				$vparry[]=$vparry;
			}
			
			/************* parking arrangement **************/
			$propertyarray = array(
		             'pro_id'=>$pro_id,
					 
					 'location'=>$this->input->post('location'), 
					 'propertyAddr'=>$this->input->post('propertyAddr'),
					 'propertyType'=>$this->input->post('propertyType'),
					 'externalPer'=>$this->input->post('externalPer'),
					 'restrictionLimit'=>$this->input->post('restrictionLimit'),
					 'outsideSpace'=>$this->input->post('outsideSpace'),
					 'user_id'=>$this->session->userdata('id'),
					 'lift'=>$this->input->post('lift'),
					 'valet'=>$this->input->post('valet'),
					 'valetFacility'=>json_encode($vparry),
					 'numFloor'=>$this->input->post('numFloor'),
					 'powerbackup'=>$this->input->post('powerbackup'),
					 'powerdetails'=>$this->input->post('powerdetails'),
					 'rainbackup'=>$this->input->post('rainbackup'),
					 'raindetails'=>$this->input->post('raindetails'),
					 'firebackup'=>$this->input->post('firebackup'),
					 'firedetails'=>$this->input->post('firedetails'),
					 'propertyName'=>$this->input->post('propertyName'),
					 'kdName'=>json_encode($kd_name),
					 'kdValue'=>json_encode($ked_value),
					 'pName'=>json_encode($p_name),
					 'pType'=>json_encode($p_type),
					 'pCarcap'=>json_encode($p_carcap),
					 'pTwocap'=>json_encode($p_twocap),
					 'sName'=>json_encode($s_name),
					 'sCapacity'=>json_encode($s_capacity),
					 'create_date'=>strtotime(date('Y-m-d'))
					 );
			$this->Venue_setting_model->add_property($propertyarray);

		 }
		 
		
	}
	/************** edit property *********************/
	public function property_edit()
	{
		$id=$this->uri->segment(3);
		$data['title']="Edit Property";
		$data['resultLoc']=$this->Setting_model->get_location();
		$data['resultPro']=$this->Venue_setting_model->get_propertyUnigue($id);
		$this->load->view('backend/add_property',$data);
	}
	
	
	/************** update property *********************/
	public function property_update()
	{
		   $id=$this->uri->segment(3);
		  /************** key distance *********************/
			$kd_name =$_REQUEST['mytext'];
		    $ked_value   =$_REQUEST['mytextval'];
			
			/************* sitting arrangement **************/
			$s_name =$_REQUEST['mytexts'];
		    $s_capacity  =$_REQUEST['mytextsval'];
			/************* parking arrangement **************/
			$p_name =$_REQUEST['mytextp'];
			$p_type =$_REQUEST['mytextptype'];
			$p_carcap =$_REQUEST['mytextpc'];
			$p_twocap =$_REQUEST['mytextptc'];
	
			
			/************* parking arrangement **************/
			$propertyarray = array(
					
					 'location'=>$this->input->post('location'), 
					 'propertyAddr'=>$this->input->post('propertyAddr'),
					 'propertyType'=>$this->input->post('propertyType'),
					 'externalPer'=>$this->input->post('externalPer'),
					 'restrictionLimit'=>$this->input->post('restrictionLimit'),
					 'outsideSpace'=>$this->input->post('outsideSpace'),
					 'lift'=>$this->input->post('lift'),
					 'valet'=>$this->input->post('valet'),
					 'valetFacility'=>json_encode($this->input->post('valetFacility')),
					 'numFloor'=>$this->input->post('numFloor'),
					 'powerbackup'=>$this->input->post('powerbackup'),
					 'powerdetails'=>$this->input->post('powerdetails'),
					 'rainbackup'=>$this->input->post('rainbackup'),
					 'raindetails'=>$this->input->post('raindetails'),
					 'firebackup'=>$this->input->post('firebackup'),
					 'firedetails'=>$this->input->post('firedetails'),
					 'propertyName'=>$this->input->post('propertyName'),
					 'kdName'=>json_encode($kd_name),
					 'kdValue'=>json_encode($ked_value),
					 'pName'=>json_encode($p_name),
					 'pType'=>json_encode($p_type),
					 'pCarcap'=>json_encode($p_carcap),
					 'pTwocap'=>json_encode($p_twocap),
					 'sName'=>json_encode($s_name),
					 'sCapacity'=>json_encode($s_capacity)
					 );
		$this->Venue_setting_model->update_property($propertyarray,$id);
	}
	
	
	
	/************** delete property *********************/
	public function property_delete()
	{
		$id=$this->uri->segment(3);
		$this->Venue_setting_model->delete_property($id);
	}
	
}
?>
