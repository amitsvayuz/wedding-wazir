<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Abid extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
      
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/ke
        $this->load->model('admin_model');
    }

    public function abid_get()
    {
	   
		
			$username =$this->get('name');//input->post("name");
			$password = md5($this->get('email'));//input->post("email"));
			
			$result = $this->admin_model->authenticate($username,$password);
                       if(count($result)==0)
                         {
                         
                           $this->response([
                                         'code' =>ERROR,
                                         'message' =>'user id or password missmatch'
                                         ], REST_Controller::HTTP_OK);

                          }else{
			// Create session array
			$sess_array = array(
			'id' => $result->id,
			'username' => $result->user_name,
			'password' => $result->password,
			'role' => $result->role_type,
			'status' => $result->status,
			'logged_in' => TRUE
			);
			// Add user value in session
			$this->session->set_userdata($sess_array);
		        $user_id = $this->session->userdata('id');
	      
                $table='ww_admin_login';
		$this->load->model('admin_model');
		$data['result']=$this->admin_model->select_single_data($table,$user_id);
                $data['venue_result']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$user_id));
                $data['property_result']=$this->admin_model->get_data('ww_venue_property',array('user_id'=>$user_id));
                $data['prop_result']=$this->admin_model->get_data('city_setting',array('proName'=>$data['venue_result'][0]->prop_name));

              if(count($data['result'])==0){
                 $this->response([
                                         'code' => ERROR,
                                         'session_id' =>"session Expired"
                                         ], REST_Controller::HTTP_OK);
                    }else{

                $final_result=[
                      'message'=>'login successfull',
                      'sessionID'=>$user_id,
                      'full_name'=>$data['result'][0]->user_name,
                      'gender'=>$data['result'][0]->gender,
                      'dob'=>$data['result'][0]->d_o_b,
                      'email'=>$data['result'][0]->user_email,
                      'contact'=>$data['result'][0]->contact_no,
                      'avtar_url'=>$data['result'][0]->pro_pic,
                      'sp_type'=>$data['result'][0]->role_type,
                      'website_gplushurl'=>$data['result'][0]->gp_url,
                      'property_detail'=>[
                                           'company_name'=>$data['result'][0]->company_name,
                                           'brand_name'=>$data['prop_result'][0]->name,  
                                           'property_adrs'=>$data['property_result'][0]->propertyAddr,
                                           'property_name'=>$data['property_result'][0]->propertyName             
                                            ],
                      'location'=>[
                                   'country'=>$data['result'][0]->country,
                                   'state'=>$data['result'][0]->state,
			           'city'=>$data['result'][0]->city,
			           'street'=>$data['result'][0]->street,
			           'zip'=>$data['result'][0]->zip_code
                                  ]



                     ];
             
                 $this->response([
                                         'code' =>'OK',
                                         'result' =>$final_result
                                         ], REST_Controller::HTTP_OK);
                }
              }
	
 









			
	

  }


  public function planner_get()
	{
 $this->response([
                                         'status' => TRUE,
                                         'session_id' =>$user_id
                                         ], REST_Controller::HTTP_OK);
			
		               

       foreach (getallheaders() as $name => $value) {
       if($name=='session_id')
       {
         $sess_id=$value;
        }
            echo $sess_id;die;                                        }

        $user_id= session_id('id');
        if($sess_id!='')
              {
	$table='ww_admin_login';
      
		$this->load->model('admin_model');
		$data['result']=$this->admin_model->select_single_data($table,$sess_id);
                $data['venue_result']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$sess_id));
                $data['property_result']=$this->admin_model->get_data('ww_venue_property',array('user_id'=>$sess_id));
                $data['prop_result']=$this->admin_model->get_data('city_setting',array('proName'=>$data['venue_result'][0]->prop_name));

              if(count($data['result'])==0){
                 $this->response([
                                         'code' => ERROR,
                                         'session_id' =>"session Expired"
                                         ], REST_Controller::HTTP_OK);
                    }else{

                $final_result=[
                      'sessionID'=>$sess_id,
                      'full_name'=>$data['result'][0]->user_name,
                      'gender'=>$data['result'][0]->gender,
                      'dob'=>$data['result'][0]->d_o_b,
                      'email'=>$data['result'][0]->user_email,
                      'contact'=>$data['result'][0]->contact_no,
                      'avtar_url'=>$data['result'][0]->pro_pic,
                      'sp_type'=>$data['result'][0]->role_type,
                      'website_gplushurl'=>$data['result'][0]->gp_url,
                      'property_detail'=>[
                                           'company_name'=>$data['result'][0]->company_name,
                                           'brand_name'=>$data['prop_result'][0]->name,  
                                           'property_adrs'=>$data['property_result'][0]->propertyAddr,
                                           'property_name'=>$data['property_result'][0]->propertyName             
                                            ],
                      'location'=>[
                                   'country'=>$data['result'][0]->country,
                                   'state'=>$data['result'][0]->state,
			           'city'=>$data['result'][0]->city,
			           'street'=>$data['result'][0]->street,
			           'zip'=>$data['result'][0]->zip_code
                                  ]



                     ];
             
                 $this->response([
                                         'status' => TRUE,
                                         'result' =>$final_result
                                         ], REST_Controller::HTTP_OK);
                }
              }
		else
               {
              
                 $this->response([
                                         'status' => false,
                                         'session_id' =>"user login failed"
                                         ], REST_Controller::HTTP_OK);  

               }
 
	}



}
?>
