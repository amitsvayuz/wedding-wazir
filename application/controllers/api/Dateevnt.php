<?php

defined('BASEPATH') OR exit('No direct script access allowed');

error_reporting(0);
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Dateevnt extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
     //   $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
      //  $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
       // $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/ke
        $this->load->model('admin_model');
        $this->load->model('calender_model');
        $this->load->model('inquiry_model');
    }


//******************************************************************************************************************************

     public function vi_post() {
$method=__FUNCTION__;


       	foreach (getallheaders() as $name => $value)
           {
                  if($name=='session_id')
                 {
                  $sess_id=$value;
                 }
           }
         //  $as=array('mww'=>$sess_id);

           $venue_id=$this->input->post('venue_id');
           $dte=$this->input->post('cal_date');

           if($method=="vi_get"){
             $sess_id="Ko45N6GUym";
             $venue_id="VEN1091477190";
             $dte="2017-04-02";
            // echo $method."-----".$sess_id;
 }




                $table='ww_cal_slot_k';
		$this->load->model('admin_model');

                $data=$this->admin_model->get_data($table,array('sp_id'=>$sess_id,'venue_id'=>$venue_id,'start_date'=>$dte));
                $da=count($data);
                $asl=array('mww'=>$data);

        if($da==0){
           $this->response([
                                         'status' => false,
                                         'message' =>'no slot available on--0'.$dte."----1".$sess_id."---2".$venue_id
                                         ], REST_Controller::HTTP_OK);
       }else{

        $this->response([
                                         'status' => true,
                                        'message' =>'slot available',
                                         'result' =>$data
                                         ], REST_Controller::HTTP_OK);
         }

         }


 //******************************************************************************************************************************


       public function enquires_post(){

         foreach (getallheaders() as $name => $value)
          {
               if($name=='session_id')
               {
                $sess_id=$value;
              }
           }
             $venue_id=$this->input->post('venue_id');
             $slot_id=$this->input->post('slot_id');
            $data=$this->admin_model->get_data('ww_inquiry_k',array('sp_id'=>$sess_id,'venue'=>$venue_id,'slot_id'=> $slot_id));

            $da=count($data);

              if($da==0){
              $this->response([
                                         'status' => false,
                                         'mes' =>'no enquires still'
                                         ], REST_Controller::HTTP_OK);
                         }else{

             $this->response([
                                         'status' => true,
                                         'result' =>$data
                                         ], REST_Controller::HTTP_OK);
         }

         }

   //*****************************************************************************************************************************
          public function portfolio_get()
          {

            foreach (getallheaders() as $name => $value)
            {
               if($name=='session_id')
               {
                $sess_id=$value;
               }
            }

            $data=$this->admin_model->get_data('ww_portfolio',array('sp_id'=>$sess_id));

             $da=count($data);

              if($da==0){
              $this->response([
                                         'status' => false,
                                         'mes' =>'no album created till now'
                                         ], REST_Controller::HTTP_OK);
                         }else{

             $this->response([
                                         'status' => true,
                                         'result' =>$data
                                         ], REST_Controller::HTTP_OK);
         }

           }


        public function albumb_post()
          {
           $album_id=$this->input->post('album_id');
           $data=$this->admin_model->get_data('ww_portfolio_images',array('album_id'=>$album_id));
           for($i=0;$i<count($data);$i++){
           $data[$i]->image_name='http://192.168.10.121:80/weddingwazir/uploads/'.$data[$i]->image_name;


           }
           $da=count($data);

              if($da==0){
              $this->response([
                                         'status' => false,
                                         'mes' =>'no album created till now'
                                         ], REST_Controller::HTTP_OK);
                         }else{

             $this->response([
                                         'status' => true,
                                         'result' =>$data
                                         ], REST_Controller::HTTP_OK);
         }

          }
//******************************************************************************************************************************************************


//KARAN APIS


  public function serice_provider_login_post()
  {

    $method = __FUNCTION__;
    //  $username = "abid";

    $username =$this->input->post('user_name');//$this->get('user_name');//input->post("name");
    $password = md5($this->input->post('password'));//input->post("email"));

    if($method=="serice_provider_login_get"){
      $username = "karan@";
      $password = md5("123");
    }


    $result = $this->admin_model->authenticate($username,$password);
    if(count($result)==0)
    {
      //  echo "karan";
      $this->response([

        'status' =>false,
        'message' =>'user id or password missmatcSSSSh'
      ], REST_Controller::HTTP_OK);

    }else{
      // Create session array
      $sess_array = array(
        'id' => $result->id,
        'username' => $result->user_name,
        'password' => $result->password,
        'role' => $result->role_type,
        'status' => $result->status,
        'logged_in' => TRUE
      );
      // Add user value in session
      $this->session->set_userdata($sess_array);
      $user_id = $this->session->userdata('id');
      
    //   echo $user_id;
      $table='ww_admin_login';
      $this->load->model('admin_model');
      $data['result']=$this->admin_model->select_single_data($table,$user_id);
      $data['venue_result']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$user_id));
      // echo '<pre>';
      // print_r($data['venue_result']);
      $data['property_result']=$this->admin_model->get_data('ww_venue_property',array('user_id'=>$user_id));
      //echo "got data2<br>";
      $data['prop_result']=$this->admin_model->get_data('city_setting',array('proName'=>$data['venue_result'][0]->prop_name));
      //echo "got data3<br>";
      if(count($data['result'])==0){
        $this->response([
          'status' => false,
          'message' =>"session Expired"
        ], REST_Controller::HTTP_OK);
      }else{

        $final_result=[
          'message'=>'login successfull',
          'sessionID'=>$user_id,
          'full_name'=>$data['result'][0]->user_name,
          'gender'=>$data['result'][0]->gender,
          'dob'=>$data['result'][0]->d_o_b,
          'email'=>$data['result'][0]->user_email,
          'contact'=>$data['result'][0]->contact_no,
          'avtar_url'=>$data['result'][0]->pro_pic,
          'sp_type'=>$data['result'][0]->role_type,
          'website_gplushurl'=>$data['result'][0]->gp_url,
          'property_detail'=>[
            'company_name'=>$data['result'][0]->company_name,
            'brand_name'=>$data['prop_result'][0]->name,
            'property_adrs'=>$data['property_result'][0]->propertyAddr,
            'property_name'=>$data['property_result'][0]->propertyName
          ],
          'location'=>[
            'country'=>$data['result'][0]->country,
            'state'=>$data['result'][0]->state,
            'city'=>$data['result'][0]->city,
            'street'=>$data['result'][0]->street,
            'zip'=>$data['result'][0]->zip_code
          ]



        ];

        $this->response([
          'status' =>TRUE,
          'result' =>$final_result
        ], REST_Controller::HTTP_OK);
      }
    }
  }


//**************************************************************************************************************************************************************************
  public function venue_post()
  {

  
    $method = __FUNCTION__;


    $dte=$this->input->post('booking_status');
    $booking_status=$this->input->post('booking_status');


    if($method=="venue_get"){
      $sess_id="KARAN95";
      $date="2017-04-15";
    }
    
    $mystring="";
    foreach (getallheaders() as $name => $value)
    {
  $mystring=$mystring."---".$name."=".$value;
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
  
    $sess_id=$this->input->post('session_id');
    $date =$this->input->post('date');

    $x="karan";
    $y="arjun";
    
     //$sess_id="KARAN95";
      //$date="2017-04-15";
  
    if($sess_id!='')
    {
      $table='ww_admin_login';
      $this->load->model('admin_model');
      $data=$this->admin_model->get_data('ww_venue2',array('sp_id'=>$sess_id));
      $final_result = array();
      for($i=0;$i<count($data);$i++){
        $data2=$this->admin_model->get_slots('ww_cal_slot_k',array('venue_id'=>$data[$i]->v_id,'start_date'=>$date));




        array_push($final_result, array(
          "v_id"=>$data[$i]->v_id,
          "v_image"=>$data[$i]->v_image,
          "v_name"=>$data[$i]->v_name,
          "v_dinein"=>$data[$i]->v_dinein,
          "sittingCapecity"=>$data[$i]->sittingCapecity,
          "v_nature"=>$data[$i]->v_nature,
          "venue_data"=>json_encode($data2)

        ));

      }


      if(count($final_result)==0)
      {
        $this->response(json_encode([
          'status' => false,
          'message' =>"session Expired or missmatch"
        ]), REST_Controller::HTTP_OK);
      }else

      {
        $this->response(json_encode([
          'status' => true,
          'result' =>$final_result,
          'message' =>"Success"
        ]), REST_Controller::HTTP_OK);
      }
    }
    else
    {

      $this->response(json_encode([
        'status' => false,
        'message' =>"doesn't get your session--".$sess_id."--".$date."--".$mystring
      ]), REST_Controller::HTTP_OK);

    }

  }


//**************************************************************************************************************************************************************************

public function date_events2_post(){
    $method=__FUNCTION__;

    $no_entry="false";

    foreach (getallheaders() as $name => $value)
    {

      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }

    // $venue_id=VEN1091477190;
    // $all_date="2017-04-01";
    // $sess_id='Ko45N6GUym';
 $sess_id=$this->input->post('session_id');
    $venue_id =$this->input->post('venue_id');
    $mont_yr =$this->input->post('mont_yr');
    //  $all_date="2017-04-01";

    $each_date;

    if ($method=="date_events2_get") {
      $venue_id="VEN1AIR";
      $mont_yr="04-2017";
      $sess_id='KARAN95';
    }
    //echo "mont_yr:".$mont_yr."\n";
    $whole_date =  explode("-", $mont_yr);
    $month = $whole_date[0];
    $year = $whole_date[1];
    $days_in_month;
    $staus;
    $isLeapYear = (bool) date('L', strtotime("$year-01-01"));

    switch($month){
      case 02:
      if($isLeapYear){
        $days_in_month=29;
      }
      else{
        $days_in_month=28;
      }
      break;

      case 04:
      $days_in_month=30;
      break;

      case 06:
      $days_in_month=30;
      break;

      case 09:
      $days_in_month=30;
      break;

      case 11:
      $days_in_month=30;
      break;

      default:
      $days_in_month=31;
    }


    $data2['main']=$this->admin_model->get_data_like('ww_date_check',array('date' => $year."-".$month,'venue_id'=>$venue_id ));
    $final_result = array();



    for($i=1;$i<=$days_in_month;$i++){
      $day=$i;
      if($i<10){
        $day="0".$i;
      }
      $each_date=$year."-".$month."-".$day;

      $data['result']=$this->admin_model->get_data('ww_cal_slot_k',array('start_date' => $each_date,'venue_id'=>$venue_id ));
      $data['result2']=$this->admin_model->get_data('ww_cal_slot_k',array('start_date' => $each_date,'booking_status'=>'BKD','venue_id'=>$venue_id ));

      $data['result3']=$this->admin_model->get_data('ww_inquiry_k',array('created_date' => $each_date,'venue_id'=>$venue_id,'sp_id'=>$sess_id ));



      $slot_no=count($data['result']);
      $bkd_slot_no=count($data['result2']);
      $inq_no=count($data['result3']);

      if($slot_no==0){
        $slot_no=3;
      }
      $each_date=$year."-".$month."-".$day;



      $result = $slot_no-$bkd_slot_no;
      //  echo $slot_no."-".$bkd_slot_no."=".$result."\n";
      $booking_status="AVL";
      if($result==0){
        $booking_status="BKD";
      }
      elseif ($result<$slot_no) {
        $booking_status="P_AVL";
      }
      elseif ($result==$slot_no) {
        $booking_status="AVL";
      }



      $k_count=count($data2['main']);
      for($t=0;$t<$k_count;$t++){
        $date_from_database=$data2['main'][$t]->date;

        $event_date="karan";
        if($each_date===$date_from_database)
        {

          $event_date=$date_from_database;
          //  echo "karan95";
          $is_auspicious=$data2['main'][$t]->is_auspicious;
          $is_dry=$data2['main'][$t]->is_dry;
          $is_under_renovation=$data2['main'][$t]->is_under_renovation;
          break;
        }else{
          $event_date="karan";

          $is_auspicious=$no_entry;
          $is_dry=$no_entry;
          $is_under_renovation=$no_entry;
        }
      }

      if($booking_status!=="AVL"||
      $is_auspicious=="true"||
      $is_dry=="true"||
      $is_under_renovation=="true"||
      $inq_no!==0  ){
        array_push($final_result, array(
          "event_date"=>$each_date,
          "booking_status"=>$booking_status,
          "is_auspicious"=>$is_auspicious,
          "is_dry"=>$is_dry,
          "is_under_renovation"=>$is_under_renovation,
          "inq_no"=>$inq_no
        ));
      }
    }


    if(count($final_result)==0){
      $this->response([
        'status' => false,
        'message' =>"session Expired-".$mont_yr."\n"."venue_id-".$venue_id
      ], REST_Controller::HTTP_OK);
    }else{


      $this->response([
        'status' =>TRUE,
        'result' =>$final_result,
        'message' =>"venue_id-".$venue_id."\n"."mont_yr-".$mont_yr
      ], REST_Controller::HTTP_OK);
    }

  }



//**************************************************************************************************************************************************************************

  public function get_slots_post() {
    $method=__FUNCTION__;


    foreach (getallheaders() as $name => $value)
    {
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
    //  $as=array('mww'=>$sess_id);

 $sess_id=$this->input->post('session_id');
    $venue_id=$this->input->post('venue_id');
    $dte=$this->input->post('cal_date');

    if($method=="get_slots_get"){
      $sess_id="Ko45N6GUym";
      $venue_id="VEN1091477190";
      $dte="2017-04-02";
      // echo $method."-----".$sess_id;
    }


    $table='ww_cal_slot_k';
    $this->load->model('admin_model');

    $data=$this->admin_model->get_slots($table,array('sp_id'=>$sess_id,'venue_id'=>$venue_id,'start_date'=>$dte));
    $da=count($data);
    $asl=array('mww'=>$data);

    if($da==0){
      $this->response([
        'status' => false,
        'message' =>'no slot available on--0--'.$dte."----1".$sess_id."---2".$venue_id
      ], REST_Controller::HTTP_OK);
    }else{

      $this->response([
        'status' => true,
        'message' =>'slot available',
        'result' =>$data
      ], REST_Controller::HTTP_OK);
    }

  }


//**************************************************************************************************************************************************************************
  public function addslot_post()
  {


    $method = __FUNCTION__;
    $this->load->model('calender_model');
    foreach (getallheaders() as $name => $value)
    {
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }

    $sess_id=$this->input->post('session_id');
    $venue_id=$this->input->post('venue_id');
    $slot_title=$this->input->post('slot_title');
    $start_time=$this->input->post('start_time');
    $end_time=$this->input->post('end_time');
    $cat_stat=$this->input->post('cat_stat');
    $created_date=$this->input->post('created_date');
    $desc=$this->input->post('desc');

    if($method=="addslot_get"){
      $sess_id="KARAN95";
      $venue_id="VEN1091477190";
      $slot_title="Static Title";
      $start_time="12:30:00";
      $end_time="15:36:20";
      $cat_stat="Reserve";
      $created_date="Bruce";
      $desc="Bruce created this.";
    }
    //$sess_id="Ko45N6GUym";

    $arrSlot =array(
      'sp_id'      => $sess_id,
      'slot_level' 		=> "date",
      'start_date'    		=>$created_date,
      'start_time'  		=> $start_time,
      'end_time'    		=> $end_time,


      'description'      		=> $desc,

      'booking_status'   		=> $cat_stat,
      'slot_name'     		=> $slot_title,

      'venue_id'				=>$venue_id
    );



    $tableSlot="ww_cal_slot_k";

    $result=$this->calender_model->insert_slot2($tableSlot,$arrSlot);
    if($result=="success"){
      $this->response([
        'status' => true,
        'message' =>"Successfully added to the DataBase.".$sess_id
      ], REST_Controller::HTTP_OK);
    }
    else{
      $this->response([
        'status' => false,
        'message' =>"Failed to add to the Database."
      ], REST_Controller::HTTP_OK);
    }
  }

//**************************************************************************************************************************************************************************
public function editslot_post()
  {

    $method = __FUNCTION__;
    $this->load->model('calender_model');
    foreach (getallheaders() as $name => $value)
    {
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
      $sess_id=$this->input->post('session_id');
    $slot_id=$this->input->post('slot_id');
    $venue_id=$this->input->post('venue_id');
    $slot_title=$this->input->post('slot_title');
    $start_time=$this->input->post('start_time');
    $end_time=$this->input->post('end_time');
    $cat_stat=$this->input->post('cat_stat');
    $created_date=$this->input->post('created_date');
    $desc=$this->input->post('desc');

    if($method=="addslot_get"){
      $sess_id="KARAN95";
      $venue_id="VEN1091477190";
      $slot_title="Static Title";
      $start_time="12:30:00";
      $end_time="15:36:20";
      $cat_stat="Reserve";
      $created_date="Bruce";
      $desc="Bruce created this.";
    }
    //  $sess_id="Ko45N6GUym";

    $arrSlot =array(
      'sp_id'      => $sess_id,
      'slot_level' 		=> "date",
      'start_date'    		=>$created_date,
      'start_time'  		=> $start_time,
      'end_time'    		=> $end_time,
      'description'      		=> $desc,
      'booking_status'   		=> $cat_stat,
      'slot_name'     		=> $slot_title,
      'venue_id'		=>$venue_id
    );




    $tableSlot="ww_cal_slot_k";

    $result=$this->calender_model->update_slot2($tableSlot,$arrSlot,$slot_id);
    if($result=="success"){
      $this->response([
        'status' => true,
        //'message' =>$sess_id."id kk"
        'message' =>"Successfully added to the DataBase.--".$created_date
      ], REST_Controller::HTTP_OK);
    }
    else{
      $this->response([
        'status' => false,
        'message' =>"Failed to add to the Database."
      ], REST_Controller::HTTP_OK);
    }
  }

//**************************************************************************************************************************************************************************
  public function inquiries_on_slot_venue_date_post(){


    $method=__FUNCTION__;
    foreach (getallheaders() as $name => $value)
    {
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
      $sess_id=$this->input->post('session_id');
    $slot_id=$this->input->post('slot_id');
    $venue_id=$this->input->post('venue_id');
    $date=$this->input->post('date');

    if($method=="inquiries_on_slot_venue_date_get"){
      $sess_id="Ko45N6GUym";
      $slot_id="25";
      $venue_id="VEN1091477190";
      $date="2017-04-02";
    }
    //  $date="2017-04-02";
    $data=$this->admin_model->get_inq('ww_inquiry_k',array('sp_id'=>$sess_id,'slot_id'=>$slot_id,'venue_id'=>$venue_id,'created_date'=>$date));

    //$data=$this->admin_model->get_data('ww_inquiry_k',array('sp_id'=>$sess_id,'slot_id'=>$slot_id,'venue_id'=>$venue_id,'created_date'=>$data));
    $da=count($data);

    if($da==0){
      $this->response([
        'status' => false,
        'mes' =>'no enquiries with session id--'.$sess_id
      ], REST_Controller::HTTP_OK);
    }else{

      $this->response([
        'status' => true,
        'result' =>$data
      ], REST_Controller::HTTP_OK);
    }

  }
//**************************************************************************************************************************************************************************
public function inquiries_by_keyword_post(){


    $method=__FUNCTION__;
    foreach (getallheaders() as $name => $value)
    {
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
     $sess_id=$this->input->post('session_id');
    $keyword=$this->input->post('keyword');


    if($method=="inquiries_by_keyword_get"){
      $sess_id="KARAN95";
      $keyword="robin";
    }
    //  $date="2017-04-02";
    $final_data =   array();
    $data=$this->admin_model->get_data_like('ww_inquiry_k',array('sp_id'=>$sess_id,'name'=>$keyword));
    $data2=$this->admin_model->get_data_like('ww_inquiry_k',array('sp_id'=>$sess_id,'contact_num'=>$keyword));
    $data3=$this->admin_model->get_data_like('ww_inquiry_k',array('sp_id'=>$sess_id,'email'=>$keyword));
    if($data!=null){
      array_push($final_data,$data);
    }

    if($data2!=null){
      array_push($final_data,$data2);
    }
    // array_push($data,$data3);
    //$data=$this->admin_model->get_data('ww_inquiry_k',array('sp_id'=>$sess_id,'slot_id'=>$slot_id,'venue_id'=>$venue_id,'created_date'=>$data));
    if($data3!=null){
      array_push($final_data,$data3);
    }
    $da=count($final_data);
    if($da==0){
      $this->response([
        'status' => false,
        'mes' =>'no enquiries with session id--'.$sess_id
      ], REST_Controller::HTTP_OK);
    }else{

      $this->response([
        'status' => true,
        'result' =>$final_data[0]
      ], REST_Controller::HTTP_OK);
    }

  }

//**************************************************************************************************************************************************************************
public function all_inquiries_post(){


    $method=__FUNCTION__;
    foreach (getallheaders() as $name => $value)
    {
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
      $sess_id=$this->input->post('session_id');
    $booking_status=$this->input->post('booking_status');
    
      $sess_id="KARAN95";
    $booking_status="ALL";


    if($method=="all_inquiries_get"){
      $sess_id="KARAN95";
      $slot_id="25";
      $venue_id="VEN1091477190";
      $date="2017-04-02";
    }
   
    
    //  $date="2017-04-02";
    switch ($booking_status) {

      case 'BKD':
      $data=$this->admin_model->get_inq('ww_inquiry_k',array('sp_id'=>$sess_id,'booking_status'=>$booking_status));
      break;

      case 'CLD':
      $data=$this->admin_model->get_inq('ww_inquiry_k',array('sp_id'=>$sess_id,'booking_status'=>$booking_status));
      break;

      default:
      $data=$this->admin_model->get_inq('ww_inquiry_k',array('sp_id'=>$sess_id));
      break;

    }

    //$data=$this->admin_model->get_data('ww_inquiry_k',array('sp_id'=>$sess_id,'slot_id'=>$slot_id,'venue_id'=>$venue_id,'created_date'=>$data));
    $da=count($data);

    if($da==0){
      $this->response([
        'status' => false,
        'mes' =>'no enquiries with session id--'.$sess_id
      ], REST_Controller::HTTP_OK);
    }else{

      $this->response([
        'status' => true,
        'result' =>$data,
        'mes' =>'enquiries found with session id--'.$sess_id
      ], REST_Controller::HTTP_OK);
    }

  }


//**************************************************************************************************************************************************************************

 public function all_inquiries2_post(){


    $method=__FUNCTION__;
    foreach (getallheaders() as $name => $value)
    {
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
      $sess_id=$this->input->post('session_id');
    $slot_id=$this->input->post('slot_id');
    $venue_id=$this->input->post('venue_id');
    $date=$this->input->post('date');

    if($method=="all_inquiries2_get"){
      $sess_id="Ko45N6GUym";
      $slot_id="25";
      $venue_id="VEN1091477190";
      $date="2017-04-02";
    }
    //  $date="2017-04-02";
    $data=$this->admin_model->get_inq('ww_inquiry_k',array('sp_id'=>$sess_id));

    //$data=$this->admin_model->get_data('ww_inquiry_k',array('sp_id'=>$sess_id,'slot_id'=>$slot_id,'venue_id'=>$venue_id,'created_date'=>$data));
    $da=count($data);

    if($da==0){
      $this->response([
        'status' => false,
        'mes' =>'no enquiries with session id--'.$sess_id
      ], REST_Controller::HTTP_OK);
    }else{

      $this->response([
        'status' => true,
        'result' =>$data
      ], REST_Controller::HTTP_OK);
    }

  }
//**************************************************************************************************************************************************************************

public function leadsbyleadaction_post(){

    $method=__FUNCTION__;



    foreach (getallheaders() as $name => $value)
    {
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
    
    $sess_id=$this->input->post('session_id');
    $lead_action=$this->input->post('lead_action');

    if($method=="leadsbyleadaction_get"){
      $sess_id="KARAN95";
      $lead_action="ALL";
    }

 $sess_id="KARAN95";
      $lead_action="ALL";

    if($lead_action=="ALL"){
      $data=$this->admin_model->get_data('ww_lead_k',array('sp_id'=>$sess_id));
    }
    else {
      $data=$this->admin_model->get_data('ww_lead_k',array('sp_id'=>$sess_id,'lead_action'=>$lead_action));
    }
    $da=count($data);

    if($da==0){
      $this->response([
        'status' => false,
        'mes' =>'no enquires still'
      ], REST_Controller::HTTP_OK);
    }else{

      $this->response([
        'status' => true,
        'result' =>$data
      ], REST_Controller::HTTP_OK);
    }

  }
//**************************************************************************************************************************************************************************

public function minimumgaurinty_post(){
  $this->load->model('calender_model');
  foreach (getallheaders() as $name => $value)
  {
    if($name=='session_id')
    {
      $sess_id=$value;
    }
  }

$sess_id=$this->input->post('session_id');
  $slot_id=$this->input->post('slot_id');
  $mg_value=$this->input->post('mg_value');
  $arrSlot =array('min_gaurantee'=> $mg_value);

  $tableSlot="ww_cal_slot_k";

  $result=$this->calender_model->update_slot2($tableSlot,$arrSlot,$slot_id);
  if($result=="success"){
    $this->response([
      'status' => true,
      //'message' =>$sess_id."id kk"
      'message' =>"Successfully added to the DataBase."
    ], REST_Controller::HTTP_OK);
  }
  else{
    $this->response([
      'status' => false,
      'message' =>"Failed to add to the Database."
    ], REST_Controller::HTTP_OK);
  }

}




//**************************************************************************************************************************************************************************

  public function change_slot_status_post(){
    $this->load->model('calender_model');
    foreach (getallheaders() as $name => $value)
    {
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
$sess_id=$this->input->post('session_id');
    $slot_id=$this->input->post('slot_id');
    $booking_status=$this->input->post('booking_status');
    $arrSlot =array('booking_status'=> $booking_status);

    $tableSlot="ww_cal_slot_k";

    $result=$this->calender_model->update_slot2($tableSlot,$arrSlot,$slot_id);
    if($result=="success"){
      $this->response([
        'status' => true,
        //'message' =>$sess_id."id kk"
        'message' =>"Successfully added to the DataBase."
      ], REST_Controller::HTTP_OK);
    }
    else{
      $this->response([
        'status' => false,
        'message' =>"Failed to add to the Database."
      ], REST_Controller::HTTP_OK);
    }

  }


//**************************************************************************************************************************************************************************


  public function get_AllAblums_post()
  {


    //  $date =$this->input->post('date');
    $method = __FUNCTION__;


  
   
    foreach (getallheaders() as $name => $value)
    {

      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
    //    $sess_id="Ko45N6GUym";
    // $date="2017-04-07";
    
     $sess_id=$this->input->post('session_id');
     
       if($method=="get_AllAblums_get"){
      $sess_id="KARAN95";
  }
    
    
    if($sess_id!='')
    {
      $table='ww_portfolio';
      $this->load->model('admin_model');
      $final_result=$this->admin_model->get_data($table,array('sp_id'=>$sess_id));



      if(count($final_result)==0)
      {
        $this->response([
          'status' => false,
          'message' =>"session Expired or missmatch"
        ], REST_Controller::HTTP_OK);
      }else

      {
        $this->response([
          'status' => true,
          'result' =>$final_result,
          'message' =>"Success"
        ], REST_Controller::HTTP_OK);
      }
    }
    else
    {

      $this->response([
        'status' => false,
        'message' =>"doesn't get your session"
      ], REST_Controller::HTTP_OK);

    }

  }

//**************************************************************************************************************************************************************************
  public function send_proposal_post(){
 $sess_id=$this->input->post('session_id');
    $cus_name=$this->input->post('cus_name[]');

    if(count($cus_name)==0)
    {
      $this->response([
        'status' => false,
        'message' =>"session Expired or missmatch"
      ], REST_Controller::HTTP_OK);
    }else

    {
      $this->response([
        'status' => true,
        'message' =>"Success".$cus_name[0]."---".$cus_name[1]
      ], REST_Controller::HTTP_OK);
    }

  }

//**************************************************************************************************************************************************************************
  public function cus_inquiries_post(){


    $method=__FUNCTION__;
    foreach (getallheaders() as $name => $value)
    {
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
     $sess_id=$this->input->post('session_id');
    $customer_name=$this->input->post('customer_name');


    if($method=="cus_inquiries_get"){
      $sess_id="KARAN95";
      $customer_name="Test Inq";
    }
    //  $date="2017-04-02";

    if($customer_name=="ALL"){
      $data=$this->admin_model->get_inq_asc('ww_inquiry_k',array('sp_id'=>$sess_id));
    }
    else{
      $data=$this->admin_model->get_inq_asc('ww_inquiry_k',array('sp_id'=>$sess_id,'name'=>trim($customer_name)));
    }




    if(count($data)==0){
      $this->response([
        'status' => false,
        'mes' =>'no enquiries with session id--'.$sess_id
      ], REST_Controller::HTTP_OK);
    }else{

      $this->response([
        'status' => true,
        'result' =>$data
      ], REST_Controller::HTTP_OK);
    }

  }

//**************************************************************************************************************************************************************************
public function add_inquiry_post()
  {
    $this->load->model('inquiry_model');
    foreach (getallheaders() as $name => $value)
    {

      $method = __FUNCTION__;
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }
$sess_id=$this->input->post('session_id');
    $date_enq = $this->input->post('date_enq');
    $venue_id = $this->input->post('venue_id');
    $venue_name = $this->input->post('venue_name');
    $slot_id = $this->input->post('slot_id');
    $cust_name = $this->input->post('cust_name');
    $cust_email = $this->input->post('cust_email');
    $contact_no = $this->input->post('contact_no');
    $event_desc = $this->input->post('event_desc');
    $guest_range = $this->input->post('guest_range');
    $food_type = $this->input->post('food_type');


    if($method == "add_inquiry_get"){
      $sess_id = "Ko45N6GUym";
      $date_enq = "2017-03-02";
      $venue_id = "VEN1091477190";
      $slot_id ="25";
      $cust_name = "Rajesh";
      $cust_email = "rj@gmail.com";
      $contact_no = "4567891235";
      $event_desc = "COCKTAIL PARTIES";
      $guest_range = "400-500";
      $food_type = "veg";


    }

    $inquiryarray = array(
      'inq_id'=>'INQ'.rand(),
      'sp_id' =>$sess_id,
      'venue_name' => $venue_name,
      'name' =>trim($cust_name),
      'email' =>trim($cust_email),
      'created_date' =>$date_enq,
      'guest_range' =>trim($guest_range),
      'decs' =>trim($event_desc),
      'status' =>0,
      'venue_id' =>$venue_id,
      'slot_id' =>$slot_id,
      'contact_num' =>$contact_no,
      'food_type' => $food_type
    );


    $data=$this->inquiry_model->insert_inquiry_k($inquiryarray);

    $dte='2016-05-27 00:00:00';
    // $data['events']=$this->admin_model->get_data('ww_cal_slot',array('sp_id'=>$sess_id,'start'=>$date_enq,'venue'=>$venue_id));
    // print_r($data);

    if($data=="fail")
    {

      $this->response([
        'status' => false,
        'message' =>'oops something wrong'
      ], REST_Controller::HTTP_OK);


    }elseif($data=="success"){

      $this->response([
        'status' => true,
        'message' =>'enquiry successfully send........'
      ], REST_Controller::HTTP_OK);
    }
  }
//**************************************************************************************************************************************************************************


}
?>
