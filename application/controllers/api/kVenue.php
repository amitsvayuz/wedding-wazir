<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
* This is an example of a few basic user interaction methods you could use
* all done with a hardcoded array
*
* @package         CodeIgniter
* @subpackage      Rest Server
* @category        Controller
* @author          Phil Sturgeon, Chris Kacerguis
* @license         MIT
* @link            https://github.com/chriskacerguis/codeigniter-restserver
*/
class kVenue extends REST_Controller {

  function __construct()
  {
    // Construct the parent class
    parent::__construct();

    // Configure limits on our controller methods
    // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
    $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
    $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
    $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/ke
    $this->load->model('admin_model');
    error_reporting(0);
  }

  public function getAvailableVenue_post()
  {

    foreach (getallheaders() as $name => $value)
    {
      if($name=='session_id')
      {
        $sess_id=$value;
      }
    }

    $dtea =$this->input->post('check');
    $data['events']=$this->admin_model->get_data('ww_cal_slot2',array('sp_id'=>$sess_id,'start_date'=>$dtea,'type'=>'EVT','booking'=>'Available'));
    $data['venue']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$sess_id));
    $cnt=count($data['events']);
    $cntvn=count($data['venue']);

    $k=0;
    for($i=0;$i<count($data['venue']);$i++)
    {
      for($j=0;$j<count($data['events']);$j++){
        if(($data['venue'][$i]->v_id)==($data['events'][$j]->venue))
        {

          $data['available_venue'][$k]=$data['venue'][$i];
          $k++;
          break;
        }


      }
    }
    if( $cnt==0){
      $this->response([
        'status' => false,
        'message' =>'no event found at this date'
        //'result' =>$data['available_venue']
      ], REST_Controller::HTTP_OK);}

      else{
        $this->response([
          'status' => true,
          'result' =>$data['available_venue']
        ], REST_Controller::HTTP_OK);

      }


    }

    public function getAvailableVenue2_post()
    {

      foreach (getallheaders() as $name => $value)
      {
        if($name=='session_id')
        {
          $sess_id=$value;
        }
      }

      $dtea =$this->input->post('check');
      $data['venue']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$sess_id,'start_date'=>$dtea,'avail_status'=>'BKD'));
    //  $data['venue']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$sess_id));
      $cnt=count($data['venue']);
      //$cntvn=count($data['venue']);


      if( $cnt==0){
        $this->response([
          'status' => false,
          'message' =>'no event found at this date'
          //'result' =>$data['available_venue']
        ], REST_Controller::HTTP_OK);}

        else{
          $this->response([
            'status' => true,
            'result' =>$data['venue']
          ], REST_Controller::HTTP_OK);

        }


      }



}
?>
