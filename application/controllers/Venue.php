<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venue extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Venue_model');
		$this->load->model('Setting_model');
		$this->load->model('admin_model');
		$this->load->model('Venue_setting_model');
	}
	public function index()
	{
		//$data['title']="Sign In";
		//$this->load->view('backend/login',$data);
	}
/****************Venue Manage****************/	
	public function add_venue()
	{
		$this->form_validation->set_rules('venueName', 'Venue ', 'required');
		$this->form_validation->set_rules('propName', 'Property ', 'required');
		$this->form_validation->set_rules('cityName', 'City', 'required');
		$this->form_validation->set_rules('brandName', 'Brand', 'required');
		$this->form_validation->set_rules('location', 'Location', 'required');
		$this->form_validation->set_rules('beltName', 'Belt', 'required');
		$this->form_validation->set_rules('venueAddr', 'Description', 'required');
		if ($this->form_validation->run() == FALSE)
		{  
			$data['title']="Add Venue";
			$data['result']=$this->Venue_setting_model->get_venue_company();
            $data['resultPro']=$this->Venue_setting_model->get_pro();
			$data['resultbelt']=$this->Venue_setting_model->get_beltt();
			$this->load->view('backend/add_venue',$data);
		}
		else
		{
			
			$user_id = $this->session->userdata('id');
			$venuearray = array(
					 'v_id'=>'VEN'.rand(),
					 'sp_id'=>$user_id,
                     'pvName'=>$this->input->post('pvName'),
					 'prop_name'=>$this->input->post('propName'),
					 'brand_name'=>$this->input->post('brandName'),
					 'city_name'=>$this->input->post('cityName'),
					 'location'=>$this->input->post('location'),
					 'belt'=>$this->input->post('beltName'),
					 'v_name'=>$this->input->post('venueName'),
					 'v_addr'=>$this->input->post('venueAddr'),
					 'v_nature'=>$this->input->post('natureType'),
					 'v_type'=>$this->input->post('aType'),
					 'v_type_value'=>json_encode($this->input->post('venueType')),
					 'v_dinein'=>$this->input->post('dineIn'),
					 'v_transport'=>$this->input->post('Transport'),
					 'v_floor'=>$this->input->post('floor'),
					 'v_lighting'=>$this->input->post('light'),
					 'v_fencing'=>$this->input->post('fencing'),
					 'v_dresser'=>$this->input->post('dresser'),
					 'v_suite_type'=>$this->input->post('suiteType'),
					 'v_washroom'=>$this->input->post('washRoom'),
					 
					 'v_accommodate'=>$this->input->post('addAccomm'),
					 'v_licencebar'=>$this->input->post('licensedBar'),
					 'v_licencebar_late'=>$this->input->post('licensedBarLate'),
					 'v_wedding_event_list'=>json_encode($this->input->post('weddingList')),
					 'v_dj'=>$this->input->post('dj'),
					 'v_dol'=>$this->input->post('dhol'),
					 'v_shenai'=>$this->input->post('shenai'),
					 'v_band'=>$this->input->post('band'),
					 'v_stage'=>$this->input->post('stage'),
					 'v_decor'=>$this->input->post('decor'),
					 'v_florist'=>$this->input->post('florist'),
					 'v_light'=>$this->input->post('lighting'),
					 
					 'v_event'=>$this->input->post('events'),
					 'v_photographer'=>$this->input->post('photo'),
					 'v_videographer'=>$this->input->post('video'),
					 'v_ghodi'=>$this->input->post('ghodi'),
					 'v_palki'=>$this->input->post('doli'),
					 'v_artist'=>$this->input->post('artist'),
					 'v_create_date'=>strtotime(date('Y-m-d')),
					 'v_status'=>0
					 );
					 //echo '<pre>';
					 //print_r($venuearray);die;
		$tablename ='ww_venue';
		
		$this->Venue_model->add_venue($tablename,$venuearray);
		}
	}
	
	public function venue_update()
	{
		$id=$this->uri->segment(3);
		$venuearray = array(
					 'prop_name'=>$this->input->post('propName'),
                     'pvName'=>$this->input->post('pvName'),
					 'brand_name'=>$this->input->post('brandName'),
					 'city_name'=>$this->input->post('cityName'),
					 'location'=>$this->input->post('location'),
					 'belt'=>$this->input->post('beltName'),
					 'v_name'=>$this->input->post('venueName'),
					 'v_addr'=>$this->input->post('venueAddr'),
					 'v_nature'=>$this->input->post('natureType'),
					 'v_type'=>$this->input->post('aType'),
					 'v_type_value'=>json_encode($this->input->post('venueType')),
					 'v_dinein'=>$this->input->post('dineIn'),
					 'v_transport'=>$this->input->post('Transport'),
					 'v_floor'=>$this->input->post('floor'),
					 'v_lighting'=>$this->input->post('light'),
					 'v_fencing'=>$this->input->post('fencing'),
					 'v_dresser'=>$this->input->post('dresser'),
					 'v_suite_type'=>$this->input->post('suiteType'),
					 'v_washroom'=>$this->input->post('washRoom'),
					 
					 'v_accommodate'=>$this->input->post('addAccomm'),
					 'v_licencebar'=>$this->input->post('licensedBar'),
					 'v_licencebar_late'=>$this->input->post('licensedBarLate'),
					 'v_wedding_event_list'=>json_encode($this->input->post('weddingList')),
					 'v_dj'=>$this->input->post('dj'),
					 'v_dol'=>$this->input->post('dhol'),
					 'v_shenai'=>$this->input->post('shenai'),
					 'v_band'=>$this->input->post('band'),
					 'v_stage'=>$this->input->post('stage'),
					 'v_decor'=>$this->input->post('decor'),
					 'v_florist'=>$this->input->post('florist'),
					 'v_light'=>$this->input->post('lighting'),
					 
					 'v_event'=>$this->input->post('events'),
					 'v_photographer'=>$this->input->post('photo'),
					 'v_videographer'=>$this->input->post('video'),
					 'v_ghodi'=>$this->input->post('ghodi'),
					 'v_palki'=>$this->input->post('doli'),
					 'v_artist'=>$this->input->post('artist')
		             );
		$tablename ='ww_venue';
		$this->Venue_model->venue_update($tablename,$venuearray,$id);
	}
	
	public function venue_manager()
	{
		$data['title']="Venue Manage";
		$data['result']=$this->Venue_setting_model->get_venue_company();
		$data['resultPro']=$this->Venue_setting_model->get_pro();
		$data['vcresult'] = $this->Venue_model->get_venue();
		$this->load->view('backend/add_venue_manager',$data);
	}
	// get all brand details
	public function get_brand_ajax()
	{
		 $id=$_POST['pro_id'];
		
		$table="city_setting";
		 $data['resultpro']=$this->Venue_setting_model->get_brand_ajax($table,$id);
		$html='';
		if(isset($data['resultpro'])):
		$html.='<option value="">Select Brand</option>';
		foreach($data['resultpro'] as $row ):
		
		$html.='<option value="'.$row->id.'">'.substr($row->name,0,20).'</option>';
	    endforeach;
		echo $html;
		else:
		echo $html='data not exit';
		endif;
	}
	
	// get all city details
	public function get_city_ajax()
	{
		 $id=$_POST['pro_id'];
		
		$table="ww_company";
		 $data['resultpro']=$this->Venue_setting_model->get_city_ajax($table,$id);
                
		$html='';
		if(isset($data['resultpro'])):
		$html.='<option value="">Select City</option>';
		foreach($data['resultpro'] as $row ):
		
               
		$html.='<option value="'.$row->c_id.'">'.$row->company_name.'</option>';
             
                endforeach;
		echo $html;
		else:
		echo $html='data not exit';
		endif;
	}
	
	// get all location details
	public function get_location_ajax()
	{
		 $id=$_POST['pro_id'];
		
		$table="ww_location";
		$data['resultpro']=$this->Venue_setting_model->get_location_ajax($table,$id);
		$html='';
		if(isset($data['resultpro'])):
		$html.='<option value="">Select Location</option>';
		foreach($data['resultpro'] as $row ):
		
               
		$html.='<option value="'.$row->loc_id.'">'.$row->location.'</option>';
             
                endforeach;
		echo $html;
		else:
		echo $html='data not exit';
		endif;
	}
	// get all property details
	public function get_property_ajax()
	{
		 $id=$_POST['pro_id'];
		
		$table="ww_venue_property";
		$data['resultpro']=$this->Venue_setting_model->get_property_ajax($table,$id);
		$html='';
		if(isset($data['resultpro'])):
		$html.='<option value="">Select Property</option>';
		foreach($data['resultpro'] as $row ):
		
               
		$html.='<option value="'.$row->prop_id.'">'.$row->propertyName.'</option>';
             
                endforeach;
		echo $html;
		else:
		echo $html='data not exit';
		endif;
	}
	//add_venue_manager;
	public function add_venue_manager()
	{
		$data['title']="Add Venue";
		$data['result']=$this->Venue_setting_model->get_venue_company();
        $data['resultPro']=$this->Venue_setting_model->get_pro();
		$data['resultbelt']=$this->Venue_setting_model->get_beltt();
		$data['resultEvent']=$this->Venue_setting_model->get_events();
		$this->load->view('backend/add_venue',$data);
	}
	
	// property details;
	public function venue_details()
	{
		$id=$this->uri->segment(3);
		$data['title']="Venue Details";
		$data['result']=$this->Venue_setting_model->get_property();
        $data['resultPro']=$this->Venue_setting_model->get_pro();
		$data['resultEvent']=$this->Venue_setting_model->get_events();
		$data['editdata']=$this->Venue_model->venue_edit_get($id);
		$this->load->view('backend/venue_details',$data);
	}
	// add property;
	public function add_property()
	{
		$data['title']="Add Property";
		$data['result']=$this->Venue_setting_model->get_city();
		$this->load->view('backend/add_property',$data);
	}
	// venue_edit_get;
	public function venue_edit_get()
	{
		$id=$this->uri->segment(3);
		$data['editdata']=$this->Venue_model->venue_edit_get($id);
		$data['title']="Edit Venue";
		$data['resultEvent']=$this->Venue_setting_model->get_events();
		$data['resultbelt']=$this->Venue_setting_model->get_beltt();
		$data['result']=$this->Venue_setting_model->get_property();
		$this->load->view('backend/add_venue',$data);
		
	}
	// venue_delete;
	public function venue_delete()
	{
	     $id=$this->uri->segment(3);
	     $this->Venue_model->venue_delete($id);
		 
	}
	public function remove_images()
	{   
		$id = $_REQUEST['id'];
		$url = $_REQUEST['url'];
		
		$record_id = $this->admin_model->get_venue_last_record($id);
		$rec_id = $this->admin_model->delete_venue_last_record($record_id->id);
		if(!empty($rec_id)){
			$this->session->set_flashdata("message","Uploaded photographs removed successfully.");
		}
		$user_id = $this->session->userdata('id');
		$images = array();
		$images = explode(',',$record_id->images);
		//print_r($images);
		//print_r($record_id);die("/");
		
		for($i=0;$i<sizeof($images);$i++){
			$x = $url.'assets/uploads/photography/'.$user_id."/".$images[$i];
			unlink($url.'assets/uploads/photography/'.$user_id."/".$images[$i]);
			echo $x;
		}
		redirect("venue/image_upload_venue");
	}
	
	public function image_uploadvenue()
	{    
	    $user_id = $this->session->userdata('id');
		$upload_id = $this->admin_model->get_venue_tags($user_id);
		//echo $this->session->userdata('id');
		//print_r($upload_id->id);die('/');
		
		$title = $_POST['title'];
		$location = $_POST['location'];
		$tags = implode(',',$_POST['tags']);
		$add_multiple_images = array(
		'title' => $title,
		'location' => $location,
		'tags' => $tags
		);
		$this->admin_model->venue_photographertags($add_multiple_images , $upload_id->id);
		$this->session->set_flashdata("message","Photography Image Uploaded Successfully");
		redirect('venue/image_upload_venue');
		
	}
    
	
public function venue_customer_proposal()
	{
		//echo 'hello';
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$customer_name = $this->input->post('customer_name');
		$email = $this->input->post('email');
		$contact_number = $this->input->post('contact_number');
		$message1 = $this->input->post('message');
		$img = $this->input->post('photos');
		$selimg = $this->input->post('selimg');
		$proposalarray = array(
		                       'proposal_id'		=>rand(),
							   'sp_id'      		=>$user_id,
							   'sp_name'    		=>$data['userinfo'][0]->user_name,
							   'sp_email'   		=>$data['userinfo'][0]->user_email,
							   'customer_name'    	=>$customer_name,
							   'customer_email'    	=>$email,
							   'contact_number'    	=>$contact_number,
							   'message'    	    =>$message1,
							   'image'    			=>json_encode($selimg),
							   'send_date'			=>strtotime(date('Y-m-d')),
							   'status'  			=>0
							  );
		$table="ww_customer_proposal";
		$response=$this->admin_model->send_proposal($table,$proposalarray);
		//die;
        /********* send mail customer_name *****************/
        if($response==true):


		$to = 'vipin.vayuz@gmail.com';
        $base_url=base_url();
		$subject = 'Customer Proposal Request';
		$message = '<html><body>';
		$message = '<div style="overflow-x:auto;">';
		$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr style='background: #eee;'><td><strong>Customer Name:</strong> </td><td>" . strip_tags($customer_name) . "</td></tr>";
		$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($email) . "</td></tr>";
		$message .= "<tr><td><strong>Contact Number:</strong> </td><td>" . strip_tags($contact_number) . "</td></tr>";
		$selimg = $this->input->post('selimg');
		if (($selimg) != '') {
		
			for($i=0;$i<count($selimg);$i++)
			{
				if($i%2==0)
				{
					
					$message .= '<tr width="300px">';
					$message .= '<td width="150px"><img width="100px" height="50px" border-style="dotted" src="'.$base_url.'assets/uploadsvenue/photography/'.$selimg[$i].'" alt="proposal image" /></td>';	
					
				}
				else
				{
					
					$message .= '<td width="150px"><img width="100px" height="50px" border-style="dotted" src="'.$base_url.'assets/uploadsvenue/photography/'.$selimg[$i].'" alt="proposal image" /></td>';	
					$message .= '</tr>';
				}
				
			}
			
		}
		$curText = htmlentities($message1);           
		if (($curText) != '') {
			$message .= "<tr><td><strong>Other Content:</strong> </td><td>" . $curText . "</td></tr>";
		}
		$message .= "</table>";
		$message .= "</div>";
		$message .= "</body></html>";
		
		
		$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		//$headers .= "CC: susan@example.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        
		if(mail($to, $subject, $message, $headers))
		{
			//$data['sendvenuesucees']="Message is Successfully";
			
		}	
		$data="Message is Successfully";
		$data = base64_encode($data);
		redirect('Photographer/list_proposal?set='.$data);
		endif;
       /********  end send mail *********************/		
		
	}
	
	
public function image_upload_venue()
	{
		$data['title']="Portfolio";
		// Valid image formats 
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
		if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
		{
	    $hidden = $_POST['hidden'];
		$image = array();
		$photography = "photography";
		$user_id = $this->session->userdata('id');
		$dirname = "assets/uploadsvenue/".$photography."/";
		
		if(is_dir($dirname)){
		$file_path = $dirname;
		}else{
		$file_path = mkdir($dirname);
		$file_path = $dirname;
		}
		
		//$uploaddir = "assets/uploads/"; //Image upload directory
		foreach ($_FILES['photos']['name'] as $name => $value)
		{
		$filename = stripslashes($_FILES['photos']['name'][$name]);
		
		$size=filesize($_FILES['photos']['tmp_name'][$name]);
		//Convert extension into a lower case format
		$ext = $this->getExtension($filename);
		$ext = strtolower($ext);
		//File extension check
		if(in_array($ext,$valid_formats))
		{
		//File size check
		if ($size < (MAX_SIZE*1024))
		{
		$image_name=time().$filename;
		$image[] = $image_name;
		echo "<img src='".$hidden.$file_path.$image_name."' class='imgList'>";
		
		$newname=$file_path.$image_name;

		//Moving file to uploads folder
		if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname))
		{
		$time=time();
		//Insert upload image files names into user_uploads table
		//mysql_query("INSERT INTO user_uploads(image_name,user_id_fk,created) VALUES('$image_name','$session_id','$time')");
		}
		else
		{
		echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>'; }
		}

		else
		{
		echo '<span class="imgList">You have exceeded the size limit!</span>';
		}

		}

		else
		{
		echo '<span class="imgList">Unknown extension!</span>';
		}

		} //foreach end
		$image_multiple_name = implode(',',$image);
		$user_id = $this->session->userdata('id');
		$upload_multiple = array(
		'id' => rand(),
		'images' => $image_multiple_name,
		'date'   => date('Y-m-d H:i:s'),
		'user_id' => $user_id
		);
		 $this->admin_model->venue_photographer_tags($upload_multiple);
		 //echo 1;
        die;
		} 
		
      $this->load ->view('backend/add_venue_portfolio',$data); 
	}
/***************** Venue Manager Ends here******************/
}
