<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		error_reporting(1);

		
	}
	
	/********User Login*******/
	public function index()
	{
	 echo   $data['title']="Sign In";
		if(empty($_POST)){
			$this->load->view('backend/login',$data);

		}else{
			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));
			
			$result = $this->admin_model->authenticate($username,$password);
			
			// Create session array
			$sess_array = array(
			'id' => $result->id,
			'username' => $result->user_name,
			'password' => $result->password,
			'role' => $result->role_type,
			'status' => $result->status,
			'logged_in' => TRUE
			);
			// Add user value in session
			$this->session->set_userdata($sess_array);
			$user_id = $this->session->userdata('id');
			$status = $this->session->userdata('status');

			if(!empty($user_id)&&($status == 1)){
				redirect('sp_manager/dashboard');
			}else{
				$this->session->set_flashdata('message','Login Unsuccessful.');
				redirect('registration');
			}
		}	
	}

	
	public function logout() {
	$sess_array = array(
                'id'  =>'',
                'username' => '',
				'role' => '',
                'logged_in' => FALSE,
               );

     $this->session->unset_userdata($sess_array);
     $this->session->sess_destroy();
     $this->session->set_flashdata('message',"Logged out successfully" );
     redirect('registration','refresh');
	}
	
	
	
	
	
	public function forget_password() {
	    $data['title']="Forget Password";
		if(!empty($_POST)){
			$email = $this->input->post('email');
			$email_detail = $this->admin_model->get_password($email);
			if(!empty($email_detail)){
				redirect('registration/forgetpassword/?email='.$email);
			}else{
				$this->session->set_flashdata('message',"Email_id is not registered." );
				redirect('registration/forget_password');
			}
		}
		$this->load->view('backend/forgetpassword',$data);
	}
	
	public function user_activation() {
	    $data['title']="User Activation";
		 $user_id = $_GET['user_id'];
print_r($user_id);die("/");
		$statusArray = array(
		'status'=> 1);
		$pass = $this->admin_model->activate_account($statusArray , $user_id);
		$this->session->set_flashdata('message',"Your account activated successfully." );
		$this->load->view('backend/activation_view',$data);
		
	}
	
	
	public function forgetpassword() {
	    $data['title']="Forget Password";
		$data['email'] = $_GET['email'];
		if(!empty($_POST)){
			$email = $_GET['hidden'];
			$password = $this->input->post(new_password);
			$new_password = array(
			'password'=>md5($this->input->post(new_password))
			);
			$update_password = $this->admin_model->update_password($new_password , $email);
			if(!empty($update_password)){
				$this->load->library('email');
				$this->email->from($email);
				$this->email->to('someone@example.com');
				$this->email->subject('Password Changed');
				$this->email->message('Your password has been changed successfully.Your password is'.$password);
				$this->email->send();
				$this->session->set_flashdata('message',"Password updated successfully" );
				redirect('registration/forget_password');
			}
		}else{
			$this->load->view('backend/forget_password',$data);
		}
	}
	
}
