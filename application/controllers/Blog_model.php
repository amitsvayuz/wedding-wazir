<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_Model extends CI_Model
{
   // Photographer by Ankit Singh
   
	public function insert_blog($additional_data)
	{
		$this->db->insert('ww_blog',$additional_data);
		return $this->db->insert_id();
	}

	 public function get_blogs() {
		$this->db->select('*');
		$this->db->from('ww_blog');
		$query = $this->db->get();
		return $query->result();
	} 
	
	public function get_blog_details($id) {
	    $this->db->select('*');
		$this->db->where('id',$id);
		$this->db->from('ww_blog');
		$query = $this->db->get();
		return $query->row();
	}
    
	public function update_blog($id , $catData) {
		$this->db->where('id',$id);
	    $this->db->update('ww_blog',$catData); //echo $this->db->last_query();die;
	}
	public function deleteblogs($id) {
		$this->db->where('id',$id);
	    $this->db->delete('ww_blog'); //echo $this->db->last_query();die;
	}
}
?>
