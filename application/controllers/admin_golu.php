<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		error_reporting(0);
		
		if(empty($this->session->userdata('id'))){
			redirect('registration');
		}
	}
	
	public function index()
	{
	    $data['title']="Sign In";
		$this->load->view('backend/login',$data);
	}
	
	
	
/****************Dashboard view****************/	
	public function dashboard()
	{
		$data['title']="Dashboard";
		$this->load->view('backend/index',$data);
	}
	
	/****************Forgot password view****************/	
	public function forgetpassword()
	{
		$data['title']="Forget Password";
		$this->load->view('backend/forgetpassword',$data);
	}
	
	
	
	/******Functions done by Manoj********/
	public function customer()
	{
		$data['title']="Customer Details";
		$data['result']=$this->admin_model->select_all_customer();
		$this->load->view('backend/customerlisting',$data);
	}
	
	public function add_customer()
	{
		$data['title']="Add Customer";
		$this->load->view('backend/add_customer',$data);
		
	}
	
	public function edit_customer()
	{
      $id=$_GET['id'];	
       $customer_id=base64_decode($id);	

		$data['title']="Edit Customer";
		$data['result']=$this->admin_model->select_single_customer($customer_id);
		$this->load->view('backend/add_customer',$data);
		
	}
public function add_customer_details()
	{
			 $idfr = $this->uri->segment(3);
		//$idfr=$_GET['id'];
		$name= $this->input->post('name');
		$email= $this->input->post('email');
		$contact= $this->input->post('contact');
		$pass= $this->input->post('password');
		$dob= $this->input->post('dob');
		$dateofbirth=strtotime($dob);
		$password=md5($pass);
		$created_date=date('d/m/y');
		$this->load->helper('string');
        $customer_id =random_string('alnum',6).'_Cust_'.random_string('alnum',4).'_'.substr($name,0,3);
		$detail=array(
					  'customer_id'=>$customer_id,
					  'customer_name'=>$name,
					  'customer_email'=>$email,
					  'customer_contact'=>$contact,
					  'password'=>$password,
					  'customer_dob'=>$dateofbirth,
					  'status'=>1,
					  'creation_date'=>strtotime($created_date),
					   );
	 
	  
	    $table="ww_customer";
		$result = $this->admin_model->add_data($detail,$table);		
		if($result=1){
		$data = "Successful Customer add";
		$data = base64_encode($data);
		redirect('Admin/customer?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/add_customer?set='.$data);
		}
	}
	public function edit_customer_details()
	{
		$idfr = $this->uri->segment(3);
		$id=$_GET['id'];
		$customer_id=base64_decode($id);
		$name= $this->input->post('name');
		$email= $this->input->post('email');
		$contact= $this->input->post('contact');
		$pass= $this->input->post('password');
		$dob= $this->input->post('dob');
		$dateofbirth=strtotime($dob);
		$password=md5($pass);
		
		$detail=array(
					  'customer_name'=>$name,
					  'customer_email'=>$email,
					  'customer_contact'=>$contact,
					  'password'=>$password,
					  'customer_dob'=>$dateofbirth
					   );
		$where=array('customer_id'=>$customer_id);
		$table="ww_customer";
		$result = $this->admin_model->update_data($table,$detail,$where);		
		if($result=1){
		$data = "Successful Customer Updated";
		$data = base64_encode($data);
		redirect('Admin/customer?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/edit_customer?set='.$data);
		}
	}
	
	 public function deactive_user()
		   {
		 	$id=$this->input->post('id');
			$deactive_customer=array('status'=>0);
			$result = $this->admin_model->update_user_status($deactive_customer , $id);
		   }  
		
	public function active_user()
	{
            $id=$this->input->post('id');
			$deactive_customer=array('status'=>1);
			$result = $this->admin_model->update_user_status($deactive_customer , $id);
	}  
	
	public function deactive_customer()
	{
			$id=$this->input->post('id');
			$deactive_customer=array('status'=>0);
			$this->db->where('customer_id',$id);
			$this->db->update('ww_customer',$deactive_customer); 
	}  
	   
	public function active_customer()
	{
			$id=$this->input->post('id');
			$active_customer=array('status'=>1);
			$this->db->where('customer_id',$id);
			$this->db->update('ww_customer',$active_customer);  
	}
		   
	public function delete_customer()
	{
		$id=base64_decode($_GET['del']);
		$this->db->where('customer_id',$id);
							$this->db->delete('ww_customer'); 
							$data="Customer Deleted Successfuly";
							$data=base64_encode($data);
							redirect('Admin/customer?set='.$data);
		
	}
	
/********************Recharge History***********************/	
		public function recharge_history()
	{
		$data['title']="Recharge History";
		$this->load->model('admin_model');
		$data['sp'] = $this->admin_model->select_all_sp();
		$data['result'] = $this->admin_model->select_recharge_history();
		$this->load->view('backend/recharge_history',$data);
		
	}
	/********************Recharge Setting***********************/	
		public function recharge_setting()
	{
		$data['title']="Recharge Setting";
		$this->load->model('admin_model');
		$data['result'] = $this->admin_model->select_all_rchrgsttng();
	
		$this->load->view('backend/recharge_setting',$data);
		
	}
		public function add_recharge_setting()
	{
		$data['title']="Add Recharge Setting";
		
	
		$this->load->view('backend/add_recharge_setting',$data);
		
	}
	public function edit_recharge_setting()
	{
		 $id=$_GET['id'];	
       $setting_id=base64_decode($id);
		$data['title']="Edit Recharge Setting";
		$data['result']=$this->admin_model->select_single_recharge_setting($setting_id);
	
		$this->load->view('backend/add_recharge_setting',$data);
		
	}
	public function add_recharge_setting_details()
	{
			 $idfr = $this->uri->segment(3);
		//$idfr=$_GET['id'];
		$name= $this->input->post('setting_name');
	
		$percentage= $this->input->post('percentage');
		$created_date=date('Y-m-d');
		
		$detail=array(
	  
	  'setting_name'=>$name,
	  'percentage'=>$percentage,
	  'status'=>1,
	  'creation_date'=>strtotime($created_date)
	   );
	   
	  
	   $table="ww_recharge_setting";
		$result = $this->admin_model->add_data($detail,$table);		
		if($result=1){
		$data = "Successful Setting add";
		$data = base64_encode($data);
		redirect('Admin/recharge_setting?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/add_recharge_setting?set='.$data);
		}
		
	}
	public function edit_recharge_setting_details()
	{
			 $idfr = $this->uri->segment(3);
		$id=$_GET['id'];
		 $setting_id=base64_decode($id);
	
		$name= $this->input->post('setting_name');
	
		$percentage= $this->input->post('percentage');
		
		$detail=array(
	 
	  
	  'setting_name'=>$name,
	  'percentage'=>$percentage,
	  
	  
	   );
	 $where=array('id'=>$setting_id);
	  
	   $table="ww_recharge_setting";
		$result = $this->admin_model->update_data($table,$detail,$where);		
		if($result=1){
		$data = "Successful Setting Updated";
		$data = base64_encode($data);
		redirect('Admin/recharge_setting?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/edit_recharge_setting?set='.$data);
		}
	}
		/***********deactivate customer********************/
	
		 public function deactive_rchrgsttng()
		   {
		  // $this->authAdLogin();
			   
			$id=$this->input->post('id');
			$deactive_customer=array('status'=>0);
			$this->db->where('id',$id);
			$this->db->update('ww_recharge_setting',$deactive_customer); 
			
		   }  
	   
	   /***********activate customer ********************/
	
		 public function active_rchrgsttng()
		   {
		 //  $this->authAdLogin();
			   
			$id=$this->input->post('id');
			$active_customer=array('status'=>1);
			$this->db->where('id',$id);
			$this->db->update('ww_recharge_setting',$active_customer);  
			
		   }
		   public function delete_rchrgsttng()
	{
		 $id=base64_decode($_GET['del']);
	 $this->db->where('id',$id);
							$this->db->delete('ww_recharge_setting'); 
							$data="Setting Deleted Successfuly";
							$data=base64_encode($data);
							
							redirect('Admin/recharge_setting?set='.$data);
		
	}
/********************Cupons And Deals***********************/	
		public function cupon()
	{
		$data['title']="Coupons And Deals";
		$this->load->model('admin_model');
		$data['result'] = $this->admin_model->select_all_cupon();
	
		$this->load->view('backend/cupons',$data);
		
	}	
	public function add_cupon()
	{
		$data['title']="Add Cupons";
		$this->load->model('admin_model');
		//$data['result'] = $this->admin_model->select_all_rchrgsttng();
	
		$this->load->view('backend/add_cupons',$data);
		
	}	
	public function edit_cupon()
	{
		$id=$_GET['id'];
		 $cupon_id=base64_decode($id);
		$data['title']="Edit Cupons";
		$this->load->model('admin_model');
		$data['result'] = $this->admin_model->select_single_cupon($cupon_id);
		
	
		$this->load->view('backend/add_cupons',$data);
		
	}	
	public function add_cupon_details()
	{
			 $idfr = $this->uri->segment(3);
		//$idfr=$_GET['id'];
		$promo= $this->input->post('promocode');
	$type= $this->input->post('type');
	$remark=$this->input->post('remark');
		$amount= $this->input->post('amount');
		$sdate= $this->input->post('sdate');
$edate= $this->input->post('edate');
		//$created_date=date('d/m/y');
		
		$detail=array(
	  
	  'promocode'=>$promo,
	  'discount_type'=>$type,
		'discount_amount'=>$amount,
	  'status'=>1,
	  'remark'=>$remark,
	  'start_date'=>strtotime($sdate),
	  'end_date'=>strtotime($edate)
	   );
	   
	  
	   $table="ww_cupons";
		$result = $this->admin_model->add_data($detail,$table);		
		if($result=1){
		$data = "Successful Cupon add";
		$data = base64_encode($data);
		redirect('Admin/cupon?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/add_cupon?set='.$data);
		}
		
	}
	public function edit_cupon_details()
	{
			 $idfr = $this->uri->segment(3);
		$id=$_GET['id'];
		 $cupon_id=base64_decode($id);
	$promo= $this->input->post('promocode');
	$type= $this->input->post('type');
	$remark=$this->input->post('remark');
		$amount= $this->input->post('amount');
		$sdate= $this->input->post('sdate');
$edate= $this->input->post('edate');
		
		$detail=array(
	 
	 'promocode'=>$promo,
	  'discount_type'=>$type,
		'discount_amount'=>$amount,
	  'remark'=>$remark,
	  'start_date'=>strtotime($sdate),
	  'end_date'=>strtotime($edate)
	  
	  
	   );
	 $where=array('id'=>$cupon_id);
	  
	   $table="ww_cupons";
		$result = $this->admin_model->update_data($table,$detail,$where);		
		if($result=1){
		$data = "Successful Cupon Updated";
		$data = base64_encode($data);
		redirect('Admin/cupon?set='.$data);
		}else{
         $data = "Something is wrong";
		 $data = base64_encode($data);
		redirect('Admin/edit_cupon?set='.$data);
		}
	}
		/***********deactivate customer********************/
	
		 public function deactive_cupon()
		   {
		  // $this->authAdLogin();
			   
			$id=$this->input->post('id');
			$deactive_cupon=array('status'=>0);
			$this->db->where('id',$id);
			$this->db->update('ww_cupons',$deactive_cupon); 
			
		   }  
	   
	   /***********activate customer ********************/
	
		 public function active_cupon()
		   {
		 //  $this->authAdLogin();
			   
			$id=$this->input->post('id');
			$active_cupon=array('status'=>1);
			$this->db->where('id',$id);
			$this->db->update('ww_cupons',$active_cupon);  
			
		   }
		   public function delete_cupon()
	{
		 $id=base64_decode($_GET['del']);
	 $this->db->where('id',$id);
							$this->db->delete('ww_cupons'); 
							$data="Cupon Deleted Successfuly";
							$data=base64_encode($data);
							
							redirect('Admin/cupon?set='.$data);
		
	}
	
	/******End of Functions done by Manoj Singh ******/
	
	
	/******Functions done by Ankit Singh ******/
	public function create_user()
	{
		$data['title'] ="Create User";
		$data['role'] = $_REQUEST['role'];
		$this->form_validation->set_rules('role','role','required');
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('contact_no','contact_no','required');
		$this->form_validation->set_rules('email','email','required');
		
		if ($this->form_validation->run() == false)
		{
		 $this->load->view('backend/add_service_provider',$data);
		}else{
			$username = strtolower($this->input->post('username'));
			$email    = strtolower($this->input->post('email'));
			
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
			$password = substr( str_shuffle( $chars ), 0, 8 );

			$contact_no = $this->input->post('contact_no');
			$companyname = strtolower($this->input->post('companyname'));
			if(empty($companyname)){
				$companyname = $username;
			}
			$this->load->helper('string');
			$login_id= substr($role,0,3).random_string('alnum',10).substr($user_name,0,3);
			$email = strtolower($this->input->post('email'));
			$role = strtolower($this->input->post('role'));
	
		    $additional_data = array(
				'id' => $login_id,
				'user_name' => $username,
				'user_email'  => $email,
				'password'  => md5($password),
				'contact_no'  => $contact_no,
				'role_type' => $this->input->post('role'),
				'create_date' => date('Y-m-d'),
				'company_name' => $companyname
			 );
			$id = $this->admin_model->registration($additional_data);
			$link = base_url().'registration/user_activation/?user_id='.$login_id;
			
			if($id){
				$to = $email;
				$subject = 'Your New Password...';
				$message = '<html><body>';
				$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
				$message .= "<tr style='background: #eee;'><td colspan=10><strong><b>Hello User</b> , please activate your account by clicking the following activation link. </strong> </td></tr>";
				$message .= "<tr style='background: #eee;'><td><strong>Email :</strong> </td><td>" . $email . "</td></tr>";
				$message .= "<tr><td><strong>Password :</strong> </td><td>" . $password . "</td></tr>";
				$message .= '<tr><td><strong>Activation Link:</strong> </td><td><a href="'.$link.'">Activate Account</a></td></tr>';
				$message .= "</table>";
				$message .= "</body></html>";
				
				/* Send the message using mail() function */
				if(mail($to, $subject, $message ))
				{
				echo "New Password has been sent to your mail, Please check your mail and SignIn.";
				}
				$this->session->set_flashdata('message'," User has been added successfully" );
				redirect('sp_manager/dashboard');	
			  }
		}	
	}

	
	public function change_password() {
	    $data['title']="Change Password";
		if(!empty($_POST)){
			$password = md5($this->input->post('password'));
			$user_role = $this->input->post('user_role');
			$sp_id = $this->input->post('sp_id');
			$passArray = array(
			'password'=> $password);
			$pass = $this->admin_model->change_password($passArray , $sp_id);
			
			$this->session->set_flashdata('message',"Password changed successfully." );
			if($user_role == 'photographer'){
					redirect('sp_manager/photographer_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'planner'){
					redirect('sp_manager/planner_profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'decorator'){
					redirect('sp_manager/profile?id='.base64_encode($sp_id)); 
				}
				if($user_role == 'venue'){
					redirect('sp_manager/venue_profile?id='.base64_encode($sp_id)); 
				}
		}
	}
	
	
	public function check_password() {
	    $data['title']="Change Password";
		$current_password = md5($this->input->post('curr_pass'));
		$user_id = $this->input->post('user_id');
		$detail = $this->admin_model->current_password($current_password , $user_id);
		if(empty($detail)){
			echo 1;
			die;
		}
	}
	
	/********Unique Email Check*******/
	public function email_exists()
	{
		$email = $this->input->post('email');
		$is_unique = $this->admin_model->emailexists($email);
		if($is_unique){
			echo 1;die;
		}else{
			echo 0;die;
		}
	}
	
	public function calendar()
	{
	    $data['title']="Calendar";
		$this->load->view('backend/calendar',$data);
	}
	
	public function events()
	{
		$json = array();
		$sql = "select * from ww_fullcalendar_dates order by id";
		try{
			$dbh = new PDO('mysql:host=localhost;dbname=wedding', 'root', '');
		}catch(Exception $exe){
			exit("Connection Unsuccessful");
		}
		$resultset = $dbh->query($sql) or die(print_r($dbh->errorInfo()));
		echo json_encode($resultset->fetchAll(PDO::FETCH_ASSOC));
	}
		
	public function add_events()
	{
		$title = $_POST['title'];
		$start = $_POST['start'];
		$end = $_POST['end'];
		$description = $_POST['description'];
		$all = $_POST['allDay'];
	
		//echo $title;
		/*if($all == 'true'){
			$allday = 'true';
		}else{
			$allday = 'false';
		}
		$user_id = $this->session->userdata('id');
		try{
			$dbh = new PDO('mysql:host=localhost;dbname=wedding', 'root', '');
		}catch(Exception $exe){
			exit("Connection Unsuccessful");
			 $sql = "insert into ww_fullcalendar_dates (title , start , end , user_id ,allday) values (:title , :start ,:end , :user_id , :allday)";
	  $resultset = $dbh->prepare($sql);
	  $resultset->execute(array(':title'=>$title,':start'=>$start,':end'=>$end ,':user_id'=>$user_id,':allday'=>$allday));
		}*/
		$user_id = $this->session->userdata('id');
		$event_array = array(
		'title' => $title,
		'start' => $start,
		'end' => $end,
		'description' => $description,
		'user_id' =>$user_id,
		'allday' =>false
		);
		
	  $id = $this->admin_model->event_insert($event_array);
	  echo $id;
	  die;
	 }
	 
	public function update_events()
	{
		$id =$_POST['id'];
		$title = $_POST['title'];
		$start = $_POST['start'];
		$end = $_POST['end'];
		try{
			$dbh = new PDO('mysql:host=localhost;dbname=wedding', 'root', '');
			//$connect = mysql_connect('localhost','root','');
			//print_r($connect);die("/");
		}catch(Exception $exe){
			exit("Connection Unsuccessful");
		}
	  $sql = "update ww_fullcalendar_dates set title = ?, start= ? , end= ? where id= ?";
	  $resultset = $dbh->prepare($sql);
	  $resultset->execute(array($title,$start,$end,$id));
	}
	
	public function event_edit()
	{
		$data['title'] = "Event Edit";
		$id = $_REQUEST['id'];
		$data['event_details'] = $this->admin_model->event_details($id);
		$this->load->view('backend/edit_events',$data);
	}
	
	public function event_delete()
	{
		$data['title'] = "Event Edit";
		$id = $_REQUEST['id'];
		$data['event_details'] = $this->admin_model->event_delete($id);
		$this->session->set_flashdata('message','Event deleted successfully.');
		redirect('admin/calendar');
	}
}
