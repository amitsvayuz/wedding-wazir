<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photographer extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model'); 
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		error_reporting(0);
		if(!$this->session->userdata('id')){
			redirect('registration');
		}
	}
	
	public function index()
	{
		$data['title']="Photographer Addition";
		$this->load->view('backend/list_photographer',$data);
	}
  
	 public function add_services()
	{
		$data['title']="Service 's Addition";
		if(!empty($_POST)){
		    $events = implode(',',$_POST[events]);
			$services_name = implode(',',$_POST[services]);
			$style_of_photography = implode(',',$_POST[style_of_photography]);
			$sharing_option  =implode(',',$_POST[sharing_option]);
			$resolution = implode(',',$_POST[resolution]);
			$studio_setup = implode(',',$_POST[studio_setup]);
	
		   
			$additionaldata = array(
				'events' => $events,
				'user_id' => $this->session->userdata('id'),
				'services' => $services_name,
				'style_of_photography' => $style_of_photography,
				'sharing_option' => $sharing_option,
				'resolution' => $resolution,
				'other_services' => $studio_setup,
			);
			$user_id = $this->session->userdata('id');
			$services_details = $this->admin_model->get_photographer_services($user_id);
			if(!empty($services_details)){
				$id = $this->admin_model->update_photographer_services($user_id , $additionaldata);
				$this->session->set_flashdata('message',"Photographer services has been updated successfully" );
			}else{
				$id = $this->admin_model->add_photographer_services($additionaldata);
				$this->session->set_flashdata('message',"Photographer services has been added successfully" );
			}
			redirect('photographer/list_services');
		}	
		$data['events_details'] = $this->admin_model->list_planner_events();
	    $this->load ->view('backend/add_services',$data);
	}
	
	 public function edit_photographer_services()
	{
		$data['title']="Service 's Eddition";
		$id = $_GET['id'];
		$user_id = $this->session->userdata('id');
		if(!empty($_POST)){
		    $events = implode(',',$_POST[events]);
			$services_name = implode(',',$_POST[services]);
			$style_of_photography = implode(',',$_POST[style_of_photography]);
			$sharing_option  =implode(',',$_POST[sharing_option]);
			$resolution = implode(',',$_POST[resolution]);
			$studio_setup = implode(',',$_POST[studio_setup]);
	
		$additionaldata = array(
				'events' => $events,
				'user_id' => $this->session->userdata('id'),
				'services' => $services_name,
				'style_of_photography' => $style_of_photography,
				'sharing_option' => $sharing_option,
				'resolution' => $resolution,
				'other_services' => $studio_setup,
			);
			
			$id = $this->admin_model->update_photographer_services($user_id , $additionaldata);
			$this->session->set_flashdata('message',"Photographer services has been updated successfully" );
			
			redirect('photographer/list_services');
		}	
		$data['services_details'] = $this->admin_model->get_photographer_services($user_id);
		$data['events_details'] = $this->admin_model->list_planner_events();
	    $this->load ->view('backend/edit_photographer_services',$data);
	}
	
	 public function admin_edit_photographer_services()
	{
		$data['title']="Service 's Eddition";
		$user_id = $this->input->post(hidden_user_id);
		$id = $this->input->post(hidden_service_id);
		if(!empty($_POST)){
		    $events = implode(',',$_POST[events]);
			$services_name = implode(',',$_POST[services]);
			$style_of_photography = implode(',',$_POST[style_of_photography]);
			$sharing_option  =implode(',',$_POST[sharing_option]);
			$resolution = implode(',',$_POST[resolution]);
			$studio_setup = implode(',',$_POST[studio_setup]);
	
		   	$additionaldata = array(
				'events' => $events,
				'user_id' => $user_id,
				'services' => $services_name,
				'style_of_photography' => $style_of_photography,
				'sharing_option' => $sharing_option,
				'resolution' => $resolution,
				'other_services' => $studio_setup,
			);
			$id = $this->admin_model->update_photographer_services($user_id , $additionaldata);
			$this->session->set_flashdata('message',"Photographer services has been updated successfully" );
			
			redirect('sp_manager/photographer_profile/?id='.base64_encode($user_id));
		}	
	}
	
	 public function detail_photographer_services()
	{
        $table='ww_admin_login';
		$this->load->model('admin_model');
		$user_id = $this->session->userdata('id');
		$data['result']=$this->admin_model->select_single_data($table,$user_id);
		$data['title']="User Profile";       

        $data['title']="Service 's Eddition";
		$id = $_GET['id'];
		$data['services_details'] = $this->admin_model->get_photographer_services($user_id);
		$this->load ->view('backend/detail_photographer_services',$data);
	}
	
	 public function list_services()
	{
		$data['title']="Photographer Service 's List";
		$data['list_details'] = $this->admin_model->list_photographer_services($this->session->userdata('id'));
		$data['events_details'] = $this->admin_model->list_planner_events();
		$this->load ->view('backend/list_photographer_services',$data);
	}
	
	 public function list_services_delete()
	{
		$id = $_GET['id'];
		$data['title']="Photographer Service 's List";
		$data['list_details'] = $this->admin_model->list_photographer_services_delete($id);
		$this->session->set_flashdata('message',"Photographer services has been deleted successfully" );
		redirect('photographer/list_services');
	}
	
	 public function list_photographer()
	{
		$data['title']="Photographer Listings";
		$data['photographer_details'] = $this->admin_model->listphotographer();
		$this->load->view('backend/list_photographer',$data);
	}
	
	 public function edit_photographer()
	{   
		$id = $_GET['id'];
		$data['title']="Photographer Edit Details";
		$data['photographer_details'] = $this->admin_model->edit_listphotographer($id);
		$this->load->view('backend/edit_photographer',$data);
	}

	 public function remove_images()
	{   
		$id = $_GET['id'];
		$url = $_GET['url'];
		
		$record_id = $this->admin_model->get_last_record($id);
		$rec_id = $this->admin_model->delete_last_record($record_id->id);
		if(!empty($rec_id)){
			$this->session->set_flashdata("message","Uploaded photographs removed successfully.");
		}
		$user_id = $this->session->userdata('id');
		$images = array();
		$images = explode(',',$record_id->images);
		//print_r($images);
		//print_r($record_id);die("/");
		
		for($i=0;$i<sizeof($images);$i++){
			$x = $url.'assets/uploads/photography/'.$user_id."/".$images[$i];
			unlink($url.'assets/uploads/photography/'.$user_id."/".$images[$i]);
			echo $x;
		}
		redirect("photographer/image_upload_photographer");
	}
	
	 public function list_photographer_images()
	{
		$data['title']="Photographer Images Listings";
		$user_id = $this->session->userdata('id');
		$data['photographer_details'] = $this->admin_model->list_photographer_title_images($user_id);
		$this->load->view('backend/list_photographer_images',$data);
	}
	
	 public function admin_list_photographer_title_images()
	{
		$data['title']="Photographer Images Listings";
		$user_id = $_GET['id'];
		$data['photographer_details'] = $this->admin_model->list_photographer_title_images($user_id);
		//print_r($data['photographer_details']);die("/");
		$this->load->view('backend/admin_list_photographer_title_images',$data);
	}
	
	 public function admin_list_photographer_images()
	{
		$data['title']="Photographer Images Listings";
		$id = $_GET['id'];
		$user_id = $_GET['user_id'];
		$data['photographer_details'] = $this->admin_model->list_photographer_images($id , $user_id);
		
		$this->load->view('backend/admin_list_photographer_images',$data);
	}
	
	 public function list_photographer_videos()
	{
		$data['title']="Photographer Videos Listings";
		$user_id = $this->session->userdata('id');
		$data['photographer_details'] = $this->admin_model->list_photographer_videos($user_id);
		$this->load->view('backend/list_photographer_videos',$data);
	}
	
	public function editphotographer()
	{
		$data['title']="Photographer Account Edition";
		
		$id = $this->input->post('hidden',true);
		//echo $id;die;
		$company_name = $this->input->post('company',true);
		$about_us = $this->input->post('about_us',true);
		$cost = $this->input->post('cost',true);
		$count_events = $this->input->post('count_events',true);
		$event_date = $this->input->post('event_date',true);
		$exp = $this->input->post('exp',true);
		$email = $this->input->post('email',true);
		$mobile_number = $this->input->post('mobile_number',true);
		$land_number = $this->input->post('land_number',true);
		$state = $this->input->post('state',true);
		$city = $this->input->post('city',true);
		
		$this->form_validation->set_rules('company', 'company', 'required'); 
        $this->form_validation->set_rules('about_us', 'about_us', 'required');
        $this->form_validation->set_rules('cost', 'cost', 'required'); 	 
		$this->form_validation->set_rules('count_events', 'count_events', 'required'); 
        $this->form_validation->set_rules('event_date', 'event_date', 'required');
        $this->form_validation->set_rules('exp', 'exp', 'required'); 	 
		$this->form_validation->set_rules('email', 'email', 'required'); 
        $this->form_validation->set_rules('mobile_number', 'mobile_number', 'required');
        $this->form_validation->set_rules('land_number', 'land_number', 'required'); 	 
		$this->form_validation->set_rules('state', 'state', 'required'); 
        $this->form_validation->set_rules('city', 'city', 'required');
        $this->form_validation->set_rules('cost', 'cost', 'required');
       
		
		if($this->form_validation->run() == false) {  //die('false'); 
		   redirect('photographer/edit_photographer?id='.$id);
		} else{
				$additionaldata = array(
					'company_name' => $company_name,
					'about_us' => $about_us,
					'cost' => $cost,
					'count_events' => $count_events,
					'event_date' => $event_date,
					'experienced' => $exp,
					'email'  => $email,
					'mobile_number' => $mobile_number,
					'landline_number'  => $land_number,
					'state'  => $state,
					'city'   =>   $city
				);
		//print_r($additionaldata);die("/");
	    $this->admin_model->update_photographer($id,$additionaldata);
		$this->session->set_flashdata('message'," Category has been added successfully" );
	    redirect('photographer/list_photographer');	
		}
	}
	
	public function image_upload_photographer()
	{
		$data['title']="Photographer Multiple Image Upload";
		define ("MAX_SIZE","9000"); // 2MB MAX file size
		function getExtension($str)
		{
		$i = strrpos($str,".");
		if (!$i) { return ""; }
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
		}
		// Valid image formats 
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
		if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
		{
	    $hidden = $_POST['hidden'];
		$image = array();
		$photography = "photography";
		$user_id = $this->session->userdata('id');
		$dirname = "assets/uploads/".$photography."/".$user_id."/";
		
		if(is_dir($dirname)){
		$file_path = $dirname;
		}else{
		$file_path = mkdir($dirname);
		$file_path = $dirname;
		}
		
		//$uploaddir = "assets/uploads/"; //Image upload directory
		foreach ($_FILES['photos']['name'] as $name => $value)
		{
		$filename = stripslashes($_FILES['photos']['name'][$name]);
		
		$size=filesize($_FILES['photos']['tmp_name'][$name]);
		//Convert extension into a lower case format
		$ext = getExtension($filename);
		$ext = strtolower($ext);
		//File extension check
		if(in_array($ext,$valid_formats))
		{
		//File size check
		if ($size < (MAX_SIZE*1024))
		{
		$image_name=time().$filename;
		$image[] = $image_name;
		echo "<img src='".$hidden.$file_path.$image_name."' class='imgList'>";
		echo "<a href='#' class='imglist'>x</a>";
		$newname=$file_path.$image_name;

		//Moving file to uploads folde
		if (move_uploaded_file($_FILES['photos']['tmp_name'][$name], $newname))
		{
		$time=time();
		//Insert upload image files names into user_uploads table
		//mysql_query("INSERT INTO user_uploads(image_name,user_id_fk,created) VALUES('$image_name','$session_id','$time')");
		}
		else
		{
		echo '<span class="imgList">You have exceeded the size limit! so moving unsuccessful! </span>'; }
		}

		else
		{
		echo '<span class="imgList">You have exceeded the size limit!</span>';
		}

		}

		else
		{
		echo '<span class="imgList">Unknown extension!</span>';
		}

		} //foreach end
		$image_multiple_name = implode(',',$image);
		$user_id = $this->session->userdata('id');
		$upload_multiple = array(
		'images' => $image_multiple_name,
		'date'   => date('Y-m-d H:i:s'),
		'user_id' => $user_id
		);
		 $this->admin_model->photographer_tags($upload_multiple);
		 //echo 1;
        die;
		} 
		
      $this->load ->view('backend/photographer_images',$data); 
	}
	
	public function image_uploadphotographer()
	{    
	    $user_id = $this->session->userdata('id');
		$upload_id = $this->admin_model->get_photographer_tags($user_id);
		//echo $this->session->userdata('id');
		//print_r($upload_id->id);die('/');
		
		$title = $_POST['title'];
		$location = $_POST['location'];
		$tags = implode(',',$_POST['tags']);
		$add_multiple_images = array(
		'title' => $title,
		'location' => $location,
		'tags' => $tags
		);
		$this->admin_model->add_photographertags($add_multiple_images , $upload_id->id);
		$this->session->set_flashdata("message","Photography Image Uploaded Successfully");
		redirect('photographer/image_upload_photographer');
		
	}
    
	public function video_photographer()
	{   
	if(!empty($_POST)){
		
		$title = $_POST['title'];
		$location = $_POST['location'];
		$tags = implode(',',$_POST['tags']);
		$location = $_POST['location'];
		$resolution = $_POST['resolution'];
		$events = implode(',',$_POST['events']);
		$style_of_photography = implode(',',$_POST['style_of_videography']);
		
		/******Video Uploading*****/
	   $fileName = $_FILES["file1"]["name"]; // The file name
	   $file_name =  time().$fileName;
	   $fileTmpLoc = $_FILES["file1"]["tmp_name"]; // File in the PHP tmp folder
	   $fileType = $_FILES["file1"]["type"]; // The type of file it is
	   $fileSize = $_FILES["file1"]["size"]; // File size in bytes
	   $fileErrorMsg = $_FILES["file1"]["error"]; // 0 for false... and 1 for true
	 
	  $file_type = array('image/png',
	  'image/gif',
	  'image/jpeg',
	  'image/pjpeg',
	  'text/plain',
	  'text/html',
	  'application/x-zip-compressed',
	  'application/pdf',
	  'application/msword');
	  
	    if($fileType == 'video/mp4'){
		$photography = "videography";
		$dirname = "assets/uploadsvideos/".$photography."/".$user_id."/";
		
		if(is_dir($dirname)){
		$file_path = $dirname;
		}else{
		$file_path = mkdir($dirname);
		$file_path = $dirname;
		}
		
        move_uploaded_file($fileTmpLoc, "$file_path/$file_name");
		}else{
		$this->session->set_flashdata("message","Please upload  only mp4 videos file.</br>Other file format are not supported.");
		redirect("photographer/image_upload_photographer");
		}
	   
		
		/******Video Uploading End*****/
		
		 $video_array = array(
		 'title' => $title,
		 'events' => $events,
		 'style_of_videography' =>  $style_of_photography,
		 'tags'  =>  $tags ,
		 'resolution' =>  $resolution,
		 'location' => $location,
		 'user_id' => $this->session->userdata('id'),
		 'videos' => $file_name,
		 'date'   => date('Y-m-d H:i:s')
		 );
		$id = $this->admin_model->video_insert($video_array);
	     if($id){
			 $this->session->set_flashdata("message","Video uploaded successfully");
			 redirect("photographer/image_upload_photographer");
		 }
	}
	$data['title'] = 'Video Uploading';
      $this->load ->view('backend/photographer_images',$data); 

	}

	/********Unique Email Check*******/
	public function email_exists()
	{
		$email = $this->input->post('email');
		$is_unique = $this->admin_model->emailexists($email);
		if($is_unique){
			echo 1;die;
		}else{
			echo 0;die;
		}
	}
	/********Unique Email Check End*******/
	
	public function delete_photographer()
	{
		$id = base64_decode($_GET['id']);
		$this->admin_model->deletephotographer($id);
		redirect('photographer/list_photographer');
		
	}
	
	/********Recharge History*******/
	
	public function recharge_history()
	{
		$res ='';
		$rechargearr = array('sp_id'=>$_POST['id'],
		                   'transaction_type'=>$_POST['remark'],
						   'amount'=>$_POST['amount'],
						   'recharge_date'=>strtotime(date('Y-m-d'))
						  );
		$table = "ww_recharge_history";
		$responce = $this->admin_model->recharge_history($table,$rechargearr);
		if($responce == true):
		$res.='Recharge successfully';
		echo $res;
		else :
		$res.='Recharge failure';
		echo $res;;
		endif;
	}
	/******Vipin Yadav****/
	
	public function photographer_customer_proposal()
	{
		
		$user_id = $this->session->userdata('id');
		$data['userinfo'] = $this->admin_model->user($user_id);
		$customer_name = $this->input->post('customer_name');
		$email = $this->input->post('email');
		$contact_number = $this->input->post('contact_number');
		$message1 = $this->input->post('message');
		$img = $this->input->post('photos');
		$selimg = $this->input->post('selimg');
		
		$proposalarray = array(
		                       'proposal_id'		=>rand(),
							   'sp_id'      		=>$user_id,
							   'sp_name'    		=>$data['userinfo'][0]->user_name,
							   'sp_email'   		=>$data['userinfo'][0]->user_email,
							   'customer_name'    	=>$customer_name,
							   'customer_email'    	=>$email,
							   'contact_number'    	=>$contact_number,
							   'message'    	    =>$message1,
							   'image'    			=>json_encode($selimg),
							   'send_date'			=>strtotime(date('Y-m-d')),
							   'status'  			=>0
							  );
		$table="ww_customer_proposal";
		$response=$this->admin_model->send_proposal($table,$proposalarray);
		//die;
        /********* send mail customer_name *****************/
        if($response==true):

		$to = $email;
        $base_url=base_url();
		$subject = 'Customer Proposal Request';
		$message = '<html><body>';
		$message = '<div style="overflow-x:auto;">';
		$message .= '<img width="200px" height="50px" src="'.$base_url.'uploads/logo-dark.png" alt="logo" />';
		$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
		$message .= "<tr style='background: #eee;'><td><strong>Customer Name:</strong> </td><td>" . strip_tags($customer_name) . "</td></tr>";
		$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($email) . "</td></tr>";
		$message .= "<tr><td><strong>Contact Number:</strong> </td><td>" . strip_tags($contact_number) . "</td></tr>";
		$selimg = $this->input->post('selimg');
		if (($selimg) != '') {
		
			for($i=0;$i<count($selimg);$i++)
			{
				if($i%2==0)
				{
					
					$message .= '<tr width="300px">';
					$message .= '<td width="150px"><img width="100px" height="50px" border-style="dotted" src="'.$base_url.'assets/uploads/photography/eJgikSRT8u/'.$selimg[$i].'" alt="proposal image" /></td>';	
					
				}
				else
				{
					
					$message .= '<td width="150px"><img width="100px" height="50px" border-style="dotted" src="'.$base_url.'assets/uploads/photography/eJgikSRT8u/'.$selimg[$i].'" alt="proposal image" /></td>';	
					$message .= '</tr>';
				}
				
			}
			
		}
		$curText = htmlentities($message1);           
		if (($curText) != '') {
			$message .= "<tr><td><strong>Other Content:</strong> </td><td>" . $curText . "</td></tr>";
		}
		$message .= "</table>";
		$message .= "</div>";
		$message .= "</body></html>";
		
		//echo $message;
//die;
		
		$headers = "From: " . strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		$headers .= "Reply-To: ". strip_tags($data['userinfo'][0]->user_email) . "\r\n";
		$headers .= "CC: susan@example.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        
		if(mail($to, $subject, $message, $headers))
		{
			$data['sendvenuesucees']="Message is Successfully";
			
		}	
	    $data="Message is Successfully";
		$data = base64_encode($data);
		redirect('Photographer/list_proposal?set='.$data);
		endif;
				
       /********  end send mail *********************/		
		
	}
     
	 public function list_proposal()
	{
		$data['title']='Customer Proposal';
		$user_id = $this->session->userdata('id');
		$data['resultProposal']=$this->admin_model->get_proposal($user_id);
		$this->load->view('backend/list_proposal',$data);
		
	}
}
