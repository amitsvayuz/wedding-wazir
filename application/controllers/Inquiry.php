<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inquiry extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('inquiry_model');
		$this->load->model('admin_model');
		error_reporting(0);
		if(!$this->session->userdata('id')){
			redirect('registration');
		}

	}
	/****************view inquiry***********************/
	public function index()
	{
	    $data['title']="Inquiry";
		$id=$this->session->userdata('id');
		$data['inquiry']=$this->inquiry_model->get_inquiry($id);
		$this->load->view('backend/inquiry',$data);
	}
	/****************view add inquiry***********************/
	public function add_inquiry()
	{
		$data['title']="Add Inquiry";
		$id=$this->uri->segment(3);
		if($id):
		$data['reltinquiry']=$this->inquiry_model->get_single_inquiry($id);

		endif;
		  if($this->session->userdata('role')=='venue'){
			$data['venue']=$this->admin_model->get_data('ww_venue',array('sp_id'=>$this->session->userdata('id')));
		}
		$this->load->view('backend/add_inquiry',$data);
	}
	/****************insert inquiry***********************/
	public function insert_inquery()
	{
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('email','Email','required');
		$this->form_validation->set_rules('contact','Contact','required');
		$this->form_validation->set_rules('date','Date','required');
		$this->form_validation->set_rules('decs','Description','required');
		if($this->session->userdata('role')=='venue'){
		$this->form_validation->set_rules('venue','Venue','required');
		}
		if ($this->form_validation->run() == false)
		{
		 $data['title']="Add Inquiry";
		 $this->load->view('backend/add_inquiry',$data);
		}
		else
		{

		        $inquiryarray = array(
								   'inq_id'=>'INQ'.rand(),
								   'sp_id' =>$this->session->userdata('id'),
								   'name' =>trim($this->input->post('name')),
								   'email' =>trim($this->input->post('email')),
								   'created_date' =>strtotime($this->input->post('date')),
								   'guest_range' =>trim($this->input->post('guestNumber')),
								   'decs' =>trim($this->input->post('decs')),
								   'status' =>0,
								   'venue' =>$this->input->post('venue'),
								   'totime' =>$this->input->post('toTime'),
								   'fromtime' =>$this->input->post('fromTime'),
								   'contact_num' =>$this->input->post('contact')


				);


				$this->inquiry_model->insert_inquiry($inquiryarray);
		}

	}
	/****************update inquiry***********************/
	public function update_inquiry()
	{
		        $id=trim($this->input->post('inq_id'));
		        $inquiryarray = array(
								   'name' =>trim($this->input->post('name')),
								   'email' =>trim($this->input->post('email')),
								   'created_date' =>strtotime($this->input->post('date')),
								   'guest_range' =>trim($this->input->post('guestNumber')),
								   'decs' =>trim($this->input->post('decs')),
								   'status' =>0,
								   'venue' =>$this->input->post('venue'),
								   'contact_num' =>$this->input->post('contact'),
								   'totime' =>$this->input->post('toTime'),
								   'fromtime' =>$this->input->post('fromTime')


				);


				$this->inquiry_model->update_inquiry($inquiryarray,$id);


	}

	
	/****************delete inquiry***********************/
	public function delete_inquiry()
	{
		        $id=$this->uri->segment(3);

				$this->inquiry_model->delete_inquiry($id);

	}

	/****************active inquiry***********************/
	public function active_inquiry()
	{
		       $id=$_POST['id'];
			   if($_POST['status'] == 0):
			  echo  $status = 1;
			   else:
			  echo  $status = 0;
			   endif;

		       $res = $this->inquiry_model->active_inquiry($id,$status);
			   if($res == TRUE):
			   echo 1;
			   endif;

	}

}
