<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->   	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
						 Recharge Setting
                       							
							
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url()?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url()?>admin/recharge_setting">Recharge Setting</a><i class="icon-angle-right"></i></li>
							<li><a href="<?php echo base_url()?>admin/add_recharge_setting">Add Recharge Setting</a></li>
						
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				
				
				 <?php 
                           
                                
									if($title=="Edit Recharge Setting"){
										
										
										
										$setting_name=$result[0]->setting_name; 
									
										$percentage=$result[0]->percentage;
										
										
										$uri=base_url().'admin/edit_recharge_setting_details?id='.base64_encode($result[0]->id);
										
									$butoon="Update";
									}
									else {
										
										$setting_name=null;
										$percentage=null;
										
										$butoon="Save";
										$uri=base_url().'admin/add_recharge_setting_details';
									}
									?>
				<div class="portlet-body form">
                                 <!-- BEGIN FORM-->
								 
								   
								   
									
                                 <form action="<?php echo $uri ?>" class="horizontal-form" method="post">

                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                            <label class="control-label" for="firstName">Setting Name</label>
                                             <div class="controls">
                                                <input type="text"  id="abouttitle" class="m-wrap span12" placeholder="Setting Name" value="<?php echo $setting_name ?>" name="setting_name">
                                                
                                             </div>
                                          </div>
                                       </div>
									   
									   <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label" for="firstName">Percentage %</label>
                                             <div class="controls">
                                                <input type="text"  id="abouttitle" class="m-wrap span12" placeholder="Percentage" value="<?php echo $percentage ?>" name="percentage">
                                                
                                             </div>
                                          </div>
                                       </div>
                                      
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" class="btn blue"><i class="icon-ok"></i><?php echo $butoon ?></button>
                                       <button type="button" class="btn">Cancel</button>
                                    </div>
                                 </form>
                                 <!-- END FORM--> 
									
                              </div>
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<?php include_once('includes/footer.php');?>