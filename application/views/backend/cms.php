<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->   	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Add CMS								
							
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>welcome/dashboard">Dashboard</a>
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>CMS/cms">CMS</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url();?>CMS/cms_home">Add CMS</a></li>
							
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				
				
				 <?php 
                                 
									if(strcmp($title,"CMS")){
										
										foreach($result as $row):
										
										$title=$row->page_title; 
										$heading=$row->page_heading;
										$dsc1=$row->page_desc1;
										$dsc2=$row->page_desc2;
										$date=$row->create_date;
										
										
										$uri=base_url().'Cms/cms_edit1?id='.$row->id;
										endforeach;
									$butoon="Update";
									}
									else {
										
										$title=null;
										$heading=null;
										$dsc1=null;
										$dsc2=null;
										$date=null;
										$butoon="Save";
										$uri=base_url().'Cms/cms_insert';
									}
									?>
				<div class="portlet-body form">
                                 <!-- BEGIN FORM-->
					<div class="tab-pane" id="tab_1_4">
			<div class="row-fluid">
               <div class="span12">
			     <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Add CMS</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action='<?php echo $uri;?>' method="post" class="form-horizontal">
                         <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                            <label class="control-label" for="firstName">Page Title</label>
                                             <div class="controls">
                                                <input type="text"  id="abouttitle" class="m-wrap span12" placeholder="title" value="<?php echo $title ?>" name="page_title" required/>
                                                
                                             </div>
                                          </div>
                                       </div>
                                        </div>
   <div class="row-fluid">					   
									   <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label" for="firstName">Page Heading</label>
                                             <div class="controls">
                                                <input type="text"  id="abouttitle" class="m-wrap span12" placeholder="Heading" value="<?php echo $heading ?>" name="page_heading" required/>
                                                
                                             </div>
                                          </div>
                                       </div>
                                      
                                    </div>
										
						   
		
                           <div class="control-group">
                              <label class="control-label">Page Body :</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" required placeholder="CMS Description" name="dsc1"><?php echo $dsc1;?></textarea>
                              </div>
                           </div>
                           <div class="form-actions">
                              <button type="submit" class="btn blue"><?php echo $butoon;?></button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
			</div>			 
			        
									
                              </div>
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<?php include_once('includes/footer.php');?>