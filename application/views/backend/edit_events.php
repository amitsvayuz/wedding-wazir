<?php include_once('includes/header.php');?>
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
    <?php include_once('includes/sidebar.php');?>
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <!--<div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>-->
                  <!-- END BEGIN STYLE CUSTOMIZER -->   
                  <h3 class="page-title">
                    Event Edit
                   </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="<?php echo base_url();?>admin/calendar">Back</a>
                        <span class="icon-angle-right"></span>
                     </li>
                    
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Edit Events Details</h4>
                      
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form id="register-form" action="<?php echo base_url();?>admin/event_edit/?id=".<?php echo $event_details->id;?> method="post" class="form-horizontal">
						
						 
						  <div class="control-group">
                              <label class="control-label">Event Title :</label>
                              <div class="controls">
                                    <input class="span4 m-wrap" id="email" required type="text" placeholder="Event Title" value="<?php echo $event_details->title;?>" name="event_title"/>   
									<input type="hidden" name="hidden" value="<?php echo $event_details->id;?>">
					           </div>
                           </div>
						   
						  <div class="control-group">
           <label class="control-label" for="inputPatient">Event Description :</label>
           <div class="controls">
		   <textarea name="description" required id="description" class="span4" rows="5"><?php echo $event_details->description;?></textarea>
               </div>
       </div>
                         <div class="control-group">
                              <label class="control-label">Start Date :</label>
                              <div class="controls">
         <input class="span4 m-wrap" value="<?php echo $event_details->start;?>" type="text" required placeholder="Start Date" id="eventdate" name="start_date"/>    
							  </div>
                           </div>
       <?php 
	   $two_slot_first = explode( ',' , $two_slots->slot1);
	   $two_slot_second = explode(',' ,$two_slots->slot2);
	   $current_event = explode('-' , $event_details->events_slots);
	   $two_slotfirst = $two_slot_first[0].'-'.$two_slot_first[1];
	   $two_slotsecond = $two_slot_second[0].'-'.$two_slot_second[1];
	   $current = $current_event[0].'-'.$current_event[1];
	   ?>           
	    <div class="control-group">
           <label class="control-label" for="when">Events Slots :</label>
           <div class="controls">
				<select id="events_slots" class="span4" data-placeholder="Choose a Category" required id="role" name="events_slots" tabindex="1">
				 <option value="none">Select Service</option>
<option <?php if($current ==  $two_slotfirst){echo 'selected';}?> value="<?php echo $two_slotfirst;?>"><?php echo  $two_slotfirst;?>
				</option>
 <option <?php if( $current == $two_slotsecond){echo 'selected';}?> value="<?php echo $two_slotsecond;?>"><?php echo $two_slotsecond ;?>
				</option>
				 </select>
         </div>
       </div>
	   <?php if($this->session->userdata('role') == 'venue') { ?>
	      <div  class="control-group">
           <label class="control-label" for="when">Venues :</label>
           <div class="controls" style="margin-top:5px;width:auto;">
				<select id="events_venues" class="span4" data-placeholder="Choose a Category" required id="role" name="venues" tabindex="1">
				<option  value="none">Select Venues</option>
				<?php foreach($venues_name as $venues) { ?>
       <option <?php if($event_details->venues ==  $venues->v_name){echo 'selected';}?> value="<?php echo $venues->v_name;?>"><?php echo $venues->v_name;?></option>
				<?php } ?>
				 </select>
         </div>
       </div>
	   <?php } ;?>
                           <div class="form-actions">
                              <button type="submit" id="submit" class="btn blue">Submit</button>
                              <a href="<?php echo base_url();?>admin/calendar" class="btn">Cancel</a>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
  
<?php include_once('includes/footer.php');?>
  <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

   <script>
   // When the browser is ready...
  $(function() {
  
    // Setup form validation on the #register-form element
    $("#register-form").validate({
    
        // Specify the validation rules
        rules: {
            companyname: "required",
            username: "required",
		    email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6,
			    maxlength: 10,
            },
		    confirm_password: { 
                    equalTo: "#password",
                     minlength: 6,
                     maxlength: 10
            },
			   },
        
        // Specify the validation error messages
        messages: {
            companyname: "Please enter your first name",
            username: "Please enter your user name",
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            email: "Please enter a valid email address",
		    
        },
       
    });
         jQuery.validator.addMethod('selectcheck', function (value) {
        return (value == 'none');
    }, "year required");
		 
		 
  });
  
  </script>
   
  <script>
    jQuery(document).ready(function() {     
      $('#register-back-btn').click(function(){
		  window.location='<?php echo base_url();?>';
	  });
	  $('#option1').click(function(){
		 $('#comp').show();
	  });
	  $('#option2').click(function(){
		 $('#comp').hide();
	  });
    });
  </script>

	
	 <script>
    $(document).ready(function() { 
		$("#email").keyup(function(){	
		var email = $("#email").val();
		var dataString = 'email=' + email;
		var base_url = "<?php echo base_url();?>";
		
			$.ajax({
				type: 'POST',
				url: base_url + 'admin/email_exists',
				data: dataString,
				success: function(result){
				   if(result == 1){
					   alert('Email Id already exists');
				   }
				}});
			return false;
		 });
    });
  </script>