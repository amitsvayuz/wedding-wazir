<div class="control-group">
                                       <label class="control-label">Events :</label>
                                       <div class="controls">
                                          <label class="checkbox">
                                          <input type="checkbox" name="Engagement" value="" />Engagement
                                          </label>
                                          <label class="checkbox">
                                          <input type="checkbox" value="" />Pre Wedding Celebrations
                                          </label>
										  <label class="checkbox">
                                          <input type="checkbox" value="" />Wedding
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Reception

                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Others

                                          </label>
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Services :</label>
                                       <div class="controls">
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Photography
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Videography
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Cinematography
                                          </label>
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Style of Photography :</label>
                                       <div class="controls">
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Candid
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Traditional
                                          </label>
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Sharing Option :</label>
                                       <div class="controls">
										   <label class="checkbox">
                                          <input type="checkbox" value="" />CD/ DVD
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Pen Drive
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Album
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Online
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Others
                                          </label>
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Resolution :</label>
                                       <div class="controls">
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Low
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Low - Medium
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Medium
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Medium - High
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />High (HD)
                                          </label>
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Other Services :</label>
                                       <div class="controls">
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Studio Setup
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" value="" />Photo/ Video Editing
                                          </label>
                                       </div>
                                    </div>