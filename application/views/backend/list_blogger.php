<?php include_once('includes/header.php');?>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Blogger
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>blog/list_blogger">Blogger</a>
								<i class="icon-angle-right"></i>
							</li>
						</ul>
						 <?php if($this->session->userdata('message')){?>
						  <div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
						   <span style='color:green;'><?php echo $this->session->userdata('message');?></span>
						  </div>
						<?php } ?>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>Blogger</h4>
							</div>
							<div class="portlet-body">
								
									
									<div class="clearfix">
									<div class="btn-group">
										<a href="<?php echo base_url();?>blog/add_blogger" id="sample_editable_1_new" class="btn green">
										Add New <i class="icon-plus"></i>
										</a>
									</div>
									
								</div>
								
								
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>S.No.</th>													
											<th>Name </th>
											<th>Email</th>
											<th>Mobile No</th>
											<th>Created Date</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									<?php $i=1;foreach($blog_result as $results){?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
											<td><?php echo $i;?></td>	
											<td><?php echo $results->name;?></td>
											<td><?php echo $results->email;?></td>
											<td><?php echo $results->mobile;?></td>
										    <td><?php echo $results->created_date;?></td>
											
										
	<td><a title="edit" href="<?php echo base_url();?>blog/edit_blogger?id=<?php echo $results->id;?>"><i class="icon-edit"></i></a>
										|	<a id="delete" onClick="return confirm('Are you sure you want to delete?');" title="remove" href="<?php echo base_url();?>blog/delete_blogger?id=<?php echo $results->id;?>"><i class="icon-trash"></i></a>
										</td>
										</tr>
									<?php $i=$i+1; } ?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
	<!-- END CONTAINER -->
	<div id="myModal1" style="margin-top:15%" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel1">Recharge for Service Provider</h3>
										<h4 id="msg" style="color:green"></h4>
									</div>
									<div class="modal-body">
									<form  method="post" action="" class="form-horizontal">
									<div class="control-group" >
                                       <label class="control-label">Enter Amount : </label>
                                       <div class="controls">
                                          <input type="text" value="" id="amount" name="amount" placeholder="Enter Amount" class="span10 m-wrap"/>
										  <input type="hidden" id="userid" value="" name="name" placeholder="Enter Amount" class="span10 m-wrap"/>
										  <span class="help-inline" id="amountErr" style="color:red;"></span>
                                       </div>
                                    </div>
									<div class="control-group" >
                                       <label class="control-label">Remarks : </label>
                                       <div class="controls">
                                          <input type="text" value="" id="remark" name="remark" placeholder="Enter Comments"  class="span10 m-wrap"/>
										 <span class="help-inline" id="remarkErr" style="color:red;"></span>
                                       </div>
                                    </div>
									<div class="control-group" >
                                       <label class="control-label"></label>
                                       <div class="controls">
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Remove</button>
										<button class="btn yellow" id="save">Save</button>
                                       </div>
                                    </div>
									</form>
									</div>
									<div class="modal-footer">
										
									</div>
								</div>
	</div>
	
	<script type="text/javascript">
	 function active(x)
	 {
		var id11=x;
		$.ajax({
			type:"POST",     
			url:'<?PHP echo base_url();?>admin/active_user',
			data:{"id":id11},  
			success: function(data){
				location.reload();
				}
		  });
	 }
 
	function deactive(r)
	 {
		var id11=r;
		 $.ajax({
			type:"POST",     
			url:'<?PHP echo base_url();?>admin/deactive_user',
			data:{"id":id11}, 
			success: function(data){
				location.reload();
			}
		  });
	 }
	 // recharge history
	 
	 function Recharge(id,user)
	 {
		var userid=id;
		$('#msg').html('');
		var useramount = $('#amount').val('');
		var userremark = $('#remark').val('');
		$('#userid').val(userid);
		$('#username').val(username);
	 }
	 
	 $('#save').click(function()
	 {
		var userid = $('#userid').val();
		var useramount = $('#amount').val();
		var userremark = $('#remark').val();
		if(useramount =="") 
		{
			$('#amountErr').html('Fill the amount');
		}
		else if(userremark =="")
		{
			$('#amountErr').html('');
			$('#remarkErr').html('Fill the comment');
		}
		else
		{
		 $.ajax({
			type:"POST",     
			url:'<?PHP echo base_url();?>photographer/recharge_history',
			data:{"id":userid,"amount":useramount,"remark":userremark}, 
			success: function(data){
				$('#amountErr').html('');
				$('#remarkErr').html('');
				$('#msg').html(data);
			}
		  });
		}
		return false;
	 });
	 
   </script>
	<?php include_once('includes/footer.php');?>
