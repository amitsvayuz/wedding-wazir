<?php include('includes/header.php');?>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
								
						<h3 class="page-title">
							Recharge History						
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>admin/recharge_history">Recharge History</a>
								
							</li>
							 
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>Recharge History </h4>
								
							</div>
							<div class="portlet-body">
								<!--<div class="clearfix">
									<div class="btn-group">
										<a href="<?php echo base_url();?>admin/add_customer" id="sample_editable_1_new" class="btn green">
										Add New <i class="icon-plus"></i>
										</a>
									</div>
									<div class="btn-group pull-right">
										<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">Print</a></li>
											<li><a href="#">Save as PDF</a></li>
											<li><a href="#">Export to Excel</a></li>
										</ul>
									</div>
								</div>-->
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>S.N.</th>	
											<th>Service Provider Name</th>
											<th class="hidden-480">Transaction Type</th>
											<th class="hidden-480">Amount</th>
									
											<th  class="hidden-480" >Transaction date</th>
											
										</tr>
									</thead>
									
									<tbody>
									<?php if(!empty($result))
									{ $i=1;
										foreach($result as $row)
										{
										
										$date= $row->recharge_date;
										//echo $id.'abc';
										?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
											<td><?php echo $i;?></td>
											<td><?php foreach($sp as $serp){ if($row->sp_id==$serp->id) {echo ucfirst($serp->user_name);}}?></td>
											<td class="hidden-480"><?php echo ucfirst($row->transaction_type);?></td>
											<td class="hidden-480"><?php echo $row->amount;?></td>											
											<td class="hidden-480"><?php echo date('d/m/Y',$date);?></td>
			
										</tr>
									<?php $i=$i+1; } } ?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
			
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
		
	<script type="text/javascript">
 function active(x)
 {
	var id11=x;
	//alert(id11);
	$.ajax({
		type:"POST",     
        url:'<?PHP echo base_url();?>admin/active_customer',
        data:{"id":id11},  
        success: function(data){
			
			location.reload();
			}
      });
 }
function deactive(r)
 {
	//alert('sni');
	var id11=r;
	//alert(id11);
	 $.ajax({
		type:"POST",     
        url:'<?PHP echo base_url();?>admin/deactive_customer',
        data:{"id":id11}, 
        success: function(data){
			
			location.reload();
		}
      });
 }
</script>
	<?php include('includes/footer.php');?>
	<!-- END CONTAINER -->

