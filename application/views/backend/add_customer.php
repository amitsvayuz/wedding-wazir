<?php include_once('includes/header.php');?>
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php include_once('includes/sidebar.php');?>
      
 <?php 
                             
                               
									if($title=="Edit Customer"){
										
										
							
										
										$name=$result[0]->customer_name; 
									 	$email=$result[0]->customer_email;
										$contact=$result[0]->customer_contact;
									
										$date= $result[0]->customer_dob;
										$pass=$result[0]->password;
										
									
										
										$uri=base_url().'admin/edit_customer_details?id='.base64_encode($result[0]->customer_id);
										
									$butoon="Update";
									}
									else {
										$name=null; 
										$email=null;
										$contact=null;
										$date= null;
										$pass=null;
										
										
										
										$uri=base_url().'admin/add_customer_details';
										
									$butoon="Save";	
									}
									?>      
      
      
      
      
      
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                
                  <h3 class="page-title">
                     Customer
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="<?php echo base_url();?>admin/customer">Customer</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="<?php echo base_url();?>admin/add_customer">Add Customer</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="portlet box light-grey" id="form_wizard_1">
                     <div class="portlet-title">
                        <h4>
                           <i class="icon-reorder"></i>Add Customer - <span class="step-title">Step 1 of 2</span>
                        </h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <form  method="post" action="<?php echo $uri;?>" class="form-horizontal">
                           <div class="form-wizard">
                              <div class="navbar steps">
                                 <div class="navbar-inner">
                                    <ul class="row-fluid">
                                       <li class="span3">
                                          <a href="#tab1" data-toggle="tab" class="step active">
                                          <span class="number">1</span>
                                          <span class="desc"><i class="icon-ok"></i> Account Setup</span>   
                                          </a>
                                       </li>
                                       <li class="span3">
                                          <a href="#tab2" data-toggle="tab" class="step">
                                          <span class="number">2</span>
                                          <span class="desc"><i class="icon-ok"></i> Profile Setup</span>   
                                          </a>
                                       </li>
                                       
                                      
                                    </ul>
                                 </div>
                              </div>
                              <div id="bar" class="progress progress-success progress-striped">
                                 <div class="bar"></div>
                              </div>
                              <div class="tab-content">
                                 <div class="tab-pane active" id="tab1">
                                    <h4 class="block">Account Setup</h4>
									
									 <div class="control-group" >
                                       <label class="control-label">Name</label>
                                       <div class="controls">
                                          <input type="text" value="<?php echo $name;?>" placeholder="Customer Name" name="name" class="span4 m-wrap" required/>
										 
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Email</label>
                                       <div class="controls">
                                          <input type="email" value="<?php echo $email;?>" placeholder="Email Id" name="email" class="span4 m-wrap" required/>
										  
                                       </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">Contact</label>
                                       <div class="controls">
                                          <input type="text" value="<?php echo $contact;?>" placeholder="Customer Contact" name="contact" class="span4 m-wrap" required/>
										  
                                       </div>
                                    </div>
                                
									
                                   
									
                                 </div>
                                 <div class="tab-pane" id="tab2">
								 <h4 class="block">Profile Setup</h4>
								     <div class="control-group">
                                       <label class="control-label">Date Of Birth</label>
                                       <div class="controls">
                                          <input type="text" <?php if(!empty($date)){ ?> value="<?php echo date('Y/m/d',$date);?>" <?php } ?> name="dob" id="dob" placeholder="Birth Date" class="span4 m-wrap" required/>
										  <h6 style="color:#D51D59"><?php echo form_error('dob'); ?></h6>
                                         </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">Password</label>
                                       <div class="controls">
                                          <input type="password" value="<?php echo $pass;?>" placeholder="Password" name="password" class="span4 m-wrap" required/>
                                           <h6 style="color:#D51D59"><?php echo form_error('password'); ?></h6>
                                       </div>
                                    </div>
                                    <?php if($pass=="") {?>
                                    
                                    <div class="control-group">
                                       <label class="control-label">Confirm Password</label>
                                       <div class="controls">
                                          <input type="password" name="confirm_password" placeholder="Confirm Password" class="span4 m-wrap" required/>
                                           <h6 style="color:#D51D59"><?php echo form_error('confirm_password'); ?></h6>
                                       </div>
                                    </div>
                                 </div>
                                
                                <?php }?>
								 
                              </div>
                              <div class="form-actions clearfix">
                                 <a href="javascript:;" class="btn button-previous">
                                 <i class="m-icon-swapleft"></i> Back 
                                 </a>
                                 <a href="javascript:;" class="btn blue button-next">
                                 Continue <i class="m-icon-swapright m-icon-white"></i>
                                 </a>
                                 <button type="submit" class="btn green button-submit">
                                 <?php echo $butoon;?> <i class="m-icon-swapright m-icon-white"></i>
                                 </button>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
 <?php include_once('includes/footer.php');?>