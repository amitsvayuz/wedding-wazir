<?php include('includes/header.php');?>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Wedding Planner 
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>sp_manager/spmanager">Manage Planner</a>
								
							</li>
							 
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>Wedding Planner</h4>
								<!--<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									
									<a href="javascript:;" class="reload"></a>
									
								</div>-->
								
							</div>
							<div class="portlet-body">
								<div class="clearfix">
								<div class="clearfix">
									<div class="btn-group">
										<a  href="<?php echo base_url();?>admin/create_user?role=planner" id="sample_editable_1_new" class="btn green">
										Add New <i class="icon-plus"></i>
										</a>
									</div>
									
								</div>
									
								</div>
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>S.No.</th>											
											<th>Name</th>
											<th class="hidden-480">Email Id</th>
											
											<th class="hidden-480">Joining Date</th>
											<th  class="hidden-480" >Status</th>
											<th  class="hidden-480" >Action</th>
										</tr>
									</thead>
									
									<tbody>
									<?php if(!empty($result))
									{$i=1;
										foreach($result as $row)
										{
										$id=base64_encode($row->id);
										$date= $row->create_date;
										//echo $id.'abc';
										?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
											<td><?php echo $i;?></td>
											<td><?php echo ucfirst($row->user_name);?></td>
											<td class="hidden-480"><?php echo $row->user_email;?></td>
											<td class="hidden-480"><?php echo $row->create_date;?></td>
											
					<td >
												<?php if($row->status == 1){?>
  <a href="#" onClick="var a=confirm('Are you sure you want to deactivate the user?');if(a){ deactive('<?php echo $row->id; ?>');}else{return false;}" title="Deactive">
                      <span class="label label-sm label-success">Active</span></a>&nbsp;
                      
                      <?php } else if($row->status == 0){?>
                       <a href="#" onClick="var a=confirm('Are you sure you want to activate the user?');if(a){ active('<?php echo $row->id;?>');}else{return false;}" title="active">
                      <span class="label label-sm label-warning">Deactive</span></a>&nbsp;
                      <?php } ?>
							</td>
											<!--<td><a href="<?php echo base_url()?>sp_manager/profile?id=<?php echo $id;?>"><i class="icon-eye-open"></i></a>&nbsp;&nbsp;<a id="delete_decorator" href="<?php echo base_url()?>sp_manager/delete_decorator?id=<?php echo $id;?>"><i class="icon-remove"></i></a></td>-->
					<td>
					  <a title="view profile" href="<?php echo base_url()?>sp_manager/planner_profile?id=<?php echo $id;?>"><i class="icon-edit"></i></a>
					| <a title="delete" onClick="return confirm('Are you sure you want to delete?');" id="delete_customer" href="<?php echo base_url()?>sp_manager/delete_planner?id=<?php echo $id;?>"><i class="icon-trash"></i></a>
					 <a style="display:none;" title="portfolio" href="<?php echo base_url()?>planner/admin_list_planner_title_images?id=<?php echo $row->id;?>"><i class=" icon-folder-open"></i> </a>
					| <a href="#myModal3" role="button"  onclick="return Rechargep('<?php echo $row->id;?>');" data-toggle="modal" title="recharge"><i class="icon-money"></i></a>
					</td>
										</tr>
									<?php $i=$i+1; } } ?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
			
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
		<!-- END PAGE -->
		<div id="myModal3" style="margin-top:15%" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel3">Recharge for Service</h3>
										<h4 id="msg" style="color:green"></h4>
									</div>
									<div class="modal-body">
									<form  method="post" action="" class="form-horizontal">
									<div class="control-group" >
                                       <label class="control-label">Enter Amount : </label>
                                       <div class="controls">
                                          <input type="text" value="" id="amount" name="amount" placeholder="Enter Amount" class="span10 m-wrap"/>
										  <input type="hidden" id="userid" value="" name="name" placeholder="Enter Amount" class="span10 m-wrap"/>
										  <span class="help-inline" id="amountErr" style="color:red;"></span>
                                       </div>
                                    </div>
									<div class="control-group" >
                                       <label class="control-label">Remarks : </label>
                                       <div class="controls">
                                          <input type="text" value="" id="remark" name="remark" placeholder="Enter Comments"  class="span10 m-wrap"/>
										 <span class="help-inline" id="remarkErr" style="color:red;"></span>
                                       </div>
                                    </div>
									<div class="control-group" >
                                       <label class="control-label"></label>
                                       <div class="controls">
                                        
					<button class="btn yellow" id="save">Save</button>
                                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                       </div>
                                    </div>
									</form>
									</div>
									<div class="modal-footer">
										
									</div>
								</div>
	</div>
	<!-- END CONTAINER -->
	<script type="text/javascript">
	 function active(x)
	 {
		var id11=x;
		$.ajax({
			type:"POST",     
			url:'<?PHP echo base_url();?>admin/active_user',
			data:{"id":id11},  
			success: function(data){
				location.reload();
				}
		  });
	 }
 
	function deactive(r)
	 {
		var id11=r;
		 $.ajax({
			type:"POST",     
			url:'<?PHP echo base_url();?>admin/deactive_user',
			data:{"id":id11}, 
			success: function(data){
				location.reload();
			}
		  });
	 }
	 // recharge history
	 
	 function Rechargep(id)
	 {
		var userid=id;
		$('#msg').html('');
		var useramount = $('#amount').val('');
		var userremark = $('#remark').val('');
		$('#userid').val(userid);
		$('#username').val(username);
	 }
	 
	 $('#save').click(function()
	 {
		var userid = $('#userid').val();
		var useramount = $('#amount').val();
		var userremark = $('#remark').val();
		if(useramount =="") 
		{
			$('#amountErr').html('Fill the amount');
		}
		else if(userremark =="")
		{
			$('#amountErr').html('');
			$('#remarkErr').html('Fill the comment');
		}
		else
		{
		 $.ajax({
			type:"POST",     
			url:'<?PHP echo base_url();?>Sp_manager/recharge_history',
			data:{"id":userid,"amount":useramount,"remark":userremark}, 
			success: function(data){
				$('#amountErr').html('');
				$('#remarkErr').html('');
				$('#msg').html(data);
			}
		  });
		}
		return false;
	 });
   </script>
	<?php include('includes/footer.php');?>
