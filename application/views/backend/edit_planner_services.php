<?php include_once('includes/header.php');?>
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php include_once('includes/sidebar.php');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                
                  <h4 class="page-title">
                     Services
                    
                  </h4>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="<?php echo base_url();?>planner/list_planner_services">Services</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     </ul>
               </div>
            </div>
			
			 <?php if($this->session->userdata('message')){?>
      <div class="alert alert-error">
        <button class="close" data-dismiss="alert"></button>
       <span style='color:green;'><?php echo $this->session->userdata('message');?></span>
      </div>
	  <?php } ?>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="portlet box blue" id="form_wizard_1">
                     <div class="portlet-title">
                        <h4>
                           <i class="icon-reorder"></i> Services <span class="step-title">Addition</span>
                        </h4>
                    </div>
                     <div class="portlet-body form">
                        <form  method="post" action="<?php echo base_url();?>planner/edit_planner_services" class="form-horizontal">
                           <div class="form-wizard">
                              <div class="tab-content">
                                 <div class="tab-pane active" id="tab1">
                                    <h4 class="block">Provide your account details :</h4>
                                 </div>
      <?php $events = explode(',' , $planner_services->events);
		?>	                          
                                <div class="control-group">
                                       <label class="control-label">Events :</label>
                                       <div class="controls">
                                        	<select name="events[]" class="span6 m-wrap" multiple="multiple" data-placeholder="Choose a Category" tabindex="1">
												<?php foreach($events_details as $details) { ?>
	<option  <?php if(in_array($details->description ,$events))echo 'selected';?> value="<?php echo $details->description;?>"> <?php echo $details->name;?></option>
												<?php } ?>
											</select>
                                       </div>
                                    </div>
      <?php $arrangements = explode(',' , $planner_services->arrangements);
		?>			
		
									 <div class="control-group">
										<label class="control-label">Arrangements :</label>
										<div class="controls">
											<select name="arrangements[]" class="span6 m-wrap" multiple="multiple" data-placeholder="Choose a Category" tabindex="1">
												<?php foreach($events_arrangements as $details) { ?>
	<option <?php if(in_array($details->description ,$arrangements))echo 'selected';?> value="<?php echo $details->description;?>"> <?php echo $details->name;?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								<?php $property_preference = explode(',' , $planner_services->property_preference);
								?>	
									<div class="control-group">
                                       <label class="control-label">Property Preferences :</label>
                                       <div class="controls">
										   <label class="checkbox">
<input <?php if(in_array('Lawn' ,$property_preference))echo 'checked';?> type="checkbox" name="property_preferences[]" value="Lawn" />Lawn
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('Hotel' ,$property_preference))echo 'checked';?>  type="checkbox" name="property_preferences[]" value="Hotel" />Hotel
                                          </label>
										   <label class="checkbox">
<input  <?php if(in_array('Banquet Hall' ,$property_preference))echo 'checked';?> type="checkbox" name="property_preferences[]" value="Banquet Hall" />Banquet Hall
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('Farm House',$property_preference))echo 'checked';?> type="checkbox" name="property_preferences[]" value="Farm House" />Farm House
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('Resort' ,$property_preference))echo 'checked';?> type="checkbox" name="property_preferences[]" value="Resort" />Resort
                                          </label>
                                       </div>
                                    </div>
							<?php $venue_preferences = explode(',' , $planner_services->venue_preference);?>		
							<div class="control-group">
							   <label class="control-label">Venue Preferences :</label>
							   <div class="controls">
								   <label class="checkbox">
 <input type="checkbox" <?php if(in_array('Indoor' ,$venue_preferences))echo 'checked';?> name="venue_preferences" value="Indoor" />Indoor
								  </label>
								   <label class="checkbox">
 <input type="checkbox" name="venue_preferences" <?php if(in_array('Outdoor' ,$venue_preferences))echo 'checked';?>  value="Outdoor" />Outdoor
								  </label>
							   </div>
							</div>
<div class="controls">
				<input  type="hidden" name="hidden_id" value="<?php echo $planner_services->id;?>" />
                                </div>
							</div>
        <div class="control-group">
							   <label class="control-label"></label>
							   <div class="controls">
                            <div class="submit-btn">
							<button type="submit" class="btn green">Save Changes</button>
								<a href="<?php echo base_url();?>planner/list_planner_services" class="btn">Cancel</a>
							</div>
</div>
</div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
 <?php include_once('includes/footer.php');?>