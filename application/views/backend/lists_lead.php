<?php include_once('includes/header.php');?>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
					
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Leads 
						</h3>
						 <?php if($this->session->userdata('message')){?>
						  <div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
						   <span style='color:green;'><?php echo $this->session->userdata('message');?></span>
						  </div>
						<?php } ?>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url();?>lead">Leads List</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>Leads List</h4>
							</div>
							<div class="portlet-body">
								<?php if($this->session->userdata('role')=='admin'){?>								
								<div class="clearfix">
									<div class="btn-group">
										<a href="<?php echo base_url();?>lead/create_lead" id="sample_editable_1_new" class="btn green">
										Add New <i class="icon-plus"></i>
										</a>
									</div>
									
								</div><?php }?>
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>S.No.</th>													
											<th>Type</th>
											<th>Customer Name</th>
											<th>Contact No</th>
											<th>Email Id</th>
											<th>Budget</th>
											<th>Response</th>
											<th>Created Date</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									<?php $i=1;foreach($leads_results as $results){ 
																		
									?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
											<td><?php echo $i;?></td>	
											<td><?php echo $results->Type;?></td>
											<td><?php echo $results->Name;?></td>
											<td><?php echo $results->Contact_Num;?></td>
											<td><?php echo $results->Email_Id;?></td>
											<td><?php echo $results->Budget;?></td>
											<td><?php echo $results->Response;?></td>
											<td><?php echo $results->created_date;?></td>
											
										
	<td>
	
<a id="view"  title="view" href="<?php echo base_url();?>lead/lead_view/<?php echo $results->lead_id;?>">
<i class="icon-eye-open" aria-hidden="true"></i>
 </a>
<?php if($this->session->userdata('role')=='admin'){?>
|<a id="delete" onClick="return confirm('Are you sure to remove this!');" title="remove" href="<?php echo base_url();?>lead/lead_delete?id=<?php echo base64_encode($results->lead_id);?>">
<i class="icon-trash"></i></a><?php }?>
										</td>
										</tr>
									<?php $i=$i+1; } ?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
	</div>
	<!-- END CONTAINER -->
	<?php include_once('includes/footer.php');?>
