
<?php include('includes/header.php');?>


	<div class="page-container row-fluid">
		<?php include('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Venue Property 				<small>Venue Property Details</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="#">Extra</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#">User Profile</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid profile">
					<div class="span12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<!--<li class="active"><a href="#tab_1_1" data-toggle="tab">Overview</a></li>-->
								<li><a href="#tab_1_2" data-toggle="tab">Property Info</a></li>
								<li><a href="#tab_1_3" data-toggle="tab">Account</a></li>
								
							</ul>
							<div class="tab-content">
								<div class="tab-pane row-fluid " id="tab_1_1">
									<ul class="unstyled profile-nav span3">
										<li><img src="<?php echo base_url()?>uploads/<?php echo @$result[0]->login_id;?>/<?php echo @$result[0]->pro_pic;?>" alt="" /> <a href="#" class="profile-edit">edit</a></li>
										<li><a href="#">Projects</a></li>
										<li><a href="#">Messages <span>3</span></a></li>
										<li><a href="#">Friends</a></li>
										<li><a href="#">Settings</a></li>
									</ul>
									<div class="span9">
										<div class="row-fluid">
											<div class="span8 profile-info">
												<h1><?php echo @$result[0]->company_name;?></h1>
												<p><?php echo @$result[0]->company_description;?></p>
												<p><a href="#">www.mywebsite.com</a></p>
												<ul class="unstyled inline">
													<li><i class="icon-map-marker"></i> Spain</li>
													<li><i class="icon-calendar"></i> 18 Jan 1982</li>
													<li><i class="icon-briefcase"></i> Design</li>
													<li><i class="icon-star"></i> Top Seller</li>
													<li><i class="icon-heart"></i> BASE Jumping</li>
												</ul>
											</div>
											
										</div>
										<!--end row-fluid-->
										<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab_1_11" data-toggle="tab">Latest Customers</a></li>
												<li class=""><a href="#tab_1_22" data-toggle="tab">Feeds</a></li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_11">
													<div class="portlet-body" style="display: block;">
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
																<tr>
																	<th><i class="icon-briefcase"></i> Company</th>
																	<th class="hidden-phone"><i class="icon-question-sign"></i> Descrition</th>
																	<th><i class="icon-bookmark"></i> Amount</th>
																	<th></th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td><a href="#">Pixel Ltd</a></td>
																	<td class="hidden-phone">Server hardware purchase</td>
																	<td>52560.10$ <span class="label label-success label-mini">Paid</span></td>
																	<td><a class="btn mini green-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td>
																		<a href="#">
																		Smart House
																		</a>	
																	</td>
																	<td class="hidden-phone">Office furniture purchase</td>
																	<td>5760.00$ <span class="label label-warning label-mini">Pending</span></td>
																	<td><a class="btn mini blue-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td>
																		<a href="#">
																		FoodMaster Ltd
																		</a>
																	</td>
																	<td class="hidden-phone">Company Anual Dinner Catering</td>
																	<td>12400.00$ <span class="label label-success label-mini">Paid</span></td>
																	<td><a class="btn mini blue-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td>
																		<a href="#">
																		WaterPure Ltd
																		</a>
																	</td>
																	<td class="hidden-phone">Payment for Jan 2013</td>
																	<td>610.50$ <span class="label label-danger label-mini">Overdue</span></td>
																	<td><a class="btn mini red-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td><a href="#">Pixel Ltd</a></td>
																	<td class="hidden-phone">Server hardware purchase</td>
																	<td>52560.10$ <span class="label label-success label-mini">Paid</span></td>
																	<td><a class="btn mini green-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td>
																		<a href="#">
																		Smart House
																		</a>	
																	</td>
																	<td class="hidden-phone">Office furniture purchase</td>
																	<td>5760.00$ <span class="label label-warning label-mini">Pending</span></td>
																	<td><a class="btn mini blue-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td>
																		<a href="#">
																		FoodMaster Ltd
																		</a>
																	</td>
																	<td class="hidden-phone">Company Anual Dinner Catering</td>
																	<td>12400.00$ <span class="label label-success label-mini">Paid</span></td>
																	<td><a class="btn mini blue-stripe" href="#">View</a></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
												<!--tab-pane-->
												<div class="tab-pane" id="tab_1_22">
													<div class="tab-pane active" id="tab_1_1_1">
														<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
															<ul class="feeds">
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-success">								
																					<i class="icon-bell"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					You have 4 pending tasks.
																					<span class="label label-important label-mini">
																					Take action 
																					<i class="icon-share-alt"></i>
																					</span>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			Just now
																		</div>
																	</div>
																</li>
																<li>
																	<a href="#">
																		<div class="col1">
																			<div class="cont">
																				<div class="cont-col1">
																					<div class="label label-success">								
																						<i class="icon-bell"></i>
																					</div>
																				</div>
																				<div class="cont-col2">
																					<div class="desc">
																						New version v1.4 just lunched!	
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="col2">
																			<div class="date">
																				20 mins
																			</div>
																		</div>
																	</a>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-important">								
																					<i class="icon-bolt"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					Database server #12 overloaded. Please fix the issue.								
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			24 mins
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			30 mins
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-success">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			40 mins
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-warning">								
																					<i class="icon-plus"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New user registered.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			1.5 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-success">								
																					<i class="icon-bell-alt"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					Web server hardware needs to be upgraded.	
																					<span class="label label-inverse label-mini">Overdue</span>					
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			2 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			3 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-warning">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			5 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			18 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			21 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			22 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			21 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			22 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			21 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			22 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			21 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			22 hours
																		</div>
																	</div>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<!--tab-pane-->
											</div>
										</div>
									</div>
									<!--end span9-->
								</div>
								<!--end tab-pane-->
								
								<div class="tab-pane profile-classic row-fluid active" id="tab_1_2 ">
									<div class="span2"><!--<img src="<?php echo base_url()?>uploads/<?php echo $result[0]->id;?>/<?php echo $result[0]->pro_pic;?>" alt="" /> <a href="<?php echo base_url();?>sp_manager/profile?id=<?php echo base64_encode($result[0]->id);?>" class="profile-edit">edit</a>--></div>
									<ul class="unstyled span10">
									
									<?php if(!empty(@$editdata['0']->prop_name)){?>
										<li><span>Property Name:</span> <?php echo ucfirst(@$editdata['0']->prop_name);?></li>
										<?php } ?>
										<?php if(!empty(@$editdata['0']->v_name)){?>
										<li><span>Venue Name:</span> <?php echo ucfirst(@$editdata['0']->v_name);?> </li>
										<?php } ?>
										<?php if(!empty(@$editdata['0']->v_dinein)){?>
										<li><span>Dine In:</span> <?php echo ucfirst(@$editdata['0']->v_dinein);?> </li>
										<?php } ?>
										<?php if(!empty(@$editdata['0']->v_floor)){?>
										<li><span>Floor:</span> <?php echo ucfirst(@$editdata['0']->v_floor);?> </li>
										<?php } ?>
										<?php if(!empty($editdata['0']->v_lighting)){?>
										<li><span>Lighting:</span> <?php echo ucfirst(@$editdata['0']->v_lighting);?> </li>
										<?php } ?>
										<?php if(!empty(@$editdata['0']->v_fencing)){?>
										<li><span>Fencing:</span> <?php echo ucfirst(@$editdata['0']->v_fencing);?></li>
										<?php } ?>
										<?php if(!empty(@$editdata['0']->v_dresser)){?>
										<li><span>Dresser:</span> <?php echo ucfirst(@$editdata['0']->v_dresser);?></li>
										<?php } ?>
										<?php if(!empty(@$editdata['0']->v_licencebar_late)){?>
										<li><span>Licence Bar Late:</span> <?php echo ucfirst(@$editdata['0']->v_licencebar_late);?></li>
										<?php } ?>
										<?php if(!empty(@$editdata['0']->v_licencebar)){?>
										<li><span>Licence Bar:</span> <?php echo ucfirst(@$editdata['0']->v_licencebar);?></li>
										<?php } ?>
										<?php if(!empty(@$editdata['0']->v_dj)){?>
										<li><span>:</span> <?php echo ucfirst(@$editdata['0']->v_dj);?></li>
										<?php } ?>
									</ul>
								</div>
								<!--tab_1_2-->
								<div class="tab-pane row-fluid profile-account" id="tab_1_3">
									<div class="row-fluid">
										<div class="span12">
											<div class="span3">
												<ul class="ver-inline-menu tabbable margin-bottom-10">
													<li class="active">
														<a data-toggle="tab" href="#tab_1-1">
														<i class="icon-cog"></i> 
														Personal info
														</a> 
														<span class="after"></span>                           			
													</li>
													<li class=""><a data-toggle="tab" href="#tab_4-4"><i class="icon-picture"></i> Company info</a></li>
													<li class=""><a data-toggle="tab" href="#tab_2-2"><i class="icon-picture"></i> Change Avatar</a></li>
													<li class=""><a data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i> Change Password</a></li>
													
												</ul>
											</div>
											<div class="span9">
												<div class="tab-content">
													<div id="tab_1-1" class="tab-pane active">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse" >
															<form action="<?php echo base_url();?>sp_manager/update_user_detail" id="validation" method="post" enctype="multipart/form-data">
																<label class="control-label">Name</label>
																<input type="text" name="user_name" <?php if(!empty(@$result[0]->d_o_b)){ ?> value="<?php echo @$result[0]->user_name;?>" <?php }?>  class="m-wrap span8" />
																<label class="control-label">Gender</label>
																<input type="radio" value="M" name="gender" class="m-wrap span8" <?php if(@$result[0]->gender == 'M'){ echo "checked"; }?>/>Male
																<input type="radio" name="gender" value="F" class="m-wrap span8" <?php if(@$result[0]->gender == 'F'){ echo "checked"; }?>/>Female
																<label class="control-label">Date of Birth</label>
																<input type="text" name="dob" id="dob" <?php if(!empty(@$result[0]->d_o_b)){ ?> value="<?php echo @$result[0]->d_o_b;?>" <?php }?> class="m-wrap span8" />
																<label class="control-label">Mobile Number</label>
																<input type="text" name="mobile" <?php if(!empty(@$result[0]->mob_no)){ ?> value="<?php echo @$result[0]->mob_no;?>" <?php }?> class="m-wrap span8" />
																<label class="control-label">Contact Number</label>
																<input type="text" name="contact" <?php if(!empty(@$result[0]->contact_no)){ ?> value="<?php echo @$result[0]->contact_no;?>" <?php }?> class="m-wrap span8" />
																<label class="control-label">Profile Image</label>
																<input name="pro_pic" value="<?php echo @$result[0]->pro_pic;?>" type="file"  class="default" />
																<input name="sp_id" value="<?php echo @$result[0]->id;?>" type="hidden"  class="default" />
			    												<div class="submit-btn">
																	<button class="btn green">Save Changes</button>
																	<a href="#" class="btn">Cancel</a>
																</div>
															</form>
														</div>
													</div>
													
													<div id="tab_2-2" class="tab-pane">
														<div style="height: auto;" id="accordion2-2" class="accordion collapse">
															<form action="<?php echo base_url();?>sp_manager/update_user_detail">
																<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</p>
																<br />
																<div class="controls">
																	<div class="thumbnail" style="width: 291px; height: 170px;">
																		<img src="http://www.placehold.it/291x170/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
																	</div>
																</div>
																<div class="space10"></div>
																<div class="fileupload fileupload-new" data-provides="fileupload">
																	<div class="input-append">
																		<div class="uneditable-input">
																			<i class="icon-file fileupload-exists"></i> 
																			<span class="fileupload-preview"></span>
																		</div>
																		<span class="btn btn-file">
																		<span class="fileupload-new">Select file</span>
																		<span class="fileupload-exists">Change</span>
																		<input type="file" class="default" />
																		</span>
																		<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
																	</div>
																</div>
																<div class="clearfix"></div>
																<div class="controls">
																	<span class="label label-important">NOTE!</span>
																	<span>You can write some information here..</span>
																</div>
																<div class="space10"></div>
																<div class="submit-btn">
																	<a href="#" class="btn green">Submit</a>
																	<a href="#" class="btn">Cancel</a>
																</div>
															</form>
														</div>
													</div>
													
													<div id="tab_3-3" class="tab-pane">
													     <div style="height: auto;" id="accordion3-3" class="accordion collapse">
															<form action="<?php echo base_url();?>registration/change_password" method="post">
																<label class="control-label">Current Password</label>
																	<input type="password" name="current_password" id="current_password" value="" class="m-wrap span8" />
																<label class="control-label">New Password</label>
																	<input id="password" type="password" name="password" class="m-wrap span8" />
																<label class="control-label">Re-type New Password</label>
																	<input id="new_password" type="password" name="password" class="m-wrap span8" />
																<div class="submit-btn">
																	<button onsubmit="return myFunction()" id="submit" class="btn green">Change Password</button>
																	<a href="#" class="btn">Cancel</a>
																</div>
																<input name="hidden" value="<?php echo @$result[0]->id;?>" type="hidden"  class="default" />
															</form>
														</div>
													</div>
													<div id="tab_4-4" class="tab-pane">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse">
															<form action="<?php echo base_url();?>sp_manager/update_company_detail" method="post">
																<label class="control-label">Company Name</label>
																<input name="company_name" type="text" value="<?php echo @$result[0]->company_name;?>"  class="m-wrap span8" />
																<label class="control-label">Description</label>
																<textarea name="company_description" class="span8 m-wrap" rows="3"><?php echo @$result[0]->company_description;?></textarea>
																<label class="control-label">Count of the events performed till date</label>
																<input name="count_of_event" type="text" value="<?php echo @$result[0]->count_of_event;?>" class="m-wrap span8" />
																<label class="control-label">Last Event Date</label>
																<input name="last_event_date" type="text" id="led" <?php if(!empty(@$result[0]->last_event_date)){ ?> value="<?php echo $result[0]->last_event_date;?>" <?php }?> class="m-wrap span8" />
																<label class="control-label">Cost Preference</label>
																 <div class="controls">
																	<select name="budget" id="budget" class="small m-wrap" tabindex="1">
																	<option <?php if(@$result[0]->budget=='15k-20k'){ echo "selected";}?> value="15k-20k">15k-20k</option>
																	<option <?php if(@$result[0]->budget=='20k-25k'){ echo "selected";}?> value="20k-25k">20k-25k</option>
																	<option <?php if(@$result[0]->budget=='25k-30k'){ echo "selected";}?> value="25k-30k">25k-30k</option>
																	<option <?php if(@$result[0]->budget=='30k-35k'){ echo "selected";}?> value="30k-35k">30k-35k</option>
																	</select>
																</div>
																
																<label class="control-label">Counrty</label>
																<div class="controls">
																	<input type="text" name="country" <?php if(!empty(@$result[0]->country)){?>value="<?php echo $result[0]->country;?>"<?php } ?> class="span8 m-wrap" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;US&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" />
																	<p class="help-block"><span class="muted">Start typing to auto complete!. E.g: US</span></p>
																</div>
																<label class="control-label">State</label>
																<div class="controls">
																	<input type="text" name="state" <?php if(!empty(@$result[0]->state)){?>value="<?php echo $result[0]->state;?>"<?php } ?> class="span8 m-wrap" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;US&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" />
																	<p class="help-block"><span class="muted">Start typing to auto complete!. E.g: US</span></p>
																</div>
																<label class="control-label">City</label>
																<input type="text" name="city" value="<?php echo @$result[0]->city;?>"  class="m-wrap span8" />
																<label class="control-label">Street</label>
																<input type="text" name="street" value="<?php echo @$result[0]->street;?>"  class="m-wrap span8" />
																<label class="control-label">Zip Code</label>
																<input type="text" name="zip_code" value="<?php echo @$result[0]->zip_code;?>"  class="m-wrap span8" />
																<label class="control-label">Lattitude</label>
																<input type="text" name="lattitude" value="<?php echo @$result[0]->latt;?>"  class="m-wrap span8" />
																<label class="control-label">Longitude</label>
																<input type="text" name="longitude" value="<?php echo @$result[0]->longatt;?>"  class="m-wrap span8" />
																<label class="control-label">Landmark</label>
																<input type="text" name="landmark" value="<?php echo @$result[0]->landmark;?>"  class="m-wrap span8" />
																<label class="control-label">Facebook Url</label>
																<input type="text" name="fb_url" value="<?php echo @$result[0]->fb_url;?>"  class="m-wrap span8" />
																<label class="control-label">Google Plus Url</label>
																<input type="text" name="gp_url" value="<?php echo @$result[0]->gp_url;?>"  class="m-wrap span8" />
																<input name="hidden" value="<?php echo @$result[0]->id;?>" type="hidden"  class="default" />
																<div class="submit-btn">
																	<button class="btn green">Save Changes</button>
																	<a href="#" class="btn">Cancel</a>
																</div>
															</form>
														</div>
													</div>
													
													
												</div>
											</div>
											<!--end span9-->                                   
										</div>
									</div>
								</div>
								<!--end tab-pane-->
								
								<!--end tab-pane-->
								
								<!--end tab-pane-->
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	
	<?php include('includes/footer.php');?>
	<script>
      jQuery(document).ready(function() { 
              
         // initiate layout and plugin
		  //App.setPage("table_managed");
          App.init();
      });
   </script>