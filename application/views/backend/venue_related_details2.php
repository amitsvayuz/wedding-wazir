<?php include('includes/header.php');?>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						
						<!-- END BEGIN STYLE CUSTOMIZER --> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Company Details				
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>admin/venue_details">Companies</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="javascript:void">Details</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid profile">
					<div class="span12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab_1_1" data-toggle="tab">Company Details</a></li>
							</ul>
							<div class="tab-content">
								
								
								
								
								
								<div class="tab-pane row-fluid active" id="tab_1_1">
									<div class="row-fluid">
										<div class="span12">
											<div class="span3">
												<ul class="ver-inline-menu tabbable margin-bottom-10">
													<li class="active">
														<a data-toggle="tab" href="#tab_1">
														<i class="icon-briefcase"></i> 
														Company
														</a> 
														<span class="after"></span>                           			
													</li>
													<li><a data-toggle="tab" href="#tab_2"><i class="icon-group"></i> Brand</a></li>
													<li><a data-toggle="tab" href="#tab_3"><i class="icon-leaf"></i> City</a></li>

													<li><a data-toggle="tab" href="#tab_4"><i class="icon-tint"></i>Venue</a></li>

												</ul>
											</div>
											<div class="span9">
												<div class="tab-content">
													<div id="tab_1" class="tab-pane active">
														<div style="height: auto;" id="accordion1" class="accordion collapse">
															<div class="accordion-group">
																<div class="accordion-heading">
																
																	<a href="#collapse_1" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
																	<?php echo ucfirst($company[0]->company_name);?>
																	</a>
																</div>
																<div class="accordion-body collapse in" id="collapse_1">
																	<div class="accordion-inner">
																		<div class="row-fluid invoice">
																		<div class="row-fluid invoice-logo">
																		<div class="span6 invoice-logo-space"><img src="<?php echo base_url();?>assets/img/logo.png" alt="" /> </div>
																		<div class="span6">
																		<p><?php echo strtoupper($company[0]->id);?><span class="muted">Company ID</span></p>
																		</div>
																		</div>
																		<hr />
																	<div class="row-fluid">
																	<div class="span8">
																		<h4>Company Details</h4>
																			<ul class="unstyled">
																				<li><b>Company name</b>&nbsp;:&nbsp; <?php echo ucfirst($company[0]->company_name); ?></li>								
																				<li><b>Contact person</b>&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($company[0]->user_name); ?></li>
																				<li><b>Contact email</b>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($company[0]->user_email); ?></li>
																				<li><b>Contact no</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo $company[0]->contact_no.'&nbsp;,&nbsp;'.$company[0]->mob_no; ?></li>
																				<li><b>Address</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($company[0]->city).','.$company[0]->state.','.$company[0]->country.','.$company[0]->zip_code; ?></li>
																				<li><b>Description</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($company[0]->company_description); ?></li>								
																				<li><b>Social handles</b>&nbsp;&nbsp;:&nbsp; <a href="<?php echo $company[0]->fb_url;?>"><span class="icon-facebook-sign"></span></a>&nbsp;&nbsp;<a href="<?php echo $company[0]->gp_url;?>"><span class="icon-google-plus-sign"></span></a></li>
																			</ul>
																	</div>
																	<div class="span2">
							
																	</div>
																	<div class="span2 invoice-payment">
							
																	</div>
																	</div>
																	<div class="row-fluid">
																		<div class="span4">
							
																		</div>
																		<div class="span8 invoice-block">
							
																			<!--<a href="<?php echo base_url();?>lead/share_lead/<?php echo $leads_results->lead_id;?>/<?php echo $leads_results->Type;?>" class="btn green big">Share Lead<i class="m-icon-big-swapright m-icon-white"></i></a>-->
																			<a href="<?php echo base_url();?>sp_manager/venue_profile?id=<?php echo base64_encode($company[0]->id);?>" role="button" class="btn green"><i class="icon-edit m-icon-white"></i>&nbsp;&nbsp;Edit</a>						
																		</div>
																	</div>
																</div>



																	</div>
																</div>
															</div>
															
														</div>
													</div>
													<div id="tab_2" class="tab-pane">
													<br>
																			<a href="<?php echo base_url();?>General_setting/add_brand" role="button" class="btn green" ><i class="icon-plus m-icon-white"></i>&nbsp;&nbsp;Add Brand</a>																									


																				<br>
																				<br>
													<?php $i=1; foreach($brandresult as $brand){?>
														<div style="height: auto;" id="accordion3" class="accordion collapse">
														
															<div class="accordion-group">
																<div class="accordion-heading">
																	<a href="#collapse_3_<?php echo $i;?>" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle collapsed">
																		<?php echo ucfirst($brand->name);?>
																	</a>
																</div>
																<div class="accordion-body collapse in" id="collapse_3_<?php echo $i;?>">
																	<div class="accordion-inner">
																		<div class="row-fluid invoice">
																			<div class="row-fluid invoice-logo">
																			<div class="span6 invoice-logo-space"><img src="<?php echo base_url();?>assets/img/logo.png" alt="" /> </div>
																			<div class="span6">
																			<p><?php echo strtoupper($brand->id);?><span class="muted">Brand ID</span></p>
																			</div>
																			</div>
																			<hr />
																			<div class="row-fluid">
																			<div class="span8">
																			<h4>Brand Details</h4>
																			<ul class="unstyled">
																				<li><b>Brand name</b>&nbsp;:&nbsp; <?php echo ucfirst($brand->name); ?></li>								
																				<li><b>Description</b>&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($brand->description); ?></li>								
																			</ul>
																			</div>
																			<div class="span2">
							
																			</div>
																			<div class="span2 invoice-payment">
							
																			</div>
																			</div>
																			<div class="row-fluid">
																			<div class="span4">
							
																			</div>
																				<div class="span8 invoice-block">
							
																					<!--<a href="<?php echo base_url();?>lead/share_lead/<?php echo $leads_results->lead_id;?>/<?php echo $leads_results->Type;?>" class="btn green big">Share Lead<i class="m-icon-big-swapright m-icon-white"></i></a>-->
																					<a href="<?php echo base_url();?>General_setting/edit_brand/<?php echo $brand->id;?>" role="button" class="btn green" ><i class="icon-edit m-icon-white"></i>&nbsp;&nbsp;Edit</a>						
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															
														</div>
														<?php }?>
													</div>
													<div id="tab_3" class="tab-pane">
																			<br>
																			<a href="<?php echo base_url();?>General_setting/add_city" role="button" class="btn green" ><i class="icon-plus m-icon-white"></i>&nbsp;&nbsp;Add City</a>																									


																				<br>
																				<br>
														<?php $i=1; foreach($brandresult as $brand){
																foreach($cityresult as $city){
																	if($city->city_id==$brand->id){															
															?>
														<div style="height: auto;" id="accordion3" class="accordion collapse">
														
															<div class="accordion-group">
																<div class="accordion-heading">
																	<a href="#collapse_3_2_<?php echo $i;?>" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle collapsed">
																		<?php echo ucfirst($city->company_name);?>
																	</a>
																</div>
																<div class="accordion-body collapse in" id="collapse_3_2_<?php echo $i;?>">
																	<div class="accordion-inner">
																		<div class="row-fluid invoice">
																			<div class="row-fluid invoice-logo">
																			<div class="span6 invoice-logo-space"><img src="<?php echo base_url();?>assets/img/logo.png" alt="" /> </div>
																			<div class="span6">
																			<p><?php echo strtoupper($city->c_id);?><span class="muted">CITY ID</span></p>
																			</div>
																			</div>
																			<hr />
																			<div class="row-fluid">
																			<div class="span8">
																			<h4>City Details</h4>
																			<ul class="unstyled">
																				<li><b>Brand name</b>&nbsp;:&nbsp; <?php echo ucfirst($brand->name); ?></li>								
																				<li><b>City name</b>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($city->company_name); ?></li>								
																				<li><b>Address</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($city->company_addr); ?></li>																											
																			</ul>
																			</div>
																			<div class="span2">
							
																			</div>
																			<div class="span2 invoice-payment">
							
																			</div>
																			</div>
																			<div class="row-fluid">
																			<div class="span4">
							
																			</div>
																				<div class="span8 invoice-block">
							

																					<a href="<?php echo base_url();?>General_setting/edit_city/<?php echo $city->c_id; ?>" role="button" class="btn green" ><i class="icon-edit m-icon-white"></i>&nbsp;&nbsp;Edit</a>						
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															
														</div>
														<?php $i++;}}}?>
													</div>
													
													<div id="tab_4" class="tab-pane">
													<br>
																			<a href="<?php echo base_url();?>Venue_setting_cntlr/property" role="button" class="btn green"><i class="icon-plus m-icon-white"></i>&nbsp;&nbsp;Add New Property</a>																									
																			<a href="<?php echo base_url();?>venue/add_venue_manager?id=<?php echo base64_encode($company[0]->id);?>" role="button" class="btn green" ><i class="icon-plus m-icon-white"></i>&nbsp;&nbsp;Add New Venue</a>						

																				<br>
																				<br>
														<?php $i=1; 
																foreach($venueresult as $venue){
																																
															?>
														<div style="height: auto;" id="accordion3" class="accordion collapse">
														
															<div class="accordion-group">
																<div class="accordion-heading">
																	<a href="#collapse<?php echo $i;?>" data-parent="#accordion3" data-toggle="collapse" class="accordion-toggle collapsed">
																		<?php echo ucfirst($venue->v_name);?>
																	</a>
																</div>
																<div class="accordion-body collapse in" id="collapse<?php echo $i;?>">
																	<div class="accordion-inner">
																		<div class="row-fluid invoice">
																			<div class="row-fluid invoice-logo">
																			<div class="span6 invoice-logo-space"><img src="<?php echo base_url();?>assets/img/logo.png" alt="" /> </div>
																			<div class="span6">
																			<p><?php echo strtoupper($venue->v_id);?><span class="muted">Venue ID</span></p>
																			</div>
																			</div>
																			
																			<hr />
																			<div class="row-fluid">
																			<div class="span6">
																			
																			<h4>Venue Details</h4>
																			<ul class="unstyled">
																				<li><b>Venue name</b>&nbsp;:&nbsp; <?php echo ucfirst($venue->v_name); ?></li>								
																				<li><b>Address</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_addr); ?></li>								
																				<li><b>Venue type</b>&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_nature); ?></li>	
																				<li><b>Food type</b>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_dinein); ?></li>																																																					
																			</ul>
																			</div>
																			<div class="span6">
																			<h4>Services</h4>
																				<ul class="unstyled">
																				<li><b>DJ.</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_dj); ?></li>								
																				<li><b>Dhol</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_dol); ?></li>								
																				<li><b>Sehnai</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_shenai); ?></li>	
																				<li><b>Band</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_band); ?></li>																																																					
																				<li><b>Stage</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_stage); ?></li>
																				<li><b>Decorator</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_decor); ?></li>
																				<li><b>Floweriest</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_florist); ?></li>
																				<li><b>Photographer</b>&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_photographer); ?></li>
																				<li><b>Videographer</b>&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_videographer); ?></li>
																				<li><b>Ghodi</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_ghodi); ?></li>
																				<li><b>Palki</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_palki); ?></li>																																																					
																				<li><b>Artists</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($venue->v_artist); ?></li>																			
																			</ul>
																			</div>
																			
																			</div>
																			<div class="row-fluid">
																			<div class="span4">
																			
							
																			</div>
																				<div class="span8 invoice-block">

																					<a style="float:right" href="<?php echo base_url();?>Venue/venue_edit_get/<?php echo $venue->v_id;?>" role="button" class="btn green"><i class="icon-edit m-icon-white"></i>&nbsp;&nbsp;Edit</a>						
																				
																				</div>
																			</div>
																		
																			
																				<div class="row-fluid">
																			<div class="span8">																				
																			<h4>Property details</h4>
																			<br>
																																						
																			<?php 
																				foreach($propertyresult as $property){
																					if($venue->pvName==$property->pro_id){?>
																				<ul class="unstyled">
																				<li><b>Property name</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($property->propertyName); ?></li>								
																				<li><b>Address</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($property->propertyAddr); ?></li>								
																				<li><b>Property type</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($property->propertyType); ?></li>	
																				<li><b>Number of Floors</b>&nbsp;&nbsp;:&nbsp; <?php echo ucfirst($property->numFloor); ?></li>																																																					
																			</ul>
																			</div>
																			<div class="row-fluid">
																			
																				<div class="span12">	
							
																					<!--<a href="<?php echo base_url();?>lead/share_lead/<?php echo $leads_results->lead_id;?>/<?php echo $leads_results->Type;?>" class="btn green big">Share Lead<i class="m-icon-big-swapright m-icon-white"></i></a>-->
																					<a style="float:right" href="<?php echo base_url();?>Venue_setting_cntlr/property_edit/<?php echo $property->pro_id;?>" role="button" class="btn green"><i class="icon-edit m-icon-white"></i>&nbsp;&nbsp;Edit</a>						
																				</div>
																			</div>
																			<?php		}															
																									
																				}																			
																			?>
																			
																			<div class="span4"></div>
																			</div>
																			
																		</div>
																	</div>
																</div>
															</div>
															
														</div>
														<?php $i++;}?>
													</div>
												</div>
											</div>
											<!--end span9-->                                   
										</div>
									</div>
								</div>
								<!--end tab-pane-->
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
<?php include('includes/footer.php');?>