 <?php include('includes/header.php');?>
       <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-timepicker/css/bootstrap-timepicker.css" />
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <?php include('includes/sidebar.php');?>
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div id="portlet-config" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button"></button>
                    <h3>portlet Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here will be a configuration form</p>
                </div>
            </div>
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <!-- BEGIN PAGE HEADER-->
                <div class="row-fluid">
                    <div class="span12">

                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                            Schedule
                        </h3>
                        <ul class="breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?php echo base_url()?>sp_manager/dashboard">Dashboard</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li><a href="<?php echo base_url()?>calender/create_schedule">Schedule</a></li>
                        </ul>

                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->



           
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
                    <div class="portlet box light-grey ">
					               <?php  if($this->session->flashdata('message')):?>
								   <?php $message=$this->session->flashdata('message');?>
								   <div class="alert alert-<?php echo $message['class'];?>">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo $message['set'];?>
								    </div>
									<?php endif;?>
					                <?php if(@$resp):?>
									<div class="alert alert-danger">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo @$resp;?>
									</div>
								    <?php endif;?>	
                        <div class="portlet-title ">
                            <h4><i class="icon-reorder"></i>Schedule</h4>
                        </div>
                        <div class="portlet-body light-grey">
                            <div class="row-fluid">
                                  <div class="span12">
                        <div class="span3">
                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                <li class="active">
                                    <a href="#tab_1" data-toggle="tab">
                                    <i class="icon-briefcase"></i>
                                    Add Schedule
                                    </a>
                                    <span class="after"></span>
                                </li>
								<li class=""><a data-toggle="tab" href="#tab_3"><i class="icon-picture"></i>View Schedule</a></li>
                            </ul>
							
				<li class="dropdown" id="header_inbox_bar">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span class="badge">
				  <div class="tile bg-green">
						<div class="tile-body">
							<i class="icon-calendar"></i>
						</div>
						<div class="tile-object">
							<div class="name">
								#Schedule
							</div>
							<div class="number">
								<?php echo count($numslot2);?>
							</div>
						</div>
					</div>
				  </span>
                  </a>
				  
                  <ul class="dropdown-menu extended inbox unstyled">
                     <li>
                        <p class="bg-green" style="color:#fff"><b>Short Description of Schedule:</b></p>
                     </li>
					 <?php if($numslot2):?>
					 <?php foreach($numslot2 as $rowslot):?>
                     <li>
                        <a href="#">
                        <span class="subject">
						<?php if($rowslot->title):?>
                        <span class="from"><?php echo ucfirst($rowslot->title);?></span>
						<?php endif;?>
						<?php if($rowslot->start):?>
                        <span class="time"><?php echo $rowslot->start;?></span>
						<?php endif;?>
                        </span>
						<span class="subject">
						<?php if($rowslot->fromTime):?>
                        <span class="from"><?php echo $rowslot->fromTime;?></span>
						<?php endif;?>
						<?php if($rowslot->toTime):?>
                        <span class="time"><?php echo $rowslot->toTime;?></span>
						<?php endif;?>
                        </span>
						<?php if($rowslot->booking):?>
                        <span class="message">
                        <?php echo $rowslot->booking;?>
                        </span>
                        <?php endif;?>						
                        </a>
                     </li>
					 <?php endforeach;?>
					 <?php endif;?>
                     
                  </ul>
               </li>
			   </ul>
            </div>
			<?php 
			if($slotresult):
			foreach($slotresult as $srow):
			$id = $srow->id;
			$sp_id = $srow->sp_id;
			$slotLevel = $srow->slotLevel;
			$allDay = $srow->allDay;
			$start = $srow->start;
			$end = $srow->end;
			$venu = $srow->venue;
	
			$location = $srow->location;
			$desc = $srow->desc;
			$backgroundColor = $srow->eventcolor;
			$gemail = $srow->gemail;
			$calendar_user = $srow->calendar_user;
			$reminder = $srow->reminder;
             
			$name =  $srow->name;
			$fromTime = $srow->fromTime;
			$toTime = $srow->toTime;
			$booking = $srow->booking;
			$title = $srow->title;
			$url=base_url().'calender/update_schedule';
			$button='Update';
			endforeach;
			else:
			$id = '';
			$sp_id = '';
			$slotLevel = '';
			$allDay = '';
			$start = '';
			$end = '';
			$location = '';
			$desc = '';
			$backgroundColor = '';
			$gemail = '';
			$venu='';
			$calendar_user = '';
			$reminder = '';
			$fromTime = '';
			$toTime = '';
			$name =  '';
			$booking = '';
			$title = '';
			$url=base_url().'calender/add_schedule';
			$button='Save';
			endif;
			
			
			
			?>
            <div class="span9">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="accordion in collapse" id="accordion1" style="height: auto;">
                            <div class="accordion-group">
                                    <div class="accordion-heading">
                                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1">
                                               Add Schedule
                                                </a>
                                    </div>
                                <div id="collapse_1" class="accordion-body collapse in">
                                    <div class="accordion-inner">

                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="<?php echo $url;?>" method="post" class="form-horizontal" onsubmit="return slotFunction()">
                                                <h3 class="form-section"></h3>
                                                <div class="row-fluid">
                                                    <div class="span6 ">
                                                        <div class="control-group">
                                                            <label class="control-label">Venue:</label>
                                                            <div class="controls">
                                                                <select class="span12 m-wrap" name="venue">
																						<?php if(!empty($venue)){
																								foreach($venue as $ven){																							
																							?>                                                                   
                                                                    <option <?php if($venu == $ven->v_id): echo 'selected'; endif;?> value="<?php echo $ven->v_id;?>"><?php echo $ven->v_name;?></option>
																								<?php }}?>                                                                   
                                                                   </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="row-fluid" id="date" >
														   <div class="span6">
														        <div class="control-group">
																  <label class="control-label">Date:</label>
																  <div class="controls">
																	 <div class="input-append date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
																		<input class="span8 m-wrap m-ctrl-medium date-picker" size="16" name="date" type="text" value="<?php echo $start;?>" /><span class="add-on"><i class="icon-calendar"></i></span>
																		<input class="form-control input-medium date-picker span8" id="lvldate" placeholder="mm/dd/yyyy" name="id" type="hidden" value="<?php echo $id;?>" size="16">
																		<span class="help-inline" style="color:red;"><?php echo form_error('date');?></span>									
																	 </div>
																  </div>
															    </div>
														   </div>
													</div>
													
													<div class="row-fluid" id="fromto">
                                                    <div class="span6 ">
													    <div class="control-group">
														  <label class="control-label">From:</label>
														  <div class="controls">
															 <div class="input-append bootstrap-timepicker-component">
																<input class="span6 m-wrap m-ctrl-small " value="<?php echo $fromTime;?>" name="fromTime" type="text" />
																<span class="add-on"><i class="icon-time"></i></span>
																<span class="help-inline" style="color:red;"><?php echo form_error('fromTime');?></span>
															 </div>
														  </div>
													   </div>
                                                       
                                                    </div>
                                                    <!--/span-->
                                                    <div class="span6">
													 <div class="control-group">
														  <label class="control-label">To:</label>
														  <div class="controls">
															 <div class="input-append bootstrap-timepicker-component">
																<input class="span6 m-wrap m-ctrl-small" value="<?php echo $toTime;?>" name="toTime" type="text" />
																<span class="add-on"><i class="icon-time"></i></span>
																<span class="help-inline" style="color:red;"><?php echo form_error('toTime');?></span>
															 </div>
														  </div>
													   </div>
                                                        
                                                    </div>
                                                    <!--/span-->
                                                </div>
                                                    
                                                
                                                    
                                                
                                                <div class="row-fluid">
                                                    <div class="span12 ">
                                                        <div class="control-group">
                                                            <label class="control-label">Title:</label>
                                                            <div class="controls">

                                                                <input type="text" name="schedule" value="<?php echo $title;?>" placeholder="Enter the title" class="m-wrap span12" >
                                                                <span class="help-inline" style="color:red;"><?php echo form_error('schedule');?></span>
                                             
															</div>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="row-fluid">
                                                    <div class="span12 ">
                                                        <div class="control-group">
                                                            <label class="control-label">Name:</label>
                                                            <div class="controls">

                                                                <input type="text" name="name" value="<?php echo $name;?>" placeholder="Enter the name" class="m-wrap span12" >
                                                                <span class="help-inline" style="color:red;"><?php echo form_error('name');?></span>
                                             
															</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-fluid">
                                                    <div class="span12 ">
                                                        <div class="control-group">
                                                            <label class="control-label">Email:</label>
                                                            <div class="controls">
                                                                <input id="tags_1" name="gemail" type="text" placeholder="" class="m-wra " value="<?php echo $gemail;?>" />
                                                                
                                                                <!--<input type="text" name="email" value="" placeholder="Enter the email address" class="m-wrap span12" >-->
                                                                <span class="help-inline" style="color:red;"><?php echo form_error('gemail');?></span>
                                             
															</div>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="row-fluid">
                                                    <div class="span12 ">
                                                        <div class="control-group">
                                                            <label class="control-label">Description:</label>
                                                            <div class="controls">
                                                                <textarea class="span12 ckeditor m-wrap" name="desc"   rows="2"><?php echo $desc;?></textarea></div>
                                                        </div>
                                                    </div>
                                                    <!--/span-->
                                                    
                                                </div>
												
                                            </div>
                                                    <!--/span-->
                                    </div>
                                                <!--/row-->
                                            <div class="form-actions">
                                                <button type="submit" class="btn blue"><i class="icon-ok"></i><?php echo $button;?></button>
                                                    <!--<button type="button" class="btn">Cancel</button>-->
                                            </div>
                                            </form>
											</div>
                                            <!-- END FORM-->
                                </div>
                            </div>
                        </div>
						<div class="tab-pane" id="tab_3">
                                    <div class="accordion in collapse" id="accordion3" style="height: auto;">
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
                                                View Schedule
                                                </a>
                                            </div>
                                            <div id="collapse_3_1" class="accordion-body collapse in">
                                                <div class="accordion-inner">
                                                     <!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th class="hidden-480">Date</th>
											<th class="hidden-480">From Time</th>
											<th class="hidden-480">To Time</th>
											<th class="hidden-480">Title</th>
											<th class="hidden-480">Name</th>
											<th class="hidden-480">Email</th>
											<th class="hidden-480">Action</th>
										</tr>
									</thead>
									<tbody>
									    <?php 	if(isset($numslot2)):?>
									    <?php 	foreach($numslot2 as $row):?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
											<td class="hidden-480"><?php echo ucfirst($row->start);?></td>
											<td class="hidden-480"><?php echo $row->fromTime;?></td>
											<td class="hidden-480"><?php echo $row->toTime;?></td>
											<td class="hidden-480"><?php echo ucfirst($row->title);?></td>
											<td class="hidden-480"><?php echo ucfirst($row->name);?></td>
											<td class="hidden-480"><?php echo ucfirst($row->gemail);?></td>
											<td class="hidden-480"><a href="<?php echo base_url();?>calender/create_schedule/<?php echo $row->id; ?>"><i class="icon-edit"></i></a> | <a  href="<?php echo base_url();?>calender/delete_schedule/<?php echo $row->id; ?>" onclick="confirm('Are you sure delete?');"><i class="icon-trash"></i></a></td>
										
										</tr>
										<?php endforeach; endif;?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
               
                                
                            </div>
                        </div>
                        <!--end span9-->
                    </div>
                                </div>
                            <!-- END CALENDAR PORTLET-->
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
      <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
     <script>
        jQuery(document).ready(function() {
           
         //  App.init();

            $("#day1").click(function(){
               $('#day').show();
               $('#month').hide();
               $('#date').hide();
               $('#year').hide();
            });

            // initiate date
            $("#day2").click(function(){
                    $('#day').hide();
                    $('#month').hide();
                    $('#date').show();
                    $('#year').hide();
            });

            // initiate month
            $("#day3").click(function(){
                    $('#day').hide();
                    $('#month').show();
                    $('#date').hide();
                    $('#year').hide();
            });

            // initiate yaer
            $("#day4").click(function(){
                    $('#day').hide();
                    $('#month').hide();
                    $('#date').hide();
                    $('#year').show();
            });
			
		
        });
    </script>
<?php include('includes/footer.php');?>


