



<script type='text/javascript'>

	$(document).ready(function() {
	
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		var calendar = $('#calendar').fullCalendar({
			editable: true,
			events: "<?php echo base_url();?>admin/events",
			selectable :true,
			selectHepler: true,
			select:function(start,end,allDay){
			var title = prompt("Events title:");
			if(title){
			start = $.fullCalendar.formatDate(start , "yyyy-MM-dd HH:mm:ss");
			end = $.fullCalendar.formatDate(end , "yyyy-MM-dd HH:mm:ss");
			$.ajax({
			url:"<?php echo base_url();?>admin/add_event",
			data: "title=" + title + "&start=" + start + "&end=" + end,
			type:"POST",
			success:function(json){
			alert('ok');
			}
			});
		 calendar.fullcalendar('renderEvent',
		 {
		 title: title,
		 start: start,
		 end:  end,
		 allDay: allDay,
		 },
		 true
		 );
		}
		calendar.fullCalendar('Unselect');
		},
		editable:true,
		eventDrop:function(event , delta){
		start = $.fullCalendar.formatDate(event.start,"yyyy-MM-dd HH:mm:ss");
		end = $.fullCalendar.formatDate(event.end,"yyyy-MM-dd HH:mm:ss");
		$.ajax({
		url:"http://localhost/ankit/calendar/update_events.php",
		data: "title=" + event.title + "&start=" + start + "&end=" + end + "&id=" +event.id,
        type:"POST",		
		success:function(json) {
		alert('ok');
		}
		});
		},
		eventResize: function(event){
		start = $.fullCalendar.formatDate(event.start,"yyyy-MM-dd HH:mm:ss");
		end = $.fullCalendar.formatDate(event.end,"yyyy-MM-dd HH:mm:ss");
		$.ajax({
		url:"http://localhost/ankit/calendar/update_events.php",
		data: "title=" + event.title + "&start=" + start + "&end=" + end + "&id=" +event.id,
        type:"POST",		
		success:function(json) {
		alert('ok');
		}
		});
		
		}
		});
		});
     </script>