<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 2.3.1
Version: 1.1.2
Author: KeenThemes
Website: http://www.keenthemes.com/preview/?theme=metronic
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469
-->
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php echo $title;?> | Wedding Wazir</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/css/metro.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/dropzone/css/dropzone.css" rel="stylesheet"/>
	<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/jquery-tags-input/jquery.tagsinput.css" />
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/css/style_responsive.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/css/style_default.css" rel="stylesheet" id="style_color" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-daterangepicker/bootstrap-datetimepicker.min.css" />
	<link href="<?php echo base_url();?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="favicon.ico" />
	<link href="<?php echo base_url();?>assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/chosen-bootstrap/chosen/chosen.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-datepicker/css/datepicker.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-timepicker/compiled/timepicker.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-colorpicker/css/colorpicker.css" />
   <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
   <link rel="stylesheet" href="<?php echo base_url();?>assets/data-tables/DT_bootstrap.css" />
 
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="/resources/demos/style.css">
          
         <script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
	<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="controls" style="margin-bottom:2%;"><input class="m-wrap span5" type="text" style="margin-right:3%;" name="mytext[]" placeholder="Add distance name"/><input type="text" class="m-wrap span5" style="margin-right:3%;" name="mytextval[]" placeholder="Add distance value"/><input type="button" value="-" class="btn red remove_field"  id="removeButton" /></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wraps"); //Fields wrapper
    var add_button      = $(".add_field_buttons"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="controls" style="margin-bottom:2%;">'+
			'<select class="m-wrap span5" style="margin-right:3%;" name="mytexts[]">'+
                                                   '<option value="U-Shape">U-Shape</option>'+
                                                   '<option value="Double U-Shape">Double U-Shape</option>'+
												   '<option value="Class Room">Class Room</option>'+
                                                   '<option value="Theatre">Theatre</option>'+
												   '<option value="Board Room">Board Room</option>'+
                                                   '<option value="Cluster Board Room">Cluster Board Room</option>'+
                                        '</select><input type="text" class="m-wrap span5" style="margin-right:3%;" name="mytextsval[]" placeholder="Add capacity"/><input type="button" value="-" class="btn red remove_fields"  /></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_fields", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrapp"); //Fields wrapper
    var add_button      = $(".add_field_buttonp"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="controls" style="margin-bottom:1%;">'+
			'<select class="m-wrap span2" style="margin-right:1%;" name="mytextp[]">'+
                                                   '<option value="In-side Available">In-side Available</option>'+
                                                   '<option value="Out-side Available">Out-side Available</option>'+
                                        '</select>'+
										'<select class="m-wrap span2" style="margin-right:1%;" name="mytextptype[]">'+
                                                   '<option value="Guarded">Guarded</option>'+
                                                   '<option value="Owner Risk">Owner Risk</option>'+
                                        '</select>'+
										'<input type="text" name="mytextpc[]" style="margin-right:1%;" class="m-wrap span2" placeholder="Add  car capacity">'+
										'<input type="text" name="mytextptc[]" style="margin-right:1%;" class="m-wrap span2" placeholder="Add twoWheeler capacity"><input type="button" value="-" class="btn red remove_fieldp"  /></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_fieldp", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="A/C"){
            $("#actype").show();
			$("#nonactype").hide();
        }
        if($(this).attr("value")=="Non-A/C"){
            $("#nonactype").show();
			 $("#actype").hide();
			
        }
		$('#apower').click(function(){
			$("#powerbackup").show();
		});
		$('#unpower').click(function(){
			$("#powerbackup").hide();
		});
		$('#arain').click(function(){
			$("#rainbackup").show();
		});
		$('#unrain').click(function(){
			$("#rainbackup").hide();
		});
		$('#afire').click(function(){
			$("#firebackup").show();
		});
		$('#unfire').click(function(){
			$("#firebackup").hide();
		});
		$('#valetone').click(function(){
			$("#valetfacility").show();
		});
		$('#valetzero').click(function(){
			$("#valetfacility").hide();
		});
    });
	// get selected country
	$('#cityId').bind('change', function () {
                  var url = $(this).val(); 
				  var base_url = '<?php echo base_url();?>';
				  if(url > 0)
				  {
					  $.ajax({
								type: "POST",
								url: base_url+"Venue_setting_cntlr/sel_ajax_company",
								data: "city_id="+url,
								cache: false,
								datatype: 'html',
								success: function(res) {	
								$("#companyId").html(res);
								}
							});
				  }
              });
	
});
</script>
   
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="<?php echo base_url();?>sp_manager/dashboard">
				<h5><span style="color:white">WEDDING</span><span style="color:red">WAZIR</span></h5>
				<!--<img src="<?php echo base_url();?>assets/img/logo.png" alt="logo" />-->
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
				<img src="<?php echo base_url();?>assets/img/menu-toggler.png" alt="" />
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<!-- BEGIN TOP NAVIGATION MENU -->					
				<ul class="nav pull-right">
					<!-- BEGIN NOTIFICATION DROPDOWN -->
					
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<!--<img alt="" src="<?php echo base_url();?>assets/img/avatar1_small.jpg" />-->
                                <?php if($this->session->userdata('role')=='admin'):?>
				<span class="username"> <?php echo ucfirst($this->session->userdata('role'));?> Manager : <?php echo ucfirst($this->session->userdata('username'));?></span>
                                <?php else:?>
                                <span class="username"> <?php echo ucfirst($this->session->userdata('role'));?> Manager : <?php echo ucfirst($this->session->userdata('username'));?></span>
                                <?php endif;?>
						<i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<!--<li><a href="extra_profile.html"><i class="icon-user"></i> My Profile</a></li>
							<li><a href="calendar.html"><i class="icon-calendar"></i> My Calendar</a></li>
							<li><a href="#"><i class="icon-tasks"></i> My Tasks</a></li>
							<li class="divider"></li>-->
							<li><a href="<?php echo base_url();?>/registration/logout"><i class="icon-key"></i> Log Out</a></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU -->	
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->