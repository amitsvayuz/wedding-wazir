<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        	
			<ul>
				<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->

				</li>
				<!--<li>
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM --
					<form class="sidebar-search">
						<div class="input-box">
							<a href="javascript:;" class="remove"></a>
							<input type="text" placeholder="Search..." />				
							<input type="button" class="submit" value=" " />
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM --
				</li>-->
				<li class="start active ">
					<a href="<?php echo base_url();?>sp_manager/dashboard">
					<i class="icon-home"></i> 
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
				
				<?php if($this->session->userdata('role') == 'admin'){ ?>
				<li class="has-sub ">
					<a href="<?php echo base_url();?>admin/create_user">
					<i class="icon-user"></i> 
					<span class="title">Service Provider</span>
					<span class="selected"></span>
					</a>
				</li>
				<li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-user"></i> 
					<span class="title">Business Manager</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="<?php echo base_url();?>sp_manager/venue_manager">Venue Manager Profile</a></li>
						<li ><a href="<?php echo base_url();?>sp_manager/spmanager">Wedding Planner Profile</a></li>
						<li ><a href="<?php echo base_url();?>sp_manager/decorator">Decorator Profile</a></li>
						<li ><a href="<?php echo base_url();?>photographer/list_photographer">Photographer Profile</a></li>
					</ul>
				</li>
				<?php } ?>
				<?php if($this->session->userdata('role') == 'venue'){ ?>
				<!--<li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-bookmark-empty"></i> 
					<span class="title">Venue Manager</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="<?php echo base_url();?>Venue/venue_manager">Venue Details</a></li>
						<li ><a href="<?php echo base_url();?>Venue/decorator">Portfolio</a></li>
					</ul>
				</li>-->
				<li class="has-sub ">
					<a href="<?php echo base_url();?>sp_manager/venue_manager">
					<i class="icon-user"></i> 
					<span class="title">Venue Manager Profile</span>
					<span class="selected"></span>
					</a>
				</li>
				
				<li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-list-alt"></i> 
					<span class="title">Venue Manager </span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<!--<li ><a href="<?php echo base_url();?>Venue_setting_cntlr/list_property">Venue Property</a></li>-->
						<li><a href="<?php echo base_url();?>Venue/venue_manager">Venue Details</a></li>
						<li ><a href="<?php echo base_url();?>Venue/image_upload_venue">Portfolio</a></li>
					</ul>
				</li>
				<li class="has-sub ">
					<a href="<?php echo base_url();?>sp_manager/customer_proposal">
					<i class="icon-user"></i> 
					<span class="title">Customer Contact</span>
					<span class="selected"></span>
					</a>
				</li>
				<?php } ?>
				<?php if($this->session->userdata('role') == 'photographer'){  ?>
				<li class="has-sub ">
					<a href="<?php echo base_url();?>photographer/list_photographer">
					<i class="icon-user"></i> 
					<span class="title">Photographer Profile</span>
					<span class="selected"></span>
					</a>
				</li>
				<li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-bookmark-empty"></i> 
					<span class="title">Photographer</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="<?php echo base_url();?>photographer/list_services">Services</a></li>
						<li ><a href="<?php echo base_url();?>photographer/image_upload_photographer">Portfolio</a></li>
					</ul>
				</li>
				
				<li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-camera-retro"></i> 
					<span class="title">Portfolio</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="<?php echo base_url();?>photographer/list_photographer_images">photographs</a></li>
						<li ><a href="<?php echo base_url();?>photographer/list_photographer_videos">videos</a></li>
					</ul>
				</li>
				<li class="has-sub ">
					<a href="<?php echo base_url();?>sp_manager/customer_proposal">
					<i class="icon-user"></i> 
					<span class="title">Customer Contact</span>
					<span class="selected"></span>
					</a>
				</li>
				<?php } ?>
				<?php if($this->session->userdata('role') == 'planner'){  ?>
				<li class="has-sub ">
					<li ><a href="<?php echo base_url();?>sp_manager/spmanager">
					<i class="icon-user"></i> 
					<span class="title">Wedding Planner Profile</span>
					<span class="selected"></span>
					</a>
				</li>
				<li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-user"></i> 
					<span class="title">Wedding Planner</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="<?php echo base_url();?>planner/list_planner_services">Services</a></li>
						<li ><a href="<?php echo base_url();?>planner/image_upload_planner">Portfolio</a></li>
					</ul>
				</li>
				
				<li class="has-sub ">
					<a href="<?php echo base_url();?>planner/list_planner_images">
					<i class="icon-camera-retro"></i> 
					<span class="title">Photographs</span>
					<span class="selected"></span>
					</a>
				</li>
				<li class="has-sub ">
					<a href="<?php echo base_url();?>sp_manager/customer_proposal">
					<i class="icon-user"></i> 
					<span class="title">Customer Contact</span>
					<span class="selected"></span>
					</a>
				</li>
				<?php } ?>
				<?php if($this->session->userdata('role') == 'decorator'){  ?>
				<li class="has-sub ">
					<li ><a href="<?php echo base_url();?>sp_manager/decorator">
					<i class="icon-user"></i> 
					<span class="title">Decorator Profile</span>
					<span class="selected"></span>
					</a>
				</li>
				
				<li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-bookmark-empty"></i> 
					<span class="title">Decorator</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="<?php echo base_url();?>decorator/add_decorator_services">Services</a></li>
						<li ><a href="<?php echo base_url();?>decorator/image_upload_decorator">Portfolio</a></li>
					</ul>
				</li>
				<li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-camera-retro"></i> 
					<span class="title">Portfolio</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="<?php echo base_url();?>decorator/list_decorator_images">photography</a></li>
					</ul>
				</li>
				<li class="has-sub ">
					<a href="<?php echo base_url();?>sp_manager/customer_proposal">
					<i class="icon-home"></i> 
					<span class="title">Customer Contact</span>
					<span class="selected"></span>
					</a>
				</li>
				<?php } ?>
				
				<?php if($this->session->userdata('role') == 'admin') { ?>
				<li class="has-sub ">
					<a href="<?php echo base_url();?>admin/customer">
					<i class="icon-group"></i> 
					<span class="title">Customer Details</span>
					<span class="selected"></span>
					</a>
				</li>
				<li class="has-sub ">
					<a href="<?php echo base_url();?>admin/recharge_history">
					<i class="icon-money"></i> 
					<span class="title">Recharge History</span>
					<span class="selected"></span>
					</a>
				</li>
				<li class="start ">
					<a href="<?php echo base_url();?>admin/recharge_setting">
					<i class="icon-money"></i> 
					<span class="title">Recharge Setting</span>
					<span class="selected"></span>
					</a>
				</li>
				<li class="has-sub ">
					<a href="<?php echo base_url();?>admin/cupon">
					<i class="icon-gift"></i> 
					<span class="title">Deals, Coupons & Tie ups</span>
					<span class="selected"></span>
					</a>
				</li>
				<?php } ?>
				<li class="has-sub ">
					<a href="<?php echo base_url();?>admin/calendar">
					<i class="icon-calendar"></i> 
					<span class="title">My Calendar</span>
					<span class="selected"></span>
					</a>
				</li>
			
				<li class="has-sub ">
					<a href="javascript:void(0)">
					<i class="icon-star"></i> 
					<span class="title">Rating and Reviews</span>
					<span class="selected"></span>
					</a>
				</li>
				<li class="has-sub ">
					<a href="<?php echo base_url();?>Cms/cms">
					<i class="icon-file"></i> 
					<span class="title">CMS</span>
					<span class="selected"></span>
					</a>
				</li>
				<li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-list-alt"></i> 
					<span class="title">Slots </span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li><a href="<?php echo base_url();?>admin/two_slots">Two Slots</a></li>
						<li ><a href="<?php echo base_url();?>admin/three_slots">Three Slots</a></li>
					</ul>
				</li>
				<!--<li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-bookmark-empty"></i> 
					<span class="title">Settings</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						<li ><a href="<?php echo base_url();?>Cms/settings">General Settings</a></li>
						<li ><a href="<?php echo base_url();?>Settings_cntlr/view_city">City Setting</a></li>
                        <li ><a href="<?php echo base_url();?>Venue_setting_cntlr/venue_setting_home1">Venue Settings</a></li>
					</ul>
				</li>-->
			<li class="has-sub ">
					<a href="javascript:;">
					<i class="icon-cog"></i> 
					<span class="title">Settings</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub">
						
						<li ><a href="<?php echo base_url();?>General_setting/list_brand">Brand</a></li>
						<li ><a href="<?php echo base_url();?>General_setting/list_city">City</a></li>
                        <li ><a href="<?php echo base_url();?>Venue_setting_cntlr/venue_setting_home1">Venue</a></li>
                        <li ><a href="<?php echo base_url();?>Venue_setting_cntlr/list_property">Venue Property</a></li>
                    </ul>
				</li>
			<!-- END SIDEBAR MENU -->
		</div>
