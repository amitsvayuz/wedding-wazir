<!-- BEGIN FOOTER -->
	<div class="footer">
		2016 &copy; vayuz.in
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<!-- END FOOTER -->
 <!-- BEGIN JAVASCRIPTS -->    
   <!-- Load javascripts at bottom, this will reduce page load time -->
   <script src="<?php echo base_url();?>assets/js/jquery-1.8.3.min.js"></script>    
   <script src="<?php echo base_url();?>assets/breakpoints/breakpoints.js"></script> 
    <script src="<?php echo base_url();?>assets/dropzone/dropzone.js"></script>   
   <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
   <script src="<?php echo base_url();?>assets/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
   <script src="<?php echo base_url();?>assets/js/jquery.blockui.js"></script>
   <script src="<?php echo base_url();?>assets/js/jquery.cookie.js"></script>
   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="<?php echo base_url();?>assets/js/excanvas.js"></script>
   <script src="<?php echo base_url();?>assets/js/respond.js"></script>
   <![endif]-->
   <?php echo @$map['js']; ?>
    <script type="text/javascript" src="<?php echo base_url();?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/uniform/jquery.uniform.min.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script> 
   <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
  

      <script>
        CKEDITOR.replace( 'ckeditor' );
      </script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-daterangepicker/date.js"></script>
   <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
   <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
   <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/data-tables/DT_bootstrap.js"></script>
  
	<script src="<?php echo base_url();?>assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>	
	<script src="<?php echo base_url();?>assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url();?>assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
	    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.2/moment.min.js"></script>
	 <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	 <script src="<?php echo base_url();?>assets/fancybox/source/jquery.fancybox.pack.js"></script>

	<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>	
	<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>	
	<script src="<?php echo base_url();?>assets/flot/jquery.flot.js"></script>
	<script src="<?php echo base_url();?>assets/flot/jquery.flot.resize.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-daterangepicker/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.pulsate.min.js"></script>
	 <script type="text/javascript" src="<?php echo base_url();?>assets/jquery-validation/dist/jquery.validate.min.js"></script>	
	<script src="<?php echo base_url();?>assets/js/app.js"></script>
	
	 <!-- END JAVASCRIPTS --> 
	<!-- Load javascripts at bottom, this will reduce page load time -->
    <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
		
			
		 //App.setPage('calendar');
		 App.setPage('table_managed');
		
         App.init();
      });
   </script>		
  
  <script type="text/javascript">
    $(function(){
    $("#eventdate").datepicker({format: 'yyyy-mm-dd'});
	$("#event_date").datepicker({format: 'yyyy-mm-dd'});
});
   </script>
 
	<script>
     $(function(){
		    $("#radio1").click(function() {
				 $("#freelancer").toggle();
				  $("#company").hide();
				 $('input[@name="optionsRadios1"]')[0].checked = false;
			});	
          
	       $("#radio2").click(function() {
			    $("#freelancer").hide();
			    $("#company").toggle();
				$('input[@name="optionsRadios1"]')[1].checked = false;
			});
     });
	</script>
	
	
	<script>
	  $(function(){
		$("#date").datepicker({format: 'yyyy-mm-dd'});
		});
		
		$(function(){
			$("#lasteventdate").datepicker({format: 'yyyy-mm-dd'});
		});

		$(function(){
			$("#dob").datepicker({format: 'yyyy-mm-dd'});
		});
		
		$(function(){
			$("#led").datepicker({format: 'yyyy-mm-dd'});
		});
   </script>
  
	 <style>
	body
	{
	font-family:arial;
	}
    .error {
	  width:220px;
      color: #a94442;
      background-color: #f2dede;
      border-color: #ebccd1;
      padding:1px 20px 1px 20px;
    }
	#preview
	{
	color:#cc0000;
	font-size:12px
	}
	.imgList 
	{
	max-height:150px;
	margin-left:5px;
	border:1px solid #dedede;
	padding:4px;	
	float:left;	
	}
	</style> 

	<script src="<?php echo base_url();?>assets/js/jquery.wallform.js"></script>
	
	<script>
	$(document).ready(function() { 
		 $('#photoimg').die('click').live('change', function(){ 
			           //$("#preview").html('');
				$("#imageform").ajaxForm({target: '#preview', 
				     beforeSubmit:function(){ 
					
					console.log('ttest');
					$("#imageloadstatus").show();
					 $("#imageloadbutton").hide();
					 }, 
					success:function(data){ //alert(data);
					if(data != false){
					 alert('Image Uploaded Successfully');
					 $("#imageform").hide();
					 $("#remove").show();	
					}else{
					 $("#imageloadstatus").hide();
					 $("#imageloadbutton").show();	
					  $("#photoimg").val("");
					}
				    console.log('test');
					}, 
					error:function(){ 
					console.log('xtest');
					 $("#imageloadstatus").hide();
					$("#imageloadbutton").show();
					} }).submit();
			});
			
			$("#remove").click(function(){  
			$("#preview").hide(); 
			$("#remove").hide();
			});
			
			$("#remove").click(function(){  
			$("#preview").hide(); 
			
			});
            
		}); 
	</script>
	<script>
	$(document).ready(function() { 
		 $('#venue_photoimg').die('click').live('change', function(){ 
			           //$("#preview").html('');
				$("#imageform").ajaxForm({target: '#preview', 
				     beforeSubmit:function(){ 
					
					console.log('ttest');
					$("#imageloadstatus").show();
					 $("#imageloadbutton").hide();
					 }, 
					success:function(data){ //alert(data);
					if(data != false){
					 alert('Image Uploaded Successfully');
					 $("#imageform").hide();
					 $("#remove").show();	
					}else{
					 $("#imageloadstatus").hide();
					 $("#imageloadbutton").show();	
					  $("#venue_photoimg").val("");
					}
				    console.log('test');
					}, 
					error:function(){ 
					console.log('xtest');
					 $("#imageloadstatus").hide();
					$("#imageloadbutton").show();
					} }).submit();
			});
			
			$("#remove").click(function(){  
			$("#preview").hide(); 
			$("#remove").hide();
			});
			
			$("#remove").click(function(){  
			$("#preview").hide(); 
			
			});
            
		}); 
	</script>
	<script>
	$(document).ready(function() { 
		 $('#planner_photoimg').die('click').live('change', function(){ 
			           //$("#preview").html('');
				$("#imageform").ajaxForm({target: '#preview', 
				     beforeSubmit:function(){ 
					
					console.log('ttest');
					$("#imageloadstatus").show();
					 $("#imageloadbutton").hide();
					 }, 
					success:function(data){ //alert(data);
					if(data != false){
					 alert('Image Uploaded Successfully');
					  $("#imageform").hide();
					 $("#remove").show();	
					}else{
					 $("#imageloadstatus").hide();
					 $("#imageloadbutton").show();	
					  $("#photoimg").val("");
					}
				    console.log('test');
					}, 
					error:function(){ 
					console.log('xtest');
					 $("#imageloadstatus").hide();
					$("#imageloadbutton").show();
					} }).submit();
			});
			
			$("#remove").click(function(){  
			$("#preview").hide(); 
			$("#remove").hide();
			});
			
			$("#remove").click(function(){  
			$("#preview").hide(); 
			
			});
            
		}); 
	</script>
	<script>
	$(document).ready(function() { 
		 $('#decorator_photoimg').die('click').live('change', function(){ 
			           //$("#preview").html('');
				$("#imageform").ajaxForm({target: '#preview', 
				     beforeSubmit:function(){ 
					
					console.log('ttest');
					$("#imageloadstatus").show();
					 $("#imageloadbutton").hide();
					 }, 
					success:function(data){ //alert(data);
					if(data != false){
					 alert('Image Uploaded Successfully');
					  $("#imageform").hide();
					 $("#remove").show();	
					}else{
					 $("#imageloadstatus").hide();
					 $("#imageloadbutton").show();	
					  $("#photoimg").val("");
					}
				    console.log('test');
					}, 
					error:function(){ 
					console.log('xtest');
					 $("#imageloadstatus").hide();
					$("#imageloadbutton").show();
					} }).submit();
			});
			
			$("#remove").click(function(){  
			$("#preview").hide(); 
			$("#remove").hide();
			});
			
			$("#remove").click(function(){  
			$("#preview").hide(); 
			
			});
            
		}); 
	</script>
	
	
<!--
     <script>
		$(document).ready(function(){
			$("#delete").click(function(){
				alert("Are you sure want to delete?.");
			});
			$("#delete_decorator").click(function(){
				alert("Are you sure want to delete?.");
			});
		});
	</script> -->
   
	 <script>
		$(document).ready(function(){
			$("#current_password").change(function(){
				var current_pass = $("#current_password").val();
				var dataString = 'curr_pass=' + current_pass;
				var base_url = "<?php echo base_url();?>";
			   
				$.ajax({
					type: 'POST',
					url: base_url + 'admin/check_password',
					data: dataString,
					success: function(result){
					  if(!empty(result)){
						  alert("Your old password is not matching");
						  $("#current_password").val("");
					  }
					  
					}});
				return false;
				
			});
		});
	</script> 
	 <script>
		$(document).ready(function(){
			$("#select_provider").change(function(){
				var select_provider = $("#select_provider").val();
				var dataString = 'role=' + select_provider;
				var base_url = "<?php echo base_url();?>";
			    //alert(select_provider);
				//alert(dataString);
				$.ajax({
					type: 'POST',
					url: base_url + 'sp_manager/get_role_names',
					data: dataString,
					success: function(result){//alert(result);
					 $("#provider_names").empty(""); 
					 $("#provider_names").append(result); 
					}});
				return false;
				
			});
		});
	</script> 
	 
</body>
<!-- END BODY -->
</html>
