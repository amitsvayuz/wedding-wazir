<?php include('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">			
						<h3 class="page-title">
							Inquiry
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li class="start active">
								<a href="<?php echo base_url();?>Inquiry">Inquiry</a>
								<i class="icon-angle-right"></i>
							</li>
							<!--<li><a href="#">Managed Tables</a></li>-->
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
					
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
						           <?php  if($this->session->flashdata('message')):?>
								   <?php $message=$this->session->flashdata('message');?>
								   <div class="alert alert-<?php echo $message['class'];?>">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo $message['set'];?>
								    </div>
									<?php endif;?>
					                <?php if(@$resp):?>
									<div class="alert alert-<?php echo $message['class'];?>">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo @$resp;?>
									</div>
								    <?php endif;?>	
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>Inquiry</h4>
								
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group">
										<a href="<?php echo base_url();?>Inquiry/add_inquiry" id="sample_editable_1_new" class="btn green">
										Add New <i class="icon-plus"></i>
										</a>
									</div>
								</div>
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th class="hidden-480">Name</th>
											<th class="hidden-480">Email</th>
											<th class="hidden-480">Contact</th>
											<th class="hidden-480">Description</th>
											<th class="hidden-480">Date</th>
											<th class="hidden-480">Range</th>
											<th class="hidden-480">Status</th>
											<th class="hidden-480">Action</th>
										</tr>
									</thead>
									<tbody>
									    <?php 	if(isset($inquiry)):?>
									    <?php 	foreach($inquiry as $row):?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
											<td class="hidden-480"><?php echo $row->name;?></td>
											<td class="hidden-480"><?php echo $row->email;?></td>
											<td class="hidden-480"><?php echo $row->contact_num;?></td>
											<td class="hidden-480"><?php echo substr($row->decs,0,50);?></td>
											<td class="hidden-480"><?php echo date('m/d/Y',$row->created_date);?></td>
											<td class="hidden-480"><?php echo $row->guest_range;?></td>
											<td class="hidden-480"><a href="" onclick=" return changestus('<?php echo $row->inq_id;?>','<?php echo $row->status;?>')" class="btn <?php if($row->status == 0): echo 'red'; else: echo 'green'; endif; ?> mini"><?php if($row->status == 0): echo 'Inactive'; else: echo 'Active'; endif; ?></a></td>
											<td class="hidden-480"><a href="<?php echo base_url();?>Inquiry/add_inquiry/<?php echo $row->inq_id; ?>"><i class="icon-edit"></i></a> | <a  href="<?php echo base_url();?>Inquiry/delete_inquiry/<?php echo $row->inq_id;?>" onclick=" return confirm('Are You Sure Delete?');" ><i class="icon-trash"></i></a></td>
										</tr>
										<?php endforeach; endif;?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<?php include('includes/footer.php');?>
	<script>
	function changestus(id,status)
	{
		var id = id;
		var status = status;
		var url = "<?php echo base_url();?>Inquiry/active_inquiry";
		$.ajax({
				type: "POST",
				url: url,
				data : {									
				'id'     : id,
                'status' : status				
				},
			    cache: false,
				datatype: 'text',
				success: function(res)
				{
					
					location.reload();
					
				}
			});
		    return false;
	}
	</script>
    