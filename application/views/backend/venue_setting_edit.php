<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->   	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Setting			
							<small>Manage Setting</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="<?php echo base_url();?>Venue_setting_cntlr/venue_setting_home1">Venue Settings</a></li>
							<i class="icon-angle-right"></i>
							<li><a href="<?php echo base_url();?>Venue_setting_cntlr/edit_venue/<?php echo $this->uri->segment(3);?>/<?php echo $this->uri->segment(4);?>">Edit Setting</a>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
		<!-- END PAGE -->
	
	<!-- END CONTAINER -->
	<div class="tab-pane" id="tab_1_4">
			<div class="row-fluid">
               <div class="span12">
			   <?php 
			   $idfrupdate=$idfru;
			   switch($idfrupdate){
				   case 1:
				   $heading='Wedding occasion list';
				   break;
				   case 2:
				   $heading='Non-wedding occasion list';
				   break;
				   case 3:
				   $heading='Nature of venue';
				   break;
				   case 4:
				   $heading='Venue type';
				   break;
				   case 5:
				   $heading='Venue type other';
				   break;
				   case 6:
				   $heading='Floor';
				   break;
				   case 7:
				   $heading='Suit type';
				   break;
				   case 8:
				   $heading='Valet facility ';
				   break;
				   case 9:
				   $heading='Seating type';
				   break;
				   case 10:
				   $heading='Other services';
				   break;
				   
			   }
			   
			   ?>
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i><?php echo $heading;?></h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Venue_setting_cntlr/venue_update/<?php echo $id;?>/<?php echo $idfru;?>" method="post" class="form-horizontal">
                           <?php 
							foreach($result as $row):
						   ?>
										
						   
						   <div class="control-group">
                              <label class="control-label">Name</label>
                               <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee" value="<?php echo $row->name; ?>"/>
                                
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"><?php echo $row->description; ?></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">update</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
						   <?php endforeach;?>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
			</div>
		</div>
		</div>
	<?php include_once('includes/footer.php');?>
