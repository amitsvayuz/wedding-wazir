<?php include_once('includes/header.php');?>
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php include_once('includes/sidebar.php');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
           <div class="span12">
                
                  <h3 class="page-title">
                    Services
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
                        <span class="icon-angle-right"></span>
                     </li>
<li>
                        <a href="<?php echo base_url();?>decorator/list_decorator_services">Services</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                  </ul>
               </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="portlet box light-grey" id="form_wizard_1">
                     <div class="portlet-title">
                        <h4>
                           <i class="icon-reorder"></i> Services
                        </h4>
                     
                     </div>
                     <div class="portlet-body form">
                        <form  method="post" action="<?php echo base_url();?>decorator/edit_decorator_services" class="form-horizontal">
                           <div class="form-wizard">
                              <div class="tab-content">
                                	<div class="control-group">
                                       <label class="control-label">Services :</label>
		<?php $services = explode(',' , $services_details->services);
		?>	
                                       <div class="controls">
										   <label class="checkbox">
     <input  <?php if(in_array('DJ And Music' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="DJ And Music" />DJ And Music
                                          </label>
										   <label class="checkbox">
    <input <?php if(in_array('Flower Decoration' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Flower Decoration" />Flower Decoration
                                          </label>
										   <label class="checkbox">
    <input  <?php if(in_array('Crockery And Furniture' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Crockery And Furniture" />Crockery And Furniture
                                          </label>
										   <label class="checkbox">
    <input <?php if(in_array('Theme Decoration' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Theme Decoration" />Theme Decoration
                                          </label>
                                       </div>
                                    </div>
					<?php $other_services = explode(',' , $services_details->other_services);
		?>				
							        <div class="control-group">
                                       <label class="control-label">Other Services :</label>
                                       <div class="controls">
										   <label class="checkbox">
  <input <?php if(in_array('Catering Service' ,$other_services))echo 'checked';?> type="checkbox" name="other_services[]" value="Catering Service" />Catering Service
                                          </label>
										   <label class="checkbox">
  <input <?php if(in_array('Audio Visual Aids' ,$other_services))echo 'checked';?> type="checkbox" name="other_services[]" value="Audio Visual Aids" />Audio Visual Aids
                                          </label>
										   <label class="checkbox">
   <input <?php if(in_array('Wedding Decoration' ,$other_services))echo 'checked';?> type="checkbox" name="other_services[]" value="Wedding Decoration" />Wedding Decoration
                                          </label>
										  <label class="checkbox">
   <input <?php if(in_array('Home Decoration' ,$other_services))echo 'checked';?> type="checkbox" name="other_services[]" value="Home Decoration" />Home Decoration
                                          </label>
                                       </div>
                                    </div>
                              </div>
  <div class="control-group">
                                      
                                       <div class="controls">
                              <div class="submit-btn">
							<button type="submit" class="btn green">Save </button>
								<a href="<?php echo base_url();?>decorator/list_services" class="btn">Cancel</a>
							</div>
</div>
</div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
 <?php include_once('includes/footer.php');?>