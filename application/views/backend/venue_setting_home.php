<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->   	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Venue Settings			
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="<?php echo base_url();?>Venue_setting_cntlr/venue_setting_home1">Venue Settings</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<?php  if(@$_REQUEST['set']):?>
								   <div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo base64_decode(@$_REQUEST['set']);?>
								    </div>
									<?php endif;?>
									
									<div class="row-fluid" style="margin-bottom:2%;">
									<div class="btn-group">
										<a href="<?php echo base_url();?>Venue_setting_cntlr/add_venue"><button id="sample_editable_1_new" class="btn green">
										Add Venue Settings <i class="icon-plus"></i>
										</button></a>
									</div>
									
								</div>

			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Wedding occasion </h4>
                        <div class="tools">
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Name</th>
											<th class="hidden-480"> Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach($result1 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?></td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>
										<a href="<?php echo base_url();?>Venue_setting_cntlr/edit_venue/<?php echo $row->id;?>/1"><i class="icon-edit"></i></a> | 
										<a href="<?php echo base_url();?>Venue_setting_cntlr/delete_venue/<?php echo $row->id;?>/1"><i class="icon-trash"></i></a>
								
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			
			
			
			<!--------------------------------------new row1-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Non-wedding occasion </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">name</th>
											<th class="hidden-480">Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach($result2 as $row):
											?>
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?></td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
								
										<a href="<?php echo base_url();?>Venue_setting_cntlr/edit_venue/<?php echo $row->id;
											?>/2"><i class="icon-edit"></i></a>
										<a href="<?php echo base_url();?>Venue_setting_cntlr/delete_venue/<?php echo $row->id;
											?>/2"><i class="icon-trash"></i></a>
		
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			
			<!--------------------------------------new row2-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Nature of venue</h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">name</th>
											<th class="hidden-480">Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result3 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									
										<a href="<?php echo base_url();?>Venue_setting_cntlr/edit_venue/<?php echo $row->id;
											?>/3"><i class="icon-edit"></i></a>
										<a href="<?php echo base_url();?>Venue_setting_cntlr/delete_venue/<?php echo $row->id;
											?>/3"><i class="icon-trash"></i></a>

								
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			
			<!--------------------------------------new row3-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Venue type </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">name</th>
											<th class="hidden-480">Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result4 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									
										<a href="<?php echo base_url();?>Venue_setting_cntlr/edit_venue/<?php echo $row->id;
											?>/4"><i class="icon-edit"></i></a>
										<a href="<?php echo base_url();?>Venue_setting_cntlr/delete_venue/<?php echo $row->id;
											?>/4"><i class="icon-trash"></i></a>
										
									
									
								
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			<!--------------------------------------new row4-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Venue type other</h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480"> name</th>
											<th class="hidden-480"> Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result5 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									
										<a href="<?php echo base_url();?>Venue_setting_cntlr/edit_venue/<?php echo $row->id;
											?>/5"><i class="icon-edit"></i></a>
										<a href="<?php echo base_url();?>Venue_setting_cntlr/delete_venue/<?php echo $row->id;
											?>/5"><i class="icon-trash"></i></a>
										
								
												</td>
										</tr>
									<?php endforeach;?>
									
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			<!--------------------------------------new row5-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Floor</h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Name</th>
											<th class="hidden-480">Style Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result6 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									
										<a href="<?php echo base_url();?>Venue_setting_cntlr/edit_venue/<?php echo $row->id;
											?>/6"><i class="icon-edit"></i></a>
										<a href="<?php echo base_url();?>Venue_setting_cntlr/delete_venue/<?php echo $row->id;
											?>/6"><i class="icon-trash"></i></a>
										
								
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			<!--------------------------------------new row6-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Suite type</h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Name</th>
											<th class="hidden-480"> Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result7 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									
										<a href="<?php echo base_url();?>Venue_setting_cntlr/edit_venue/<?php echo $row->id;
											?>/7"><i class="icon-edit"></i></a>
										<a href="<?php echo base_url();?>Venue_setting_cntlr/delete_venue/<?php echo $row->id;
											?>/7"><i class="icon-trash"></i></a>
										
								
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			<!--------------------------------------new row7-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Valet facility</h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Name</th>
											<th class="hidden-480">Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result8 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									
										<a href="<?php echo base_url();?>Venue_setting_cntlr/edit_venue/<?php echo $row->id;
											?>/8"><i class="icon-edit"></i></a>
										<a href="<?php echo base_url();?>Venue_setting_cntlr/delete_venue/<?php echo $row->id;
											?>/8"><i class="icon-trash"></i></a>
										
								
												</td>
										</tr>
									<?php endforeach;?>
									
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			
			
			<!--------------------------------------new row8-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Seating type</h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Name</th>
											<th class="hidden-480"> Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result7 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									
										<a href="<?php echo base_url();?>Venue_setting_cntlr/edit_venue/<?php echo $row->id;
											?>/9"><i class="icon-edit"></i></a>
										<a href="<?php echo base_url();?>Venue_setting_cntlr/delete_venue/<?php echo $row->id;
											?>/9"><i class="icon-trash"></i></a>
										
								
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			
			
			<!--------------------------------------new row9-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Other services</h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Name</th>
											<th class="hidden-480"> Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result7 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									
										<a href="<?php echo base_url();?>Venue_setting_cntlr/edit_venue/<?php echo $row->id;
											?>/10"><i class="icon-edit"></i></a>
										<a href="<?php echo base_url();?>Venue_setting_cntlr/delete_venue/<?php echo $row->id;
											?>/10"><i class="icon-trash"></i></a>
										
								
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
	<?php include_once('includes/footer.php');?>
