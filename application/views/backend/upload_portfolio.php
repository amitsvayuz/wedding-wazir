<?php include('includes/header.php');?>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/fileinput.js" type="text/javascript"></script>

           <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/chosen-bootstrap/chosen/chosen.css" />
          <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/jquery-tags-input/jquery.tagsinput.css" />

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid full-width-page">
		<!-- BEGIN EMPTY PAGE SIDEBAR -->
		<div class="page-sidebar nav-collapse">
		</div>
		<!-- END EMPTY PAGE SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>
						<!-- END BEGIN STYLE CUSTOMIZER --> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Portfolio				<small>Upload portfolio</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><?php if($this->session->userdata('role')=="venue"){?>
								<a href="<?php echo base_url();?>planner/list_planner_images/<?php echo $vid;?>">View portfolio</a>
								<?php }else{?>								
								<a href="<?php echo base_url();?>planner/list_planner_images">View portfolio</a>
								<?php }?>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="javascript:void">Portfolio</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				
				<div class="row-fluid">
					<div class="span12">
					
						
						<div id="image_div">
           
             <form id="image_form" method="post" action="../php/upload.php"  enctype="multipart/form-data">
             
                 
                <div class="form-group">
                    <input id="file-1" name="file" type="file" multiple class="file" data-overwrite-initial="false" data-min-file-count="2">
                </div>
               
            </form>
            
            
            
          
        </div>
						
	

					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer">
		2013 &copy; Metronic by keenthemes.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	<script>
  
     $("#file-1").fileinput({
      //showRemove:false,
      //showPreview: false,
      uploadUrl: "<?php echo base_url()?>planner/upload", // server upload action
      uploadAsync: true,
      uploadExtraData: function() {
          return {
              album: '<?php echo $album_id;?>'
          };
      }
   });

 // CATCH RESPONSE
$('#file-1').on('filebatchuploaderror', function(event, data, previewId, index) {
var form = data.form, files = data.files, extra = data.extra, 
    response = data.response, reader = data.reader;

});


$('#file-1').on('filebatchuploadsuccess', function(event, data, previewId, index) {
   var form = data.form, files = data.files, extra = data.extra, 
    response = data.response, reader = data.reader;
    alert (extra.bdInteli + " " +  response.uploaded);
});
	</script>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS -->
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="<?php echo base_url()?>assets/js/jquery-1.8.3.min.js"></script>			
	<script src="<?php echo base_url()?>assets/breakpoints/breakpoints.js"></script>			
	<script src="<?php echo base_url()?>assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>	
	<script src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery.blockui.js"></script>
	<script src="<?php echo base_url()?>assets/js/jquery.cookie.js"></script>
	<script src="<?php echo base_url()?>assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>	
	<script type="text/javascript" src="<?php echo base_url()?>assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	 <script type="text/javascript" src="<?php echo base_url()?>assets/jquery-tags-input/jquery.tagsinput.min.js"></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script src="<?php echo base_url()?>assets/js/app.js"></script>		
	<script>
		jQuery(document).ready(function() {			
			// initiate layout and plugins
			App.setPage('calendar');
			App.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
