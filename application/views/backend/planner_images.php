   <?php include_once('includes/header.php');?>
  
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php include_once('includes/sidebar.php');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                 
                  <h3 class="page-title">
                    Portfolio
                     
                  </h3>
				   <?php if($this->session->userdata('message')){?>
					  <div class="alert alert-error">
						<button class="close" data-dismiss="alert"></button>
					   <span style='color:green;'><?php echo $this->session->userdata('message');?></span>
					  </div>
					<?php } ?>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                    <li><a href="<?php echo base_url();?>planner/image_upload_planner">Portfolio</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
			<div id="preview">
			</div>
<a style="display:none;" id="remove" href="<?php echo base_url();?>planner/remove_images?id=<?php echo $this->session->userdata('id');?>&url=<?php echo base_url();?>">Remove</a>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Photography</h4>
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form id='imageform' style="clear:both" action="<?php echo base_url();?>planner/image_upload_planner" method='post' enctype="multipart/form-data" class="form-horizontal">
                        <div class="control-group">
                              <label class="control-label">Image Upload</label>
                              <div id='imageloadbutton' class="controls">
							       <input type="file" name="photos[]" id="planner_photoimg" required multiple class="default" />
								    <input type="hidden" name="hidden" value="<?php echo base_url();?>"/>
                              </div>
							  <div id='imageloadstatus' style='display:none'>
									<img src="<?php echo base_url();?>assets/img/loader.gif" alt="Uploading...."/>
							  </div>
						 </div>
                        </form>
                        <!-- END FORM-->  
						 <form id='imagenewform' action="<?php echo base_url();?>planner/image_uploadplanner" method='post' enctype="multipart/form-data" class="form-horizontal">
						     
							<div class="control-group">
                              <label class="control-label">Title :</label>
                              <div class="controls">
                                 <div class="input-icon left">
                                    <input class="m-wrap " type="text" name='title' required placeholder="Photograph Title" />    
                                 </div>
                              </div>
                           </div>
						 
						   	<div class="control-group">
                              <label class="control-label">Events Tags:</label>
                              <div class="controls">
                                 <div class="input-icon left">
                                    <input class="m-wrap " type="text" name='event_tags[]' value="engagement,pre_wedding_services,wedding" required placeholder="Photograph Title" />    
                                 </div>
                              </div>
                           </div>
						 
						    <div class="control-group">
                              <label class="control-label">Arrangements Tags :</label>
                              <div class="controls">
                                 <div class="input-icon left">
                                    <input class="m-wrap " type="text" name='arrangement_tags[]' value="engagement,pre_wedding_services,weddings" required placeholder="Photograph Title" />    
                                 </div>
                              </div>
                           </div>
						   
							<div class="control-group">
                              <label class="control-label">Location :</label>
                              <div class="controls">
                                 <div class="input-icon left">
                                    <input class="m-wrap " type="text" required name='location' required placeholder="Location" />    
                                 </div>
                              </div>
                           </div>
						   
						   <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                           </div>
						</form>
                     </div>
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div> 
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
  
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php include_once('includes/footer.php');?>
   