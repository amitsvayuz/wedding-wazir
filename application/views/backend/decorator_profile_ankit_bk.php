
<?php include('includes/header.php');?>


	<div class="page-container row-fluid">
		<?php include('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							 Profile
						</h3>
						<ul class="breadcrumb">
						 <li>
                        <i class="icon-home"></i>
							<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
							<span class="icon-angle-right"></span>
                        </li>
							<li><a href="<?php echo base_url();?>sp_manager/profile?id=<?php echo $_GET['id']?>">Decorator Profile</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid profile">
					<div class="span12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab_1_1" data-toggle="tab">Overview</a></li>
								<li><a href="#tab_1_3" data-toggle="tab">Account</a></li>
								<li><a href="#tab_1_4" data-toggle="tab">Business Manager</a></li>
								
							</ul>
							<div class="tab-content">
								<div class="tab-pane row-fluid active" id="tab_1_1">
									<ul class="unstyled profile-nav span3">
										<li><img src="<?php echo base_url()?>uploads/<?php echo $result[0]->id;?>/<?php echo $result[0]->pro_pic;?>" alt="" /></li>
									</ul>
									<div class="span9">
										<div class="row-fluid">
										<div class="span8 profile-info">
												<h1><?php echo $result[0]->user_name;?></h1>
														<div style="border:1px;border:solid;border-color:green;width:500px;padding-left:10px;">
		<?php if(!empty($result[0]->company_description)){ 
		echo substr($result[0]->company_description , 0 , 70);
		}else{ ?>
		Description
	<?php } ?>
		</div>
												<ul class="unstyled inline">
				<li><b><i class="icon-map-marker"></i> <?php echo ucfirst($result[0]->country);?> , <?php echo ucfirst($result[0]->state);?> , <?php echo ucfirst($result[0]->city);?> </li></b>
					<li><i class="icon-calendar"></i> <b> <?php echo date('Y-m-d',$result[0]->d_o_b);?></b></li>
					<li><i class="icon-envelope-alt"></i> <b><?php echo $result[0]->user_email;?></b></li>
					<li><i class="icon-phone"></i> <b><?php echo $result[0]->contact_no;?></b></li>
					<li><i class="icon-facebook"></i> </li>
					<li><i class="icon-google-plus"></i> </li>
												</ul>
											</div>
											
										</div>
										<!--end row-fluid-->
										<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab_1_11" data-toggle="tab">Latest Customers</a></li>
												<li class=""><a href="#tab_1_22" data-toggle="tab">Feeds</a></li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_11">
													<div class="portlet-body" style="display: block;">
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
																<tr>
																	<th><i class="icon-briefcase"></i> Company</th>
																	<th class="hidden-phone"><i class="icon-question-sign"></i> Descrition</th>
																	<th><i class="icon-bookmark"></i> Amount</th>
																	<th></th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td><a href="#">Pixel Ltd</a></td>
																	<td class="hidden-phone">Server hardware purchase</td>
																	<td>52560.10$ <span class="label label-success label-mini">Paid</span></td>
																	<td><a class="btn mini green-stripe" href="#">View</a></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
												<!--tab-pane-->
												<div class="tab-pane" id="tab_1_22">
													<div class="tab-pane active" id="tab_1_1_1">
														<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
															<ul class="feeds">

																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			3 hours
																		</div>
																	</div>
																</li>
															
															</ul>
														</div>
													</div>
												</div>
												<!--tab-pane-->
											</div>
										</div>
									</div>
									<!--end span9-->
								</div>
								<!--end tab-pane-->
								
								<!--tab_1_2-->
								<div class="tab-pane profile-classic row-fluid" id="tab_1_5">
									<div class="span2"><img src="<?php echo base_url()?>uploads/<?php echo $result[0]->id;?>/<?php echo $result[0]->pro_pic;?>" alt="" /> <a href="<?php echo base_url();?>sp_manager/profile?id=<?php echo base64_encode($result[0]->id);?>" class="profile-edit">edit</a></div>
									<ul class="unstyled span10">
									<?php if(!empty($result[0]->user_name)){?>
										<li><span>User Name:</span> <?php echo ucfirst($result[0]->user_name);?></li>
										<?php } ?>
										<?php if(!empty($result[0]->country)){?>
										<li><span>Country:</span> <?php echo ucfirst($result[0]->country);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->state)){?>
										<li><span>State:</span> <?php echo ucfirst($result[0]->state);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->city)){?>
										<li><span>City:</span> <?php echo ucfirst($result[0]->city);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->street)){?>
										<li><span>Street:</span> <?php echo ucfirst($result[0]->street);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->d_o_b)){?>
										<li><span>Birthday:</span> <?php echo date('Y-m-d',$result[0]->d_o_b);?></li>
										<?php } ?>
										<?php if(!empty($result[0]->role_type)){?>
										<li><span>Occupation:</span> <?php echo ucfirst($result[0]->role_type);?></li>
										<?php } ?>
										<li><span>Email:</span> <a href="#"><?php echo $result[0]->user_email;?></a></li>
										<li><span>Mobile Number:</span><?php echo $result[0]->contact_no;?></li>
									</ul>
								</div>
								<div class="tab-pane row-fluid profile-account" id="tab_1_3">
									<div class="row-fluid">
										<div class="span12">
											<div class="span3">
												<ul class="ver-inline-menu tabbable margin-bottom-10">
													<li class="active">
														<a data-toggle="tab" href="#tab_1-1">
														<i class="icon-cog"></i> 
														Personal Details
														</a> 
														<span class="after"></span>                           			
													</li>
													<li class=""><a data-toggle="tab" href="#tab_4-4"><i class="icon-picture"></i> Company Details</a></li>
													<li class=""><a data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i> Change Password</a></li>
													
												</ul>
											</div>
											<div class="span9">
												<div class="tab-content">
													<div id="tab_1-1" class="tab-pane active">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse" >
	<form action="<?php echo base_url();?>sp_manager/update_user_detail" id="validation" method="post" class="form-horizontal" enctype="multipart/form-data">
	 <div class="control-group">
		<label class="control-label">Name :</label>
	   <div class="controls">
		<input type="text" name="user_name" <?php if(!empty($result[0]->user_name)){ ?> value="<?php echo $result[0]->user_name;?>" <?php }?>  class="m-wrap span8" />
	</div>
	</div>
	<div class="control-group">
		<label class="control-label">Gender :</label>
	 <div class="controls">
		<input type="radio" value="M" name="gender" class="m-wrap span8" <?php if($result[0]->gender == 'M'){ echo "checked"; }?>/>Male
		<input type="radio" name="gender" value="F" class="m-wrap span8" <?php if($result[0]->gender == 'F'){ echo "checked"; }?>/>Female
	</div>
	</div>
	<div class="control-group">
		<label class="control-label">Date of Birth :</label>
		 <div class="controls">
		<input type="text" name="dob" id="dob" <?php if(!empty($result[0]->d_o_b)){ ?> value="<?php echo $result[0]->d_o_b;?>" <?php }?> class="m-wrap span8" />
	</div>
	</div>
	<div class="control-group">
		<label class="control-label">Mobile Number :</label>
	 <div class="controls">
		<input type="text" name="mobile" <?php if(!empty($result[0]->mob_no)){ ?> value="<?php echo $result[0]->mob_no;?>" <?php }?> class="m-wrap span8" />
	</div>
	</div>
     <div class="control-group">
		<label class="control-label">Alternate Number :</label>
	 <div class="controls">
		<input type="text" name="contact" <?php if(!empty($result[0]->contact_no)){ ?> value="<?php echo $result[0]->contact_no;?>" <?php }?> class="m-wrap span8" />
	</div>
	</div>
	 <div class="control-group">
		<label class="control-label">Profile Image :</label>
	 <div class="controls">
		<input name="pro_pic" value="<?php echo $result[0]->pro_pic;?>" type="file"  class="default" />
		<input name="sp_id" value="<?php echo $result[0]->id;?>" type="hidden"  class="default" />
		<input name="user_role" value="<?php echo $result[0]->role_type;?>" type="hidden"  class="default" />
	</div>
	</div>
	 <div class="control-group">
	 <div class="controls">
		<div class="submit-btn">
			<button class="btn green">Save</button>
			<a href="<?php echo base_url();?>sp_manager/decorator" class="btn">Cancel</a>
		</div>
	</div>
	</div>
	</form>
														</div>
													</div>
													
													
													
													<div id="tab_3-3" class="tab-pane">
													     <div style="height: auto;" id="accordion3-3" class="accordion collapse">
															<form action="<?php echo base_url();?>admin/change_password" method="post" class="form-horizontal">
															  <div class="control-group">
																<label class="control-label">Current Password :</label>
																<div class="controls">
																	<input type="password" name="current_password" id="current_password" value="" class="m-wrap span8" />
																</div>
																</div>
																<div class="control-group">
																<label class="control-label">New Password :</label>
																<div class="controls">
																	<input id="password" type="password" name="password" class="m-wrap span8" />
															    </div>
																</div>
																<div class="control-group">
																<label class="control-label">Re-type New Password :</label>
															    <div class="controls">
																	<input id="new_password" type="password" name="password" class="m-wrap span8" />
															    </div>
																</div>
																<div class="control-group">
																<div class="controls">
																<div class="submit-btn">
																	<button onsubmit="return myFunction()" id="submit" class="btn green">Update Password</button>
																	<a href="<?php echo base_url();?>sp_manager/decorator" class="btn">Cancel</a>
																<input name="sp_id" value="<?php echo $result[0]->id;?>" id="hidden_password" type="hidden"  class="default" />
																<input name="user_role" value="<?php echo $result[0]->role_type;?>" type="hidden"  class="default" />
																</div>
																 </div>
																</div>
															</form>
														</div>
													</div>
													<div id="tab_4-4" class="tab-pane">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse">
	<form action="<?php echo base_url();?>sp_manager/update_company_detail" method="post" class="form-horizontal">
	 <div class="control-group">	
		<label class="control-label">Company Name :</label>
	 <div class="controls">
		<input name="company_name" type="text" value="<?php echo $result[0]->company_name;?>"  class="m-wrap span8" />
	</div>
</div>
 <div class="control-group">	
		<label class="control-label">Description :</label>
		 <div class="controls">
		<textarea name="company_description" class="span8 m-wrap" rows="3"><?php echo $result[0]->company_description;?></textarea>
</div>
</div>
 
 <div class="control-group">	
		<label class="control-label">Country :</label>
		<div class="controls">
			<input type="text" name="country" <?php if(!empty($result[0]->country)){?>value="<?php echo $result[0]->country;?>"<?php } ?> class="span8 m-wrap" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;US&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" />
			<p class="help-block"><span class="muted"></span></p>
		</div>
	</div>
	 <div class="control-group">	
		<label class="control-label">State :</label>
		<div class="controls">
			<input type="text" name="state" <?php if(!empty($result[0]->state)){?>value="<?php echo $result[0]->state;?>"<?php } ?> class="span8 m-wrap" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;US&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" />
			<p class="help-block"><span class="muted"></span></p>
		</div>
		</div>
		 <div class="control-group">	
		<label class="control-label">City :</label>
		<div class="controls">
		<input type="text" name="city" value="<?php echo $result[0]->city;?>"  class="m-wrap span8" />
		</div>
		</div>
		 <div class="control-group">
		<label class="control-label">Street :</label>
		<div class="controls">
		<input type="text" name="street" value="<?php echo $result[0]->street;?>"  class="m-wrap span8" />
		</div>
		</div>
		 <div class="control-group">
		<label class="control-label">Zip Code :</label>
		<div class="controls">
		<input type="text" name="zip_code" value="<?php echo $result[0]->zip_code;?>"  class="m-wrap span8" />
		</div>
		</div>
		 
		<div class="control-group">
		<label class="control-label">Landmark :</label>
		<div class="controls">
		<input type="text" name="landmark" value="<?php echo $result[0]->landmark;?>"  class="m-wrap span8" />
		</div>
		</div>
		<div class="control-group">
		<label class="control-label">Facebook Url :</label>
		<div class="controls">
		<input type="text" name="fb_url" value="<?php echo $result[0]->fb_url;?>"  class="m-wrap span8" />
			</div>
		</div>
			<div class="control-group">
		<label class="control-label">Google Plus Url :</label>
		<div class="controls">
		<input type="text" name="gp_url" value="<?php echo $result[0]->gp_url;?>"  class="m-wrap span8" />
		<input name="hidden" value="<?php echo $result[0]->id;?>" type="hidden"  class="default" />
		<input name="user_role" value="<?php echo $result[0]->role_type;?>" type="hidden"  class="default" />
			</div>
		</div>
		<div class="control-group">
		<div class="controls">
		<div class="submit-btn">
			<button class="btn green">Save</button>
			<a href="<?php echo base_url();?>sp_manager/decorator" class="btn">Cancel</a>
		</div>
		</div>
		</div>
	</form>
														</div>
													</div>
													
													
												</div>
											</div>
											<!--end span9-->                                   
										</div>
									</div>
								</div>
								<!--end tab-pane-->
								
								<!--end tab-pane-->
								
								<!--end tab-pane-->
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	
	<?php include('includes/footer.php');?>