<?php include_once('includes/header.php');?>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                 
                  <!-- END BEGIN STYLE CUSTOMIZER -->   
                 
                 
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="portlet box blue" id="form_wizard_1">
                     <div class="portlet-title">
                        <h4>
                           <i class="icon-reorder"></i> Photographer <span class="step-title">Addition</span>
                        </h4>
                        <div class="tools hidden-phone">
                           <a href="javascript:;" class="collapse"></a>
                           <a href="#portlet-config" data-toggle="modal" class="config"></a>
                           <a href="javascript:;" class="reload"></a>
                           <a href="javascript:;" class="remove"></a>
                        </div>
                     </div>

                     <div class="portlet-body form">
                        <form action="<?php echo base_url();?>photographer/editphotographer" method = "post" class="form-horizontal">
                           <div class="form-wizard">
                               <h4 class="block">Provide your account details</h4>
                                    <div class="control-group">
                                       <label class="control-label">Company Name</label>
                                       <div class="controls">
                                          <input type="text" name="company" value="<?php echo $photographer_details->company_name;?>" class="span4 m-wrap"/>
										  <h6 style="color:#D51D59"><?php echo form_error('company'); ?></h6>
                                       </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">About Us</label>
                                       <div class="controls">
                                          <textarea rows="5" cols="15" name="about_us" class="span4 m-wrap"><?php echo $photographer_details->about_us;?></textarea>
										  <h6 style="color:#D51D59"><?php echo form_error('about_us'); ?></h6>
                                       </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">Cost</label>
                                       <div class="controls">
                                          <input type="text" name="cost" value="<?php echo $photographer_details->cost;?>" class="span4 m-wrap" />
										   <h6 style="color:#D51D59"><?php echo form_error('cost'); ?></h6>
                                        </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Count of events</label>
                                       <div class="controls">
                                          <input type="text" name="count_events" value="<?php echo $photographer_details->count_events;?>"class="span4 m-wrap" />
										   <h6 style="color:#D51D59"><?php echo form_error('count_events'); ?></h6>
                                        </div>
                                    </div>
                                     <div class="control-group">
                                       <label class="control-label">Last Event Date</label>
                                       <div class="controls">
                                          <input type="text" name="event_date" id="eventdate" value="<?php echo $photographer_details->event_date;?>" class="span4 m-wrap event_date" />
										  <h6 style="color:#D51D59"><?php echo form_error('event_date'); ?></h6>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">Experience</label>
                                       <div class="controls">
                                          <select name="exp" class="span4 m-wrap">
										  <option value="">Please select option</option>
										  <option <?php if($photographer_details->experienced == 0) echo 'selected="selected"'; ?> value="0">Less than 1 year</option>
										  <option <?php if($photographer_details->experienced == 1) echo 'selected="selected"'; ?> value="1">1 year</option>
										  <option <?php if($photographer_details->experienced == 2) echo 'selected="selected"'; ?> value="2">2 year</option>
										  <option <?php if($photographer_details->experienced == 3) echo 'selected="selected"'; ?> value="3">3 year</option>
										  </select>
                                       </div>
									   <h6 style="color:#D51D59"><?php echo form_error('exp'); ?></h6>
									</div>
                                    <div class="control-group">
                                       <label class="control-label">Email</label>
                                       <div class="controls">
                                          <input type="email" name="email"  value="<?php echo $photographer_details->email;?>" class="span4 m-wrap" />
										  <h6 style="color:#D51D59"><?php echo form_error('email'); ?></h6>
                                         </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">Mobile Number</label>
                                       <div class="controls">
                                          <input type="text" name="mobile_number"  value="<?php echo $photographer_details->mobile_number;?>" class="span4 m-wrap" />
										  <h6 style="color:#D51D59"><?php echo form_error('mobile_number'); ?></h6>
                                        </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Landline Number</label>
                                       <div class="controls">
                                          <input type="text" name="land_number"  value="<?php echo $photographer_details->landline_number;?>" class="span4 m-wrap" />
									      <h6 style="color:#D51D59"><?php echo form_error('land_number'); ?></h6>
                                       </div>
                                    </div>
                                   
                                    <div class="control-group">
                                       <label class="control-label">State</label>
                                       <div class="controls">
                                         <input type="text" name="state" value="<?php echo $photographer_details->state;?>" class="span4 m-wrap" />
                                         <input type="hidden" name="hidden" value="<?php echo $photographer_details->id;?>" class="span4 m-wrap" />
										<h6 style="color:#D51D59"><?php echo form_error('state'); ?></h6>
                                     </div>
                                    </div>
                                    <div class="control-group">
                                       <label class="control-label">City/Town</label>
                                       <div class="controls">
                                          <input type="text" name="city" class="span4 m-wrap" value="<?php echo $photographer_details->city;?>" />
					                      <h6 style="color:#D51D59"><?php echo form_error('city'); ?></h6>
                                        </div>
                                    </div>
                                  
                         
                               <div class="control-group">
                                   <div class="controls">
                                      <input type="submit" name="submit" value="submit" />
                                   </div>
                             </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
			<!-- END PAGE CONTAINER-->
		
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<?php include_once('includes/footer.php');?>
