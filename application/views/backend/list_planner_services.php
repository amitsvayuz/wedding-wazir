<?php include_once('includes/header.php');?>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						
						<!-- END BEGIN STYLE CUSTOMIZER -->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Services
						</h3>
						 <?php if($this->session->userdata('message')){?>
							  <div class="alert alert-error">
								<button class="close" data-dismiss="alert"></button>
							   <span style='color:green;'><?php echo $this->session->userdata('message');?></span>
							  </div>
						<?php } ?>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url();?>planner/list_planner_services">Services </a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>Services</h4>
							</div>
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th>Sno.</th>
											<th>Events</th>
											<th>Arrangements</th>
											<th>Venue References</th>
											<th>Action</th>
										</tr>
									</thead>
										<?php $i=0;?>
										<tr class="odd gradeX">
											<td><?php echo ++$i.'.';?></td>
											<td>
											<?php $events_name = array();
											$events_name = explode(',',$list_details->events);
											foreach($events_details as $events) {
											for($i=0;$i<sizeof($events_name);$i++){
											if($events->description == $events_name[$i]){?>
											<?php echo $events->name.',';?>
											<?php }}} ?>
											</td>				
											<td><?php $events_name = array();
											$events_name = explode(',',$list_details->arrangements);
											foreach($events_arrangements as $events) {
											for($i=0;$i<sizeof($events_name);$i++){
											if($events->description == $events_name[$i]){?>
											<?php echo $events->name.',';?>
											<?php }}} ?></td>
											<td><?php echo $list_details->venue_preference;?></td>
											<?php if(!empty( $list_details->id)) { ?>
<td><a href="<?php echo base_url();?>planner/detail_planner_services?id=<?php echo $list_details->id;?>"><i class="icon-eye-open"></i></a>  | <a href='<?php echo base_url();?>planner/list_planner_services_delete?id=<?php echo $list_details->id;?>'><i class="icon-remove"></i></a> | <a href='<?php echo base_url();?>planner/edit_planner_services?id=<?php echo $list_details->id;?>'><i class="icon-edit"></i></a></td>
											<?php } else{ ?>
<td><a href="<?php echo base_url();?>planner/add_planner_services">Add Services</a></td>												
										    <?php 	}?>
											</tr>
									</thead>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
	</div>
	<!-- END CONTAINER -->
	<?php include_once('includes/footer.php');?>
