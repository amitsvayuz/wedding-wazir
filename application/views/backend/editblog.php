
   <?php include_once('includes/header.php');?>
  
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php include_once('includes/sidebar.php');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                 
                  <h3 class="page-title">
                     Blog
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="index.html">portfolio</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                   </ul>
               </div>
            </div>
		   	<?php if($this->session->userdata('message')){?>
			<div class="alert alert-error">
				<button class="close" data-dismiss="alert"></button>
				<span style='color:green;'><?php echo $this->session->userdata('message');?></span>
			</div>
			<?php } ?>
            <!-- END PAGE HEADER-->
			<div id='preview'>
		
		    </div>	
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
			   
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Blog</h4>
                     </div>
				 
                     <div class="portlet-body form">
         <form id='imageform' style="clear:both" action="<?php echo base_url();?>blog/edit_blog" method='post' enctype="multipart/form-data" class="form-horizontal">
							<div class="control-group">
                              <label class="control-label">Blog Title :</label>
                              <div class="controls">
                                 <div class="span12">
								 <div class="span6">
                                    <input class="m-wrap" type="text" name='blog_title' value="<?php echo $blog_info->title;?>" required placeholder="Blog Title" />    
     <input type="hidden" name='hidden' value="<?php echo $blog_info->id;?>"/>    
								</div>
								<div class="span6">
								</div>
                                 </div>
                              </div>
                           </div>
						 
							<div class="control-group">
                              <label class="control-label">Blog Description :</label>
                              <div class="controls">
                                 <div >
                             <textarea class="m-wrap large" 
									required name='description' required placeholder="Blog Description"> <?php echo $blog_info->description;?> </textarea>   
                                 </div>
                              </div>
                           </div>
						   
						  	<div class="control-group">
								<label class="control-label">Keywords :</label>
									<div class="controls">
									<div class="span12">
									<div class="span6">
										<input id="tags_1" name='keywords[]' value="<?php echo $blog_info->keywords;?>" type="text" class="m-wra tags" required />
									</div>
									<div class="span6">
									</div>
								     </div>
									</div>
							</div>
					         
							<div class="control-group">
                              <label class="control-label">When To Published :</label>
                              <div class="controls">
                                 <div >
                             <input type="text" class="m-wrap" id="date" value="<?php echo $blog_info->published_date;?>"
									required name='published_date' placeholder="Publishing Date"> </textarea>   
                                 </div>
                              </div>
                           </div>
						    
							<div style="display:none;" class="control-group">
                             <label class="control-label">Publishing Time :</label>
						    <div class="input-append bootstrap-timepicker-component">
								  <input class="m-wrap m-ctrl-small timepicker-default" name="publishing_time" type="text" />
                                    <span class="add-on"><i class="icon-time"></i></span>
							</div>
							</div>
                           
						    <div class="control-group">
                              <label class="control-label">Allow people to like :</label>
                              <div class="controls">
                                 <div class="danger-toggle-button">
   <input  <?php if($blog_info->allow_like == 'on'){ echo 'checked';}?> type="checkbox" class="toggle" name="allow_like"/>
                                 </div>
                               </div>
                           </div>
						     
							<div class="control-group">
                              <label class="control-label">Allow people to comments :</label>
                              <div class="controls">
                                 <div class="danger-toggle-button">
    <input <?php if($blog_info->allow_comments == 'on'){ echo 'checked';}?> type="checkbox" class="toggle" name="allow_comments"  />
                                 </div>
                               </div>
                           </div>
						   
						   <div class="control-group">
                              <label class="control-label">Allow people to sharing :</label>
                              <div class="controls">
                                 <div class="danger-toggle-button">
   <input <?php if($blog_info->allow_sharing == 'on'){ echo 'checked';}?>  type="checkbox" class="toggle" name="allow_sharing" />
                                 </div>
                               </div>
                           </div> 
						   
							<div class="control-group">
                              <label class="control-label">Image Upload :</label>
                              <div id='imageloadbutton' class="controls">
							       <input type="file" name="file1"   class="default" />
							   </div>
							   <div style="margin-left:150px;width:50px;height:50px;">
							   <img src="<?php echo base_url();?>uploads/<?php echo $blog_info->image;?>">
							   </div>
						    </div>
						  
						  <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                           </div>
						</form>
                     </div>
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div> 
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
  
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php include_once('includes/footer.php');?>
   