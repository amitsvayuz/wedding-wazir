<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">	
						<h3 class="page-title">
							Brand								
							
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url()?>sp_manager/dashboard">Dashboard</a>
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="<?php echo base_url()?>General_setting/list_brand">Brand</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<?php  if(@$_REQUEST['set']):?>
								   <div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo base64_decode(@$_REQUEST['set']);?>
								    </div>
									<?php endif;?>
									
									<div class="row-fluid"style="margin-bottom:2%">
									<div class="btn-group" >
										<a href="<?php echo base_url();?>General_setting/add_brand"><button id="sample_editable_1_new" class="btn green">
										Add Brand <i class="icon-plus"></i>
										</button></a>
									</div>
									
								    </div>

			<!--------------------------------------new row7-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Brand Setting </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Brand </th>
											<th class="hidden-480">Company</th>
											<th class="hidden-480">Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
									        if(isset($result)):
											foreach($result as $row):
											
											
											?>
										
										<tr class="odd gradeX">
										     
											<td><?php echo $row->name;?></td>
							<?php if(isset($resultPro)):?>
							<?php foreach($resultPro as $rowPro):?>
							<?php if($rowPro->id==$row->proName):?>
							<td><?php echo $rowPro->company_name ;?></td>
							<?php endif;?>
							<?php endforeach;?>
							<?php endif;?> 
							<td class="center hidden-480"><?php echo $row->description;?></td>
											
											<td>	
			
		<a href="<?php echo base_url();?>General_setting/edit_brand/<?php echo $row->id;?>" title="Edit"><i class="icon-edit"></i></a> | 
		<a onclick="return confirm('Are you sure delete?');" href="<?php echo base_url();?>General_setting/delete_brand/<?php echo $row->id;?>" title="Trash"><i class="icon-trash"></i></a>
<!--| <a  href="#myModal1" role="button" class="btn btn-primary" data-toggle="modal" title="City"><i class="icon-home"></i></a>-->
                       
									    </td>
										</tr>
									
									<?php endforeach;endif;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->	

<div id="myModal1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h3 id="myModalLabel1">Add City</h3>
		</div>
		<div class="modal-body">
		<form action="#" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">City Name</label>
                              <div class="controls">
                                 <input type="text" id="cityName" name="cityName" placeholder="Enter the city name" class="span12 m-wrap" />
                                 <span class="help-inline"></span>
                              </div>
                           </div>
					
		</div>
		<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<button id="btnCity" class="btn yellow">Save</button>
		</div>
                </form>
		</div>	
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			</div>
</div>
			
	<?php include_once('includes/footer.php');?>
      <script type="text/javascript">
	$(document).ready(function(){
		.

..$('#email').focus();
	    $('.alert-success').hide(); 
		 $('.alert-danger').hide(); 
		$('#btnlogin').click(function(){
		 
					var url = "<?php echo base_url();?>General_setting/signin";
					var redirect = "<?php echo base_url();?>General_setting/index";
					var email =$('#email').val();
					var pass =$('#password').val();
					var emailchk = isEmail(email);
					if(email == '')
					{
						$('#erremail').html('Fill the email address');
						$('#email').focus();
						return false;
					}
					else if(!emailchk)
					{
						
						$('#erremail').html('Fill the right email format: demo@gmail.com');
						$('#email').focus();
						return false;
					}
					if(pass == '')
					{
						$('#erremail').hide();
						$('#errpass').html('Fill the  password');
						$('#password').focus();
						return false;
					}
					
					$('#errpass').hide();
						$.ajax({
											type: "POST",
											url: url,
											data : {
													
													'email'     : $('#email').val(),
													'password'  : $('#password').val()
													
												},
											cache: false,
											datatype: 'text',
											success: function(res)
											{
													if(res=='1')
													{
														
														window.location.href = redirect;
													}
													else
													{
														$('.alert-danger').show(); 								
														$("#danger").html('Enter username or password is wrong');
													}
											}
										});
										return false;
							 
					
				  });
	});
	function isEmail(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}
	</script>