 <?php include('includes/header.php');?>
   
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid">
		<?php include('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						  	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Calendar
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url()?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url()?>admin/calendar">Calendar</a></li>
						</ul>

						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
<div id="createEventModal1" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
   <div class="modal-header">
          <!--<h3 id="myModalLabel1">Create Events</h3>-->
		  <h4 id="msg" style="color:green"></h4>
   </div>
   <div class="modal-body">
   <form id="createAppointmentForm" method="post" action="#" class="form-horizontal">
   <?php if($this->session->userdata('role') == 'venue'){ ?>
	    <div class="control-group">             
			<div class="controls">
				  <label class="checkbox">
				  <input type="checkbox" value="" />Under Construction
				  </label>
				  <label class="checkbox">
				  <input type="checkbox" value="" />UnAvailable
				  </label>
			</div>
		</div>
	   
	   
  <?php } ?>
      
		
       <div class="control-group">
           <label class="control-label" for="inputPatient">Event Title :</label>
           <div class="controls">
               <input type="text" name="events_title" id="events_title" required tyle="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Value 1&quot;,&quot;Value 2&quot;,&quot;Value 3&quot;]">
                 <input type="hidden" id="apptStartTime"/>
                 <input type="hidden" id="apptEndTime"/>
                 <input type="hidden" id="apptAllDay" />
           </div>
       </div>
	   <div class="control-group">
           <label class="control-label" for="inputPatient">Event Description :</label>
           <div class="controls">
		       <textarea name="description" id="description" class="span8" required data-source="[&quot;Value 1&quot;,&quot;Value 2&quot;,&quot;Value 3&quot;] rows="3"></textarea>
                <input type="hidden" id="apptStartTime"/>
                <input type="hidden" id="apptEndTime"/>
                <input type="hidden" id="apptAllDay" />
           </div>
       </div>
	     
		 <div class="control-group">
           <label class="control-label" for="when">Choose Slots :</label>
           <div required class="controls">
				<select id="choose_slots" class="span8" data-placeholder="Choose a Category" required id="role" name="role" tabindex="1">
					 <option value="none">Select Slots</option>
					 <option  value="1">1</option>
					 <option  value="2">2</option>
					 <option value="3">3</option>
			    </select>
         </div>
       </div>
		
         
	   <div id="slots_1" style="display:none;" class="control-group">
           <label class="control-label" for="when">Events Slots :</label>
           <div class="controls" style="margin-top:5px;width:auto;">
				<select id="events_full_slots" required class="span8" data-placeholder="Choose a Category" required  name="role" tabindex="1">
				 <option value="none">Select Slots</option>
				 <option   value="Full Day">Full Day</option>
				 </select>
         </div>
       </div>
       
	   <?php $slot1 = explode( ',' , $two_slots->slot1);
	   $slot2 = explode( ',' , $two_slots->slot2);
	  
	   ?>
		 <div id="slots_2" style="display:none;" class="control-group">
           <label class="control-label" for="when">Events Slots :</label>
           <div class="controls" style="margin-top:5px;width:auto;">
				<select id="events_second_slots" class="span8" data-placeholder="Choose a Category" required name="role" tabindex="1">
				 <option value="none">Select Slots</option>
				<option  value="<?php echo $slot1[0];?>-<?php echo $slot1[1];?>"><?php echo $slot1[0];?>-<?php echo $slot1[1];?></option>
				<option  value="<?php echo $slot2[0];?>-<?php echo $slot2[1];?>"><?php echo $slot2[0];?>-<?php echo $slot2[1];?></option>
				 </select>
         </div>
       </div>
	   
	    <?php $slot1 = explode( ',' , $three_slots->slot1);
	   $slot2 = explode( ',' , $three_slots->slot2);
	   $slot3 = explode( ',' , $three_slots->slot3);
	    ?>
	    <div id="slots_3" style="display:none;" class="control-group">
           <label class="control-label" for="when">Events Slots :</label>
           <div class="controls"  style="margin-top:5px;width:auto;">
				<select id="events_three_slots" class="span8" data-placeholder="Choose a Category" required name="role" tabindex="1">
				 <option value="none">Select Slots</option>
				<option  value="<?php echo $slot1[0];?>-<?php echo $slot1[1];?>"><?php echo $slot1[0];?>-<?php echo $slot1[1];?></option>
				<option  value="<?php echo $slot2[0];?>-<?php echo $slot2[1];?>"><?php echo $slot2[0];?>-<?php echo $slot2[1];?></option>
				<option  value="<?php echo $slot3[0];?>-<?php echo $slot3[1];?>"><?php echo $slot3[0];?>-<?php echo $slot3[1];?></option>
				 </select>
         </div>
       </div>
	  
	  
		
	   <?php if($this->session->userdata('role') == 'venue'){ ?>
	    <div  class="control-group">
           <label class="control-label" for="when">Venues :</label>
           <div class="controls" style="margin-top:5px;width:auto;">
				<select id="events_venues" class="span8" data-placeholder="Choose a Category" required id="role" name="venues" tabindex="1">
				<option  value="none">Select Venues</option>
				<?php foreach($venues_name as $venues) { ?>
				<option  value="<?php echo $venues->v_name;?>"><?php echo $venues->v_name;?></option>
				<?php } ?>
				 </select>
         </div>
       </div>
	    <div class="control-group">             
			<div class="controls">
			<label class="checkbox">
				  <input type="checkbox" id="merge_venue" name="merge_venue" value="" />Merge Venues
		    </label>
			</div>
		</div>
		 <div  style="display:none;" id="venue_merge" class="control-group">
           <label class="control-label" for="when">Second Venues :</label>
           <div class="controls" style="margin-top:5px;width:auto;">
				<select id="events_merge_venues" class="span8" data-placeholder="Choose a Category" required id="role" name="venues" tabindex="1">
				<option  value="none">Select Venues</option>
				<?php foreach($venues_name as $venues) { ?>
				<option  value="<?php echo $venues->v_name;?>"><?php echo $venues->v_name;?></option>
				<?php } ?>
				 </select>
         </div>
       </div>
	     
	  <?php  } ?>
   </form>
   </div>
   <div class="modal-footer">
       <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
       <button type="submit" class="btn btn-primary" id="submitButton">Save</button>
   </div>
</div>


<div id="eventContent" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
   <div class="modal-header">
          <h3 id="myModalLabel1">Events Details</h3>
   </div>
   <div class="modal-body">
      <div style="color:green;"><b>Event Start Date :</b></div> <span id="startTime"></span><br>
									
					<div style="color:green;"><b>Event Title :</b></div><div id="eventTitle"></div>
					<div style="color:green;"><b>Event Description :</b></div><div id="eventInfo"></div>
									<strong><a id="eventLink" style="float:left;" target="_blank">Edit Event</a></strong>
									<strong><a style="float:right;" id="event_delete_Link">Delete Event</a></strong>
   </div>
   <div class="modal-footer">
       <a href="<?php echo base_url();?>admin/calendar" class="btn" data-dismiss="modal" aria-hidden="true">Cancel</a>
    </div>
</div>
<!-- BEGIN PAGE Full Calender Model -->


<div id="myModal1"  style=" margin-left: -14%;margin-top: 10%;width: 29%;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header" style="background-color:lightgrey">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel1" class="light-grey"><p id="slotname">Event Details</p></h3>
									</div>
									<div class="modal-body">
									
                                    <!-- BEGIN NOTIFICATION DROPDOWN -->	
					  <div class="col-xs-18 col-md-12" >
					  <div class="row" style="margin-left:10px;">
							<address>
							  <strong>When</strong><br>
							  <span id="sdate"></span> , <span id="ftime"></span> , <span id="ttime"></span><br>
							  <strong>Calendar</strong><br>
							  <span id="cal_user"></span><br>
							  <strong>Created by</strong><br>
							  <span id="cal_by"></span>
							</address>
							
					  </div>
                     </div>                                   
									
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
										<!--<a href="#" class="btn red" id="delete">Delete</a>
										<a href="#" class="btn yellow" id="edit">Edit events</a>-->
									</div>
								</div>


<!-- END PAGE Full Calender Model -->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="portlet box light-grey calendar">
						<div class="portlet-title float-right">
							<h4><i class="icon-reorder"></i>Calendar</h4>
						</div>
		<div class="portlet-body light-grey">
		 <div class="row-fluid">

		<div class="span3 responsive" data-tablet="span12 fix-margin" style="background-color:#eee;margin-top: 3%;" data-desktop="span8">
                                      
              <button class="btn btn-primary" style="background-color:" type="button">
              Event <span class="badge"><?php echo count($numslot1);?></span></button></br></br>
	      <button class="btn btn-primary" style="background-color:" type="button">
	      Schedule <span class="badge"><?php echo count($numslot2);?></span></button></br></br>
              <button class="btn btn-primary" style="background-color:" type="button">
	      Special Day <span class="badge"><?php echo count($numslot3);?></span></button></br></br>
	      <button class="btn btn-primary" style="" type="button">
	      Food Invite <span class="badge"><?php echo count($numslot4);?></span></button></div>
              <div class="span9">
              <div id="calendar" class="has-toolbar"></div></div></div>
							<!-- END CALENDAR PORTLET-->
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	
<?php include('includes/footer.php');?>

<style>
.fc-event-time {
	display:none;
}
</style>

	<script>
		$(document).ready(function(){
			
			$("#choose_slots").change(function(){
				var slots = $("#choose_slots").val();
				if(slots == '1'){
					$("#slots_1").show();
					$("#slots_2").hide();
					$("#slots_3").hide();
				}
			    if(slots == '2'){
					$("#slots_2").show();
					$("#slots_1").hide();
					$("#slots_3").hide();
				}
				if(slots == '3'){
					$("#slots_3").show();
					$("#slots_1").hide();
					$("#slots_2").hide();
				}
				if(slots == 'none'){
					$("#slots_3").hide();
					$("#slots_1").hide();
					$("#slots_2").hide();
				}
					return false;
				
			});
			
			$("#merge_venue").click(function(){
			     $("#venue_merge").toggle();
				 $('input:checkbox').removeAttr('checked');
				 $('input[name=merge_venue]').attr('checked',false);
				return false;
			});
			
			$("#submitButton").click(function(){
				var apptStartTime = $("#apptStartTime").val();
				var apptEndTime = $("#apptEndTime").val();
				var title = $("#events_title").val();
				if(title == ''){
					alert("Please enter event title");
					return false;
				}
				var description = $("#description").val();
				if(description == ''){
					alert("Please enter event title description");
					return false;
				}
				var events_slots = $("#events_slots").val();
				var allDay = $("#apptAllDay").val();
				var events_venues = $("#events_venues").val();
				var second_slots = $("#events_second_slots").val();
				var three_slots = $("#events_three_slots").val();
				var events_full = $("#events_full_slots").val();
				var merge_venue = $("#events_merge_venues").val();
				var base_url = "<?php echo base_url();?>";
			  
				$.ajax({
					type: 'POST',
					url: base_url + 'admin/add_events',
					data: {'start':apptStartTime,'title':title,'description':description ,'allDay':allDay ,'events_second_slots':second_slots,'events_third_slots':three_slots,'events_full':events_full,'merge_venue':merge_venue,'venues':events_venues},
					success: function(data){
					
							$('#msg').html(data);
									 
					}});
				return false;
				
			});
			
			
		});
	</script> 
	<script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
		 App.setPage('calendar');
      });
   </script>
 