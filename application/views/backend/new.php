<?php include_once('includes/header.php');?>
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php include_once('includes/sidebar.php');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                
                  <h3 class="page-title">
                     Form Wizard
                     <small>form wizard sample</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="index.html">Home</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">Form Stuff</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">Form Wizard</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="portlet box blue" id="form_wizard_1">
                     <div class="portlet-title">
                        <h4>
                           <i class="icon-reorder"></i> Services <span class="step-title">Addition</span>
                        </h4>
                        <div class="tools hidden-phone">
                           <a href="javascript:;" class="collapse"></a>
                           <a href="#portlet-config" data-toggle="modal" class="config"></a>
                           <a href="javascript:;" class="reload"></a>
                           <a href="javascript:;" class="remove"></a>
                        </div>
                     </div>
                     <div class="portlet-body form">
                        <form  method="post" action="<?php echo base_url();?>photographer/add_services" class="form-horizontal">
                           <div class="form-wizard">
                              <div class="tab-content">
                                 <div class="tab-pane active" id="tab1">
                                    <h4 class="block">Provide your account details</h4>
                                 </div>
                                
                                
                               <div class="control-group">
                                       <label class="control-label">Events :</label>
                                       <div class="controls">
                                          <label class="checkbox">
                                          <input type="checkbox" name="events[]" value="engagement" />Engagement
                                          </label>
                                          <label class="checkbox">
                                          <input type="checkbox" name="events[]" value="pre_wedding_services" />Pre Wedding Celebrations
                                          </label>
										  <label class="checkbox">
                                          <input type="checkbox" name="events[]" value="wedding" />Wedding
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="events[]" value="reception" />Reception
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="events[]" value="others" />Others

                                          </label>
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Services :</label>
                                       <div class="controls">
										   <label class="checkbox">
                                          <input type="checkbox" name="services[]" value="photography" />Photography
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="services[]" value="videography" />Videography
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="services[]" value="cinematography" />Cinematography
                                          </label>
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Style of Photography :</label>
                                       <div class="controls">
										   <label class="checkbox">
                                          <input type="checkbox" name="style_of_photography[]" value="candid" />Candid
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="style_of_photography[]" value="traditional" />Traditional
                                          </label>
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Sharing Option :</label>
                                       <div class="controls">
										   <label class="checkbox">
                                          <input type="checkbox" name="sharing_option[]" value="cd" />CD/ DVD
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="sharing_option[]" value="pen_drive" />Pen Drive
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="sharing_option[]" value="album" />Album
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="sharing_option[]" value="online" />Online
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="sharing_option[]" value="others" />Others
                                          </label>
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Resolution :</label>
                                       <div class="controls">
										   <label class="checkbox">
                                          <input type="checkbox" name="resolution[]" value="low" />Low
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="resolution[]" value="low_medium" />Low - Medium
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="resolution[]" value="medium" />Medium
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="resolution[]" value="medium_high" />Medium - High
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="resolution[]" value="high" />High (HD)
                                          </label>
                                       </div>
                                    </div>
									<div class="control-group">
                                       <label class="control-label">Other Services :</label>
                                       <div class="controls">
										   <label class="checkbox">
                                          <input type="checkbox" name="studio_setup[]" value="studio_setup" />Studio Setup
                                          </label>
										   <label class="checkbox">
                                          <input type="checkbox" name="studio_setup[]" value="video_editing" />Photo/ Video Editing
                                          </label>
                                       </div>
                                    </div>
								 
                              </div>
                              <div class="form-actions clearfix">
                                 <input type="submit" name="submit" value="submit" />
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
 <?php include_once('includes/footer.php');?>