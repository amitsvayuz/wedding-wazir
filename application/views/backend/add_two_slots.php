<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
							
						<h3 class="page-title">
							Add Slot
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url()?>sp_manager/dashboard">Dashboard</a>
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="<?php echo base_url()?>General_setting/list_city">City</a></li>
							<i class="icon-angle-right"></i>
							<li><a href="<?php echo base_url()?>General_setting/add_city">Add City</a>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				
				
				
		
		<!-- END PAGE -->
	
	<!-- END CONTAINER -->
	<div class="tab-pane" id="tab_1_4">
			<div class="row-fluid">
               <div class="span12">
			
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Add Slots</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url();?>admin/two_slots" method="post" class="form-horizontal">
						
                            <div class="control-group">
                              <label class="control-label">Slot 1:</label>
							   <div class="row">
                              <div class="controls">
							  <div class="span6">
                                 <div class="input-append bootstrap-timepicker-component">
								  <input class="m-wrap m-ctrl-small timepicker-default" name="slot_1_first" type="text" />
                                    <span class="add-on"><i class="icon-time"></i></span>
							    </div>
								</div>
								  </div>
								<div class="controls">
								 <div class="span6">
								 <div class="input-append bootstrap-timepicker-component">
                                    <input class="m-wrap m-ctrl-small timepicker-default" type="text" name="slot_1_second"  />
                                    <span class="add-on"><i class="icon-time"></i></span>
								</div>
                                 </div>
                                 </div> 
                            
                             </div>
							 </div>
							 
							 
							 
						   
                            <div class="control-group">
                              <label class="control-label">Slot 2:</label>
							   <div class="row">
                              <div class="controls">
							  <div class="span6">
                                 <div class="input-append bootstrap-timepicker-component">
								  <input class="m-wrap m-ctrl-small timepicker-default" name="slot_2_first" type="text" />
                                    <span class="add-on"><i class="icon-time"></i></span>
							    </div>
								</div>
								  </div>
								<div class="controls">
								 <div class="span6">
								 <div class="input-append bootstrap-timepicker-component">
                                    <input class="m-wrap m-ctrl-small timepicker-default" type="text" name="slot_2_second"  />
                                    <span class="add-on"><i class="icon-time"></i></span>
								</div>
                                 </div>
                                 </div> 
                            
                             </div>
							 </div>
                          
						  
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                             
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
			</div>
												</div>
												</div>
	<?php include_once('includes/footer.php');?>