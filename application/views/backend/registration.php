<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <title><?php echo $title;?> | Wedding Wazir</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <link href="<?php echo base_url()?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/css/metro.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/css/style_responsive.css" rel="stylesheet" />
  <link href="<?php echo base_url();?>assets/css/style_default.css" rel="stylesheet" id="style_color" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/uniform/css/uniform.default.css" />
  <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
  <!-- BEGIN LOGO -->
  <div class="logo">
  <h3><span style="color:white">WEDDING</span><span style="color:red">WAZIR</span></h3>
    <!--<img src="assets/img/logo-big.png" alt="" />--> 
  </div>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div class="content">
    <!-- BEGIN REGISTRATION FORM -->
	<?php if(@$user_unique_error!=''):?>
	<div class="alert">
		<button class="close" data-dismiss="alert"></button>
			<strong>Warning!</strong> <?php echo @$user_unique_error;?>.
    </div>
	<?php endif;?>
    <form class="form-vertical register-form1" action="<?php echo base_url();?>registration/create_user" method="post">
	  
      <h3 class="">Sign Up</h3>
      <p>Enter your account details below:</p>
	    <div class="control-group">
            <div class="controls">
            <select name="role" tabindex="1">
					<option value="">Select Role</option>
					<option value="admin">Admin</option>
					 <option value="venue">Venue Manager</option>
					 <option value="planner">Wedding Planner</option>
					 <option value="decorater">Decorater</option>
					 <option value="photographer">Photographer</option>
				</select>
			<h6 style="color:#D51D59"><?php echo form_error('username'); ?></h6>
         </div>
        </div>
	  
	   <div class="control-group">                      
		   <div class="controls">
			  <label class="radio">
			  <input type="radio" name="optionsRadios1" value="option1" />
			  Company
			  </label>
			  <label class="radio">
			  <input type="radio" name="optionsRadios1" value="option2" checked />
			  Freelancer
			  </label>  
			</div>
        </div>
							
		  <div class="control-group" id="comp" style="display:none">
			<label class="control-label visible-ie8 visible-ie9">Company Name</label>
			<div class="controls">
			  <div class="input-icon left">
				<i class="icon-user"></i>
				<input class="m-wrap placeholder-no-fix" type="text" placeholder="Companyname" name="companyname"/>
			  </div>
			</div>
		  </div>
	  
	   <div class="control-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-envelope"></i>
            <input class="m-wrap placeholder-no-fix" id="email" type="email" placeholder="Email" name="email"/>
			<h6 style="color:#D51D59"><?php echo form_error('email'); ?></h6>
          </div>
        </div>
      </div>
	  
      <div class="control-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-lock"></i>
            <input class="m-wrap placeholder-no-fix" type="password" id="password" placeholder="Password" name="password"/>
			<h6 style="color:#D51D59"><?php echo form_error('password'); ?></h6>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-ok"></i>
            <input class="m-wrap placeholder-no-fix" type="password" id="new_password" placeholder="Re-type Your Password" name="rpassword"/>
          </div>
        </div>
      </div>
     
	  <div class="control-group">
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-user"></i>
            <input class="m-wrap placeholder-no-fix" type="text" placeholder="Username" name="username"/>
			<h6 style="color:#D51D59"><?php echo form_error('username'); ?></h6>
          </div>
        </div>
      </div>
	  
      <div class="control-group">
        <div class="controls">
          <label class="checkbox">
          <input type="checkbox" name="tnc"/> I agree to the <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a>
          </label>  
          <div id="register_tnc_error"></div>
        </div>
      </div>
      <div class="form-actions">
        <button id="register-back-btn" type="button" class="btn">
        <i class="m-icon-swapleft"></i>  Back
        </button>
        <button type="submit" id="register-submit-btn" class="btn green pull-right">
        Sign Up <i class="m-icon-swapright m-icon-white"></i>
        </button>            
      </div>
    </form>
    <!-- END REGISTRATION FORM -->
  </div>
  <!-- END LOGIN -->
  <!-- BEGIN COPYRIGHT -->
  <div class="copyright">
    2016 &copy; vayuz.in  
  </div>
  <!-- END COPYRIGHT -->
  <!-- BEGIN JAVASCRIPTS -->
  <script src="<?php echo base_url();?>assets/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url();?>assets/uniform/jquery.uniform.min.js"></script> 
  <script src="<?php echo base_url();?>assets/js/jquery.blockui.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/app.js"></script>
  <script>
    jQuery(document).ready(function() {     
      $('#register-back-btn').click(function(){
		  window.location='<?php echo base_url();?>';
	  });
	  $('#option1').click(function(){
		 $('#comp').show();
	  });
	  $('#option2').click(function(){
		 $('#comp').hide();
	  });
    });
  </script>
   <script>
		$(document).ready(function(){
			$("#submit").click(function(){
				$pass = $("#password").val();
				$new_pass = $("#new_password").val();
				if($pass != $new_pass){
					alert("Password is not matching with Reconfirm Password.");
					return false;
				}
			});
		});
	</script> 
  <script>
  $(document).ready(function(){
	$('#register-submit-btn').submit(function(){
		var username = $('#username').val();
		alert(username);
	});   
  });
  </script>
   <script>
    $(document).ready(function() { 
		$("#email").keyup(function(){	
		var email = $("#email").val();
		var dataString = 'email=' + email;
		var base_url = "<?php echo base_url();?>";
		
			$.ajax({
				type: 'POST',
				url: base_url + 'admin/email_exists',
				data: dataString,
				success: function(result){
				   if(result == 1){
					   alert('Email Id already exists');
				   }
				}});
			return false;
		 });
    });
  </script>
  
  <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>