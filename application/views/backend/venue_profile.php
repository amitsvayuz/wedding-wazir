<?php include('includes/header.php');?>


	<div class="page-container row-fluid">
		<?php include('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Profile
						</h3>
		<ul class="breadcrumb">
		 <li>
		<i class="icon-home"></i>
			<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
			<span class="icon-angle-right"></span>
		</li>
		
			<li><a href="<?php echo base_url();?>sp_manager/venue_profile?id=<?php echo $_GET['id']?>">Manager Profile</a></li>
		</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid profile">
					<div class="span12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab_1_1" data-toggle="tab">Overview</a></li>
								<li><a href="#tab_1_3" data-toggle="tab">Account</a></li>
								<li><a href="#tab_1_4" data-toggle="tab">Business Manager</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane row-fluid active" id="tab_1_1">
									<ul class="unstyled profile-nav span3">
										<li>
										<?php if(!empty($result[0]->pro_pic)) { ?>
										<img src="<?php echo base_url()?>uploads/<?php echo $result[0]->id;?>/<?php echo $result[0]->pro_pic;?>" alt="" />
										<?php }else{ ?>
								<img src="<?php echo base_url()?>assets/img/person.png" alt="" />			
							

				
											
									<?php 	} ?>
										</li>
									</ul>
									<div class="span9">
										<div class="row-fluid">
											<div  class="span8 profile-info">
												<h1><?php echo $result[0]->user_name;?></h1>
<?php if(!empty($result[0]->company_description)){ ?>
		
	<p><?php	echo $result[0]->company_description; ?></p>
		
		
<?php } ?>
										
		<ul class="unstyled inline">
<?php if(isset($result[0]->country) AND isset($result[0]->state) AND isset($result[0]->city)) { ?>
				<li><i class="icon-map-marker"></i> <?php echo ucfirst($result[0]->country);?> , <?php echo ucfirst($result[0]->state);?> , <?php echo ucfirst($result[0]->city);?> </li> <?php } ?>
					<?php if(isset($result[0]->d_o_b)) { ?><li><i class="icon-calendar"></i> <?php echo $result[0]->d_o_b;?></li><?php } ?>
					<li><i class="icon-envelope-alt"></i> <?php echo $result[0]->user_email;?></li>
					<li><i class="icon-phone"></i> <?php echo $result[0]->contact_no;?></li>
					
					<li><i class="icon-facebook"></i> </li>
					<li><i class="icon-google-plus"></i> </li>
		</ul>
		</div>						
		</div>
										<!--end row-fluid-->
										<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab_1_11" data-toggle="tab">Requested Leads</a></li>
												<li class=""><a href="#tab_1_22" data-toggle="tab">Accepted Leads</a></li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_11">
													<div class="portlet-body" style="display: block;">
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
															<?php
															if(!empty($pendingleads)){
															 foreach($pendingleads as $leads){
																
																?>
																<tr>
																	<th class="span9"><i class="icon-briefcase"></i>You got a lead</th>
																	
																	<th class="span3"><a href="#" class="btn green icn-only"><i class="icon-ok icon-white"></i></a> <a href="#" class="btn red icn-only"><i class="icon-remove icon-white"></i></a></th>
																																	</tr>
																<?php }}?>
																</thead>
															
														</table>
													</div>
												</div>
												<!--tab-pane-->
												<div class="tab-pane" id="tab_1_22">
													<div class="tab-pane active" id="tab_1_1_1">
														<div class="portlet-body" style="display: block;">
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
															<tr>
																	<th class="span9"><i class="icon-user"></i>Customer name</th>
																	<th class="span9"><i class="icon-envelope"></i>Customer email</th>
																	<th class="span9"><i class="icon-phone"></i>Customer contact</th>
																	<th class="span9"><i class="icon-money"></i>Budget</th>
																	<th class="span9"><i class="icon-home"></i>Event</th>
																	</tr>
																</thead>																
															<?php
															if(!empty($acceptedleads)){
															 foreach($acceptedleads as $aleads){
																foreach($leaddetails as $ld){
																	if($ld->lead_id==$aleads->lead_id){
																		$name=$ld->Name;
																		$email=$ld->Email_Id;
																		$phn=$ld->Contact_Num;
																		$budget=$ld->Budget;
																		$event=$ld->event;																	
																	}																
																}
																?>
																<tbody>
																<tr>	
																<td><?php echo ucfirst($name);?></td>
																<td><?php echo ucfirst($email);?></td>
																<td><?php echo $phn;?></td>
																<td><?php echo $budget;?></td>
																<td><?php echo ucfirst($event);?></td>
																	</tr>																
																
																</tbody>
																<?php }} else{ echo '<tbody><tr>No data available</tr></tbody>';}?>
																
															
														</table>
													</div>

													</div>
												</div>
												<!--tab-pane-->
											</div>
										</div>
									</div>
									<!--end span9-->
								</div>
								<!--end tab-pane-->
								
								<div class="tab-pane profile-classic row-fluid" id="tab_1_2">
									<div class="span2"><img src="<?php echo base_url()?>uploads/<?php echo $result[0]->id;?>/<?php echo $result[0]->pro_pic;?>" alt="" /> <a href="<?php echo base_url();?>sp_manager/profile?id=<?php echo base64_encode($result[0]->id);?>" class="profile-edit">edit</a></div>
									<div class="span10">
									<ul class="unstyled span10">
									<?php if(!empty($result[0]->user_name)){?>
										<li><span>User Name:</span> <?php echo ucfirst($result[0]->user_name);?></li>
										<?php } ?>
										<?php if(!empty($result[0]->country)){?>
										<li><span>Country:</span> <?php echo ucfirst($result[0]->country);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->state)){?>
										<li><span>State:</span> <?php echo ucfirst($result[0]->state);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->city)){?>
										<li><span>City:</span> <?php echo ucfirst($result[0]->city);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->street)){?>
										<li><span>Street:</span> <?php echo ucfirst($result[0]->street);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->d_o_b)){?>
										<li><span>Birthday:</span> <?php echo date('Y-m-d',$result[0]->d_o_b);?></li>
										<?php } ?>
										<?php if(!empty($result[0]->role_type)){?>
										<li><span>Occupation:</span> <?php echo ucfirst($result[0]->role_type);?></li>
										<?php } ?>
										<li><span>Email:</span> <a href="#"><?php echo $result[0]->user_email;?></a></li>
										<li><span>Mobile Number:</span><?php echo $result[0]->contact_no;?></li>
									</ul>
								</div>
								</div>
								
								
								<div class="tab-pane row-fluid profile-account" id="tab_1_3">
									<div class="row-fluid">
										<div class="span12">
											<div class="span3">
												<ul class="ver-inline-menu tabbable margin-bottom-10">
													<li class="active">
														<a data-toggle="tab" href="#tab_1-1">
														<i class="icon-cog"></i> 
														Personal Details
														</a> 
														<span class="after"></span>                           			
													</li>
													<li class=""><a data-toggle="tab" href="#tab_4-4"><i class="icon-picture"></i> Company Details</a></li>
													<li class=""><a data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i> Change Password</a></li>
													
												</ul>
											</div>
											<div class="span9">
												<div class="tab-content">
													<div id="tab_1-1" class="tab-pane active">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse" >
<form action="<?php echo base_url();?>sp_manager/update_user_detail" id="validation" method="post" class="form-horizontal" enctype="multipart/form-data">
     <div class="control-group">
	<label class="control-label">Name :</label>
	<div class="controls">
	<input type="text" name="user_name" <?php if(!empty($result[0]->d_o_b)){ ?> value="<?php echo $result[0]->user_name;?>" <?php }?>  class="m-wrap span8" />
	</div>
	</div>
	 <div class="control-group">
	<label class="control-label">Gender :</label>
	<div class="controls">
	<input type="radio" value="M" name="gender" class="m-wrap span8" <?php if($result[0]->gender == 'M'){ echo "checked"; }?>/>Male
	<input type="radio" name="gender" value="F" class="m-wrap span8" <?php if($result[0]->gender == 'F'){ echo "checked"; }?>/>Female
		</div>
	</div>
	 <div class="control-group">
	<label class="control-label">Date of Birth :</label>
	<div class="controls">
	<input type="text" name="dob" id="dob" <?php if(!empty($result[0]->d_o_b)){ ?> value="<?php echo $result[0]->d_o_b;?>" <?php }?> class="m-wrap span8" />
	</div>
	</div>
	 <div class="control-group">
	<label class="control-label">Contact Number :</label>
	<div class="controls">
	<input type="text" name="mobile" <?php if(!empty($result[0]->mob_no)){ ?> value="<?php echo $result[0]->mob_no;?>" <?php }?> class="m-wrap span8" />
	</div>
	</div>
	 <div class="control-group">
	<label class="control-label">Alternate Number :</label>
	<div class="controls">
	<input type="text" name="contact" <?php if(!empty($result[0]->contact_no)){ ?> value="<?php echo $result[0]->contact_no;?>" <?php }?> class="m-wrap span8" />
	</div>
	</div>
	 <div class="control-group">
	<label class="control-label">Profile Image :</label>
	<div class="controls">
	<input name="pro_pic" value="<?php echo $result[0]->pro_pic;?>" type="file"  class="default" />
	<input name="sp_id" value="<?php echo $result[0]->id;?>" type="hidden"  class="default" />
	<input name="user_role" value="<?php echo $result[0]->role_type;?>" type="hidden"  class="default" />
	</div>
	</div>
	 <div class="control-group">
	<div class="controls">
	<div class="submit-btn">
		<button class="btn green">Save</button>
		<a href="<?php echo base_url();?>sp_manager/spmanager" class="btn">Cancel</a>
	</div>
	</div>
	</div>
</form>
														</div>
													</div>
													
													
													
				<div id="tab_3-3" class="tab-pane">
					 <div style="height: auto;" id="accordion3-3" class="accordion collapse">
						<form action="<?php echo base_url();?>admin/change_password" class="form-horizontal" method="post">
						 <div class="control-group">
							<label class="control-label">Current Password :</label>
						<div class="controls">
			<input type="password" placeholder="Old Password" name="current_password" id="current_password" required class="m-wrap span8" />
		</div>
						</div>
						 <div class="control-group">
							<label class="control-label">New Password :</label>
						<div class="controls">
								<input id="password" type="password" placeholder="New Password" name="password" required class="m-wrap span8" />
									</div>
							</div>
						<div class="control-group">
							<label class="control-label">Re-type New Password :</label>
						<div class="controls">
								<input id="new_password" type="password" placeholder="Confirm Password" name="password" required class="m-wrap span8" />
								<input name="sp_id" value="<?php echo $result[0]->id;?>" id="hidden_password" type="hidden"  class="default" />
								<input name="user_role" value="<?php echo $result[0]->role_type;?>" type="hidden"  class="default" />
								</div>
							</div>
								<div class="control-group">
							<label class="control-label"></label>
						<div class="controls">
							<div class="submit-btn">
								<button onsubmit="return myFunction()" id="submit" class="btn green">Update Password</button>
								<a href="#" class="btn">Cancel</a>
							</div>
									</div>
							</div>
							<input name="hidden" value="<?php echo $result[0]->id;?>" type="hidden"  class="default" />
						</form>
					</div>
				</div>
									
									<div id="tab_4-4" class="tab-pane">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse">
	<form action="<?php echo base_url();?>sp_manager/update_company_detail" class="form-horizontal" method="post">
	 <div class="control-group">
		<label class="control-label">Company Name :</label>
	<div class="controls">
		<input name="company_name" type="text" value="<?php echo $result[0]->company_name;?>"  class="m-wrap span8" />
	</div>
																</div>
	 <div class="control-group">
		<label class="control-label">Description :</label>
	<div class="controls">
		<textarea name="company_description" class="span8 m-wrap" rows="3"><?php echo $result[0]->company_description;?></textarea>
</div>
</div>
 <div style="display:none;" class="control-group">
		<label class="control-label">Count of the events performed till date :</label>
		<div class="controls">
		<input name="count_of_event" type="text" value="<?php echo $result[0]->count_of_event;?>" class="m-wrap span8" />
</div>
</div>
 <div style="display:none;" class="control-group">
		<label class="control-label">Last Event Date :</label>
	<div class="controls">
		<input name="last_event_date" type="text" id="led" <?php if(!empty($result[0]->last_event_date)){ ?> value="<?php echo $result[0]->last_event_date;?>" <?php }?> class="m-wrap span8" />
		</div>
</div>
 <div style="display:none;" class="control-group">
		<label class="control-label">Cost Preference :</label>
		 <div class="controls">
			<select name="budget" id="budget" class="small m-wrap" tabindex="1">
			<option <?php if($result[0]->budget=='15k-20k'){ echo "selected";}?> value="15k-20k">15k-20k</option>
			<option <?php if($result[0]->budget=='20k-25k'){ echo "selected";}?> value="20k-25k">20k-25k</option>
			<option <?php if($result[0]->budget=='25k-30k'){ echo "selected";}?> value="25k-30k">25k-30k</option>
			<option <?php if($result[0]->budget=='30k-35k'){ echo "selected";}?> value="30k-35k">30k-35k</option>
			</select>
		</div>
		</div>
		 <div class="control-group">
		<label class="control-label">Country :</label>
		<div class="controls">
			<input type="text" name="country" <?php if(!empty($result[0]->country)){?>value="<?php echo $result[0]->country;?>"<?php } ?> class="span8 m-wrap" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;US&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" />
			<p class="help-block"><span style="display:none;" class="muted">Start typing to auto complete!. E.g: US</span></p>
		</div>
		</div>
		 <div class="control-group">
		<label class="control-label">State :</label>
		<div class="controls">
			<input type="text" name="state" <?php if(!empty($result[0]->state)){?>value="<?php echo $result[0]->state;?>"<?php } ?> class="span8 m-wrap" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;US&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" />
			<p class="help-block"><span style="display:none;" class="muted">Start typing to auto complete!. E.g: US</span></p>
		</div>
		</div>
		 <div class="control-group">
		<label class="control-label">City :</label>
		<div class="controls">
		<input type="text" name="city" value="<?php echo $result[0]->city;?>"  class="m-wrap span8" />
			</div>
		</div>
		 <div class="control-group">
		<label class="control-label">Street :</label>
		<div class="controls">
		<input type="text" name="street" value="<?php echo $result[0]->street;?>"  class="m-wrap span8" />
				</div>
		</div>
		 <div class="control-group">
		<label class="control-label">Zip Code :</label>
		<div class="controls">
		<input type="text" name="zip_code" value="<?php echo $result[0]->zip_code;?>"  class="m-wrap span8" />
				</div>
		</div>
		 <div style="display:none;" class="control-group">
		<label class="control-label">Lattitude :</label>
		<div class="controls">
		<input type="text" name="lattitude" value="<?php echo $result[0]->latt;?>"  class="m-wrap span8" />
				</div>
		</div>
		 <div style="display:none;" class="control-group">
		<label class="control-label">Longitude :</label>
			<div class="controls">
		<input type="text" name="longitude" value="<?php echo $result[0]->longatt;?>"  class="m-wrap span8" />
				</div>
		</div>
		 <div class="control-group">
		<label class="control-label">Landmark :</label>
		<div class="controls">
		<input type="text" name="landmark" value="<?php echo $result[0]->landmark;?>"  class="m-wrap span8" />
				</div>
		</div>
		 <div class="control-group">
		<label class="control-label">Facebook Url :</label>
		<div class="controls">
		<input type="text" name="fb_url" value="<?php echo $result[0]->fb_url;?>"  class="m-wrap span8" />
				</div>
		</div>
		 <div class="control-group">
		<label class="control-label">Google Plus Url :</label>
		<div class="controls">
		<input type="text" name="gp_url" value="<?php echo $result[0]->gp_url;?>"  class="m-wrap span8" />
		<input name="hidden" value="<?php echo $result[0]->id;?>" type="hidden"  class="default" />
		<input name="user_role" value="<?php echo $result[0]->role_type;?>" type="hidden"  class="default" />
				</div>
		</div>
		 <div class="control-group">
		<div class="controls">
		<div class="submit-btn">
			<button class="btn green">Save</button>
			<a href="<?php echo base_url();?>sp_manager/spmanager" class="btn">Cancel</a>
		</div>
			</div>
		</div>
	</form>
														</div>
													</div>
													
													
												</div>
											</div>
											<!--end span9-->                                   
										</div>
									</div>
								</div>
								<!--end tab-pane-->
								<!-- start tab1-4-->
								<div class="tab-pane row-fluid profile-account" id="tab_1_4">
								<div class="row-fluid">
								<div class="span12">
									<div class="span3">
									<ul class="ver-inline-menu tabbable margin-bottom-10">
									<li class="active">
										<a data-toggle="tab" href="#tab_4-1">
										<i class="icon-tags"></i> 
										Venue Information
										</a> 
									<span class="after"></span>                           			
											</li>
										<li class=""><a data-toggle="tab" href="#tab_4-2"><i class="icon-tags"></i> Property Information</a></li>
													
													
												</ul>
											</div>
											<div class="span9">
												<div class="tab-content">
													<div id="tab_4-2" class="tab-pane ">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse" >
<div class="accordion-group">
																<div class="accordion-heading">
																	<a href="#collapse_1" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
																	Property General Information
																	</a>
																</div>
																<div class="accordion-body collapse in" id="collapse_1">
																	<div class="accordion-inner">
<?php foreach($resultProperty as $rowPro):
$proId=$rowPro->pro_id;
$propertyName=$rowPro->propertyName;
$numFloor=$rowPro->numFloor;
$propertyAddr=$rowPro->propertyAddr;
$propertyType=$rowPro->propertyType;
$externalPer=$rowPro->externalPer;
$restrictionLimit=$rowPro->restrictionLimit;
$outsideSpace=$rowPro->outsideSpace;
endforeach;?>


<ul class="unstyled span6">
<?php if(isset($propertyName)):?>  
<li><span><h4>Property Name:</h4></span> <span><?php echo ucfirst($propertyName);?></span></li>
<?php endif;?>	

<?php if(isset($numFloor )):?> 	
						
<li><span><h4>Number Of Floor:</h4></span><?php echo $numFloor;?></li>

<?php endif;?>

<?php if(isset($propertyAddr)):?> 								
<li><span><h4>Property Address:</h4></span> <?php echo ucfirst($propertyAddr);?> </li>
<?php endif;?>	
<?php if(isset($propertyType)):?> 								
<li><span><h4>Property Type:</h4></span> <?php echo ucfirst($propertyType);?> </li>
<?php endif;?>										
</ul>
<ul class="unstyled span6">

<?php if(isset($externalPer)):?> 

<li><span><h4>External Permission :</h4></span> <?php echo ucfirst($externalPer);?></li>

<?php endif;?>

<?php if(isset($restrictionLimit)):?> 										
<li><span><h4>Restriction && Limitation:</h4></span><?php echo ucfirst($restrictionLimit);?></li>
<?php endif;?>
<?php if(isset($outsideSpace)):?> 										
<li><span><h4>Outside Space:</h4></span> <?php echo ucfirst($outsideSpace);?></li>
<?php endif;?>
								
</ul>
																			





	</div>
																</div>
															</div>
															<!--<div class="accordion-group">
																<div class="accordion-heading">
																	<a href="#collapse_2" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
																	Bridal Suite Information
																	</a>
																</div>
																<div class="accordion-body collapse" id="collapse_2">
																	<div class="accordion-inner">
																		Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
																	</div>
																</div>
															</div>
															<div class="accordion-group">
																<div class="accordion-heading">
																	<a href="#collapse_3" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
																	Keys Distance Information
																	</a>
																</div>
																<div class="accordion-body collapse" id="collapse_3">
																	<div class="accordion-inner">
																		Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
																	</div>
																</div>
															</div>
															<div class="accordion-group">
																<div class="accordion-heading">
																	<a href="#collapse_4" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
																	Sitting Arrangement Information
																	</a>
																</div>
																<div class="accordion-body collapse" id="collapse_4">
																	<div class="accordion-inner">
																		Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
																	</div>
																</div>
															</div>
															<div class="accordion-group">
																<div class="accordion-heading">
																	<a href="#collapse_5" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
																	Parking Information
																	</a>
																</div>
																<div class="accordion-body collapse" id="collapse_5">
																	<div class="accordion-inner">
																		Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
																	</div>
																</div>
															</div>-->
															
														</div>
													</div>
													
													
													
				<div id="tab_4-1" class="tab-pane active">
					 <div style="height: auto;" id="accordion3-3" class="accordion collapse">
					<div class="accordion-group">
																<div class="accordion-heading">
																	<a href="#collapse_6" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
																	Venue General Information
																	</a>
																</div>
																<div class="accordion-body collapse in" id="collapse_6">
																	<div class="accordion-inner">
																		<?php foreach($resultVenue as $rowVen):
$location=$rowVen->location;
$propertyName1=$rowVen->prop_name;
$brand_name =$rowVen->brand_name;
$city_name =$rowVen->city_name;
$belt =$rowVen->belt;
$v_name=$rowVen->v_name;
$v_addr=$rowVen->v_addr;
$v_nature=$rowVen->v_nature;
$v_dinein=$rowVen->v_dinein;
$v_lighting=$rowVen->v_lighting;
$v_fencing=$rowVen->v_fencing;
$v_suite_type=$rowVen->v_suite_type;
$v_washroom=$rowVen->v_washroom;
$v_dresser=$rowVen->v_dresser;
$v_transport=$rowVen->v_transport;
$v_gustroomsuite=$rowVen->v_gustroomsuite;
$v_accommodate=$rowVen->v_accommodate;
$v_licencebar=$owVen->v_licencebar;
$v_licencebar_late=$rowVen->v_licencebar_late;
endforeach;?>

<ul class="unstyled span6">
 <?php if(isset($v_name)):?>                                                                                    
<li><span><h4>Venue Name:</h4></span><?php echo ucfirst($v_name);?></li>
<?php endif;?>
<?php if(isset($propertyName1)):?>
<?php if($proId==$propertyName1):?>  
<li><span><h4>Property Name:</h4></span> <?php echo ucfirst($propertyName);?></li>
<?php endif;?>
<?php endif;?>		
<?php if(isset($resultBrand)):?>
<?php foreach($resultBrand as $rowBrand):?>
<?php if(isset($brand_name )):?> 	
<?php if($rowBrand->id==$brand_name):?>								
<li><span><h4>Brand Name:</h4></span><?php echo ucfirst($rowBrand->name);?></li>
<?php endif;?>	
<?php endif;?>
<?php endforeach;?>
<?php endif;?>	
<?php if(isset($v_addr)):?> 								
<li><span><h4>Description:</h4></span> <?php echo ucfirst($v_addr);?> </li>
<?php endif;?>										
</ul>
<ul class="unstyled span6">
<?php if(isset($resultBelt)):?>
<?php foreach($resultBelt as $rowBelt):?>
<?php if(isset($belt)):?> 
<?php if($rowBelt->id==$belt):?>
<li><span><h4>Belt Name:</h4></span> <?php echo ucfirst($rowBelt->beltName);?></li>
<?php endif;?>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
<?php if(isset($v_nature)):?> 										
<li><span><h4>Nature Type:</h4></span><?php echo ucfirst($v_nature);?></li>
<?php endif;?>
<?php if(isset($v_dinein)):?> 										
<li><span><h4>Dine In:</h4></span> <?php echo ucfirst($v_dinein);?></li>
<?php endif;?>
<?php if(isset($location)):?> 										
<li><span><h4>Location:</h4></span> <?php echo ucfirst($location);?> </li>
<?php endif;?>									
</ul>
																						</div>
																</div>
															</div>
											<!--<div class="accordion-group">
																<div class="accordion-heading">
																	<a href="#collapse_7" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
																	Bridal Suite Information
																	</a>
																</div>
																<div class="accordion-body collapse" id="collapse_7">
																	<div class="accordion-inner">
																		<ul class="unstyled span6">
 <?php if(isset($v_lighting)):?>                                                                                    
<li><span><h4>Venue :</h4></span><?php echo ucfirst($v_name);?></li>
<?php endif;?>
<?php if(isset($propertyName)):?>  
<li><span><h4>Property Name:</h4></span> <?php echo ucfirst($propertyName);?></li>
<?php endif;?>	
<?php if(isset($brand_name )):?> 									
<li><span><h4>Brand Name:</h4></span><?php echo ucfirst($brand_name);?></li>
<?php endif;?>		
<?php if(isset($v_addr)):?> 								
<li><span><h4>Description:</h4></span> <?php echo ucfirst($v_addr);?> </li>
<?php endif;?>										
</ul>
<ul class="unstyled span6">
<?php if(isset($belt)):?> 
<li><span><h4>Belt Name:</h4></span> <?php echo ucfirst($belt);?></li>
<?php endif;?>
<?php if(isset($v_nature)):?> 										
<li><span><h4>Nature Type:</h4></span><?php echo ucfirst($v_nature);?></li>
<?php endif;?>
<?php if(isset($v_dinein)):?> 										
<li><span><h4>Dine In:</h4></span> <?php echo ucfirst($v_dinein);?></li>
<?php endif;?>
<?php if(isset($location)):?> 										
<li><span><h4>Location:</h4></span> <?php echo ucfirst($location);?> </li>
<?php endif;?>									
</ul>
																	</div>
																</div>
															</div>
															<div class="accordion-group">
																<div class="accordion-heading">
																	<a href="#collapse_8" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
																	Events Information
																	</a>
																</div>
																<div class="accordion-body collapse" id="collapse_8">
																	<div class="accordion-inner">
																		Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
																	</div>
																</div>
															</div>
													<div class="accordion-group">
													<div class="accordion-heading">
																	<a href="#collapse_9" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle collapsed">
																	Services Information
																	</a>
																</div>
					<div class="accordion-body collapse" id="collapse_9">
																	<div class="accordion-inner">
																		Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
																	</div>
																</div>
															</div>-->
															
																
					</div>
				</div>
									
									
													
													
												</div>
											</div>
											<!--end span9-->                                   
										</div>
									</div>
								</div>
								<!--end tab-pane-->
								<!-- end tab1-4-->
								
								<!--end tab-pane-->
								
								<!--end tab-pane-->
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	
	<?php include('includes/footer.php');?>