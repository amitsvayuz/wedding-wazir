<?php include('includes/header.php');?>
<style>
.portlet-title {
    border-bottom: 1px solid #eee;
    color: #fff !important;
    padding: 8px 10px 2px;
    margin-bottom: 0;
    width:45%;
    background-color: #aaa;
}
.imgcls{
   height: 50%;
   width:50%;
}
</style>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include('includes/sidebar.php');?>
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						
						<!-- END BEGIN STYLE CUSTOMIZER --> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Companies				<small>Venue</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="javascript:void">Companies</a>
								<i class="icon-angle-right"></i>
							</li>
							
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
				<?php if(!empty($venue)){
					foreach($venue as $listv){
											
					?>
					<div class="span4">
						<a href="<?php echo base_url();?>admin/venue_related_details/<?php echo $listv->id; ?>"><img class="imgcls" src="<?php echo base_url()?>/assets/img/venue.png"></a>
						<div class="portlet-title">
						<a href="<?php echo base_url();?>admin/venue_related_details/<?php echo $listv->id; ?>"><span style="font-size:16px;" class="hidden-480"><center><?php echo ucfirst($listv->company_name);?></center></span></a>
						</div>					
					</div>
					<?php }}?>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
		<?php include('includes/footer.php');?>
