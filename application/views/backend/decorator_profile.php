<?php include('includes/header.php');?>


	<div class="page-container row-fluid">
		<?php include('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							 Profile
						</h3>
						<ul class="breadcrumb">
						 <li>
                        <i class="icon-home"></i>
							<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
							<span class="icon-angle-right"></span>
                        </li>
							<li><a href="<?php echo base_url();?>sp_manager/profile?id=<?php echo $_GET['id']?>">Decorator Profile</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid profile">
					<div class="span12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab_1_1" data-toggle="tab">Overview</a></li>
								<li><a href="#tab_1_3" data-toggle="tab">Account</a></li>
								<li><a href="#tab_1_4" data-toggle="tab">Business Manager</a></li>
								
							</ul>
							<div class="tab-content">
								<div class="tab-pane row-fluid active" id="tab_1_1">
									<ul class="unstyled profile-nav span3">
										<li><img src="<?php echo base_url()?>uploads/<?php echo $result[0]->id;?>/<?php echo $result[0]->pro_pic;?>" alt="" /></li>
									</ul>
									<div class="span9">
										<div class="row-fluid">
										<div class="span8 profile-info">
												<!--start -->
                                <?php if($result[0]->user_name):?>
				<h1><?php echo $result[0]->user_name;?></h1>
                                <?php endif;?>
                                <?php if($result[0]->company_description):?>
				<p><?php echo substr($result[0]->company_description , 0 , 70); ?></p>
                                <?php endif;?>
				
				<ul class="unstyled inline">
				<li><i class="icon-map-marker"></i><?php if($result[0]->country): echo ucfirst($result[0]->country); endif;?> , <?php if($result[0]->state): echo ucfirst($result[0]->state);endif;?> , <?php if($result[0]->city): echo ucfirst($result[0]->city);endif;?></li>
				<li><i class="icon-calendar"></i><?php if($result[0]->d_o_b): echo date('m/d/Y',$result[0]->d_o_b); endif;?></li>
			        <li><i class="icon-envelope-alt"></i><?php if($result[0]->user_email): echo $result[0]->user_email; endif;?></li>
				<li><i class="icon-phone"></i><?php if($result[0]->contact_no): echo $result[0]->contact_no; endif;?></li>
				<li><i class="icon-facebook"></i><?php if($result[0]->fb_url): echo $result[0]->fb_url; endif;?></li>
                                <li><i class="icon-google-plus"></i><?php if($result[0]->gp_url): echo $result[0]->gp_url; endif;?></li>
				</ul>
											
<!--End -->
											</div>
											
										</div>
										<!--end row-fluid-->
										<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab_1_11" data-toggle="tab">Requested Leads</a></li>
												<li class=""><a href="#tab_1_22" data-toggle="tab">Leads Accepted</a></li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_11">
													<div class="portlet-body" style="display: block;">
														<div class="clearfix">
                                                                <?php if($this->session->userdata('role')== 'admin'):?>	
									<div class="btn-group">
										<!--<a href="<?php echo base_url();?>lead/create_lead" id="sample_editable_1_new" class="btn green">
										Add New <i class="icon-plus"></i>
										</a>-->
									</div>
									<?php endif;?>	
								</div>
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>S.No.</th>							<?php if($this->session->userdata('role')== 'admin'):?>			
											<th>Type</th>
                                                                <?php endif;?>	
											<th>Customer Name</th>
											<th>Contact No</th>
											<th>Email Id</th>
											<th>Budget</th>
											<th>Response</th>
											<th>Created Date</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									<?php $i=1;foreach($leads_results as $results){ ?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
											<td><?php echo $i;?></td>	
                                                                               <?php if($this->session->userdata('role')=='admin'):?>	
											<td><?php echo $results->Type;?></td>
                                                                               <?php endif;?>	
											<td><?php echo $results->Name;?></td>
											<td><?php echo $results->Contact_Num;?></td>
											<td><?php echo $results->Email_Id;?></td>
											<td><?php echo $results->Budget;?></td>
											<td><?php echo $results->Response;?></td>
											<td><?php echo $results->created_date;?></td>
											
										
	<td>
	
<a id="view"  title="view" href="<?php echo base_url();?>lead/lead_view?id=<?php echo base64_encode($results->lead_id);?>">
<i class="icon-eye-open" aria-hidden="true"></i>
 </a>
|
<a id="delete" onClick="return confirm('Are you sure to remove this!');" title="remove" href="<?php echo base_url();?>lead/lead_delete?id=<?php echo base64_encode($results->lead_id);?>">
<i class="icon-trash"></i></a>
										</td>
										</tr>
									<?php $i=$i+1; } ?>
									</tbody>
								</table>
													</div>
												</div>
												<!--tab-pane-->
												<div class="tab-pane" id="tab_1_22">
													<div class="tab-pane active" id="tab_1_1_1">
														<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
															<ul class="feeds">

																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			3 hours
																		</div>
																	</div>
																</li>
															
															</ul>
														</div>
													</div>
												</div>
												<!--tab-pane-->
											</div>
										</div>
									</div>
									<!--end span9-->
								</div>
								<!--end tab-pane-->
								
		
								
								<!--tab_1_2-->
								<div class="tab-pane profile-classic row-fluid" id="tab_1_5">
									<div class="span2"><img src="<?php echo base_url()?>uploads/<?php echo $result[0]->id;?>/<?php echo $result[0]->pro_pic;?>" alt="" /> <a href="<?php echo base_url();?>sp_manager/profile?id=<?php echo base64_encode($result[0]->id);?>" class="profile-edit">edit</a></div>
									<ul class="unstyled span10">
									<?php if(!empty($result[0]->user_name)){?>
										<li><span>User Name:</span> <?php echo ucfirst($result[0]->user_name);?></li>
										<?php } ?>
										<?php if(!empty($result[0]->country)){?>
										<li><span>Country:</span> <?php echo ucfirst($result[0]->country);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->state)){?>
										<li><span>State:</span> <?php echo ucfirst($result[0]->state);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->city)){?>
										<li><span>City:</span> <?php echo ucfirst($result[0]->city);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->street)){?>
										<li><span>Street:</span> <?php echo ucfirst($result[0]->street);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->d_o_b)){?>
										<li><span>Birthday:</span> <?php echo date('Y-m-d',$result[0]->d_o_b);?></li>
										<?php } ?>
										<?php if(!empty($result[0]->role_type)){?>
										<li><span>Occupation:</span> <?php echo ucfirst($result[0]->role_type);?></li>
										<?php } ?>
										<li><span>Email:</span> <a href="#"><?php echo $result[0]->user_email;?></a></li>
										<li><span>Mobile Number:</span><?php echo $result[0]->contact_no;?></li>
									</ul>
								</div>
								<div class="tab-pane row-fluid profile-account" id="tab_1_3">
									<div class="row-fluid">
										<div class="span12">
											<div class="span3">
												<ul class="ver-inline-menu tabbable margin-bottom-10">
													<li class="active">
														<a data-toggle="tab" href="#tab_1-1">
														<i class="icon-cog"></i> 
														Personal Details
														</a> 
														<span class="after"></span>                           			
													</li>
													<li class=""><a data-toggle="tab" href="#tab_4-4"><i class="icon-picture"></i> Company Details</a></li>
													<li class=""><a data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i> Change Password</a></li>
													
												</ul>
											</div>
											<div class="span9">
												<div class="tab-content">
													<div id="tab_1-1" class="tab-pane active">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse" >
	<form action="<?php echo base_url();?>sp_manager/update_user_detail" id="validation" method="post" class="form-horizontal" enctype="multipart/form-data">
	 <div class="control-group">
		<label class="control-label">Name :</label>
	   <div class="controls">
		<input type="text" name="user_name" <?php if(!empty($result[0]->user_name)){ ?> value="<?php echo $result[0]->user_name;?>" <?php }?>  class="m-wrap span8" />
	</div>
	</div>
	<div class="control-group">
		<label class="control-label">Gender :</label>
	 <div class="controls">
		<input type="radio" value="M" name="gender" class="m-wrap span8" <?php if($result[0]->gender == 'M'){ echo "checked"; }?>/>Male
		<input type="radio" name="gender" value="F" class="m-wrap span8" <?php if($result[0]->gender == 'F'){ echo "checked"; }?>/>Female
	</div>
	</div>
	<div class="control-group">
		<label class="control-label">Date of Birth :</label>
		 <div class="controls">
		<input type="text" name="dob" id="dob" <?php if(!empty($result[0]->d_o_b)){ ?> value="<?php echo $result[0]->d_o_b;?>" <?php }?> class="m-wrap span8" />
	</div>
	</div>
	<div class="control-group">
		<label class="control-label">Mobile Number :</label>
	 <div class="controls">
		<input type="text" name="mobile" <?php if(!empty($result[0]->mob_no)){ ?> value="<?php echo $result[0]->mob_no;?>" <?php }?> class="m-wrap span8" />
	</div>
	</div>
     <div class="control-group">
		<label class="control-label">Alternate Number :</label>
	 <div class="controls">
		<input type="text" name="contact" <?php if(!empty($result[0]->contact_no)){ ?> value="<?php echo $result[0]->contact_no;?>" <?php }?> class="m-wrap span8" />
	</div>
	</div>
	 <div class="control-group">
		<label class="control-label">Profile Image :</label>
	 <div class="controls">
		<input name="pro_pic" value="<?php echo $result[0]->pro_pic;?>" type="file"  class="default" />
		<input name="sp_id" value="<?php echo $result[0]->id;?>" type="hidden"  class="default" />
		<input name="user_role" value="<?php echo $result[0]->role_type;?>" type="hidden"  class="default" />
	</div>
	</div>
	 <div class="control-group">
	 <div class="controls">
		<div class="submit-btn">
			<button class="btn green">Save</button>
			<a href="<?php echo base_url();?>sp_manager/decorator" class="btn">Cancel</a>
		</div>
	</div>
	</div>
	</form>
														</div>
													</div>
													
													
													
													<div id="tab_3-3" class="tab-pane">
													     <div style="height: auto;" id="accordion3-3" class="accordion collapse">
															<form action="<?php echo base_url();?>admin/change_password" method="post" class="form-horizontal">
															  <div class="control-group">
																<label class="control-label">Current Password :</label>
																<div class="controls">
<input type="password" name="current_password" id="current_password" value="" class="m-wrap span8" />
																</div>
																</div>
																<div class="control-group">
																<label class="control-label">New Password :</label>
																<div class="controls">
																	<input id="password" type="password" name="password" class="m-wrap span8" />
															    </div>
																</div>
																<div class="control-group">
																<label class="control-label">Re-type New Password :</label>
															    <div class="controls">
																	<input id="new_password" type="password" name="password" class="m-wrap span8" />
															    </div>
																</div>
																<div class="control-group">
																<div class="controls">
																<div class="submit-btn">
																	<button onsubmit="return myFunction()" id="submit" class="btn green">Update Password</button>
																	<a href="<?php echo base_url();?>sp_manager/decorator" class="btn">Cancel</a>
																<input name="sp_id" value="<?php echo $result[0]->id;?>" id="hidden_password" type="hidden"  class="default" />
																<input name="user_role" value="<?php echo $result[0]->role_type;?>" type="hidden"  class="default" />
																</div>
																 </div>
																</div>
															</form>
														</div>
													</div>
													<div id="tab_4-4" class="tab-pane">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse">
	<form action="<?php echo base_url();?>sp_manager/update_company_detail" method="post" class="form-horizontal">
	 <div class="control-group">	
		<label class="control-label">Company Name :</label>
	 <div class="controls">
		<input name="company_name" type="text" value="<?php echo $result[0]->company_name;?>"  class="m-wrap span8" />
	</div>
</div>
 <div class="control-group">	
		<label class="control-label">Description :</label>
		 <div class="controls">
		<textarea name="company_description" class="span8 m-wrap" rows="3"><?php echo $result[0]->company_description;?></textarea>
</div>
</div>
 
 <div class="control-group">	
		<label class="control-label">Country :</label>
		<div class="controls">
			<input type="text" name="country" <?php if(!empty($result[0]->country)){?>value="<?php echo $result[0]->country;?>"<?php } ?> class="span8 m-wrap" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;US&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" />
			<p class="help-block"><span class="muted"></span></p>
		</div>
	</div>
	 <div class="control-group">	
		<label class="control-label">State :</label>
		<div class="controls">
			<input type="text" name="state" <?php if(!empty($result[0]->state)){?>value="<?php echo $result[0]->state;?>"<?php } ?> class="span8 m-wrap" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;US&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" />
			<p class="help-block"><span class="muted"></span></p>
		</div>
		</div>
		 <div class="control-group">	
		<label class="control-label">City :</label>
		<div class="controls">
		<input type="text" name="city" value="<?php echo $result[0]->city;?>"  class="m-wrap span8" />
		</div>
		</div>
		 <div class="control-group">
		<label class="control-label">Street :</label>
		<div class="controls">
		<input type="text" name="street" value="<?php echo $result[0]->street;?>"  class="m-wrap span8" />
		</div>
		</div>
		 <div class="control-group">
		<label class="control-label">Zip Code :</label>
		<div class="controls">
		<input type="text" name="zip_code" value="<?php echo $result[0]->zip_code;?>"  class="m-wrap span8" />
		</div>
		</div>
		 
		<div class="control-group">
		<label class="control-label">Landmark :</label>
		<div class="controls">
		<input type="text" name="landmark" value="<?php echo $result[0]->landmark;?>"  class="m-wrap span8" />
		</div>
		</div>
		<div class="control-group">
		<label class="control-label">Facebook Url :</label>
		<div class="controls">
		<input type="text" name="fb_url" value="<?php echo $result[0]->fb_url;?>"  class="m-wrap span8" />
			</div>
		</div>
			<div class="control-group">
		<label class="control-label">Google Plus Url :</label>
		<div class="controls">
		<input type="text" name="gp_url" value="<?php echo $result[0]->gp_url;?>"  class="m-wrap span8" />
		<input name="hidden" value="<?php echo $result[0]->id;?>" type="hidden"  class="default" />
		<input name="user_role" value="<?php echo $result[0]->role_type;?>" type="hidden"  class="default" />
			</div>
		</div>
		<div class="control-group">
		<div class="controls">
		<div class="submit-btn">
			<button class="btn green">Save</button>
			<a href="<?php echo base_url();?>sp_manager/decorator" class="btn">Cancel</a>
		</div>
		</div>
		</div>
	</form>
														</div>
													</div>
													
													
												</div>
											</div>
											<!--end span9-->                                   
										</div>
									</div>
								</div>
								<!--end tab-pane-->
			<div class="tab-pane profile-classic row-fluid" id="tab_1_4">
			<div class="span10">
								<form  method="post" action="<?php echo base_url();?>decorator/edit_decorator_services" class="form-horizontal">
                           <div class="form-wizard">
                              <div class="tab-content">
							  <div class="span3">
							  <img src="<?php echo base_url()?>uploads/<?php echo $result[0]->id;?>/<?php echo $result[0]->pro_pic;?>" alt="" />
							  </div>
							  <div class="span9">
                                	<div class="control-group">
                                       <label class="control-label">Services :</label>
		<?php $services = explode(',' , $business_result->services);
		?>	
                                       <div class="controls">
										   <label class="checkbox">
     <input  <?php if(in_array('DJ And Music' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="DJ And Music" />DJ And Music
                                          </label>
										   <label class="checkbox">
    <input <?php if(in_array('Flower Decoration' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Flower Decoration" />Flower Decoration
                                          </label>
										   <label class="checkbox">
    <input  <?php if(in_array('Crockery And Furniture' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Crockery And Furniture" />Crockery And Furniture
                                          </label>
										   <label class="checkbox">
    <input <?php if(in_array('Theme Decoration' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Theme Decoration" />Theme Decoration
                                          </label>
                                       </div>
                                    </div>
					<?php $other_services = explode(',' , $business_result->other_services);
		?>				
							        <div class="control-group">
                                       <label class="control-label">Other Services :</label>
                                       <div class="controls">
										   <label class="checkbox">
  <input <?php if(in_array('Catering Service' ,$other_services))echo 'checked';?> type="checkbox" name="other_services[]" value="Catering Service" />Catering Service
                                          </label>
										   <label class="checkbox">
  <input <?php if(in_array('Audio Visual Aids' ,$other_services))echo 'checked';?> type="checkbox" name="other_services[]" value="Audio Visual Aids" />Audio Visual Aids
                                          </label>
										   <label class="checkbox">
   <input <?php if(in_array('Wedding Decoration' ,$other_services))echo 'checked';?> type="checkbox" name="other_services[]" value="Wedding Decoration" />Wedding Decoration
                                          </label>
										  <label class="checkbox">
   <input <?php if(in_array('Home Decoration' ,$other_services))echo 'checked';?> type="checkbox" name="other_services[]" value="Home Decoration" />Home Decoration
                                          </label>
                                       </div>
                                    </div>
									  <input type="hidden" name="hidden_user_id" value="<?php echo $business_result->user_id;?>" />
							  <input type="hidden" name="hidden_id" value="<?php echo $business_result->id;?>" />
							  <div class="control-group">
							   <label class="control-label"></label>
                                       <div class="controls">
                              <div class="submit-btn">
							<button type="submit" class="btn green">Save </button>
								</div>
								</div>
								</div>
                              </div>
							
								</div>
                           </div>
						   </div>
                        </form>
					
								<!--end tab-pane-->
								
								<!--end tab-pane-->
							
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	
	<?php include('includes/footer.php');?>