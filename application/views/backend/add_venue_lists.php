<?php include('includes/header.php');?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Venue Details
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>Venue/venue_manager">Venue  Details Manage</a>
								<i class="icon-angle-right"></i>
							</li>
							<!--<li><a href="#">Managed Tables</a></li>-->
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
					<?php if(isset($_GET['set'])):?>
					   <div class="alert alert-success">
											<button class="close" data-dismiss="alert"></button>
											<strong><?php echo base64_decode($_GET['set']);?></strong> 
					   </div>
			   <?php endif;?>
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i> Venue Details</h4>
								<!--<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>-->
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group">
										<a href="<?php echo base_url();?>Venue/add_venue_manager" id="sample_editable_1_new" class="btn green">
										Add New <i class="icon-plus"></i>
										</a>
									</div>
									<!--<div class="btn-group pull-right">
										<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">Print</a></li>
											<li><a href="#">Save as PDF</a></li>
											<li><a href="#">Export to Excel</a></li>
										</ul>
									</div>-->
								</div>
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>Venue Company Name</th>
											<th class="hidden-480">Venue Name</th>
											<th class="hidden-480">Venue City</th>
											<th class="hidden-480">Created Date</th>
											<th >Action</th>
										</tr>
									</thead>
									<tbody>
									    <?php if(isset($result)):?>
									    <?php foreach($result as $row):?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
											<td><?php echo $row->v_company;?></td>
											<td class="hidden-480"><a href="mailto:shuxer@gmail.com"><?php echo $row->v_name;?></a></td>
											<td class="hidden-480"><?php echo $row->v_city;?></td>
											<td class="center hidden-480"><?php echo $row->v_create_date;?></td>
											<td ><!--<a href=""><i class="icon-eye-open"></i></a>| --> <a title="Edit"  href="<?php echo base_url();?>Venue/venue_edit_get/<?php echo $row->v_id;?>"><i class="icon-edit"></i></a> | <a title="Remove"  href="<?php echo base_url();?>Venue/venue_delete/<?php echo $row->v_id;?>"><i class="icon-remove"></i></a></td>
										</tr>
										<?php endforeach;?>
										<?php else:?>
										<tr class="odd gradeX">
										<td></td><td></td><td></td><td>Not Exit Any Row</td><td></td><td></td>
										</tr>
										<?php endif;?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	
	<?php include('includes/footer.php');?>
    