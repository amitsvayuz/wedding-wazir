<?php include_once('includes/header.php');?>
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php include_once('includes/sidebar.php');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                
                  <h3 class="page-title">
                     Photographer Service
                     <small>form wizard sample</small>
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="index.html">Home</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="#">Form Stuff</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="#">Form Wizard</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <div class="portlet box blue" id="form_wizard_1">
                     <div class="portlet-title">
                        <h4>
                           <i class="icon-reorder"></i> Services <span class="step-title">Addition</span>
                        </h4>
                       
                     </div>
                     <div class="portlet-body form">
                        <form  method="post" action="<?php echo base_url();?>photographer/edit_photographer_services" class="form-horizontal">
                           <div class="form-wizard">
                              <div class="tab-content">
                                 <div class="tab-pane active" id="tab1">
                                    <h4 class="block">Provide your account details</h4>
                                 </div>
              <?php $events = explode(',' , $services_details->events);
			 ?>	                   
                                
                               <div class="control-group">
                                       <label class="control-label">Events :</label>
                                       <div class="controls">
										<select name="events[]" class="span6 m-wrap" multiple="multiple" data-placeholder="Choose a Category" tabindex="1">
											<?php foreach($events_details as $details) { ?>
	<option  <?php if(in_array($details->description ,$events))echo 'selected';?> value="<?php echo $details->description;?>"> <?php echo $details->name;?></option>
											<?php } ?>
										</select>
                                       </div>
                                    </div>
			<?php $services = explode(',' , $services_details->services);
		?>			
		
									<div class="control-group">
                                       <label class="control-label">Services :</label>
                                       <div class="controls">
										   <label class="checkbox">
 <input  <?php if(in_array('Photography' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Photography" />Photography
                                          </label>
										   <label class="checkbox">
 <input  <?php if(in_array('Videography' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Videography" />Videography
                                          </label>
										   <label class="checkbox">
 <input  <?php if(in_array('Cinematography' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Cinematography" />Cinematography
                                          </label>
                                       </div>
                                    </div>
									
			<?php $style_of_photography = explode(',' , $services_details->style_of_photography);
			?>	
									<div class="control-group">
                                       <label class="control-label">Style of Photography :</label>
                                       <div class="controls">
										   <label class="checkbox">
<input type="checkbox"  <?php if(in_array('Candid' ,$style_of_photography))echo 'checked';?>   name="style_of_photography[]" value="Candid" />Candid
                                          </label>
										   <label class="checkbox">
<input type="checkbox"  <?php if(in_array('Traditional' ,$style_of_photography))echo 'checked';?>  name="style_of_photography[]" value="Traditional" />Traditional
                                          </label>
                                       </div>
                                    </div>
									
		<?php $sharing_option = explode(',' , $services_details->sharing_option);
			?>								
									<div class="control-group">
                                       <label class="control-label">Sharing Option :</label>
                                       <div class="controls">
										   <label class="checkbox">
  <input <?php if(in_array('CD' ,$sharing_option))echo 'checked';?>  type="checkbox" name="sharing_option[]" value="CD" />CD/ DVD
                                          </label>
										   <label class="checkbox">
  <input <?php if(in_array('Pen Drive' ,$sharing_option))echo 'checked';?>  type="checkbox" name="sharing_option[]" value="Pen Drive" />Pen Drive
                                          </label>
										   <label class="checkbox">
  <input <?php if(in_array('Album' ,$sharing_option))echo 'checked';?>  type="checkbox" name="sharing_option[]" value="Album" />Album
                                          </label>
										   <label class="checkbox">
   <input <?php if(in_array('Online' ,$sharing_option))echo 'checked';?>  type="checkbox" name="sharing_option[]" value="Online" />Online
                                          </label>
										   <label class="checkbox">
  <input <?php if(in_array('Others' ,$sharing_option))echo 'checked';?>  type="checkbox" name="sharing_option[]" value="Others" />Others
                                          </label>
                                       </div>
                                    </div>
									
				<?php $resolution = explode(',' , $services_details->resolution);
			?>							
									<div class="control-group">
                                       <label class="control-label">Resolution :</label>
                                       <div class="controls">
										   <label class="checkbox">
<input <?php if(in_array('Low' ,$resolution))echo 'checked';?> type="checkbox" name="resolution[]" value="Low" />Low
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('Low Medium' ,$resolution))echo 'checked';?>  type="checkbox" name="resolution[]" value="Low Medium" />Low - Medium
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('Medium' ,$resolution))echo 'checked';?> type="checkbox" name="resolution[]" value="Medium" />Medium
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('Medium High' ,$resolution))echo 'checked';?> type="checkbox" name="resolution[]" value="Medium High" />Medium - High
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('High' ,$resolution))echo 'checked';?> type="checkbox" name="resolution[]" value="High" />High (HD)
                                          </label>
                                       </div>
                                    </div>
					<?php $other_services = explode(',' , $services_details->other_services);
			?>	
									<div class="control-group">
                                       <label class="control-label">Other Services :</label>
                                       <div class="controls">
										   <label class="checkbox">
<input <?php if(in_array('Studio Setup' ,$other_services))echo 'checked';?>  type="checkbox" name="studio_setup[]" value="Studio Setup" />Studio Setup
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('Video Editing' ,$other_services))echo 'checked';?>  type="checkbox" name="studio_setup[]" value="Video Editing" />Photo/ Video Editing
                                          </label>
                                       </div>
                                    </div>
								 
                              </div>
                              <div class="submit-btn">
							<button type="submit" class="btn green">Save Changes</button>
								<a href="<?php echo base_url();?>photographer/list_services" class="btn">Cancel</a>
							</div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
 <?php include_once('includes/footer.php');?>