<style>
a:hover{
	text-decoration: none !important;
	
}
.modal-header-primary {
	color:#fff;
    padding:9px 15px;
    border-bottom:1px solid #eee;
    background-color: #5bc0de;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
}
</style>
<?php include('includes/header.php');?>
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>
						<!-- END BEGIN STYLE CUSTOMIZER --> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Companies				<small>Venue</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="javascript:void">Companies</a>
								<i class="icon-angle-right"></i>
							</li>
							
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="tiles">
					<?php if(!empty($venue)){
					foreach($venue as $listv){
											
					?>
					<?php 
						 
								$href=base_url().'sp_manager/venue_profile?id='.base64_encode($listv->id);
						
						 
					
					?>
					
					
					<div class="tile double bg-purple">
					<a href="#myModal1" class="company" id="<?php echo $listv->id; ?>" data-toggle="modal">
						<div class="tile-body">
						<?php if(!empty($listv->pro_pic)){?>
							<img style="width:60px;height:60px;" src="<?php echo base_url()?>uploads/<?php echo $listv->id;?>/<?php echo $listv->pro_pic;?>" alt="" />
						<?php }else{?>
						<img style="width:60px;height:60px;" src="<?php echo base_url()?>assets/img/person.png" alt="" />
						<?php }?>
						<p>
							<?php echo ucfirst($listv->company_name);?><br>
							
								<?php echo $listv->user_email; ?><br>
								<?php echo ucfirst($listv->city).','.$listv->state.','.$listv->country.','.$listv->zip_code; ?>
							</p>
						</div>
						</a>
						<div class="tile-object">
							<a href="<?php echo $href;?>" class="name">
								<i class="icon-edit"></i>
							</a>
							<div class="number">
								<?php
								echo date('d M Y',strtotime($listv->create_date));?>
							</div>
						</div>
					</div>
					
					<?php }}?>
					
					
					
				</div>
				<br>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<div id="myModal1" style="width:70%;margin-left:-470px !important;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header modal-header-primary">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 >Brands</h3>
										
										<span id="flashmsg"></span>
                                        
									</div>
									
									<div id="albumdiv" class="modal-body">
									
									</div>
									<div class="modal-footer">
									<a id="myModalLabel1" href="<?php echo base_url();?>General_setting/add_brand" class="btn green"><i class="icon-plus"></i> Add Brand</a>
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
									
									</div>
	
<script type="text/javascript">
$(function(){
$('.company').click(function(){
	//alert("/");
//alert($(this).attr('id'));
var id = $(this).attr('id');
 $.ajax({type:'post',
		    url:'<?PHP echo base_url()?>admin/venue_brand',
		    data:{id:id},
		    success:function(res){
			                     //alert(res);
                                  $('#albumdiv').html(res);
								 }
	      });

 });
});
</script>
							</div>
	<!-- BEGIN FOOTER -->
	
		<?php include('includes/footer.php');?>