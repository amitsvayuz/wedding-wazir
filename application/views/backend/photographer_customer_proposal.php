<?php include('includes/header.php');?>
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN PAGE -->  
	  <?php include('includes/sidebar.php');?>
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  
                  <h3 class="page-title">
                     Customer Proposal
                   
                  </h3>
                  <ul class="breadcrumb">
                     
                     <li>
						<i class="icon-home"></i>
                        <a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a>
                        <span class="icon-angle-right"></span>
                     </li>
<li>
                        
                        <a href="<?php echo base_url();?>sp_manager/customer_proposal;?>">Customer Proposal</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey tabbable">
                     <div class="portlet-title">
                        <h4>
                           <i class="icon-reorder"></i>
                           <span class="hidden-480">Customer Proposal</span>
                           &nbsp;
                        </h4>
                     </div>
                     <div class="portlet-body form">
                        <div class="tabbable portlet-tabs">
                          
                           <div class="tab-content">
                              <div class="tab-pane active" id="portlet_tab1">
                                 <!-- BEGIN FORM-->
                                 <form action="<?php echo base_url();?>Photographer/photographer_customer_proposal" method="post" class="form-horizontal">
								     <!--<div class="control-group" style="margin-top: 2%;">
                                       <label class="control-label">Service Provider:</label>
                                       <div class="controls">
                                          <select class="large m-wrap" name="select_provider" id="select_provider" tabindex="1" required/>
                                             <option value="">Select Provider</option>
                                             <option value="photographer">Photographer</option>
                                             <option value="decorator">Decorator</option>
                                             <option value="planner">Planner</option>
                                             <option value="venue">Venue Manager</option>
                                          </select>
                                       </div>
                                      </div>
									
                                    <div class="control-group">
                                       <label class="control-label">Provider Names :</label>
                                       <div class="controls">
                                          <select class="large m-wrap" name="provider_names" id="provider_names" tabindex="1" required/>
                                           
                                          </select>
                                       </div>
                                    </div>-->
                                   
                                    <div class="control-group" style="margin-top: 2%;">
                                       <label class="control-label">Customer Name :</label>
                                       <div class="controls">
                                          <input type="text" placeholder="Customer Name" name="customer_name" class="m-wrap large" required//>
                                       </div>
                                    </div>
									
                                    <div class="control-group">
                                       <label class="control-label">Email Address :</label>
                                       <div class="controls">
                                          <input type="text" placeholder="Email Address" name="email" class="m-wrap large" required//>
                                        </div>
                                    </div>
                                   
                                    <div class="control-group">
                                       <label class="control-label">Contact Number :</label>
                                       <div class="controls">
                                          <input type="text" placeholder="Contact Number" name="contact_number" class="m-wrap large" required//>
                                        </div>
                                    </div>
                                  
                                    <div class="control-group">
                                       <label class="control-label">Message :</label>
                                       <div class="controls">
                                          <textarea class="large m-wrap" placeholder="Enter the message" name="message" rows="3" required/></textarea>
                                       </div>
                                    </div>
                                    
									  <!--<div class="control-group">
										  <label class="control-label">Attach Photos :</label>
										  <div class="controls">
											 <input type="file" name="photos[]" class="default large" />
										  </div>
									  </div>-->
									<div class="portlet-body">
								<!-- BEGIN GALLERY MANAGER PANEL-->
								<?php foreach($images_result as $details) { ?>
								<div class="row-fluid">
									<div class="span12">
										<div class="portlet-title">
										<h4>
										   <i class="icon-reorder"></i>
										   <span class="hidden-480"> Album: <?php echo ucfirst($details->title);?></span>
										   &nbsp;
										</h4>
									</div>
									</div>
									<?php 
									$image = array();
									$image = explode(',',$details->images);
									//print_r($image);?>
								</div>
								<!-- END GALLERY MANAGER PANEL-->
								<hr class="clearfix" />
								<!-- BEGIN GALLERY MANAGER LISTING-->
								
								<div class="space10"></div>
								<div class="row-fluid">
							<?php for($i=0;$i<sizeof($image);$i++) { ?>
	<div class="span3">
									
		<div class="item">
		<div class="controls">
		<?php 
		$imgext= explode('.',$image[$i]);
		$imgName=$imgext['0'];
		?>
		<input type="checkbox" name="selimg[]" value="<?php echo $image[$i];?>" class="default large" />
		</div>
<a class="fancybox-button" data-rel="fancybox-button" title="Metronic Tablet Preview" href="<?php echo base_url();?>assets/uploads/photography/<?php echo $this->session->userdata('id');?>/<?php echo $image[$i];?>">
	<div class="zoom">
<img src="<?php echo base_url();?>assets/uploads/photography/<?php echo $this->session->userdata('id');?>/<?php echo $image[$i];?>" alt="Photo" />							
	<div class="zoom-icon"></div>
	</div>
</a>
<!--<div class="details">
	<a href="#" class="icon"><i class="icon-paper-clip"></i></a>
	<a href="#" class="icon"><i class="icon-link"></i></a>
	<a href="#" class="icon"><i class="icon-pencil"></i></a>
	<a href="#" class="icon"><i class="icon-remove"></i></a>		
</div>-->
</div>
</div>
									<?php } ?>
								<!-- END GALLERY MANAGER LISTING-->
								</div>
								<?php } ?>
							</div>   
                                    <div class="form-actions">
                                       <button type="submit" class="btn blue"><i class="icon-ok"></i> Save</button>
                  <a href="<?php echo base_url();?>sp_manager/profile?id=<?php echo base64_encode($this->session->userdata('id')); ?>"><button type="button" class="btn">Cancel</button></a>
						            </div>
                                 </form>
                                 <!-- END FORM-->  
                              </div>
                            </div>
                        </div>
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
   
 <?php include('includes/footer.php'); ?>