<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
	<?php include_once('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Photographer
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url();?>photographer/admin_list_photographer_title_images?id=<?php echo $_REQUEST['id'];?>">Gallery</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN GALLERY MANAGER PORTLET-->
					<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-reorder"></i>Photographer</h4>
							</div>
							<div class="portlet-body">
							<div class="row-fluid">
							<?php foreach($photographer_details as $details) { ?>
							
								<!-- BEGIN GALLERY MANAGER PANEL-->
							
								
									<div class="span2">
		<a href="<?php echo base_url();?>photographer/admin_list_photographer_images/?user_id=<?php echo $details->user_id;?>&id=<?php echo $details->id;?>">
	<div class="tile image selected">
<div class="tile-body">
<img src="<?php echo base_url();?>assets/img/gallery/folder-icon.png" alt="">
</div>
<div class="tile-object">
<div class="name">
<h4 style="margin: -43px 0px;"><?php echo ucfirst($details->title);?></h4>
</div>
</div>
</div> </a>
									</div>
								
								
							
						<?php } ?>	
						</div>
			     				
						</div>
						<!-- END GALLERY MANAGER PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->			
		</div>
		<!-- END PAGE -->	 	
	</div>
  </div>
	<!-- END CONTAINER -->
<?php include('includes/footer.php');?>