<?php include('includes/header.php');?>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						
						<!-- END BEGIN STYLE CUSTOMIZER -->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Customer
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>admin/customer">Customer</a>
								
							</li>
							 
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>Customer</h4>
								<!--<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									
									<a href="javascript:;" class="reload"></a>
									
								</div>-->
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group">
										<a href="<?php echo base_url();?>admin/add_customer" id="sample_editable_1_new" class="btn green">
										Add New <i class="icon-plus"></i>
										</a>
									</div>
									
								</div>
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>S.N.</th>												
											<th>Name</th>
											<th class="hidden-480">Email</th>
											<th class="hidden-480">Date of birth</th>
											<th class="hidden-480">Contact</th>
											<th  class="hidden-480" >Status</th>
											<th  class="hidden-480" >Action</th>
										</tr>
									</thead>
									
									<tbody>
									<?php if(!empty($result))
									{ $i=1;
										foreach($result as $row)
										{
										$id=base64_encode($row->customer_id);
										$date= $row->customer_dob;
										//echo $id.'abc';
										?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
											<td><?php echo $i;?></td>
											<td><?php echo ucfirst(ucfirst($row->customer_name));?></td>
											<td class="hidden-480"><?php echo $row->customer_email;?></td>
											<td class="hidden-480"><?php echo date('d/m/Y',$date);?></td>
											<td class="hidden-480"><?php echo ucfirst($row->customer_contact);?></td>
											
											
											<td >
											<?php if($row->status == 1){?>
  <a href="#" onClick="var a=confirm('Are you sure to Deactive this!');if(a){ deactive('<?php echo $row->customer_id; ?>');}else{return false;}" title="Deactive">
                      <span class="label label-sm label-success">Active</span></a>&nbsp;
                      
                      <?php } else if($row->status == 0){?>
                       <a href="#" onClick="var a=confirm('Are you sure to Active this!');if(a){ active('<?php echo $row->customer_id;?>');}else{return false;}" title="active">
                      <span class="label label-sm label-warning">Deactive</span></a>&nbsp;
                      <?php } ?>
											</td>
											
											<td><a href="<?php echo base_url()?>admin/edit_customer?id=<?php echo $id;?>" class=""><i class="icon-edit"></i></a> | <a onClick="return confirm('Are you sure you want to delete?');" id="delete_customer" href="<?php echo base_url()?>admin/delete_customer?del=<?php echo $id;?>" class=""><i class="icon-trash"></i></a></td>
										</tr>
									<?php $i=$i+1; } } ?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
			
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
		
	<script type="text/javascript">
 function active(x)
 {
	var id11=x;
	//alert(id11);
	$.ajax({
		type:"POST",     
        url:'<?PHP echo base_url();?>admin/active_customer',
        data:{"id":id11},  
        success: function(data){
			
			location.reload();
			}
      });
 }
function deactive(r)
 {
	//alert('sni');
	var id11=r;
	//alert(id11);
	 $.ajax({
		type:"POST",     
        url:'<?PHP echo base_url();?>admin/deactive_customer',
        data:{"id":id11}, 
        success: function(data){
			
			location.reload();
		}
      });
 }
</script>
	<?php include('includes/footer.php');?>

	<!-- END CONTAINER -->

