<?php include('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">			
						<h3 class="page-title">
							Belt 
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>General_setting/list_belt">Belt</a>
								<i class="icon-angle-right"></i>
							</li>
							<!--<li><a href="#">Managed Tables</a></li>-->
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
					<?php if(isset($_GET['set'])):?>
					   <div class="alert alert-success">
											<button class="close" data-dismiss="alert"></button>
											<strong><?php echo base64_decode($_GET['set']);?></strong> 
					   </div>
			   <?php endif;?>
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>Venue Belt</h4>
								<!--<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a>
								</div>-->
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group">
										<a href="<?php echo base_url();?>General_setting/add_belt" id="sample_editable_1_new" class="btn green">
										Add New <i class="icon-plus"></i>
										</a>
									</div>
								</div>
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th class="hidden-480">Belt Name</th>
											<th class="hidden-480">Belt Description</th>
											<th class="hidden-480">Status</th>
										</tr>
									</thead>
									<tbody>
									    <?php 	if(isset($result)):?>
									    <?php 	foreach($result as $row):?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
											<td class="hidden-480"><?php echo $row->beltName;?></td>
											<td class="hidden-480"><?php echo $row->beltDesc;?></td>
											<td class="hidden-480"><a href="<?php echo base_url();?>General_setting/edit_belt/<?php echo $row->id; ?>"><i class="icon-edit"></i></a> | <a onclick="confirm('Are You Sure Delete?');" href="<?php echo base_url();?>General_setting/delete_belt/<?php echo $row->id; ?>" ><i class="icon-trash"></i></a></td>
										</tr>
										<?php endforeach; endif;?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<?php include('includes/footer.php');?>
    