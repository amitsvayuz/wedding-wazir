<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
	<?php include_once('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Photography
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url();?>decorator/list_decorator_images">photography</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN GALLERY MANAGER PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-reorder"></i>Photography</h4>
							</div>
							
							<div class="portlet-body">
								
								<a href="<?php echo base_url();?>decorator/image_upload_decorator" class="btn pull-right green"><i class="icon-plus"></i> Upload</a>
							<?php foreach($decorator_details as $details) { ?>
								<div class="row-fluid">
									<div class="span12">
									 <div class="portlet-title">
										<h4>
										   <i class="icon-reorder"></i>
										   <span class="hidden-480"> Album :<?php echo ucfirst($details->title);?></span>
										   &nbsp;
										</h4>
									 </div>
										
									</div>
									<?php 
									$image = array();
									$image = explode(',',$details->images);
									//print_r($image);?>
								</div>
								<!-- END GALLERY MANAGER PANEL-->
								<hr class="clearfix" />
								<!-- BEGIN GALLERY MANAGER LISTING-->
								
								<div class="space10"></div>
								<div class="row-fluid">
							<?php for($i=0;$i<sizeof($image);$i++) { ?>
	<div class="span3">
									
		<div class="item">
<a class="fancybox-button" data-rel="fancybox-button" title="Metronic Tablet Preview" href="<?php echo base_url();?>assets/uploaddecorator/photography/<?php echo $image[$i];?>">
	<div class="zoom">
<img src="<?php echo base_url();?>assets/uploaddecorator/photography/<?php echo $image[$i];?>" alt="Photo" />							
	<div class="zoom-icon"></div>
	</div>
</a>
<div class="details">
	<a href="#" class="icon"><i class="icon-paper-clip"></i></a>
	<a href="#" class="icon"><i class="icon-link"></i></a>
	<a href="#" class="icon"><i class="icon-pencil"></i></a>
	<a href="#" class="icon"><i class="icon-remove"></i></a>		
</div>
</div>
</div>
									<?php } ?>
								<!-- END GALLERY MANAGER LISTING-->
								</div>
								<?php } ?>
							</div>
							
						<!-- END GALLERY MANAGER PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->			
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
<?php include('includes/footer.php');?>