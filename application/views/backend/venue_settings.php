<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->   	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Add Venue Settings
							<small>Manage Venue Setting</small>
							
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a>  
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="<?php echo base_url();?>Venue_setting_cntlr/venue_setting_home1">Venue Settings</a></li>
							<i class="icon-angle-right"></i>
							<li><a href="<?php echo base_url();?>Venue_setting_cntlr/add_venue">Add Venue Settings</a>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				
				<div class="span12">
										<!--BEGIN TABS-->
										<div class="tabbable tabbable-custom">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab_1_1" data-toggle="tab">Wedding occasion list</a></li>
												<li><a href="#tab_1_2" data-toggle="tab">Non-wedding occasion list</a></li>
												<li><a href="#tab_1_3" data-toggle="tab">Nature of venue</a></li>
												<li><a href="#tab_1_4" data-toggle="tab">Venue type</a></li>
												<li><a href="#tab_1_5" data-toggle="tab">Venue type other</a></li>
												<li><a href="#tab_1_6" data-toggle="tab">Floor</a></li>
												<li><a href="#tab_1_7" data-toggle="tab">Suite type</a></li>
												<li><a href="#tab_1_8" data-toggle="tab">Valet facility</a></li>
												<li><a href="#tab_1_9" data-toggle="tab">Seating type</a></li>
												<li><a href="#tab_1_10" data-toggle="tab">Other services</a></li>
											</ul>
											<div class="tab-content">
											       <!-- tab1-->  
												   
												   
												<div class="tab-pane active" id="tab_1_1">
													  <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
				  <?php  if(@$_REQUEST['set']):?>
								   <div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo base64_decode(@$_REQUEST['set']);?>
								    </div>
									<?php endif;?>
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Wedding occasion list</h4>
                        
                     </div>
					 
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Venue_setting_cntlr/insert_venue/1" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee" />
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
						   
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												
										       <!-- tab 2-->  		
												<div class="tab-pane" id="tab_1_2">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Non-wedding occasion list</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Venue_setting_cntlr/insert_venue/2" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee" />
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
						   
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												       <!-- tab3-->  
												
												<div class="tab-pane" id="tab_1_3">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Nature of venue</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Venue_setting_cntlr/insert_venue/3" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
						   
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												
												
												<!-- tab4-->
		<div class="tab-pane" id="tab_1_4">
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Venue type</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Venue_setting_cntlr/insert_venue/4" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												<!-- tab5 -->
												<div class="tab-pane" id="tab_1_5">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Venue type other</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Venue_setting_cntlr/insert_venue/5" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												
												<!-- tab 6 -->
												<div class="tab-pane" id="tab_1_6">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Floor</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Venue_setting_cntlr/insert_venue/6" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												<!--tab7-->
												<div class="tab-pane" id="tab_1_7">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Suite type</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Venue_setting_cntlr/insert_venue/7" method="post" class="form-horizontal">
<div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												
												<!--tab8-->
												<div class="tab-pane" id="tab_1_8">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Valet facility</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Venue_setting_cntlr/insert_venue/8" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												<!--tab9-->
												<div class="tab-pane" id="tab_1_9">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Seating type</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Venue_setting_cntlr/insert_venue/9" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												<!--tab10-->
												<div class="tab-pane" id="tab_1_10">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Other services</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Venue_setting_cntlr/insert_venue/10" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee" />
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												
											</div>
										</div>
										<!--END TABS-->
									</div>
				
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<?php include_once('includes/footer.php');?>
