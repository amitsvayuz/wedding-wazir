<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
	<?php include_once('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
								
						<h3 class="page-title">
							videos
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url();?>photographer/list_photographer_videos">videos</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN GALLERY MANAGER PORTLET-->
					
						<div class="portlet box light-grey">
						
						
							<div class="portlet-title">
								<h4><i class="icon-reorder"></i>videos</h4>
							</div>
							
							
							
							<div class="portlet-body">
							<!-- BEGIN GALLERY MANAGER PANEL-->
							<a href="<?php echo base_url();?>photographer/image_upload_photographer" class="btn pull-right green"><i class="icon-plus"></i> Upload</a>
								<?php foreach($photographer_details as $details) { ?>
							<div class="row-fluid">
								<div class="span4">
									<h4><?php echo $details->title;?>
									</h4>
								</div>
							</div>
								<!-- END GALLERY MANAGER PANEL-->
							<hr class="clearfix" />
							<!-- BEGIN GALLERY MANAGER LISTING-->
							<div class="space10"></div>
							<div class="row-fluid">
							<div class="span3">		
							<div class="item">		 
							 <video width="150" height="150" controls>
							<source src="<?php echo base_url();?>assets/uploadsvideos/videography/<?php echo $details->videos;?>" type="video/mp4">
							</video> 
							</div>
							</div>
							<!-- END GALLERY MANAGER LISTING-->
							</div>
								<?php } ?>
							</div>
							
						<!-- END GALLERY MANAGER PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->			
		</div>
		<!-- END PAGE -->	 	
	</div>
	</div>
	<!-- END CONTAINER -->
<?php include('includes/footer.php');?>