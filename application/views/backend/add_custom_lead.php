<?php include_once('includes/header.php');?>
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
    <?php include_once('includes/sidebar.php');?>
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <!--<div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>-->
                  <!-- END BEGIN STYLE CUSTOMIZER -->   
                  <h3 class="page-title">
                      Lead Module
                   </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="<?php echo base_url();?>lead">Leads</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="javascript:void">Add leads</a>
                        <span class="icon-angle-right"></span>
                     </li>
                    
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i> Lead Module</h4>
                      
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form id="register-form" action="<?php echo base_url();?>lead/create_lead" method="post" class="form-horizontal">
						
						    <div class="control-group">
                              <label class="control-label"> Type :</label>
                              <div class="controls">
                                 <select class="span4 chosen" data-placeholder="Choose a Category" required id="type" name="type" tabindex="1">
									  <option value="none">Select Type</option>
									 <option  value="venue">Venue</option>
									 <option  value="planner">Wedding</option>
									 <option value="decorator">Decorater</option>
									 <option   value="photographer">Photographer</option>
                                 </select>
				            </div>
							<h6 style="color:#D51D59"><?php echo form_error('type'); ?></h6>
                           </div>
						   
						  
						   
                           <div class="control-group" id="comp">
                              <label class="control-label">Customer Name :</label>
                              <div class="controls">
                                 <input type="text"  placeholder="Customer Name" name="customer_name" required class="span4 m-wrap" />
								 <h6 style="color:#D51D59"><?php echo form_error('customer_name'); ?></h6>
                              </div>
                           </div>
                        
						  <div class="control-group">
                              <label class="control-label">Email  :</label>
                              <div class="controls">
                                    <input class="span4 m-wrap" id="email" type="email" placeholder="Email" required name="email"/>   
									<h6 style="color:#D51D59"><?php echo form_error('email'); ?></h6>									
                              </div>
                           </div>
						  
                    						   
                           <div class="control-group">
                              <label class="control-label">Contact :</label>
                              <div class="controls">
                                 <input class="span4 m-wrap" type="text" id="Contact" placeholder="+91" required name="contact_no"/>
                                 <h6 style="color:#D51D59"><?php echo form_error('contact_no'); ?></h6>
                              </div>
                           </div>
                           
						     <div class="control-group">
                              <label class="control-label">Budget :</label>
                              <div class="controls">
                                 <input class="span4 m-wrap" type="text" id="budget" required name="budget"/>
                                 <h6 style="color:#D51D59"><?php echo form_error('budget'); ?></h6>
                              </div>
                           </div>
                           
                            <div class="control-group">
                              <label class="control-label">Event :</label>
                              <div class="controls">
                                 <input class="span4 m-wrap" type="text" id="event" required name="event"/>
                                 <h6 style="color:#D51D59"><?php echo form_error('event'); ?></h6>
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label">Description :</label>
                              <div class="controls">
                                 <textarea class="span4 m-wrap" style="resize:none;" id="description"  name="description"/></textarea>
                                 <h6 style="color:#D51D59"><?php echo form_error('description'); ?></h6>
                              </div>
                           </div> 
						  
                           <div class="form-actions">
                              <button type="submit" id="submit" class="btn blue">Submit</button>
                              
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
  
<?php include_once('includes/footer.php');?>
  <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

   <script>
   // When the browser is ready...
  $(function() {
  
    // Setup form validation on the #register-form element
    $("#register-form").validate({
    
        // Specify the validation rules
        rules: {
            companyname: "required",
            username: "required",
		    email: {
                required: true,
                email: true
            },
            contact_no: {
                required: true,
                minlength: 10,
				number: true,
			    maxlength: 10,
            },
		},
        
        // Specify the validation error messages
        messages: {
            companyname: "Please enter your first name",
            username: "Please enter your user name",
            password: {
				 number: "Please enter only digits only",
                required: "Please provide a contact number",
                minlength: "Your contact number must be at least 10 digits long"
            },
            email: "Invalid email address",
		    
        },
       
    });
    
  });
  
  </script>
 
  <script>
    jQuery(document).ready(function() {     
      $('#register-back-btn').click(function(){
		  window.location='<?php echo base_url();?>';
	  });
	  $('#option1').click(function(){
		 $('#comp').show();
	  });
	  $('#option2').click(function(){
		 $('#comp').hide();
	  });
    });
  </script>

	
	 <script>
    $(document).ready(function() { 
		$("#email").keyup(function(){	
		var email = $("#email").val();
		var dataString = 'email=' + email;
		var base_url = "<?php echo base_url();?>";
		
			$.ajax({
				type: 'POST',
				url: base_url + 'admin/email_exists',
				data: dataString,
				success: function(result){
				   if(result == 1){
					   alert('Email Id already exists');
				   }
				}});
			return false;
		 });
    });
  </script>