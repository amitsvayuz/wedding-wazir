<?php include('includes/header.php');?>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Coupons And Deals				
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>admin/cupon">Coupons And Deals</a>
								
							</li>
							 
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>Coupons And Deals Listing</h4>
								<!--<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									
									<a href="javascript:;" class="reload"></a>
									
								</div>-->
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group">
										<a href="<?php echo base_url();?>admin/add_cupon" id="sample_editable_1_new" class="btn green">
										Add New <i class="icon-plus"></i>
										</a>
									</div>
									
								</div>
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>S.N.</th>
											<th class="hidden-480">Promo Code</th>
											<th class="hidden-480">Discount Type</th>
											<th class="hidden-480">Discount Amount</th>
											<th  class="hidden-480" >Remarks</th>
											<th  class="hidden-480" >Start Date</th>
											<th  class="hidden-480" >End Date</th>
											<th  class="hidden-480" >Status</th>
											<th  class="hidden-480" >Action</th>
										</tr>
									</thead>
									
									<tbody>
									<?php if(!empty($result))
									{
										$i=1;										
										foreach($result as $row)
										{
											
										$id=base64_encode($row->id);
										$sdate= $row->start_date;
										$edate= $row->end_date;
										//echo $id.'abc';
										?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
										<td class="hidden-480"><?php echo $i;?></td>
											<td><?php echo ucfirst($row->promocode);?></td>
											<td class="hidden-480"><?php echo ucfirst($row->discount_type);?></td>
											<td class="hidden-480"><?php echo $row->discount_amount;?></td>
											<td class="hidden-480"><?php echo $row->remark;?></td>
											<td class="hidden-480"><?php echo date('d/M/Y',$sdate);?></td>
											<td class="hidden-480"><?php echo date('d/M/Y',$edate);?></td>
											
											
											
											<td >
											<?php if($row->status == 1){?>
  <a href="#" onClick="var a=confirm('Are you sure to Deactive this!');if(a){ deactive('<?php echo $row->id; ?>');}else{return false;}" title="Deactive">
                      <span class="label label-sm label-success">Active</span></a>&nbsp;
                      
                      <?php } else if($row->status == 0){?>
                       <a href="#" onClick="var a=confirm('Are you sure to Active this!');if(a){ active('<?php echo $row->id;?>');}else{return false;}" title="active">
                      <span class="label label-sm label-warning">Deactive</span></a>&nbsp;
                      <?php } ?>
											</td>
											
											<td><a href="<?php echo base_url()?>admin/edit_cupon?id=<?php echo $id;?>" ><i class="icon-edit"></i></a> | <a onClick="return confirm('Are you sure you want to delete?');" id="delete_cupon" href="<?php echo base_url()?>admin/delete_cupon?del=<?php echo $id;?>" ><i class="icon-trash"></i></a></td>
										</tr>
									<?php $i=$i+1; } } ?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
			
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
		
	<script type="text/javascript">
 function active(x)
 {
	var id11=x;
	//alert(id11);
	$.ajax({
		type:"POST",     
        url:'<?PHP echo base_url();?>admin/active_cupon',
        data:{"id":id11},  
        success: function(data){
			
			location.reload();
			}
      });
 }
function deactive(r)
 {
	//alert('sni');
	var id11=r;
	//alert(id11);
	 $.ajax({
		type:"POST",     
        url:'<?PHP echo base_url();?>admin/deactive_cupon',
        data:{"id":id11}, 
        success: function(data){
			
			location.reload();
		}
      });
 }
</script>
	<?php include('includes/footer.php');?>
	<!-- END CONTAINER -->

