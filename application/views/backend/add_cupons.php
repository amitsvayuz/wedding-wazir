<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->   	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Add Coupons 
                           <small>Manage Add Coupons</small>							
							
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url()?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url()?>admin/cupon">Cupons And Deals</a><i class="icon-angle-right"></i></li>
							<li><a href="<?php echo base_url()?>admin/add_cupon">Add Coupons </a></li>
						
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				
				
				 <?php 
                           
                                
									if($title=="Edit Cupons"){
										
										
										
										$promocode=$result[0]->promocode;
										$type=$result[0]->discount_type;
										$amount=$result[0]->discount_amount;
										$sdate=$result[0]->start_date;
										$edate=$result[0]->end_date;
										$remark=$result[0]->remark;
										
										
										
										
										$uri=base_url().'admin/edit_cupon_details?id='.base64_encode($result[0]->id);
										
									$butoon="Update";
									}
									else {
										
										$promocode=null;
										$type=null;
										$amount=null;
										$sdate=null;
										$edate=null;
										$remark='';
										
										$butoon="Save";
										$uri=base_url().'admin/add_cupon_details';
									}
									?>
				<div class="portlet-body form">
                                 <!-- BEGIN FORM-->
								 
								   
								   
									
                                 <form action="<?php echo $uri ?>" class="horizontal-form" method="post">

                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                            <label class="control-label" for="promocode">Pomo Code</label>
                                             <div class="controls">
                                                <input type="text"  id="promocode" class="m-wrap span12" placeholder="Promo Code" value="<?php echo $promocode ?>" name="promocode">
                                                
                                             </div>
                                          </div>
                                       </div>
									   
									   <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label" for="amount">Discount Type</label>
                                             <div class="controls">
                                                <select class="span12 m-wrap" name="type">
                                    <option value="">Select...</option>
                            
                                    <option <?php if($type=="flat"){echo "selected";}?> value="flat">Flat</option>
                                    <option <?php if($type=="percent"){echo "selected";}?>  value="percent">Percent %</option>
                                 </select>
                                                
                                             </div>
                                          </div>
                                       </div>
                                      
                                    </div>
									<!--/row-->
									 <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label" for="amount">Discount Amount</label>
                                             <div class="controls">
                                                <input type="text"  id="amount" class="m-wrap span12" placeholder="Discount Amount" value="<?php echo $amount; ?>" name="amount">
                                                
                                             </div>
                                          </div>
                                       </div>
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label" for="amount">Remark</label>
                                             <div class="controls">
                                               <textarea style="resize:none;" name="remark" class="m-wrap span12"><?php echo $remark;?></textarea>
                                                
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid">
                                       <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label" for="amount">Start Date</label>
                                             <div class="controls">
                                                <input type="text"  id="dob" class="m-wrap span12" placeholder="Start Date" value="<?php echo date('Y-m-d',$sdate); ?>" name="sdate">
                                                
                                             </div>
                                          </div>
                                       </div>
                                      
									  <div class="span6 ">
                                          <div class="control-group">
                                             <label class="control-label" for="amount">End Date</label>
                                             <div class="controls">
                                                <input type="text"  id="date" class="m-wrap span12" placeholder="End Date" value="<?php echo date('Y-m-d',$enddate); ?>" name="edate">
                                                
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" class="btn blue"><i class="icon-ok"></i><?php echo $butoon ?></button>
                                       <button type="button" class="btn">Cancel</button>
                                    </div>
                                 </form>
                                 <!-- END FORM--> 
									
                              </div>
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<?php include_once('includes/footer.php');?>