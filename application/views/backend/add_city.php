<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->   	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Add City
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url()?>sp_manager/dashboard">Dashboard</a>
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="<?php echo base_url()?>General_setting/list_city">City</a></li>
							<i class="icon-angle-right"></i>
							<li><a href="<?php echo base_url()?>General_setting/add_city">Add City</a>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				
				
				
		
		<!-- END PAGE -->
	
	<!-- END CONTAINER -->
	<div class="tab-pane" id="tab_1_4">
			<div class="row-fluid">
               <div class="span12">
			   <?php 
			                        if(isset($result))
									{
										foreach($result as $row):
										$name=$row->name; 
										$descrip=$row->description;
										endforeach;
									    $uri=base_url().'General_setting/city_update/'.$row->id;
									    $butoon="Update";
									}
									else 
									{
										$name=null;
										$descrip=null;
										$butoon="Add City";
										$uri=base_url().'General_setting/adding_city';
									}
									?>
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Add City</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo $uri; ?>" method="post" class="form-horizontal">
                         
										
						   
						   <div class="control-group">
                              <label class="control-label">Name</label>
                               <div class="controls">
                                 <input type="text" class="span6 m-wrap" placeholder="City Name" name="cityName" value="<?php echo $name;?>"/>
                                
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" placeholder="City Description" name="cityDesc"><?php echo $descrip;?></textarea>
                              </div>
                           </div>
                           <div class="form-actions">
                              <button type="submit" class="btn blue"><?php echo $butoon;?></button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
			</div>
												</div>
	<?php include_once('includes/footer.php');?>