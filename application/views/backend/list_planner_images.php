
<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
	<?php include_once('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Album	
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url();?>planner/list_planner_images">Album</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN GALLERY MANAGER PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-reorder"></i>Portfolio</h4>
							</div>
							
							<div class="portlet-body">
								<!-- BEGIN GALLERY MANAGER PANEL-->
									<?php if($this->session->userdata('role')=="venue"){?>
								<a href="<?php echo base_url();?>planner/add_portfolio/<?php echo $venue_id;?>" class="btn pull-right green"><i class="icon-plus"></i> Add</a>
								<?php } else{?>
								<a href="<?php echo base_url();?>planner/add_portfolio" class="btn pull-right green"><i class="icon-plus"></i> Add</a>							
								<?php }?>								
							<br><br>
																
								
								
								<div class="row-fluid">
								
			<?php 
		if(!empty($album_details)){			
 foreach($album_details as $details) { 
					
				?>				
	<div  class="span3">
	
									
		<div>
		<a href="#myModal1" role="button" class="album" id="<?php echo $details->album_id; ?>" data-toggle="modal"><img src="<?php echo base_url()?>/assets/img/folder.png"></a>
			<div class="portlet-title">
	 <span style="font-size:16px;" class="hidden-480"><a href="#myModal1" role="button" class="album" id="<?php echo $details->album_id; ?>" data-toggle="modal">  <?php echo ucfirst($details->album_title);?></a><a style="float:right;" title="Edit Album" type="button" href="<?php echo base_url();?>/planner/edit_portfolio/<?php echo $details->album_id;?>"><i class="icon-edit"></i></a></span>
		</div>
</div>
</div>
<?php }}?>
								
								<!-- END GALLERY MANAGER LISTING-->
								</div>
								
							</div>
							
						<!-- END GALLERY MANAGER PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->			
		</div>
		<!-- END PAGE -->	 	
	</div>
	<div id="myModal1" style="width:90%;margin-left:-590px !important;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel1">Album</h3>
										<span id="flashmsg"></span>

									</div>
									<div id="albumdiv" class="modal-body">
									
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
									
									</div>
								</div>
	<!-- END CONTAINER -->
	<script type="text/javascript">
$(function(){
$('.album').click(function(){
//	alert("/");
//alert($(this).attr('id'));
var id = $(this).attr('id');
 $.ajax({type:'post',
		    url:'<?PHP echo base_url()?>planner/select_album',
		    data:{id:id},
		    success:function(res){
			                    // alert(res);
                                  $('#albumdiv').html(res);
								 }
	      });

 });
});

$(document).click(function(){
  $('.save1').click(function(){
  	 var res=confirm("Are you sure");
    var aname = $(this).attr('id');
    var id =    $(this).attr('rel');
    if(res)
    {
    $.ajax({
		type:"POST",     
        url:'<?PHP echo base_url();?>planner/delete_pic',
        data:{id:id,name:aname},  
        success: function(data){
			if(data==1){
					$('#flashmsg').html("Image Deleted").show();		
			}
			else {
				$('#flashmsg').html("Error Occured").show();
				}
			}
			//location.reload();
			
      });
      }
  });
});
/*
function deleted()
 {
 	//alert(id+nameid);
 	var res=confirm("Are you sure");
 	var id = $(this).attr("rel");
 	var aname = $(this).attr("id");
	//var id11=x;
	//var id12=y;	
	alert(id);
	alert(aname);
	$.ajax({
		type:"POST",     
        url:'<?PHP echo base_url();?>planner/delete_pic',
        data:{id:id,name:aname},  
        success: function(data){
			if(data==1){
					$('#flashmsg').html("Image Deleted");		
			}
			else {
				$('#flashmsg').html("Error Occured");
				}
			}
			//location.reload();
			
      });
 }

*/
</script>
<?php include('includes/footer.php');?>