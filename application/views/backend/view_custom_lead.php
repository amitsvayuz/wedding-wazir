	
	<!-- BEGIN CONTAINER -->	
	<?php include_once('includes/header.php');?>
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
    <?php include_once('includes/sidebar.php');?>
	
		<!-- BEGIN SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>
						<!-- END BEGIN STYLE CUSTOMIZER --> 
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Lead				<small>Lead management</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>lead">Leads</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="javascript:void">Lead details</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid invoice">
					<div class="row-fluid invoice-logo">
						<div class="span6 invoice-logo-space"><img src="<?php echo base_url();?>assets/img/logo.png" alt="" /> </div>
						<div class="span6">
							<p><?php echo $leads_results->lead_id;?><span class="muted">Lead ID</span></p>
						</div>
					</div>
					<hr />
					<div class="row-fluid">
						<div class="span4">
							<h4>Lead Details</h4>
							<ul class="unstyled">
								<li><b>Lead type</b>&nbsp;:&nbsp; <?php echo ucfirst($leads_results->Type); ?></li>								
								<li><b>Customer name</b>&nbsp;:&nbsp; <?php echo ucfirst($leads_results->Name); ?></li>
								<li><b>Customer email</b>&nbsp;:&nbsp; <?php echo ucfirst($leads_results->Email_Id); ?></li>
								<li><b>Customer Contact</b>&nbsp;:&nbsp; <?php echo ucfirst($leads_results->Contact_Num); ?></li>
								<li><b>Budget</b>&nbsp;:&nbsp; <?php echo ucfirst($leads_results->Budget); ?></li>
								<li><b>Event</b>&nbsp;:&nbsp; <?php echo ucfirst($leads_results->event); ?></li>								
								<li><b>Description</b>&nbsp;:&nbsp; <?php echo ucfirst($leads_results->description); ?></li>						
							</ul>
						</div>
						<div class="span4">
							
						</div>
						<div class="span4 invoice-payment">
							
						</div>
					</div>
					<?php if($this->session->userdata('role')=='admin'){?>
					<div class="row-fluid">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>#</th>
									
									<th class="hidden-480">Short listed</th>
									<th class="hidden-480">Response</th>
									
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($leads_status)){
								$i=1;
								foreach($leads_status as $lead){
											
									foreach($user_data as $user){
									if($lead->sp_id == $user->id){
									$sp=$user->user_name;									
									}									
									}	
									/***********Check the status of lead********/	
									if($lead->status==1){
										$status='Accepted';									
									}				
									if($lead->status==0) {
										$status='Rejected';									
									}
									if($lead->status==Null) {
										$status='Pending';									
									}
								?>
								<tr>
									<td><?php echo $i;?></td>
									<td><?php echo ucfirst($sp);?></td>
									<td class="hidden-480"><?php echo $status;?></td>
									
								</tr>
								<?php $i++;}}?>
														</tbody>
						</table>
					</div><?php }?>
					<?php if($this->session->userdata('role')=='admin'){?>					
					<div class="row-fluid">
						<div class="span4">
							
						</div>
						<div class="span8 invoice-block">
							
							<!--<a href="<?php echo base_url();?>lead/share_lead/<?php echo $leads_results->lead_id;?>/<?php echo $leads_results->Type;?>" class="btn green big">Share Lead<i class="m-icon-big-swapright m-icon-white"></i></a>-->
							<a href="#myModal3" role="button" class="btn green big" data-toggle="modal">Share Lead<i class="m-icon-big-swapright m-icon-white"></i></a>						
						</div>
					</div><?php }?>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel3">Share lead</h3>
									</div>
									<div class="modal-body">
										<form id="register-form" action="<?php echo base_url();?>lead/share_lead" method="post" class="form-horizontal">
						 <div class="control-group">
                              <label class="control-label">Service Provider :</label>
                              <div class="controls">
		 <select class="span6 m-wrap" multiple="multiple" data-placeholder="Choose a Category" name="service_provider[]" tabindex="1">
			<?php foreach($leads_provider as $provider) { ?>
<option  value="<?php echo $provider->id;?>"><?php echo $provider->user_name;?></option>
			  <?php } ?>
		 </select>
		 <input type="hidden" name="hidden_id" value="<?php echo $id;?>" id="hidden_id"/>
                              </div>
                           </div>
						   
                                                   
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
										<button  type="submit" id="submit"  class="btn blue">Confirm</button>
									</div>
									</form>
								</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
			<?php include_once('includes/footer.php');?>