
<?php include('includes/header.php');?>


	<div class="page-container row-fluid">
		<?php include('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
						
						<h3 class="page-title">
							User Profile				<small>user profile sample</small>
						</h3>
						 <?php if(!empty($this->session->userdata('message'))){?>
						  <div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
						   <span style='color:green;'><?php echo $this->session->userdata('message');?></span>
						  </div>
						<?php } ?>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="#">Extra</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#">User Profile</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid profile">
					<div class="span12">
						<!--BEGIN TABS-->
						<div class="tabbable tabbable-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab_1_1" data-toggle="tab">Overview</a></li>
								<li><a href="#tab_1_2" data-toggle="tab">Profile Info</a></li>
								<li><a href="#tab_1_3" data-toggle="tab">Account</a></li>
								<li><a href="#tab_1_5" data-toggle="tab">Business Manager</a></li>
								
							</ul>
							<div class="tab-content">
								<div class="tab-pane row-fluid active" id="tab_1_1">
									<ul class="unstyled profile-nav span3">
										<li><img src="<?php echo base_url()?>uploads/<?php echo $result[0]->id;?>/<?php echo $result[0]->pro_pic;?>" alt="" /> <a href="#" class="profile-edit">edit</a></li>
									</ul>
									<div class="span9">
										<div class="row-fluid">
											<div class="span8 profile-info">
												<h1><?php echo $result[0]->company_name;?></h1>
												<p><?php echo $result[0]->company_description;?></p>
												<p></a></p>
												<ul class="unstyled inline">
				<li><b><i class="icon-briefcase"></i> <?php echo ucfirst($result[0]->country);?> , <?php echo ucfirst($result[0]->state);?> , <?php echo ucfirst($result[0]->city);?> </li></b>
					<li><i class="icon-map-marker"></i> <b> <?php echo date('Y-m-d',$result[0]->d_o_b);?></b></li>
					<li><i class="icon-envelope-alt"></i> <b><?php echo $result[0]->user_email;?></b></li>
					<li><i class="icon-phone"></i> <b><?php echo $result[0]->contact_no;?></b></li>
					
					<li><i class="icon-facebook"></i> </li>
					<li><i class="icon-twitter"></i> </li>
					<li><i class="icon-google-plus"></i> </li>
												</ul>
											</div>
											
										</div>
										<!--end row-fluid-->
										<div class="tabbable tabbable-custom tabbable-custom-profile">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab_1_11" data-toggle="tab">Leads Accepted</a></li>
												<li class=""><a href="#tab_1_22" data-toggle="tab">Requested Leads</a></li>
											</ul>
											<div class="tab-content">
												<div class="tab-pane active" id="tab_1_11">
													<div class="portlet-body" style="display: block;">
														<table class="table table-striped table-bordered table-advance table-hover">
															<thead>
																<tr>
																	<th><i class="icon-briefcase"></i> Company</th>
																	<th class="hidden-phone"><i class="icon-question-sign"></i> Descrition</th>
																	<th><i class="icon-bookmark"></i> Amount</th>
																	<th></th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td><a href="#">Pixel Ltd</a></td>
																	<td class="hidden-phone">Server hardware purchase</td>
																	<td>52560.10$ <span class="label label-success label-mini">Paid</span></td>
																	<td><a class="btn mini green-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td>
																		<a href="#">
																		Smart House
																		</a>	
																	</td>
																	<td class="hidden-phone">Office furniture purchase</td>
																	<td>5760.00$ <span class="label label-warning label-mini">Pending</span></td>
																	<td><a class="btn mini blue-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td>
																		<a href="#">
																		FoodMaster Ltd
																		</a>
																	</td>
																	<td class="hidden-phone">Company Anual Dinner Catering</td>
																	<td>12400.00$ <span class="label label-success label-mini">Paid</span></td>
																	<td><a class="btn mini blue-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td>
																		<a href="#">
																		WaterPure Ltd
																		</a>
																	</td>
																	<td class="hidden-phone">Payment for Jan 2013</td>
																	<td>610.50$ <span class="label label-danger label-mini">Overdue</span></td>
																	<td><a class="btn mini red-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td><a href="#">Pixel Ltd</a></td>
																	<td class="hidden-phone">Server hardware purchase</td>
																	<td>52560.10$ <span class="label label-success label-mini">Paid</span></td>
																	<td><a class="btn mini green-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td>
																		<a href="#">
																		Smart House
																		</a>	
																	</td>
																	<td class="hidden-phone">Office furniture purchase</td>
																	<td>5760.00$ <span class="label label-warning label-mini">Pending</span></td>
																	<td><a class="btn mini blue-stripe" href="#">View</a></td>
																</tr>
																<tr>
																	<td>
																		<a href="#">
																		FoodMaster Ltd
																		</a>
																	</td>
																	<td class="hidden-phone">Company Anual Dinner Catering</td>
																	<td>12400.00$ <span class="label label-success label-mini">Paid</span></td>
																	<td><a class="btn mini blue-stripe" href="#">View</a></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
												<!--tab-pane-->
												<div class="tab-pane" id="tab_1_22">
													<div class="tab-pane active" id="tab_1_1_1">
														<div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
															<ul class="feeds">
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-success">								
																					<i class="icon-bell"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					You have 4 pending tasks.
																					<span class="label label-important label-mini">
																					Take action 
																					<i class="icon-share-alt"></i>
																					</span>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			Just now
																		</div>
																	</div>
																</li>
																<li>
																	<a href="#">
																		<div class="col1">
																			<div class="cont">
																				<div class="cont-col1">
																					<div class="label label-success">								
																						<i class="icon-bell"></i>
																					</div>
																				</div>
																				<div class="cont-col2">
																					<div class="desc">
																						New version v1.4 just lunched!	
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="col2">
																			<div class="date">
																				20 mins
																			</div>
																		</div>
																	</a>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-important">								
																					<i class="icon-bolt"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					Database server #12 overloaded. Please fix the issue.								
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			24 mins
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			30 mins
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-success">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			40 mins
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-warning">								
																					<i class="icon-plus"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New user registered.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			1.5 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-success">								
																					<i class="icon-bell-alt"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					Web server hardware needs to be upgraded.	
																					<span class="label label-inverse label-mini">Overdue</span>					
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			2 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			3 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-warning">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			5 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			18 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			21 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			22 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			21 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			22 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			21 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			22 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			21 hours
																		</div>
																	</div>
																</li>
																<li>
																	<div class="col1">
																		<div class="cont">
																			<div class="cont-col1">
																				<div class="label label-info">								
																					<i class="icon-bullhorn"></i>
																				</div>
																			</div>
																			<div class="cont-col2">
																				<div class="desc">
																					New order received. Please take care of it.						
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col2">
																		<div class="date">
																			22 hours
																		</div>
																	</div>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<!--tab-pane-->
											</div>
										</div>
									</div>
									<!--end span9-->
								</div>
								<!--end tab-pane-->
								
								<div class="tab-pane profile-classic row-fluid" id="tab_1_2">
									<div class="span2"><img src="<?php echo base_url()?>uploads/<?php echo $result[0]->id;?>/<?php echo $result[0]->pro_pic;?>" alt="" /> <a href="<?php echo base_url();?>sp_manager/profile?id=<?php echo base64_encode($result[0]->id);?>" class="profile-edit">edit</a></div>
									<ul class="unstyled span10">
									<?php if(!empty($result[0]->user_name)){?>
										<li><span>User Name:</span> <?php echo ucfirst($result[0]->user_name);?></li>
										<?php } ?>
										<?php if(!empty($result[0]->country)){?>
										<li><span>Country:</span> <?php echo ucfirst($result[0]->country);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->state)){?>
										<li><span>State:</span> <?php echo ucfirst($result[0]->state);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->city)){?>
										<li><span>City:</span> <?php echo ucfirst($result[0]->city);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->street)){?>
										<li><span>Street:</span> <?php echo ucfirst($result[0]->street);?> </li>
										<?php } ?>
										<?php if(!empty($result[0]->d_o_b)){?>
										<li><span>Birthday:</span> <?php echo date('Y-m-d',$result[0]->d_o_b);?></li>
										<?php } ?>
										<?php if(!empty($result[0]->role_type)){?>
										<li><span>Occupation:</span> <?php echo ucfirst($result[0]->role_type);?></li>
										<?php } ?>
										<li><span>Email:</span> <a href="#"><?php echo $result[0]->user_email;?></a></li>
										<li><span>Mobile Number:</span><?php echo $result[0]->contact_no;?></li>
									</ul>
								</div>
								<!--tab_1_2-->
								<div class="tab-pane profile-classic row-fluid" id="tab_1_5">
				<div class="span2"><img src="<?php echo base_url()?>uploads/<?php echo $result[0]->id;?>/<?php echo $result[0]->pro_pic;?>" alt="" /> <a href="<?php echo base_url();?>sp_manager/profile?id=<?php echo base64_encode($result[0]->id);?>" class="profile-edit">edit</a></div>
					 <div class="tab-content span10">
		 <form  method="post" action="<?php echo base_url();?>photographer/admin_edit_photographer_services" class="form-horizontal">			 
	   <?php $events = explode(',' , $business_result[0]->events);
	  	 ?>	  
				 <div class="control-group">
                              <label class="control-label">Events  :</label>
                              <div class="controls">
    <select data-placeholder="Your Favorite Football Teams" name="events[]" class="chosen" style="width:300px;" multiple="multiple" tabindex="6">
         <?php foreach($events_details as $details) { ?>
<option  <?php if(in_array($details->description ,$events))echo 'selected';?> value="<?php echo $details->description;?>"> <?php echo $details->name;?></option>
				<?php } ?>
                                    
                                 </select>
                              </div>
                           </div>	 
						   
		<?php $services = explode(',' , $business_result[0]->services);
		?>			
									
									
	<div class="control-group">
                                       <label class="control-label">Services :</label>
                                       <div class="controls">
										   <label class="checkbox">
 <input  <?php if(in_array('Photography' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Photography" />Photography
                                          </label>
										   <label class="checkbox">
 <input  <?php if(in_array('Videography' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Videography" />Videography
                                          </label>
										   <label class="checkbox">
 <input  <?php if(in_array('Cinematography' ,$services))echo 'checked';?> type="checkbox" name="services[]" value="Cinematography" />Cinematography
                                          </label>
                                       </div>
                                    </div>	
									
	<?php $style_of_photography = explode(',' , $business_result[0]->style_of_photography);
			?>	

	<div class="control-group">
                                       <label class="control-label">Style of Photography :</label>
                                       <div class="controls">
										   <label class="checkbox">
<input type="checkbox"  <?php if(in_array('Candid' ,$style_of_photography))echo 'checked';?>   name="style_of_photography[]" value="Candid" />Candid
                                          </label>
										   <label class="checkbox">
<input type="checkbox"  <?php if(in_array('Traditional' ,$style_of_photography))echo 'checked';?>  name="style_of_photography[]" value="Traditional" />Traditional
                                          </label>
                                       </div>
                                    </div>	

<?php $sharing_option = explode(',' , $business_result[0]->sharing_option);
			?>		
									
	<div class="control-group">
                                       <label class="control-label">Sharing Option :</label>
                                       <div class="controls">
										   <label class="checkbox">
  <input <?php if(in_array('CD' ,$sharing_option))echo 'checked';?>  type="checkbox" name="sharing_option[]" value="CD" />CD/ DVD
                                          </label>
										   <label class="checkbox">
  <input <?php if(in_array('Pen Drive' ,$sharing_option))echo 'checked';?>  type="checkbox" name="sharing_option[]" value="Pen Drive" />Pen Drive
                                          </label>
										   <label class="checkbox">
  <input <?php if(in_array('Album' ,$sharing_option))echo 'checked';?>  type="checkbox" name="sharing_option[]" value="Album" />Album
                                          </label>
										   <label class="checkbox">
   <input <?php if(in_array('Online' ,$sharing_option))echo 'checked';?>  type="checkbox" name="sharing_option[]" value="Online" />Online
                                          </label>
										   <label class="checkbox">
  <input <?php if(in_array('Others' ,$sharing_option))echo 'checked';?>  type="checkbox" name="sharing_option[]" value="Others" />Others
                                          </label>
                                       </div>
                                    </div>
		<?php $resolution = explode(',' , $business_result[0]->resolution);
			?>									
									
									
<div class="control-group">
                                       <label class="control-label">Resolution :</label>
                                       <div class="controls">
										   <label class="checkbox">
<input <?php if(in_array('Low' ,$resolution))echo 'checked';?> type="checkbox" name="resolution[]" value="Low" />Low
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('Low Medium' ,$resolution))echo 'checked';?>  type="checkbox" name="resolution[]" value="Low Medium" />Low - Medium
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('Medium' ,$resolution))echo 'checked';?> type="checkbox" name="resolution[]" value="Medium" />Medium
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('Medium High' ,$resolution))echo 'checked';?> type="checkbox" name="resolution[]" value="Medium High" />Medium - High
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('High' ,$resolution))echo 'checked';?> type="checkbox" name="resolution[]" value="High" />High (HD)
                                          </label>
                                       </div>
                                    </div>
									
					<?php $other_services = explode(',' , $business_result[0]->other_services);
					?>						
									

<div class="control-group">
                                       <label class="control-label">Other Services :</label>
                                       <div class="controls">
										   <label class="checkbox">
<input <?php if(in_array('Studio Setup' ,$other_services))echo 'checked';?>  type="checkbox" name="studio_setup[]" value="Studio Setup" />Studio Setup
                                          </label>
										   <label class="checkbox">
<input <?php if(in_array('Video Editing' ,$other_services))echo 'checked';?>  type="checkbox" name="studio_setup[]" value="Video Editing" />Photo/ Video Editing
                                          </label>
                                       </div>
								<div class="controls">
										<input  type="hidden" name="hidden_user_id" value="<?php echo $business_result[0]->user_id;?>" />
										<input  type="hidden" name="hidden_service_id" value="<?php echo $business_result[0]->id;?>" />
                                 
										 </div>
                                    </div>
			  <div class="submit-btn">
							<button type="submit" class="btn green">Save Changes</button>
								<a href="<?php echo base_url();?>photographer/list_services" class="btn">Cancel</a>
							</div>

</form>							
			
</div>


									
								</div>
								<div class="tab-pane row-fluid profile-account" id="tab_1_3">
									<div class="row-fluid">
										<div class="span12">
											<div class="span3">
												<ul class="ver-inline-menu tabbable margin-bottom-10">
													<li class="active">
														<a data-toggle="tab" href="#tab_1-1">
														<i class="icon-cog"></i> 
														Personal info
														</a> 
														<span class="after"></span>                           			
													</li>
													<li class=""><a data-toggle="tab" href="#tab_4-4"><i class="icon-picture"></i> Company info</a></li>
													<li class=""><a data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i> Change Password</a></li>
													
												</ul>
											</div>
											<div class="span9">
												<div class="tab-content">
													<div id="tab_1-1" class="tab-pane active">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse" >
															<form action="<?php echo base_url();?>sp_manager/update_user_detail" id="validation" method="post" enctype="multipart/form-data">
																<label class="control-label">Name</label>
<input type="text" name="user_name" <?php if(!empty($result[0]->d_o_b)){ ?> value="<?php echo $result[0]->user_name;?>" <?php }?>  class="m-wrap span6" />
																<label class="control-label">Gender</label>
																<input type="radio" value="M" name="gender" class="m-wrap span8" <?php if($result[0]->gender == 'M'){ echo "checked"; }?>/>Male
																<input type="radio" name="gender" value="F" class="m-wrap span8" <?php if($result[0]->gender == 'F'){ echo "checked"; }?>/>Female
																<label class="control-label">Date of Birth</label>
																<input type="text" name="dob" id="dob" <?php if(!empty($result[0]->d_o_b)){ ?> value="<?php echo $result[0]->d_o_b;?>" <?php }?> class="m-wrap span6" />
																<label class="control-label">Mobile Number</label>
																<input type="text" name="mobile" <?php if(!empty($result[0]->mob_no)){ ?> value="<?php echo $result[0]->mob_no;?>" <?php }?> class="m-wrap span6" />
																<label class="control-label">Contact Number</label>
																<input type="text" name="contact" <?php if(!empty($result[0]->contact_no)){ ?> value="<?php echo $result[0]->contact_no;?>" <?php }?> class="m-wrap span6" />
																<label class="control-label">Profile Image</label>
																<input name="pro_pic" value="<?php echo $result[0]->pro_pic;?>" type="file"  class="default" />
																<input name="sp_id" value="<?php echo $result[0]->id;?>" type="hidden"  class="default" /></br></br>
			    												<div class="submit-btn">
																	<button class="btn green">Save Changes</button>
																	<a href="#" class="btn">Cancel</a>
																</div>
															</form>
														</div>
													</div>
													
													
													
													<div id="tab_3-3" class="portlet-body form tab-pane">
													     <div style="height: auto;" id="accordion3-3" class="accordion collapse">
															<form action="<?php echo base_url();?>registration/change_password" method="post">
															
																<label class="control-label">Current Password :</label>
																	<input type="password" name="current_password" id="current_password" value="" class="m-wrap span6" />
																<label class="control-label">New Password :</label>
																	<input id="password" type="password" name="password" class="m-wrap span6" />
																<label class="control-label">Re-type New Password :</label>
																	<input id="new_password" type="password" name="password" class="m-wrap span6" />
																<div class="submit-btn">
																	<button onsubmit="return myFunction()" id="submit" class="btn green">Change Password</button>
																	<a href="#" class="btn">Cancel</a>
																</div>
																<input name="hidden" value="<?php echo $result[0]->id;?>" type="hidden"  class="default" />
															</form>
														</div>
													</div>
													<div id="tab_4-4" class="tab-pane">
														<div style="height: auto;" id="accordion1-1" class="accordion collapse">
															<form action="<?php echo base_url();?>sp_manager/update_company_detail" method="post">
																<label class="control-label">Company Name</label>
																<input name="company_name" type="text" value="<?php echo $result[0]->company_name;?>"  class="m-wrap span6" />
																<label class="control-label">Description</label>
																<textarea name="company_description" class="span6 m-wrap" rows="3"><?php echo $result[0]->company_description;?></textarea>
																<label class="control-label">Count of the events performed till date</label>
																<input name="count_of_event" type="text" value="<?php echo $result[0]->count_of_event;?>" class="m-wrap span6" />
																<label class="control-label">Last Event Date</label>
																<input name="last_event_date" type="text" id="led" <?php if(!empty($result[0]->last_event_date)){ ?> value="<?php echo $result[0]->last_event_date;?>" <?php }?> class="m-wrap span6" />
																<label class="control-label">Cost Preference</label>
																 <div class="controls">
																	<select name="budget" id="budget" class="small m-wrap" tabindex="1">
																	<option <?php if($result[0]->budget=='15k-20k'){ echo "selected";}?> value="15k-20k">15k-20k</option>
																	<option <?php if($result[0]->budget=='20k-25k'){ echo "selected";}?> value="20k-25k">20k-25k</option>
																	<option <?php if($result[0]->budget=='25k-30k'){ echo "selected";}?> value="25k-30k">25k-30k</option>
																	<option <?php if($result[0]->budget=='30k-35k'){ echo "selected";}?> value="30k-35k">30k-35k</option>
																	</select>
																</div>
																
																<label class="control-label">Counrty</label>
																<div class="controls">
																	<input type="text" name="country" <?php if(!empty($result[0]->country)){?>value="<?php echo $result[0]->country;?>"<?php } ?> class="span6 m-wrap" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;US&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" />
																	<p class="help-block"><span class="muted">Start typing to auto complete!. E.g: US</span></p>
																</div>
																<label class="control-label">State</label>
																<div class="controls">
																	<input type="text" name="state" <?php if(!empty($result[0]->state)){?>value="<?php echo $result[0]->state;?>"<?php } ?> class="span6 m-wrap" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Alabama&quot;,&quot;Alaska&quot;,&quot;Arizona&quot;,&quot;Arkansas&quot;,&quot;US&quot;,&quot;Colorado&quot;,&quot;Connecticut&quot;,&quot;Delaware&quot;,&quot;Florida&quot;,&quot;Georgia&quot;,&quot;Hawaii&quot;,&quot;Idaho&quot;,&quot;Illinois&quot;,&quot;Indiana&quot;,&quot;Iowa&quot;,&quot;Kansas&quot;,&quot;Kentucky&quot;,&quot;Louisiana&quot;,&quot;Maine&quot;,&quot;Maryland&quot;,&quot;Massachusetts&quot;,&quot;Michigan&quot;,&quot;Minnesota&quot;,&quot;Mississippi&quot;,&quot;Missouri&quot;,&quot;Montana&quot;,&quot;Nebraska&quot;,&quot;Nevada&quot;,&quot;New Hampshire&quot;,&quot;New Jersey&quot;,&quot;New Mexico&quot;,&quot;New York&quot;,&quot;North Dakota&quot;,&quot;North Carolina&quot;,&quot;Ohio&quot;,&quot;Oklahoma&quot;,&quot;Oregon&quot;,&quot;Pennsylvania&quot;,&quot;Rhode Island&quot;,&quot;South Carolina&quot;,&quot;South Dakota&quot;,&quot;Tennessee&quot;,&quot;Texas&quot;,&quot;Utah&quot;,&quot;Vermont&quot;,&quot;Virginia&quot;,&quot;Washington&quot;,&quot;West Virginia&quot;,&quot;Wisconsin&quot;,&quot;Wyoming&quot;]" />
																	<p class="help-block"><span class="muted">Start typing to auto complete!. E.g: US</span></p>
																</div>
																<label class="control-label">City</label>
																<input type="text" name="city" value="<?php echo $result[0]->city;?>"  class="m-wrap span6" />
																<label class="control-label">Street</label>
																<input type="text" name="street" value="<?php echo $result[0]->street;?>"  class="m-wrap span6" />
																<label class="control-label">Zip Code</label>
																<input type="text" name="zip_code" value="<?php echo $result[0]->zip_code;?>"  class="m-wrap span6" />
																<label class="control-label">Lattitude</label>
																<input type="text" name="lattitude" value="<?php echo $result[0]->latt;?>"  class="m-wrap span6" />
																<label class="control-label">Longitude</label>
																<input type="text" name="longitude" value="<?php echo $result[0]->longatt;?>"  class="m-wrap span6" />
																<label class="control-label">Landmark</label>
																<input type="text" name="landmark" value="<?php echo $result[0]->landmark;?>"  class="m-wrap span6" />
																<label class="control-label">Facebook Url</label>
																<input type="text" name="fb_url" value="<?php echo $result[0]->fb_url;?>"  class="m-wrap span6" />
																<label class="control-label">Google Plus Url</label>
																<input type="text" name="gp_url" value="<?php echo $result[0]->gp_url;?>"  class="m-wrap span6" />
																<input name="hidden" value="<?php echo $result[0]->id;?>" type="hidden"  class="default" /></br>
																<div class="submit-btn">
																	<button class="btn green">Save Changes</button>
																	<a href="#" class="btn">Cancel</a>
																</div>
															</form>
														</div>
													</div>
													
													
												</div>
											</div>
											<!--end span9-->                                   
										</div>
									</div>
								</div>
								<!--end tab-pane-->
								
								<!--end tab-pane-->
								
								<!--end tab-pane-->
							</div>
						</div>
						<!--END TABS-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	
	<?php include('includes/footer.php');?>