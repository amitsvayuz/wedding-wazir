 <?php include('includes/header.php');?>
   
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid">
		<?php include('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->    	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Calendar
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url()?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url()?>admin/calendar">Calendar</a></li>
						</ul>
						 <?php if(!empty($this->session->userdata('message'))){?>
						  <div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
						   <span style='color:green;'><?php echo $this->session->userdata('message');?></span>
						  </div>
						<?php } ?>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div id="createEventModal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
   <div class="modal-header">
          <h3 id="myModalLabel1">Create Appointment</h3>
   </div>
   <div class="modal-body">
   <form id="createAppointmentForm" class="form-horizontal">
       <div class="control-group">
           <label class="control-label" for="inputPatient">Event Title :</label>
           <div class="controls">
               <input type="text" name="events_title" id="events_title" tyle="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Value 1&quot;,&quot;Value 2&quot;,&quot;Value 3&quot;]">
                 <input type="text" value="" id="apptStartTime"/>
                 <input type="hidden" value="" id="apptEndTime"/>
                 <input type="hidden" value="" id="apptAllDay" />
           </div>
       </div>
	   <div class="control-group">
           <label class="control-label" for="inputPatient">Event Description :</label>
           <div class="controls">
               <input type="text" name="description" id="description" tyle="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source="[&quot;Value 1&quot;,&quot;Value 2&quot;,&quot;Value 3&quot;]">
                 <input type="hidden" id="apptStartTime"/>
                 <input type="hidden" id="apptEndTime"/>
                 <input type="hidden" id="apptAllDay" />
           </div>
       </div>
	   <div class="control-group">
           <label class="control-label" for="when">Events Slots :</label>
           <div class="controls" style="margin-top:5px;width:auto;">
				<select class="span8" data-placeholder="Choose a Category" required id="role" name="role" tabindex="1">
				 <option value="none">Select Service</option>
				 <option  value="venue">10:00AM - 12:00AM</option>
				 <option  value="planner">1:00PM - 4:00PM</option>
				 <option value="decorator">5:00PM - 8:00PM</option>
				 <option   value="photographer">Full Day</option>
				 </select>
         </div>
       </div>
      
   </form>
   </div>
   <div class="modal-footer">
       <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
       <button type="submit" class="btn btn-primary" id="submitButton">Save</button>
   </div>
</div>
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="portlet box blue calendar">
						<div class="portlet-title">
							<h4><i class="icon-reorder"></i>Calendar</h4>
						</div>
						<div class="portlet-body light-grey">
							<div class="row-fluid">
								<div class="span3 responsive" data-tablet="span12 fix-margin" data-desktop="span8">
									<!-- BEGIN DRAGGABLE EVENTS PORTLET-->		
									<h3 class="event-form-title">Draggable Events</h3>
									<div id="external-events">
										<form class="inline-form">
											<input type="text" value="" class="m-wrap span12" placeholder="Event name" id="event_title" /><br />
											<a href="javascript:;" id="event_add" class="btn green">Add Event</a>
										</form>
										<hr />
										<div id="event_box">
										</div>
										<label for="drop-remove">
										<input type="checkbox" id="drop-remove" />remove after drop									
										</label>
										<hr class="visible-phone" />
									</div>
									<!-- END DRAGGABLE EVENTS PORTLET-->				
								</div>
								<div class="span9">
									<div id="calendar" class="has-toolbar"></div>
								</div>
								<div id="eventContent" style="display:none;" title="Event Details">
								    Start: <span id="startTime"></span><br>
									End: <span id="endTime"></span><br>
									Id: <span id="event_id"></span><br>
									<div id="eventInfo"></div>
									<strong><a id="eventLink" target="_blank">Edit Event</a></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<strong><a id="event_delete_Link">Delete Event</a></strong>
								</div>
							</div>
							<!-- END CALENDAR PORTLET-->
						</div>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->	
		</div>
		<!-- END PAGE -->	 	
	</div>
	<!-- END CONTAINER -->
	
<?php include('includes/footer.php');?>
<script>
		$(document).ready(function(){
			$("#submitButton").click(function(){alert("/");
				var apptStartTime = $("#apptStartTime").val();
				var apptEndTime = $("#apptEndTime").val();
				var title = $("#events_title").val();
				var description = $("#description").val();
				var allDay = $("#apptAllDay").val();
				var base_url = "<?php echo base_url();?>";
			  
				$.ajax({
					type: 'POST',
					url: base_url + 'admin/add_events',
					data: {'start':apptStartTime,'end':apptEndTime ,'title':title,'description':description ,'allDay':allDay },
					success: function(result){
						alert(result);
					 
					}});
				return false;
				
			});
		});
	</script> 
<script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
		 App.setPage('calendar');
      });
   </script>
 