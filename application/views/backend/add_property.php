<?php include('includes/header.php');?>
<?php 
if(isset($resultPro)):
foreach($resultPro as $row):
$pro_id=$row->pro_id;
$numFloor=$row->numFloor;
$propertyName=$row->propertyName;
$propertyAddr=$row->propertyAddr;
$propertyType=$row->propertyType;
$lift=$row->lift;
$externalPer=$row->externalPer;
$restrictionLimit=$row->restrictionLimit;
$outsideSpace=$row->outsideSpace;
$valet=$row->valet;
$belt=$row->belt;
$valetFacility=json_decode($row->valetFacility);
$powerbackup=$row->powerbackup;
$powerdetails=$row->powerdetails;
$rainbackup=$row->rainbackup;
$raindetails=$row->raindetails;
$firebackup=$row->firebackup;
$firedetails=$row->firedetails;

$kdName=json_decode($row->kdName);
$kdValue=json_decode($row->kdValue);
$dis_value =  array_combine($kdName,$kdValue);
$pName=json_decode($row->pName);
$pType=json_decode($row->pType);
$parkingnt_value =  array_combine($pName,$pType);
$pCarcap=json_decode($row->pCarcap);
$pTwocap=json_decode($row->pTwocap);
$parking_value =  array_combine($pCarcap,$pTwocap);
$sName =json_decode($row->sName);
$sCapacity =json_decode($row->sCapacity);
$sitting_value =  array_combine($sName,$sCapacity);

$keys = $pType;
$ptype  = $pType;
$pCarcap = $pCarcap;
$pTwocap    = $pTwocap;
$parking = array();

foreach ($keys as $id => $key) {
    $parking[$key] = array(
	    'pName' =>$keys[$id],
        'pType'  => $pType[$id],
        'pCarcap' => $pCarcap[$id],
        'pTwocap'    => $pTwocap[$id],
    );
}

//echo '<pre>';
//print_r($parking);
//die;
$action=base_url().'Venue_setting_cntlr/property_update/'.$pro_id;
$button='Update';

endforeach;
else:
$pro_d='';
$numFloor='';
$propertyName='';
$propertyAddr='';
$propertyType='';
$lift='';
$externalPer='';
$restrictionLimit='';
$outsideSpace='';
$valet='';
$valetFacility=array();
$belt='';
$powerbackup='';
$powerdetails='';
$rainbackup='';
$raindetails='';
$firebackup='';
$firedetails='';
$kdName=array();
$kdValue=array();
$pName=array();
$pType=array();
$pCarcap=array();
$pTwocap=array();
$sName=array();
$sCapacity=array();
$parking_value=array();
$sitting_value=array();
$dis_value = array();
$parkingnt_value =array();
$parking =array();


$action=base_url().'Venue_setting_cntlr/add_Property';
$button='Save';
endif;
?>
<style>
.switchwidth{
width: 200px;height: 25px;line-height: 25px;}

.lablewidth{
	width:200px;}
.marginleft{
	margin-left:20px !important;}	
	
</style>	
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php include('includes/sidebar.php');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                 
                  <h3 class="page-title">
                     Add Venue Property
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url();?>welcome/dashboard">Dashboard</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="<?php echo base_url();?>Venue_setting_cntlr/list_property">Venue Property  Manage</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="<?php echo base_url();?>Venue_setting_cntlr/property">Add Venue  Property</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
			   
                  <div class="portlet box blue" id="form_wizard_1">
                     <div class="portlet-title">
                        <h4>
                           <i class="icon-reorder"></i>Venue Property - <span class="step-title">Step 1 of 4</span>
                        </h4>
                     </div>
                     <div class="portlet-body form">
                        <form action="<?php echo $action;?>" method="post" class="form-horizontal">
                           <div class="form-wizard">
                              <div class="navbar steps">
                                 <div class="navbar-inner">
                                    <ul class="row-fluid">
                                       <li class="span3">
                                          <a href="<?php echo base_url();?>#tab1" data-toggle="tab" class="step active">
                                          <span class="number">1</span>
                                          <span class="desc"><i class="icon-ok"></i>Property Setup</span>   
                                          </a>
                                       </li>
                                       <li class="span3">
                                          <a href="<?php echo base_url();?>#tab2" data-toggle="tab" class="step">
                                          <span class="number">2</span>
                                          <span class="desc"><i class="icon-ok"></i>Key Distances Setup</span>   
                                          </a>
                                       </li>
                                       <li class="span3">
                                          <a href="<?php echo base_url();?>#tab3" data-toggle="tab" class="step">
                                          <span class="number">3</span>
                                          <span class="desc"><i class="icon-ok"></i>Sitting Arrangement Setup</span>   
                                          </a>
                                       </li>
                                       <li class="span3">
                                          <a href="<?php echo base_url();?>#tab4" data-toggle="tab" class="step">
                                          <span class="number">4</span>
                                          <span class="desc"><i class="icon-ok"></i>Parking Setup</span>   
                                          </a> 
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div id="bar" class="progress progress-success progress-striped">
                                 <div class="bar"></div>
                              </div>
                              <div class="tab-content">
                                 <div class="tab-pane active" id="tab1">
                                 <h3 class="form-section">Property Setup</h3>
                                 
                                    <div class="row-fluid">
										<div class="span6">
											<div class="control-group">
													 <label class="control-label">Location</label>
													 <div class="controls">
													  <select class="m-wrap chosen-with-diselect span12" data-required="1"  name="location">
													 <?php foreach($resultLoc as $rowLoc):?>
													 
														   <option   value=""></option>
														   <option   <?php if($location==$rowLoc->loc_id): echo 'selected'; endif;?> value="<?php echo $rowLoc->loc_id;?>"><?php echo $rowLoc->location;?></option>
													<?php  endforeach;?> 
								  
													</select> 
												  <span class="help-inline" style="color:red"><?php echo form_error('location'); ?></span>
													 </div>
											</div>
										</div>
										<div class="span6">
											      <div class="control-group">
													 <label class="control-label">Property</label>
													 <div class="controls">
														<input type="text" id="propertyName" value="<?php echo $propertyName;?>" name="propertyName" class="m-wrap span12" placeholder="Company Name">
														<span class="help-inline" style="color:red"><?php echo form_error('propertyName'); ?></span>
													 </div>
												  </div>
										</div>
										
										
                                    
                                    </div>
									
									<div class="row-fluid">
                                    <div class="span6">
										<div class="control-group">
                                             <label class="control-label"> Type</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="propertyType">
                                                   <option <?php if($propertyType=='Normal'): echo 'selected'; endif;?> value="Normal">Normal</option>
                                                   <option <?php if($propertyType=='Buffet'): echo 'selected'; endif;?> value="Buffet">Buffet</option>
                                                </select>
                                             </div>
                                        </div>
									</div>
				                    <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Floor</label>
                                             <div class="controls">
                                               <input type="text" value="<?php echo $numFloor;?>" id="numFloor" name="numFloor" class="m-wrap span12" placeholder="Number Of Floor">
                                                <span class="help-inline" style="color:red"><?php echo form_error('numFloor'); ?></span>
                                             </div>
                                          </div>
									</div>
                                    
                                    </div>
									
                                    <div class="row-fluid">
                                    <div class="span12">
									<div class="control-group">
                                             <label class="control-label">Address</label>
                                             <div class="controls">
                                                <textarea class="span12 m-wrap"  name="propertyAddr"  cols="5" placeholder="Property Address"><?php echo $propertyAddr;?></textarea>
                                                <span class="help-inline" style="color:red"><?php echo form_error('propertyAddr'); ?></span>
                     
                                             </div>
                                          </div></div>
                                    
                                    </div>
                                    
                                    <div class="row-fluid">
                                    <div class="span12">
									<div class="control-group">
                                             <label class="control-label">External Permissions</label>
                                             <div class="controls">
                                                <textarea class="span12 m-wrap"  name="externalPer"  cols="5" placeholder="External permissions description"><?php echo $externalPer;?></textarea>
                                          
                                                <span class="help-block"></span>
                                             </div>
                                          </div></div>
                                    
                                    </div>
                                    <div class="row-fluid">
                                    <div class="span12">
									<div class="control-group">
                                             <label class="control-label">Restriction & Limitation</label>
                                             <div class="controls">
                                                <textarea class="span12 m-wrap"  name="restrictionLimit"  cols="5" placeholder="Restriction & limitation description"><?php echo $restrictionLimit;?></textarea>
                                          
                                                <span class="help-block"></span>
                                             </div>
                                          </div></div>
                                    
                                    </div>
                                    <div class="row-fluid">
                                    <div class="span12">
									<div class="control-group">
                                             <label class="control-label">Outside Space Details</label>
                                             <div class="controls">
                                                <textarea class="span12 m-wrap"  name="outsideSpace"  cols="5" placeholder="Outside Space description"><?php echo $outsideSpace;?></textarea>
                                          
                                                <span class="help-block"></span>
                                             </div>
                                          </div>
									</div>
                                    
                                    </div>
                                    <div class="row-fluid">
                                    <div class="span6">
									<div class="control-group">
                                       <label class="control-label">Power Backup</label>
                                       <div class="controls">
										  <label class="radio">
										  
                                          <input <?php if($powerbackup =='1'): echo 'checked'; endif;?> id="apower" type="radio" name="powerbackup" value="1"  />
                                          Available
                                          </label>
                                          <label class="radio">
                                          <input <?php if($powerbackup =='0'): echo 'checked'; endif;?> id="unpower" type="radio" name="powerbackup" value="0"  checked />
                                          Not-Available
                                          </label> 
                                          <span class="help-inline"></span>
                                       </div>
                                    </div>
                                    <div class="control-group" id="powerbackup" style="display:none">
                                       <label class="control-label"></label>
                                       <div class="controls">
                                           <textarea class="span6 m-wrap" name="powerdetails"  cols="5" placeholder=" power back-up description"><?php echo $powerdetails;?></textarea>
                                          <span class="help-inline"></span>
                                       </div>
                                    </div>
                                    
                                    </div>
                                    <div class="span6">
									<div class="control-group">
                                       <label class="control-label">Rain Backup</label>
                                       <div class="controls">
										  <label class="radio">
                                          <input <?php if($rainbackup=='1'): echo 'checked'; endif;?> id="arain" type="radio" name="rainbackup" value="1"  />
                                          Available
                                          </label>
                                          <label class="radio">
                                          <input <?php if($rainbackup=='0'): echo 'checked'; endif;?> id="unrain" type="radio" name="rainbackup" value="0"  checked />
                                          Not-Available
                                          </label> 
                                          <span class="help-inline"></span>
                                       </div>
                                    </div>
                                    <div class="control-group" id="rainbackup" style="display:none">
                                       <label class="control-label"></label>
                                       <div class="controls">
                                           <textarea class="span6 m-wrap" name="raindetails"  cols="5" placeholder="Rain back-up description"><?php echo $raindetails;?></textarea>
                                          <span class="help-inline"></span>
                                       </div>
                                    </div>
                                    
                                    </div>
                                    </div>
                                    <div class="row-fluid">
                                    <div class="span6">
									
                                    <div class="control-group">
                                      <label class="control-label">Fire Safety Backup</label>
									  <div class="controls">
										 <label class="radio ">
                                          <input <?php if($firebackup=='1'): echo 'checked'; endif;?> id="afire" type="radio" name="firebackup" value="1"  />
                                          Available
                                          </label>
                                          <label class="radio">
                                          <input <?php if($firebackup=='0'): echo 'checked'; endif;?> id="unfire" type="radio" name="firebackup" value="0"  checked />
                                          Not-Available
                                          </label>
										 <span class="help-inline"></span>
									  </div>
                                    </div>
                                    <div class="control-group" id="firebackup" style="display:none">
                                       <label class="control-label"></label>
                                       <div class="controls">
                                           <textarea class="span6 m-wrap" name="firedetails"  cols="5" placeholder=" fire safety description"><?php echo $firedetails;?></textarea>
                                           <span class="help-inline"></span>
                                       </div>
                                    </div>
                                    
                                    
                                    
                                    </div>
									<div class="span6">
									  <div class="control-group">
                                      <label class="control-label">Valet Parking</label>
									  <div class="controls">
										 <label class="radio">
                                          <input <?php if("1" == $valet): echo "checked"; endif;?> id="valetone" type="radio" name="valet" value="1"  />
                                          Available
                                          </label>
                                          <label class="radio">
                                          <input <?php if("0" == $valet): echo "checked"; endif;?> id="valetzero" type="radio" name="valet" value="0" checked />
                                          Not-Available
                                          </label> 
										 <span class="help-inline"></span>
                                         
									  </div>
                                      
                                    <div class="control-group" style="display:none" id="valetfacility">
                                    
									  <div class="controls">
										 <label class="checkbox">
										  <input <?php if(!empty($valetFacility)): if(in_array("Complementary",$valetFacility)): echo "checked"; endif; endif;?> type="checkbox" name="valetFacility[]" value="Complementary" /> Complementary
                                          </label>
                                          <label class="checkbox">
                                          <input <?php if(in_array("Chargeable",$valetFacility)): echo "checked"; endif;?> type="checkbox" name="valetFacility[]" value="Chargeable" /> Chargeable
                                          </label>
										 <span class="help-inline"></span>
									  </div>
                                    </div>
                                    </div>
									</div>
                                    </div>
									<div class="row-fluid">
									<div class="span12">
										<div class="control-group">
                                             <label class="control-label">Lift</label>
                                             <div class="controls ">
											    <label class="radio">
                                                <input <?php if($lift=='1'): echo 'checked'; endif;?>  type="radio" name="lift" value="1"  />
                                                 Available
                                                </label>
                                                <label class="radio">
                                                <input <?php if($lift=='0'): echo 'checked'; endif;?>  type="radio" name="lift" value="0" checked />
                                                Not-Available
                                                </label>
                                             </div>
                                        </div>
									</div>
									</div>
                                    </div>
                                <div class="tab-pane" id="tab2">
                                    <h3 class="form-section">Key Distances Setup</h3>
                                    <div class="input_fields_wrap">
									<input type='button' value='Add Key Distance' style="margin-bottom:2%" class="btn green add_field_button"/>
                                    									
									   <?php
									  
									   ?>
									   <?php foreach($dis_value as $key => $val) {?>
										<div class="controls" style="margin-bottom:2%;">
					
			<input type="text" class="m-wrap span5" style="margin-right:3%;" name="mytext[]" value="<?php echo $key;?>" placeholder="Add distance name">
			<input type="text" name="mytextval[]" style="margin-right:3%;" value="<?php echo $val;?>" class="m-wrap span5" placeholder="Add distance value">
			
			<input type="button" value="-" class="btn red remove_field"  id="removeButton" />
			<span class="help-inline"></span>
				</div>
				<?php } ?>					
									
								
									</div>
								</div>
								<div class="tab-pane" id="tab3">
                                     <h3 class="form-section">Sitting Arrangement Setup</h3>
                                   <div class="input_fields_wraps">
								   <input type='button' value='Add Sitting Arrangement' style="margin-bottom:2%" class="btn green add_field_buttons"/>
										<?php foreach($sitting_value as $key => $val):?>
										<div class="controls" style="margin-bottom:2%;">
										<select class="m-wrap span5" style="margin-right:3%;" name="mytexts[]">
                                                   <option <?php if($key == 'U-Shape'): echo 'Selected'; endif;?> value="U-Shape">U-Shape</option>
                                                   <option <?php if($key == 'Double U-Shape'): echo 'Selected'; endif;?> value="Double U-Shape">Double U-Shape</option>
												   <option <?php if($key == 'Class Room'): echo 'Selected'; endif;?> value="Class Room">Class Room</option>
                                                   <option <?php if($key == 'Theatre'): echo 'Selected'; endif;?> value="Theatre">Theatre</option>
												   <option <?php if($key == 'Board Room'): echo 'Selected'; endif;?> value="Board Room">Board Room</option>
                                                   <option <?php if($key == 'Cluster Board Room'): echo 'Selected'; endif;?> value="Cluster Board Room">Cluster Board Room</option>
                                        </select>
										<input type="text" name="mytextsval[]" value="<?php echo $val; ?>" style="margin-right:3%;" class="m-wrap span5" placeholder="Add capacity">
										<input type="button" value="-" class="btn red remove_fields"  />
										<span class="help-inline"></span>
										</div>
										<?php endforeach;?>
									</div>
                                    
                                    </div>
									<div class="tab-pane " id="tab4">
                                     <h3 class="form-section">Parking Setup</h3>
									<div class="input_fields_wrapp">
									<input type='button' value='Add Parking' style="margin-bottom:2%" class="btn green add_field_buttonp"/>
										<div class="controls" style="margin-bottom:2%;">
										
										<?php 
										
										foreach($parking as $rowpkey=>$rowpvalue):
										?>
										<div>	
										<select class="m-wrap span2" style="margin-right:1%;" name="mytextp[]">
                                                   <option <?php if($rowpvalue['pName'] == 'In-side Available'): echo 'Selected'; endif;?> value="In-side Available">In-side Available</option>
                                                   <option <?php if($rowpvalue['pName'] == 'Out-side Available'): echo 'Selected'; endif;?> value="Out-side Available">Out-side Available</option>
                                        </select>
										<select class="m-wrap span2" style="margin-right:1%;" name="mytextptype[]">
                                                   <option <?php if($rowpvalue['pType'] == 'Guarded'): echo 'Selected'; endif;?> value="Guarded">Guarded</option>
                                                   <option <?php if($rowpvalue['pType'] == "Owner Risk"): echo 'Selected'; endif;?> value="Owner Risk">Owner's Risk</option>
                                        </select>
										
									
<input type="text" name="mytextpc[]" value="<?php echo $rowpvalue['pCarcap'];?>" style="margin-right:1%;" class="m-wrap span2" placeholder="Add  car capacity">
<input type="text" name="mytextptc[]" value="<?php echo $rowpvalue['pTwocap'];?>" style="margin-right:1%;" class="m-wrap span2" placeholder="Add twoWheeler capacity">
<input type="button" value="-" class="btn red remove_fieldp"  />
</div>

										
										<span class="help-inline"></span></br></br>
										
										<?php endforeach;?>
										</div>
									</div>
									</div>
                              <div class="form-actions clearfix1">
                                 <a href="javascript:;" class="btn button-previous">
                                 <i class="m-icon-swapleft"></i> Back 
                                 </a>
                                 <a href="javascript:;" class="btn blue button-next">
                                 Continue <i class="m-icon-swapright m-icon-white"></i>
                                 </a>
                                 <button type="submit" class="btn green button-submit">
                                 <?php echo $button;?> <i class="m-icon-swapright m-icon-white"></i>
                                 </button>
                              </div>
                           </div>
                        </form>
                     </div>
					 <!--------key distance start div---------------->
					<div id="myModal1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel1">Add Keys Distance</h3>
									</div>
									<div class="modal-body">
										<div class="input_fields_wrap">
											<a href="javascript:void(0)" class="add_field_button"><i class="icon-plus"></i> Add New </a>
											<div><input type="text" id="mykeys" name="mytext[]" placeholder="key distance name"><input type="text" id="myvalues" name="myvalue[]" placeholder="key distance value"/></div>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
										<button class="btn yellow keys">Save</button>
									</div>
								</div>
					<!--------key distance  div---------------->
					<!--------setting arrangement start div---------------->
					<div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel2">Add Setting Arrangement</h3>
									</div>
									<div class="modal-body">
										<div class="input_fields_wrap1">
											<a href="javascript:void(0)" class="add_field_button1"><i class="icon-plus"></i> Add New </a>
											<div><input type="text" name="seatingtype[]" placeholder="setting type name" ><input type="text" name="seatingcapesity[]" placeholder="setting type capacity"><input type="text" name="buffettype[]" placeholder="buffet type value" ></div>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
										<button class="btn yellow">Save</button>
									</div>
								</div>
					<!--------setting arrangement  div---------------->
					<!--------parking start div---------------->
					<div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel1">Add Parking</h3>
									</div>
									<div class="modal-body">
										<div class="input_fields_wrap2">
											<a href="javascript:void(0)" class="add_field_button2"><i class="icon-plus"></i> Add New </a>
											<div><input type="text" name="mytext[]"></div>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
										<button class="btn yellow">Save</button>
									</div>
								</div>
					<!--------parking  div---------------->
					<!--------floor start div---------------->
					<div id="myModal4" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel1">Add Floor</h3>
									</div>
									<div class="modal-body">
										<div class="input_fields_wrap3">
											<a href="javascript:void(0)" class="add_field_button3"><i class="icon-plus"></i> Add New </a>
											<div><input type="text" name="mytext[]"></div>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
										<button class="btn yellow">Save</button>
									</div>
								</div>
					<!--------floor  div---------------->
					
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
 
   
</div>

   <?php include_once('includes/footer.php');?>    
   <script>
      jQuery(document).ready(function() {   
         // initiate layout and plugins
         App.setPage("form_validation");
         //App.init();
      });
   </script>
 