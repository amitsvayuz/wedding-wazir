<?php include('includes/header.php');?>
                              <?php if(isset($resultCompanyId)){
										
										foreach($resultCompanyId as $row):
										$c_id=$row->c_id; 
										$city_id=$row->city_id;
										$company_name=$row->company_name;
										$company_addr=$row->company_addr;
										endforeach;
										$action=base_url().'General_setting/update_city/'.$c_id;
									    $butoon="Update";
									}
									else 
									{
										
										$c_id=null;
										$city_id=null;
										$company_name=null;
										$company_addr=null;
										$butoon="Save";
										$action=base_url().'General_setting/add_city';
									}
									?>
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php include('includes/sidebar.php');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <!--<div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>-->
                  <!-- END BEGIN STYLE CUSTOMIZER -->   
                  <h3 class="page-title">
                     Add City
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url();?>welcome/dashboard">Dashboard</a> 
                        <span class="icon-angle-right"></span>
                     </li>
					 <li>
								<a href="<?php echo base_url();?>General_setting/list_city">City</a>
								<i class="icon-angle-right"></i>
							</li>
                     <li><a href="<?php echo base_url();?>General_setting/add_city">Add City</a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
			   <div class="portlet box green">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Add City</h4>
                        <!--<div class="tools">
                           <a href="javascript:;" class="collapse"></a>
                           <a href="#portlet-config" data-toggle="modal" class="config"></a>
                           <a href="javascript:;" class="reload"></a>
                           <a href="javascript:;" class="remove"></a>
                        </div>-->
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <h3></h3>
                        <form action="<?php echo $action;?>" id="form_sample_2" class="form-horizontal" method="post">
                           
                           <div class="control-group">
						   
                              <label class="control-label">Brand<span class="required"></span></label>
                              <div class="controls chzn-controls1">
							  
                                 <select id="form_2_education" class="span6 chosen-with-diselect" name="city_name" data-placeholder="Choose a brand" tabindex="11">
                                    <option value=""></option>
									<?php foreach($result as $row):?>
                                    <option <?php if($row->id==$city_id): echo 'Selected'; endif;?> value="<?php echo $row->id;?>"><?php echo $row->name;?></option>
									<?php endforeach;?>
                                 </select>
								 <span class="help-inline" style="color:red" ><?php echo form_error('city_name'); ?></span>
                              </div>
                           </div>
                           <div class="control-group">
                           <label class="control-label">City</label>
                           <div class="controls chzn-controls1">
                          <select  class="span6 chosen-with-diselect" name="company_name" tabindex="12">
                           <option value=""></option>
		           <?php foreach($resultCity as $rowCity):?>
                           <option <?php if($rowCity->id==$company_name): echo 'Selected'; endif;?> 
                            value="<?php echo $rowCity->id;?>"><?php echo $rowCity->city;?></option>
				<?php endforeach;?>
                                 </select>
                                 
			<span class="help-inline" style="color:red" ><?php echo form_error('company_name'); ?></span>
							  </div>
                           </div>
						   <div class="control-group">
                              <label class="control-label">Description&nbsp;&nbsp;</label>
                              <div class="controls">
                                 <textarea name="company_desc" placeholder="Add description" class="span6 m-wrap"/><?php echo $company_addr;?></textarea>

								 <span class="help-inline" style="color:red" ><?php echo form_error('company_desc'); ?></span>
								 </div>
                           </div>
                           <div class="form-actions">
                              <button type="submit" class="btn green"><?php echo $butoon;?></button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->
                     </div>
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
 
   
<style>
 #map_canvas
 {
	 width:49% !important;
	 height:100px !important;
 }
 </style>
			
  
   <?php include_once('includes/footer.php');?>    
 