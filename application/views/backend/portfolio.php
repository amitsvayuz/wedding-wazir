<?php 
if($title=='Edit Portfolio')
{
$album_name=$details[0]->album_title;
$location=$details[0]->location;
$desc=$details[0]->description;
$tags=$details[0]->tags;
$button="Update";
$url=base_url().'planner/edit_portfolio/'.$details[0]->album_id;
}
else {
$album_name='';
$location='';
$desc='';
$tags='';
$button="Save";
$url=base_url().'planner/add_portfolio';
}

?>

<?php include_once('includes/header.php');?>
   <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap-tagsinput.css" />
   
   
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
	<?php include_once('includes/sidebar.php');?>
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
								
						<h3 class="page-title">
							Photographs
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="<?php echo base_url();?>photographer/list_planner_images">Photographs</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN GALLERY MANAGER PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-reorder"></i>Album</h4>
							</div>
							
							<div class="portlet-body">
								<!-- BEGIN GALLERY MANAGER PANEL-->

								
								<div class="row-fluid">
									<div class="span12">
									 
										
									</div>
									<div class="row-fluid">
									<div class="span8">
										<form action="<?php echo $url;?>" method="post" >
										<div class="control-group">
                              	<label class="control-label">Title</label>
                              	<div class="controls">
                                 	<input name="title" value="<?php echo $album_name;?>" type="text" class="span8 m-wrap" required />
                                 	<?php if($this->session->userdata('role')=='venue'){
																$id=@$venue_id;		
		    												}else{
																$id='';			    												
		    												}
		    												?>
                                 	<input name="vid" type="hidden" value="<?php echo $id;?>" />                       
                              	</div>
                           	</div>
                           	<div class="control-group">
                             		<label class="control-label">Location</label>
                              	<div class="controls">
                                	 <input name="location" type="text" value="<?php echo $location;?>" class="span8 m-wrap"  />                       
                             	 	</div>
                           	</div>
                           	<div class="control-group">
                              	<label class="control-label">Description</label>
                              	<div class="controls">
                                 	<textarea name="desc" class="span8 m-wrap" rows="3"><?php echo $desc;?></textarea>
                              	</div>
                           	</div>
										<div class="control-group">
                              	<label class="control-label">Tags</label>
                              	<div class="controls">
                                 	<input type="text" name="tags" data-role="tagsinput" value="<?php echo $tags;?>" class=" span8 m-wrap" />
                              	</div>
                           	</div>
                           	<div class="control-group">
                             		<div class="controls">
                                 	<input id="submit" type="submit" value="<?php echo $button;?>" name="btn" class="btn btn-primary" />
                              	</div>
                           	</div>
									</form>
								</div>
						</div>
								</div>
								<!-- END GALLERY MANAGER PANEL-->
								
								<!-- BEGIN GALLERY MANAGER LISTING-->
								
								<div class="space10"></div>
								<div class="row-fluid">
							
	
									
								<!-- END GALLERY MANAGER LISTING-->
								</div>
							
							</div>
							
						<!-- END GALLERY MANAGER PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->			
		</div>
		<!-- END PAGE -->	 	
	</div>
	 <script src="<?php echo base_url();?>assets/bootstrap-tagsinput.min.js"></script>
	<!-- END CONTAINER -->
<?php include('includes/footer.php');?>