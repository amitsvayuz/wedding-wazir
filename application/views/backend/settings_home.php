<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>
						<!-- END BEGIN STYLE CUSTOMIZER -->   	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							SETTINGS			
							
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="#">Settings</a></li>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<?php  if(@$_REQUEST['set']):?>
								   <div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo base64_decode(@$_REQUEST['set']);?>
								    </div>
									<?php endif;?>
									
									<div class="row-fluid">
									<div class="btn-group">
										<a href="<?php echo base_url();?>Settings_cntlr/add_settings"><button id="sample_editable_1_new" class="btn green">
										Add New Settings <i class="icon-plus"></i>
										</button></a>
									</div>
									
								</div>

	<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box purple">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Venue prefrence </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Name</th>
											<th class="hidden-480"> Description</th>
											<th class="hidden-480">Edit/Delete</th>
											
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach($result1 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?></td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									<div class="btn-group">
										<a href="<?php echo base_url();?>Settings_cntlr/edit_settings/<?php echo $row->id;
											?>/1"><button id="sample_editable_1_new" class="btn green">
										edit
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/delete_setting/<?php echo $row->id;
											?>/1"><button id="sample_editable_1_new" class="btn red">
										delete
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/setting_view/<?php echo $row->id;
											?>/1"><button id="sample_editable_1_new" class="btn yellow">
									     view
										</button></a>
									
									
								</div>
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			
			
			
			<!--------------------------------------new row1-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box purple">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Arangements </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Arangement name</th>
											<th class="hidden-480">Arangement Description</th>
											<th class="hidden-480">Edit/Delete</th>
											
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach($result2 as $row):
											?>
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?></td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									<div class="btn-group">
										<a href="<?php echo base_url();?>Settings_cntlr/edit_settings/<?php echo $row->id;
											?>/2"><button id="sample_editable_1_new" class="btn green">
										edit
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/delete_setting/<?php echo $row->id;
											?>/2"><button id="sample_editable_1_new" class="btn red">
										delete
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/setting_view/<?php echo $row->id;
											?>/2"><button id="sample_editable_1_new" class="btn yellow">
									     view
										</button></a>
									
									
								</div>
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			
			<!--------------------------------------new row2-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box purple">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Events </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Event name</th>
											<th class="hidden-480">Event Description</th>
											<th class="hidden-480">Edit/Delete</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result3 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									<div class="btn-group">
										<a href="<?php echo base_url();?>Settings_cntlr/edit_settings/<?php echo $row->id;
											?>/3"><button id="sample_editable_1_new" class="btn green">
										edit
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/delete_setting/<?php echo $row->id;
											?>/3"><button id="sample_editable_1_new" class="btn red">
										delete
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/setting_view/<?php echo $row->id;
											?>/3"><button id="sample_editable_1_new" class="btn yellow">
									     view
										</button></a>
									
									
								</div>
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			
			<!--------------------------------------new row3-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box purple">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Photographer service </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">service name</th>
											<th class="hidden-480">service Description</th>
											<th class="hidden-480">Edit/Delete</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result4 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									<div class="btn-group">
										<a href="<?php echo base_url();?>Settings_cntlr/edit_settings/<?php echo $row->id;
											?>/4"><button id="sample_editable_1_new" class="btn green">
										edit
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/delete_setting/<?php echo $row->id;
											?>/4"><button id="sample_editable_1_new" class="btn red">
										delete
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/setting_view/<?php echo $row->id;
											?>/4"><button id="sample_editable_1_new" class="btn yellow">
									     view
										</button></a>
									
									
								</div>
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			<!--------------------------------------new row4-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box purple">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Photography resolution </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480"> name</th>
											<th class="hidden-480"> Description</th>
											<th class="hidden-480">Edit/Delete</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result5 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									<div class="btn-group">
										<a href="<?php echo base_url();?>Settings_cntlr/edit_settings/<?php echo $row->id;
											?>/5"><button id="sample_editable_1_new" class="btn green">
										edit
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/delete_setting/<?php echo $row->id;
											?>/5"><button id="sample_editable_1_new" class="btn red">
										delete
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/setting_view/<?php echo $row->id;
											?>/5"><button id="sample_editable_1_new" class="btn yellow">
									     view
										</button></a>
									
									
								</div>
												</td>
										</tr>
									<?php endforeach;?>
									
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			<!--------------------------------------new row5-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box purple">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Photography style </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Style name</th>
											<th class="hidden-480">Style Description</th>
											<th class="hidden-480">Edit/Delete</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result6 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									<div class="btn-group">
										<a href="<?php echo base_url();?>Settings_cntlr/edit_settings/<?php echo $row->id;
											?>/6"><button id="sample_editable_1_new" class="btn green">
										edit
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/delete_setting/<?php echo $row->id;
											?>/6"><button id="sample_editable_1_new" class="btn red">
										delete
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/setting_view/<?php echo $row->id;
											?>/6"><button id="sample_editable_1_new" class="btn yellow">
									     view
										</button></a>
									
									
								</div>
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			<!--------------------------------------new row6-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box purple">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Photography sharing </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Name</th>
											<th class="hidden-480"> Description</th>
											<th class="hidden-480">Edit/Delete</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result7 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									<div class="btn-group">
										<a href="<?php echo base_url();?>Settings_cntlr/edit_settings/<?php echo $row->id;
											?>/7"><button id="sample_editable_1_new" class="btn green">
										edit
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/delete_setting/<?php echo $row->id;
											?>/7"><button id="sample_editable_1_new" class="btn red">
										delete
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/setting_view/<?php echo $row->id;
											?>/7"><button id="sample_editable_1_new" class="btn yellow">
									     view
										</button></a>
									
									
								</div>
												</td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			<!--------------------------------------new row7-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box purple">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Property prefrence </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">Refrence name</th>
											<th class="hidden-480">Refrence Description</th>
											<th class="hidden-480">Edit/Delete</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result8 as $row):
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name; ?>	</td>
											
											<td class="center hidden-480"><?php echo $row->description; ?></td>
											
											<td>	
									<div class="btn-group">
										<a href="<?php echo base_url();?>Settings_cntlr/edit_settings/<?php echo $row->id;
											?>/8"><button id="sample_editable_1_new" class="btn green">
										edit
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/delete_setting/<?php echo $row->id;
											?>/8"><button id="sample_editable_1_new" class="btn red">
										delete
										</button></a>
										<a href="<?php echo base_url();?>Settings_cntlr/setting_view/<?php echo $row->id;
											?>/8"><button id="sample_editable_1_new" class="btn yellow">
									     view
										</button></a>
									
									
								</div>
												</td>
										</tr>
									<?php endforeach;?>
									
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			
			
	<?php include_once('includes/footer.php');?>