<style>
.modal-header-primary {
	color:#fff;
    padding:9px 15px;
    border-bottom:1px solid #eee;
    background-color: #5bc0de;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
}
</style>
<?php include('includes/header.php');?>
	<!-- BEGIN CONTAINER -->	
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include('includes/sidebar.php');?>
		<!-- BEGIN SIDEBAR -->
		
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>
						<!-- END BEGIN STYLE CUSTOMIZER -->  
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Companies				<small>Venue</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="javascript:void">Venues</a>
								<i class="icon-angle-right"></i>
							</li>
							
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>Venue Details</h4>
								
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group">
										<a href="<?php echo base_url();?>General_setting/add_city" id="sample_editable_1_new" class="btn green">
										Add New City <i class="icon-plus"></i>
										</a>
									</div>
									
								</div>
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>City</th>
											<th class="hidden-480">Property</th>
											<th class="hidden-480">View</th>
											
											
										</tr>
									</thead>
									<tbody>
									<?php foreach($cityresult as $cityr){?>
										<tr class="odd gradeX">
											<td><input type="checkbox" class="checkboxes" value="1" /></td>
											<?php foreach($city as $vcity){
											if($cityr->cityName==$vcity->id){$c= $vcity->city;}
											}
											$i=0;
											foreach($propertyresult as $prop){
												if($cityr->cityName==$prop->cityName){
													$i=$i+1;
												}
											}
											?>
											<td><?php echo $c;?></td>
											
											<td class="hidden-480"><a href="javascript:void"><?php echo $i;?> Properties in <?php echo ucfirst($c);?> </a></td>
											<td class="hidden-480"><a href="#myModal1" class="prop btn mini green-stripe" id="<?php echo $cityr->cityName; ?>" data-toggle="modal"><i class="icon-eye-open">View</i></a></td>
											
										</tr>
									<?php }?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<div id="myModal1" style="width:70%;margin-left:-470px !important;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header modal-header-primary">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 >Property</h3>	
										
										<span id="flashmsg"></span>
                                        
									</div>
									
									<div id="propdiv" class="modal-body">
									
									</div>
									<div class="modal-footer">
										<a id="myModalLabel1" href="<?php echo base_url();?>Venue_setting_cntlr/property" class="btn green"><i class="icon-plus"></i> Add Property</a>
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
									
									</div>
									</div>
	<div id="myModal2" style="width:70%;margin-left:-470px !important;" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header modal-header-primary">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 >Venue</h3>	
										
										<span id="flashmsg"></span>
                                        
									</div>
									
									<div id="venuediv" class="modal-body">
									
									</div>
									<div class="modal-footer">
									<a id="myModalLabel1" href="<?php echo base_url();?>venue/add_venue_manager/<?php echo $company;?>" class="btn green"><i class="icon-plus"></i> Add Venue</a>
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
									
									</div>
									</div>
	

	<script type="text/javascript">
$(function(){
$('.prop').click(function(){
	//alert("/");
//alert($(this).attr('id'));
var id = $(this).attr('id');
 $.ajax({type:'post',
		    url:'<?PHP echo base_url()?>admin/venue_prop',
		    data:{id:id},
		    success:function(res){
			                     //alert(res);
                                  $('#propdiv').html(res);
								 }
	      });

 });
});
</script>
							
	
<?php include('includes/footer.php');?>