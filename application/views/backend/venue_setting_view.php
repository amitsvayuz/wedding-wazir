<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>
						<!-- END BEGIN STYLE CUSTOMIZER -->   	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							Venue Settings			
							
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="#">Venue Settings</a>
							<i class="icon-angle-right"></i><li><a href="#">View</a>
							</li>
							<li><a href="#"></a></li>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				
				
				 <?php 
			   $idfrupdate=$idfru;
			   switch($idfrupdate){
				  case 1:
				   $heading='Wedding occasion list';
				   break;
				   case 2:
				   $heading='Non-wedding occasion list';
				   break;
				   case 3:
				   $heading='Nature of venue';
				   break;
				   case 4:
				   $heading='Venue type';
				   break;
				   case 5:
				   $heading='Venue type other';
				   break;
				   case 6:
				   $heading='Floor';
				   break;
				   case 7:
				   $heading='Suit type';
				   break;
				   case 8:
				   $heading='Valet facility ';
				   break;
				   case 9:
				   $heading='Seating type';
				   break;
				   case 10:
				   $heading='Other services';
				   break;
			   }
			   
			   ?>
		</div>
		<!-- END PAGE -->
	
	<!-- END CONTAINER -->
	<div class="tab-pane" id="tab_1_4">
			<div class="row-fluid">
               <div class="span12">
			    <?php 
							foreach($result as $row):
						   ?>
								
			  
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title" >
                        <h4><?php echo $heading;?></h4>
                        
                     </div>
					 
					 <div class="span4 ">
						<!-- BEGIN Portlet PORTLET-->
						
							<div class="portlet-title">
								<h4>Name</h4>
								
								</div>
							
							<div class="portlet-body">
							<?php echo $row->name; ?>
							</div>
							
						<!-- BEGIN Portlet PORTLET-->
						
						
							<div class="portlet-title">
								<h4>Description</h4>
								
								</div>
						
							<div class="portlet-body">
								<?php echo $row->description; ?>
							</div>
						

						</div>
						
					</div>
                        <?php endforeach;?>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
			</div>
												</div>
	<?php include_once('includes/footer.php');?>
