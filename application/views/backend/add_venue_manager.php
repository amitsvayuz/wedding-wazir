<?php include('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
	
		<?php include('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>portlet Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->			
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
								
						<h3 class="page-title">
							Venue 
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url();?>Venue/venue_manager">Venue</a>
								<i class="icon-angle-right"></i>
							</li>
							<!--<li><a href="#">Managed Tables</a></li>-->
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
					<?php  if($this->session->flashdata('message')):?>
								   <?php $message=$this->session->flashdata('message');?>
								   <div class="alert alert-<?php echo $message['class'];?>">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo $message['set'];?>
								    </div>
									<?php endif;?>
					                <?php if(@$resp):?>
									<div class="alert alert-danger">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo @$resp;?>
									</div>
								    <?php endif;?>
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box light-grey">
							<div class="portlet-title">
								<h4><i class="icon-globe"></i>Venue</h4>
								
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									<div class="btn-group">
										<a href="<?php echo base_url();?>Venue/add_venue_manager" id="sample_editable_1_new" class="btn green">
										Add New <i class="icon-plus"></i>
										</a>
									</div>
									
								</div>
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
											<th>Property</th>
											<th class="hidden-480">Venue </th>
											<th class="hidden-480">Venue Nature</th>
											<th class="hidden-480">Created Date</th>
											<th >Action</th>
										</tr>
									</thead>
									<tbody>
									    <?php if(isset($vcresult)):?>
									    <?php foreach($vcresult as $row):?>
										<tr class="odd gradeX">
							<td><input type="checkbox" class="checkboxes" value="1" /></td>
							<?php if(isset($result)){ ?>
							<?php foreach($result as $rowprop):?>
							<?php if($row->prop_name==$rowprop->id):?>
							<td><?php echo ucfirst($rowprop->company_name);?></td>
							<?php endif;?>
							<?php endforeach;?>
							<?php } ?>
											<td class="hidden-480"><?php echo ucfirst($row->v_name);?></td>
											<td class="hidden-480"><?php echo ucfirst($row->v_nature);?></td>
											<td class="center hidden-480"><?php echo date('d/m/Y',$row->v_create_date);?></td>
											<td ><!--<a class="btn mini green"  href="<?php echo base_url();?>Venue/venue_details/<?php echo $row->v_id;?>"><i class="icon-edit1"></i>View</a> |--> <a   href="<?php echo base_url();?>Venue/venue_edit_get/<?php echo $row->v_id;?>"><i class="icon-edit"></i></a> | <a  onclick="return confirm('Are you sure delete?');" href="<?php echo base_url();?>Venue/venue_delete/<?php echo $row->v_id;?>"><i class="icon-trash"></i></a> | <a title="Portfolio" href="<?php echo base_url();?>planner/list_planner_images/<?php echo $row->v_id;?>"><i class="icon-camera-retro"></i></a></td>
										</tr>
										<?php endforeach;?>
										<?php else:?>
										<tr class="odd gradeX">
										<td></td><td></td><td></td><td>Not Exit Any Row</td><td></td><td></td>
										</tr>
										<?php endif;?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
			<!-- END PAGE CONTAINER-->
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->

	<?php include('includes/footer.php');?>
    