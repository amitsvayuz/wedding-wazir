
   <?php include_once('includes/header.php');?>
  
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php include_once('includes/sidebar.php');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                 
                  <h3 class="page-title">
                     Blogger Registration
                  </h3>
                  <ul class="breadcrumb">
				    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="<?php echo base_url();?>blog/list_blogs">Blog List</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                   </ul>
               </div>
            </div>
		   	<?php if($this->session->userdata('message')){?>
			<div class="alert alert-error">
				<button class="close" data-dismiss="alert"></button>
				<span style='color:green;'><?php echo $this->session->userdata('message');?></span>
			</div>
			<?php } ?>
            <!-- END PAGE HEADER-->
			<div id='preview'>
		
		    </div>	
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
			   
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Blog</h4>
                     </div>
				 
                     <div class="portlet-body form">
         <form action="<?php echo base_url();?>blog/edit_blogger" method='post' id="blogger_form" enctype="multipart/form-data" class="form-horizontal">
		 
		                   <div class="control-group">
                              <label class="control-label">Blogger Name :</label>
                              <div class="controls">
              <input class="span4 m-wrap" id="blogger_name" type="text" value="<?php echo $blogger_info->name;?>" placeholder="Blogger Name" required name="blogger_name"/>   
		                      </div>
                           </div>
		 
		                    <div class="control-group">
                              <label class="control-label">Email  :</label>
                              <div class="controls">
             <input class="span4 m-wrap" id="email" type="email" value="<?php echo $blogger_info->email;?>" placeholder="Email" required name="email"/>   
		                      </div>
                           </div> 
						   
						   
						    <div class="control-group">
                              <label class="control-label">Mobile No :</label>
                              <div class="controls">
     <input class="span4 m-wrap" id="mobile_no" type="text" value="<?php echo $blogger_info->mobile;?>" placeholder="Mobile No" required name="mobile_no"/>   
	  <input type="hidden" name="hidden_id" value="<?php echo $blogger_info->id;?>">
							  </div>
                           </div>
						   
						   
						    <div class="control-group">
                              <label class="control-label"></label>
							  <div class="controls">
						         <button type="submit" class="btn blue">Submit</button>
                               </div>
						   </div>
						</form>
                     </div>
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div> 
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>

   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
  <?php include_once('includes/footer.php');?>
  
  
   	
   <script>
   // When the browser is ready...
  $(function() {
  
    // Setup form validation on the #register-form element
    $("#blogger_form").validate({
    
        // Specify the validation rules
        rules: {
            blogger_name: "required",
		    email: {
                required: true,
                email: true
            },
            mobile_no: {
                required: true,
                minlength: 10,
				number: true,
			    maxlength: 10,
            },
		},
        
        // Specify the validation error messages
        messages: {
            blogger_name: "Please enter blogger name",
            mobile_no: {
				number: "Please enter only digits only",
                required: "Please provide a contact number",
                minlength: "Your contact number must be at least 10 digits long"
            },
            email: "Invalid email address",
		    
        },
       
    });
    
  });
  
  </script>
