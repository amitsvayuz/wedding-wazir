<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>
						<!-- END BEGIN STYLE CUSTOMIZER -->   	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							SETTINGS			
							
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="index.html">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="#">SETTINGS</a>
							<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"></a></li>
							<li class="pull-right no-text-shadow">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				
				<div class="span12">
										<!--BEGIN TABS-->
										<div class="tabbable tabbable-custom">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab_1_1" data-toggle="tab">Arangement</a></li>
												<li><a href="#tab_1_2" data-toggle="tab">Photographer service</a></li>
												<li><a href="#tab_1_3" data-toggle="tab">Photography resolution</a></li>
												<li><a href="#tab_1_4" data-toggle="tab">Photography sharing</a></li>
												<li><a href="#tab_1_5" data-toggle="tab">Photography style</a></li>
												<li><a href="#tab_1_6" data-toggle="tab">Property prefrence</a></li>
												<li><a href="#tab_1_7" data-toggle="tab">Venue Prefrence</a></li>
												<li><a href="#tab_1_8" data-toggle="tab">Event</a></li>
											</ul>
											<div class="tab-content">
											       <!-- tab1-->  
												   
												   
												<div class="tab-pane active" id="tab_1_1">
													  <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
				  <?php  if(@$_REQUEST['set']):?>
								   <div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo base64_decode(@$_REQUEST['set']);?>
								    </div>
									<?php endif;?>
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Arangement Form</h4>
                        
                     </div>
					 
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Settings_cntlr/setting_insert/1" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee" />
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
						   
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												
										       <!-- tab 2-->  		
												<div class="tab-pane" id="tab_1_2">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Photographer Service Form</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Settings_cntlr/setting_insert/2" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee" />
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
						   
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												       <!-- tab3-->  
												
												<div class="tab-pane" id="tab_1_3">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Photography resolution form</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Settings_cntlr/setting_insert/3" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
						   
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												
												
												<!-- tab4-->
		<div class="tab-pane" id="tab_1_4">
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Photography sharing form</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Settings_cntlr/setting_insert/4" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												<!-- tab5 -->
												<div class="tab-pane" id="tab_1_5">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Photography style form</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Settings_cntlr/setting_insert/5" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												
												<!-- tab 6 -->
												<div class="tab-pane" id="tab_1_6">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Property prefrence form</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Settings_cntlr/setting_insert/6" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												<!--tab7-->
												<div class="tab-pane" id="tab_1_7">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Venue prefrence form</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Settings_cntlr/setting_insert/7" method="post" class="form-horizontal">
<div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee"/>
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												<!--tab8-->
												<div class="tab-pane" id="tab_1_8">
													<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Event form</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url()?>Settings_cntlr/setting_insert/8" method="post" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label">Name</label>
                              <div class="controls">
                                 <input type="text" class="span6 m-wrap" name="namee" />
                                
                              </div>
                           </div>
                         
                          
                           
                           
                          
                          
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="desc"></textarea>
                              </div>
                           </div>
						   
						   
                           <div class="form-actions">
                              <button type="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
												</div>
												
											</div>
										</div>
										<!--END TABS-->
									</div>
				
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<?php include_once('includes/footer.php');?>