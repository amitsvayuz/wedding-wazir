<?php include('includes/header.php');?>
<?php 
if(isset($editdata)):
foreach($editdata as $row):
                     $v_id=$row->v_id;
                     $prop_name=$row->prop_name;
					 $brand_name=$row->brand_name;
					 $city_name=$row->city_name;
                                         $pvName=$row->pvName;
                                         
					 $location=$row->location;
					 $v_name=$row->v_name;
					 $belt=$row->belt;
					 $v_addr=$row->v_addr;
					 $v_nature=$row->v_nature;
					 $v_transport=$row->v_transport;
					 $v_type=$row->v_type;
					 $v_type_value=json_decode($row->v_type_value);
					 $v_dinein=$row->v_dinein;
					 $v_floor=$row->v_floor;
					 $v_lighting=$row->v_lighting;
					 $v_fencing=$row->v_fencing;
					 $v_dresser=$row->v_dresser;
					 $v_suite_type=$row->v_suite_type;
					 $v_washroom=$row->v_washroom;
					 
					 $v_gustroomsuite=$row->v_gustroomsuite;
					 $v_accommodate=$row->v_accommodate;
					 $v_licencebar=$row->v_licencebar;
					 $v_licencebar_late=$row->v_licencebar_late;
					 $v_wedding_event_list=json_decode($row->v_wedding_event_list);
					 $v_dj=$row->v_dj;
					 $v_dol=$row->v_dol;
					 $v_shenai=$row->v_band;
					 $v_band=$row->v_id;
					 $v_stage=$row->v_stage;
					 $v_decor=$row->v_decor;
					 $v_florist=$row->v_florist;
					 $v_light=$row->v_light;
					 
					 $v_event=$row->v_event;
					 $v_photographer=$row->v_photographer;
					 $v_videographer=$row->v_videographer;
					 $v_ghodi=$row->v_ghodi;
					 $v_palki=$row->v_palki;
					 $v_artist=$row->v_artist;

$action=base_url().'Venue/venue_update/'.$v_id;
$button='Update';

endforeach;
else:
					 $v_id='';
                     $prop_name='';
					 $brand_name='';
					 $city_name='';
					 $location='';
					 $v_name='';
					 $belt='';
                                         $pvName='';
                                 
					 $v_gustroomsuite='';
					 $v_transport='';
					 $v_addr='';
					 $v_nature='';
					 $v_type='';
					 $v_type_value=array();
					 $v_dinein='';
					 $v_floor='';
					 $v_lighting='';
					 $v_fencing='';
					 $v_dresser='';
					 $v_suite_type='';
					 $v_washroom='';
					 
					 $v_accommodate='';
					 $v_licencebar='';
					 $v_licencebar_late='';
					 $v_wedding_event_list=array();
					 $v_dj='';
					 $v_dol='';
					 $v_shenai='';
					 $v_band='';
					 $v_stage='';
					 $v_decor='';
					 $v_florist='';
					 $v_light='';
					 
					 $v_event='';
					 $v_photographer='';
					 $v_videographer='';
					 $v_ghodi='';
					 $v_palki='';
					 $v_artist='';

$action=base_url().'Venue/add_venue';
$button='Save';
endif;
?>
<script type="text/javascript">
            // When the document is ready
            $(document).ready(function () {
                
                $('#lastdate').datepicker({
                    format: "dd/mm/yyyy"
                });  
            
            });
	
$(document).ready(function(){
	$('.keys').click(function()
	{
		var n = $("input[name^='mytext']").length;
        var array = $("input[name^='mytext']");
        for(i=0; i < n; i++) 
		{
			card_value1 = array.eq(i).attr('value'); 
			alert(card_value1);
        }
		var m = $("input[name^='myvalue']").length;
        var array = $("input[name^='myvalue']");
        for(i=0; i < m; i++) 
		{
			card_value2 = array.eq(i).attr('value'); 
			alert(card_value2);
        }
	});

</script>
<script type="text/javascript">
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        if($(this).attr("value")=="A/C"){
            $("#actype").show();
			$("#nonactype").hide();
        }
        if($(this).attr("value")=="Non-A/C"){
            $("#nonactype").show();
			 $("#actype").hide();
			
        }
		$('#apower').click(function(){
			$("#powerbackup").show();
		});
		$('#unpower').click(function(){
			$("#powerbackup").hide();
		});
		$('#arain').click(function(){
			$("#rainbackup").show();
		});
		$('#unrain').click(function(){
			$("#rainbackup").hide();
		});
		$('#afire').click(function(){
			$("#firebackup").show();
		});
		$('#unfire').click(function(){
			$("#firebackup").hide();
		});
		$('#valetone').click(function(){
			$("#valetfacility").show();
			$("#nonac").hide();
		});
		$('#valetzero').click(function(){
			$("#valetfacility").hide();
			$("#nonac").show();
		});
    });
	
	// get selected brand
	$('#proId').bind('change', function () {
                  var url = $(this).val();
                  
				  var base_url = '<?php echo base_url();?>';
                                  
				  

					  $.ajax({
								type: "POST",
								url: base_url+"Venue/get_brand_ajax",
								data: "pro_id="+url,
								cache: false,
								datatype: 'html',
		                                                success: function(res)                           {								
								$("#proBelt").html(res);
								}
							});
				  
              });
	
	
	// get selected city
	$('#proBelt').bind('change', function () {
                  var url = $(this).val(); 
				  var base_url = '<?php echo base_url();?>';
				  
					  $.ajax({
								type: "POST",
								url: base_url+"Venue/get_city_ajax",
								data: "pro_id="+url,
								cache: false,
								datatype: 'html',
								success: function(res) {								
								$("#proCity").html(res);
								}
							});
				 
              });
			  
	// get selected 
	$('#proCity').bind('change', function () {
                  var url = $(this).val(); 
				  var base_url = '<?php echo base_url();?>';
				  
					  $.ajax({
								type: "POST",
								url: base_url+"Venue/get_location_ajax",
								data: "pro_id="+url,
								cache: false,
								datatype: 'html',
								success: function(res) {								
								$("#location").html(res);
								}
							});
				 
              });	
// get selected 
	$('#location').bind('change', function () {
                  var url = $(this).val(); 
				  var base_url = '<?php echo base_url();?>';
				  
					  $.ajax({
								type: "POST",
								url: base_url+"Venue/get_property_ajax",
								data: "pro_id="+url,
								cache: false,
								datatype: 'html',
								success: function(res) {								
								$("#pvName").html(res);
								}
							});
				 
              });				  
	
});
</script>
	<style>
.switchwidth{
width: 200px;height: 25px;line-height: 25px;}

.lablewidth{
	width:200px;}
.marginleft{
	margin-left:20px !important;}	
	
</style>	
   <!-- END HEADER -->
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
      <!-- BEGIN SIDEBAR -->
      <?php include('includes/sidebar.php');?>
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                   
                  <h3 class="page-title">
                     Add Venue
             
                  </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="<?php echo base_url();?>Venue/venue_manager">Venue  Manage</a>
                        <span class="icon-angle-right"></span>
                     </li>
                     <li><a href="<?php echo base_url();?>Venue/add_venue">Add Venue </a></li>
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
			   
                  <div class="portlet box blue" id="form_wizard_1">
                     <div class="portlet-title">
                        <h4>
                           <i class="icon-reorder"></i>Add Venue - <span class="step-title">Step 1 of 4</span>
                        </h4>
            
                     </div>
                     <div class="portlet-body form">
                        <form action="<?php echo $action;?>" id="form_venue12" method="post" class="form-horizontal">
						    
                           <div class="form-wizard">
                              <div class="navbar steps">
                                 <div class="navbar-inner">
                                    <ul class="row-fluid">
                                       <li class="span3">
                                          <a href="<?php echo base_url();?>#tab1" data-toggle="tab" class="step active">
                                          <span class="number">1</span>
                                          <span class="desc"><i class="icon-ok"></i>General Setup</span>   
                                          </a>
                                       </li>
                                       <li class="span3">
                                          <a href="<?php echo base_url();?>#tab2" data-toggle="tab" class="step">
                                          <span class="number">2</span>
                                          <span class="desc"><i class="icon-ok"></i>Bridal Suite Setup</span>   
                                          </a>
                                       </li>
                                       <li class="span3">
                                          <a href="<?php echo base_url();?>#tab3" data-toggle="tab" class="step">
                                          <span class="number">3</span>
                                          <span class="desc"><i class="icon-ok"></i>Events Setup</span>   
                                          </a>
                                       </li>
                                       <li class="span3">
                                          <a href="<?php echo base_url();?>#tab4" data-toggle="tab" class="step">
                                          <span class="number">4</span>
                                          <span class="desc"><i class="icon-ok"></i>Services Setup</span>   
                                          </a> 
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div id="bar" class="progress progress-success progress-striped">
                                 <div class="bar"></div>
                              </div>
							  
                              <div class="tab-content">
                                 <div class="tab-pane active" id="tab1">
                                 <h3 class="form-section">General Setup</h3>
								    <div class="row-fluid">
                                    <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Company</label>
                                             <div class="controls">
											    <!--<select class="m-wrap chosen-with-diselect span6" id="proId" name="propName">-->
                                                <select class="m-wrap  span12" id="proId" name="propName">
                                                   <option value="">Select Company</option>
                                            <?php foreach($result as $row1):?>
					                        <option <?php if($prop_name==$row1->id): echo 'selected'; endif;?> value="<?php echo $row1->id;?>"><?php echo $row1->company_name;?></option>
					                        <?php endforeach;?>
                                                </select>
                                                <span class="help-inline" style="color:red;"><?php echo form_error('propName');?></span>
                                             </div>
                                    </div>
									</div>
									<div class="span6">
									<div class="control-group">
                                             <label class="control-label">Brand</label>
                                             <div class="controls">
											    <!--<select class="m-wrap chosen-with-diselect span6" id="proId" name="propName">-->
                                                <select class="m-wrap  span12" id="proBelt" name="brandName">
                                                   <option value="">Select Brand</option>
                                                </select>
                                                <span class="help-inline" style="color:red;"><?php echo form_error('brandName');?></span>
                                             </div>
                                    </div>
									</div>
									</div>
									<div class="row-fluid">
									<div class="span6">
									<div class="control-group">
                                             <label class="control-label">City</label>
                                             <div class="controls">
											    <!--<select class="m-wrap chosen-with-diselect span6" id="proId" name="propName">-->
                                                <select class="m-wrap span12" id="proCity" name="cityName">
                                                   <option value="">Select City</option>
                                                </select>
                                                <span class="help-inline" style="color:red;"><?php echo form_error('cityName');?></span>
                                             </div>
                                    </div>
									</div>
									<div class="span6">
									<div class="control-group">
                                             <label class="control-label">Location</label>
                                             <div class="controls">
											    <!--<select class="m-wrap chosen-with-diselect span6" id="proId" name="propName">-->
                                                <select class="m-wrap span12" id="location" name="location">
                                                   <option value="">Select Location</option>
                                                </select>
                                                <span class="help-inline" style="color:red;"><?php echo form_error('location');?></span>
                                             </div>
                                    </div>
									</div>
									</div>
									<div class="row-fluid">
									 <div class="span6">
				<div class="control-group">
                                <label class="control-label">Property</label>
                                <div class="controls">
                                <select class="m-wrap span12" id="pvName" name="pvName">
                                <option value="">Select Property</option>
				
                                </select>
                                <span class="help-inline" style="color:red;"><?php echo form_error('pvName');?></span>
                                </div>
                                </div>
				</div>
				<div class="span6">
				<div class="control-group">
                                <label class="control-label">Belt</label>
                                <div class="controls">
                            <!--<select class="m-wrap chosen-with-diselect span6" id="proId" name="propName">-->
                                <select class="m-wrap span12" id="proBelt" name="beltName">
                                <option value="">Select Belt</option>
				<?php if($resultbelt):?>
				<?php foreach($resultbelt as $rowbelt):?>
				<option <?php if($belt==$rowbelt->id): echo 'selected'; endif;?> value="<?php echo $rowbelt->id;?>"><?php echo $rowbelt->beltName;?></option>
		                <?php endforeach;?>
				<?php endif;?>
                                </select>
                                <span class="help-inline" style="color:red;"><?php echo form_error('beltName');?></span>
                                </div>
                                </div>
				</div>
				</div>


                                <div class="row-fluid">
                               
                                <div class="span6">
				<div class="control-group">
                                <label class="control-label">Floor</label>
                                <div class="controls">
                                <select class="m-wrap span12" id="v_floor" name="floor">
                                <option value="">Select Floor</option>
				<?php if($resultPro):?>
                                
                <?php for($i=0;$i<=$resultPro[0]->numFloor;$i++):?>
                                
		<option <?php if($v_floor==$i): echo 'selected'; endif;?> value="<?php echo $i;?>"><?php echo $i;?></option>
                                
		                <?php endfor;?>
				<?php endif;?>
                                </select>
                                <span class="help-inline" style="color:red;"><?php echo form_error('v_floor');?></span>
                                </div>
                                </div>
				</div>
				</div>
				<div class="row-fluid">
                                <div class="span12">
				<div class="control-group">
                                <label class="control-label">Venue</label>
                                <div class="controls">
                                <input type="text" id="firstName" name="venueName" value="<?php echo $v_name;?>" class="m-wrap span12" placeholder="Venue Name">
                                <span class="help-inline" style="color:red;"><?php echo form_error('venueName');?></span>
                                </div>
                                </div>
				</div>
				</div>
                                
				
                                    <div class="row-fluid">
                                    <div class="span12">
									<div class="control-group">
                                             <label class="control-label">Description</label>
                                             <div class="controls">
                                                <textarea class="span12 m-wrap"  name="venueAddr"  value="" cols="5" placeholder="venue description"><?php echo $v_addr;?></textarea>
                                          
                                                <span class="help-inline" style="color:red;"><?php echo form_error('venueAddr');?></span>
                                             </div>
                                          </div></div>
                                    
                                    </div>
                                    <div class="row-fluid">
                                    <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Nature Type</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" data-required="1"  name="natureType">
                                                   <option <?php if($v_nature=='Indoor'): echo 'selected'; endif;?> value="Indoor">Indoor</option>
                                                   <option <?php if($v_nature=='Outdoor'): echo 'selected'; endif;?> value="Outdoor">Outdoor</option>
                                                   <option <?php if($v_nature=='Banquet'): echo 'selected'; endif;?> value="Banquet">Banquet</option>
                                                </select>
                                               <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                    <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Dine In</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" data-required="1"  name="dineIn">
                                                   <option <?php if($v_dinein=='Veg'): echo 'selected'; endif;?> value="Veg">Veg</option>
                                                   <option  <?php if($v_dinein=='Non-Veg'): echo 'selected'; endif;?>value="Non-Veg">Non-Veg</option>
												   <option <?php if($v_dinein=='Both'): echo 'selected'; endif;?> value="Both">Both</option>
                                                </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                    </div>
                                 
								 
                                    </div>
                                 <div class="tab-pane" id="tab2">
                                     <h3 class="form-section">Bridal Suite Setup</h3>
                                     
                                     <div class="row-fluid">
									 	<div class="span6">
											<div class="control-group">
                              <label class="control-label">Suit Type</label>
                              <div class="controls">
                                 <div class="text-toggle-button">
                                    <input <?php if($v_suite_type=='on'): echo 'checked'; endif;?> type="checkbox" name="suiteType" class="toggle" />
                                 </div>
                              </div>
                           </div>
                           				</div>
                                        <div class="span6">
											<div class="control-group">
                              <label class="control-label">Washroom</label>
                              <div class="controls">
                                 <div class="text-toggle-button">
                                    <input <?php if($v_washroom=='on'): echo 'checked'; endif;?> type="checkbox" name="washRoom" class="toggle" />
                                 </div>
                              </div>
                           </div>
                           				</div>
                           
                           			</div>
                                    
                                    
                                    
                                    <div class="row-fluid">
									 	<div class="span6">
											<div class="control-group">
                              <label class="control-label">Dresser</label>
                              <div class="controls">
                                 <div class="text-toggle-button">
                                    <input <?php if($v_dresser=='on'): echo 'checked'; endif;?> type="checkbox" name="dresser" class="toggle" />
                                 </div>
                              </div>
                           </div>
                           				</div>
                                        <div class="span6">
											<div class="control-group">
                              <label class="control-label">Licensed Bar</label>
                              <div class="controls">
                                 <div class="text-toggle-button">
                                    <input <?php if($v_licencebar=='on'): echo 'checked'; endif;?> type="checkbox" name="licensedBar" class="toggle" />
                                 </div>
                              </div>
                           </div>
                           				</div>
                           
                           			</div>
                                    <div class="row-fluid">
									 	<div class="span6">
											<div class="control-group">
                              <label class="control-label">Late Licensed Bar</label>
                              <div class="controls">
                                 <div class="text-toggle-button">
                                    <input <?php if($v_licencebar_late=='on'): echo 'checked'; endif;?> type="checkbox" name="licensedBarLate" class="toggle" />
                                 </div>
                              </div>
                           </div>
                           				</div>
                                        <div class="span6">
											<div class="control-group">
                              <label class="control-label">Late Night Transport Facility</label>
                              <div class="controls">
                                 <div class="text-toggle-button">
                                    <input <?php if($v_transport=='on'): echo 'checked'; endif;?> type="checkbox" name="Transport" class="toggle" />
                                 </div>
                              </div>
                           </div>
                           	</div>
                          </div>  
						  <div class="row-fluid">
									 	<div class="span6">
											<div class="control-group">
                              <label class="control-label">Guest Room Suits</label>
                              <div class="controls">
                                 <div class="text-toggle-button">
                                    <input <?php if($v_gustroomsuite=='on'): echo 'checked'; endif;?> type="checkbox" name="gustRoom" class="toggle" />
                                 </div>
                              </div>
                           </div>
                           				</div>
                                        <div class="span6">
											<div class="control-group">
                              <label class="control-label">Additional Accommodation</label>
                              <div class="controls">
                                 <div class="text-toggle-button">
                                    <input <?php if($v_accommodate=='on'): echo 'checked'; endif;?> type="checkbox" name="addAccomm" class="toggle" />
                                 </div>
                              </div>
                           </div>
                           	</div>
                          </div>  
						  <div class="row-fluid">
									 	<div class="span6">
											<div class="control-group">
                              <label class="control-label">Light</label>
                              <div class="controls">
                                 <div class="text-toggle-button">
                                    <input <?php if($v_lighting=='on'): echo 'checked'; endif;?> type="checkbox" name="light" class="toggle" />
                                 </div>
                              </div>
                           </div>
                           				</div>
                                        <div class="span6">
											<div class="control-group">
                              <label class="control-label">Fencing</label>
                              <div class="controls">
                                 <div class="text-toggle-button">
                                    <input <?php if($v_fencing=='on'): echo 'checked'; endif;?> type="checkbox" name="fencing" class="toggle" />
                                 </div>
                              </div>
                           </div>
                           	</div>
                          </div> 
						</div>
								 <div class="tab-pane" id="tab3">
                                     <h3 class="form-section">Events Setup</h3>
                                    <div class="row-fluid">
                                    <div class="span6">
                                    
									<div class="control-group">
                                       <label class="control-label">Venue Type</label>
									   <div class="controls">
										 <label class="radio">
                                          <input <?php if("A/C" == $v_type): echo "checked"; endif;?> id="ac" type="radio" name="aType" value="A/C" checked />
                                          A/C
                                          </label>
                                          <label class="radio">
                                          <input <?php if("Non-A/C" == $v_type): echo "checked"; endif;?> id="nonac" type="radio" name="aType" value="Non-A/C"  />
                                          Non-A/C
                                          </label>
                                         <span class="help-inline"></span>
                                       </div>
                                       <div class="controls" id="actype">
										  <label class="checkbox">
                                          <input <?php if(in_array("De-Centralized A/C",$v_type_value)): echo "checked"; endif;?> type="checkbox" name="venueType[]" value="De-Centralized A/C" /> De-Centralized A/C
                                          </label>
										  <label class="checkbox">
                                          <input <?php if(in_array("Window A/C",$v_type_value)): echo "checked"; endif;?> type="checkbox" name="venueType[]" value="Window A/C" /> Window A/C
                                          </label>
										 <span class="help-inline"></span>
									  </div>
                                      <div class="controls" id="nonactype" style="display:none">
                                          <input <?php if(in_array("Desert Coolers",$v_type_value)): echo "checked"; endif;?> type="checkbox" name="venueType[]" value="Desert Coolers" /> Desert Coolers
                                         
										 <span class="help-inline"></span>
									  </div>
                                      </div>  
                                      </div>
                                    </div>
                                    
                                    <div class="row-fluid">
                                    <div class="span2">
                                    <div class="control-group">
									<label class="control-label">Events List</label></div></div>
                                    <div class="span10 marginleft">
                                    <div class="control-group">
									   <div class="controls">
                                            <select name="weddingList[]" class="span6 m-wrap" multiple="multiple" data-placeholder="Choose a Category" tabindex="1">
                                                <?php if(isset($v_wedding_event_list)):?>
												<?php foreach($resultEvent as $details):?>
                                                <option <?php if(in_array($details->id,$v_wedding_event_list)): echo 'selected'; endif;?> value="<?php echo $details->id;?>"> <?php echo $details->name;?></option>
                                                <?php endforeach; ?>
												<?php endif;?>
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
									<div class="tab-pane" id="tab4">
                                     <h3 class="form-section">Services Setup</h3>
									<div class="row-fluid">
                                    <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Select Dhol Service</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="dhol">
                                                   <option <?php if($v_dol=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_dol=='In-House & Optional'): echo 'selected'; endif;?> value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_dol=='On Request'): echo 'selected'; endif;?> value="On Request">On Request</option>
                                                   <option <?php if($v_dol=='Not Applicable'): echo 'selected'; endif;?> value="Not Applicable">Not Applicable</option>
                                                </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                          <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Select DJ Service</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="dj">
                                                  <option <?php if($v_dj=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_dj=='In-House & Optional'): echo 'selected'; endif;?> value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_dj=='On Request'): echo 'selected'; endif;?> value="On Request">On Request</option>
                                                   <option <?php if($v_dj=='Not Applicable'): echo 'selected'; endif;?> value="Not Applicable">Not Applicable</option>
                                               </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                    
                                    </div>
									<div class="row-fluid">
                                    <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Band Arrangement</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="band">
                                                  <option <?php if($v_band=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_band=='In-House & Optional'): echo 'selected'; endif;?> value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_band=='On Request'): echo 'selected'; endif;?> value="On Request">On Request</option>
                                                   <option <?php if($v_band=='Not Applicable'): echo 'selected'; endif;?> value="Not Applicable">Not Applicable</option>
                                               </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                          <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Shenai Arrangement</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="shenai">
                                                  <option <?php if($v_shenai=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_shenai=='In-House & Optional'): echo 'selected'; endif;?> value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_shenai=='On Request'): echo 'selected'; endif;?> value="On Request">On Request</option>
                                                   <option <?php if($v_shenai=='Not Applicable'): echo 'selected'; endif;?> value="Not Applicable">Not Applicable</option>
                                               </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                    
                                    </div>
									<div class="row-fluid">
                                    <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Stage Set-up</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="stage">
                                                   <option <?php if($v_stage=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_stage=='In-House & Optional'): echo 'selected'; endif;?> value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_stage=='On Request'): echo 'selected'; endif;?> value="On Request">On Request</option>
                                                   <option <?php if($v_stage=='Not Applicable'): echo 'selected'; endif;?> value="Not Applicable">Not Applicable</option>
                                                </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                          <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Décor</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="decor">
                                                   <option <?php if($v_decor=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_decor=='In-House & Optional'): echo 'selected'; endif;?> value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_decor=='On Request'): echo 'selected'; endif;?> value="On Request">On Request</option>
                                                   <option <?php if($v_decor=='Not Applicable'): echo 'selected'; endif;?> value="Not Applicable">Not Applicable</option>
                                               </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                    
                                    </div>
									<div class="row-fluid">
                                    <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Florist</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="florist">
                                                  <option <?php if($v_florist=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_florist=='In-House & Optional'): echo 'selected'; endif;?> value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_florist=='On Request'): echo 'selected'; endif;?> value="On Request">On Request</option>
                                                   <option <?php if($v_florist=='Not Applicable'): echo 'selected'; endif;?> value="Not Applicable">Not Applicable</option>
                                                </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                          <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Lighting Arrangement</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="lighting">
                                                 <option <?php if($v_light=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_light=='In-House & Optional'): echo 'selected'; endif;?> value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_light=='On Request'): echo 'selected'; endif;?> value="On Request">On Request</option>
                                                   <option <?php if($v_light=='Not Applicable'): echo 'selected'; endif;?> value="Not Applicable">Not Applicable</option>
                                               </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                    
                                    </div>
									<div class="row-fluid">
                                    <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Photography</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="photo">
                                                  <option <?php if($v_photographer=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_photographer=='In-House & Optional'): echo 'selected'; endif;?> value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_photographer=='On Request'): echo 'selected'; endif;?> value="On Request">On Request</option>
                                                   <option <?php if($v_photographer=='Not Applicable'): echo 'selected'; endif;?> value="Not Applicable">Not Applicable</option>
                                                </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                          <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Event Planners</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="events">
                                                   <option <?php if($v_event=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_event=='In-House & Optional'): echo 'selected'; endif;?> value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_event=='On Request'): echo 'selected'; endif;?> value="On Request">On Request</option>
                                                   <option <?php if($v_event=='Not Applicable'): echo 'selected'; endif;?> value="Not Applicable">Not Applicable</option>
                                               </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                    
                                    </div>
									<div class="row-fluid">
                                    
                                          <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Videography</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="video">
                                                  <option <?php if($v_videographer=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_videographer=='In-House & Optional'): echo 'selected'; endif;?>value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_videographer=='On Request'): echo 'selected'; endif;?>value="On Request">On Request</option>
                                                   <option <?php if($v_videographer=='Not Applicable'): echo 'selected'; endif;?>value="Not Applicable">Not Applicable</option>
                                               </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
										  <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Ghodi/Baggi</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="ghodi">
                                                 <option <?php if($v_ghodi=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_ghodi=='In-House & Optional'): echo 'selected'; endif;?>value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_ghodi=='On Request'): echo 'selected'; endif;?>value="On Request">On Request</option>
                                                   <option <?php if($v_ghodi=='Not Applicable'): echo 'selected'; endif;?>value="Not Applicable">Not Applicable</option>
                                               </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
                                    
                                    </div>
									<div class="row-fluid">
                                    
                                          <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Doli/Palki</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="doli">
                                                  <option <?php if($v_palki=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_palki=='In-House & Optional'): echo 'selected'; endif;?>value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_palki=='On Request'): echo 'selected'; endif;?>value="On Request">On Request</option>
                                                   <option <?php if($v_palki=='Not Applicable'): echo 'selected'; endif;?>value="Not Applicable">Not Applicable</option>
                                               </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div></div>
										   <div class="span6">
									<div class="control-group">
                                             <label class="control-label">Make-up Artist</label>
                                             <div class="controls">
                                                <select class="m-wrap span12" name="artist">
                                                  <option <?php if($v_artist=='In-House & Mandatory'): echo 'selected'; endif;?> value="In-House & Mandatory">In-House & Mandatory</option>
                                                   <option <?php if($v_artist=='In-House & Optional'): echo 'selected'; endif;?>value="In-House & Optional">In-House & Optional</option>
												   <option <?php if($v_artist=='On Request'): echo 'selected'; endif;?>value="On Request">On Request</option>
                                                   <option <?php if($v_artist=='Not Applicable'): echo 'selected'; endif;?>value="Not Applicable">Not Applicable</option>
                                                </select>
                                                <span class="help-inline"></span>
                                             </div>
                                          </div>
										  </div>
                                    
                                    </div>
									
									</div>
                              <div class="form-actions clearfix">
                                 <a href="javascript:;" class="btn button-previous">
                                 <i class="m-icon-swapleft"></i> Back 
                                 </a>
                                 <a href="javascript:;" class="btn blue button-next">
                                 Continue <i class="m-icon-swapright m-icon-white"></i>
                                 </a>
                                 <button type="submit" class="btn green button-submit">
                                 <?php echo $button;?> <i class="m-icon-swapright m-icon-white"></i>
                                 </button>
                              </div>
                           </div>
                        </form>
                     </div>
					 <!--------key distance start div---------------->
					<div id="myModal1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel1">Add Keys Distance</h3>
									</div>
									<div class="modal-body">
										<div class="input_fields_wrap">
											<a href="javascript:void(0)" class="add_field_button"><i class="icon-plus"></i> Add New </a>
											<div><input type="text" id="mykeys" name="mytext[]" placeholder="key distance name"><input type="text" id="myvalues" name="myvalue[]" placeholder="key distance value"/></div>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
										<button class="btn yellow keys">Save</button>
									</div>
								</div>
					<!--------key distance  div---------------->
					<!--------setting arrangement start div---------------->
					<div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel2">Add Setting Arrangement</h3>
									</div>
									<div class="modal-body">
										<div class="input_fields_wrap1">
											<a href="javascript:void(0)" class="add_field_button1"><i class="icon-plus"></i> Add New </a>
											<div><input type="text" name="seatingtype[]" placeholder="setting type name" ><input type="text" name="seatingcapesity[]" placeholder="setting type capacity"><input type="text" name="buffettype[]" placeholder="buffet type value" ></div>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
										<button class="btn yellow">Save</button>
									</div>
								</div>
					<!--------setting arrangement  div---------------->
					<!--------parking start div---------------->
					<div id="myModal3" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel1">Add Parking</h3>
									</div>
									<div class="modal-body">
										<div class="input_fields_wrap2">
											<a href="javascript:void(0)" class="add_field_button2"><i class="icon-plus"></i> Add New </a>
											<div><input type="text" name="mytext[]"></div>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
										<button class="btn yellow">Save</button>
									</div>
								</div>
					<!--------parking  div---------------->
					<!--------floor start div---------------->
					<div id="myModal4" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h3 id="myModalLabel1">Add Floor</h3>
									</div>
									<div class="modal-body">
										<div class="input_fields_wrap3">
											<a href="javascript:void(0)" class="add_field_button3"><i class="icon-plus"></i> Add New </a>
											<div><input type="text" name="mytext[]"></div>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
										<button class="btn yellow">Save</button>
									</div>
								</div>
					<!--------floor  div---------------->
					
                  </div>
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
 
   
</div>
  
   <?php include_once('includes/footer.php');?>    
 