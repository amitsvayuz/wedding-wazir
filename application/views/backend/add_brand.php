<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
									
						<h3 class="page-title">
							Add Brand
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url()?>sp_manager/dashboard">Dashboard</a>
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="<?php echo base_url()?>General_setting/list_brand">Brand</a></li>
							<i class="icon-angle-right"></i>
							<li><a href="<?php echo base_url()?>General_setting/add_brand">Add Brand</a>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				
				
				
		
		<!-- END PAGE -->
	
	<!-- END CONTAINER -->
	<div class="tab-pane" id="tab_1_4">
			<div class="row-fluid">
               <div class="span12">
			   <?php 
			                        if(isset($result))
									{
										foreach($result as $row):
										$name=$row->name; 
										$proName =$row->proName;
										$descrip=$row->description;
										endforeach;
									    $uri=base_url().'General_setting/brand_update/'.$row->id;
									    $butoon="Update";
									}
									else 
									{
										$proName=null;
										$name=null;
										$descrip=null;
										$butoon="Save";
										$uri=base_url().'General_setting/adding_brand';
									}
									?>
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Add Brand</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo $uri; ?>" method="post" class="form-horizontal">
                         
						<div class="control-group">
                              <label class="control-label">Company</label>
                               <div class="controls">
                                 <select class="m-wrap chosen-with-diselect span6" data-required="1"  name="proName">
								 <?php foreach($resultPro as $rowPro):?>
								       <option   value=""></option>
                                       <option   <?php if($proName==$rowPro->id): echo 'selected'; endif;?> value="<?php echo $rowPro->id;?>"><?php echo $rowPro->company_name;?></option>
                                <?php  endforeach;?>               
                                 </select>
                                 <span class="help-inline"></span>
                              </div>
                        </div>			
						   
						   <div class="control-group">
                              <label class="control-label">Name</label>
                               <div class="controls">
                                 <input type="text" id="myPlaceTextBox1" class="span6 m-wrap" placeholder="Brand Name" name="cityName" value="<?php echo substr($name,0,5);?>"/>
                                 
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label">Description</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" placeholder="Brand Description" name="cityDesc"><?php echo substr($descrip,0,10);?></textarea>
                              </div>
                           </div>
                           <div class="form-actions">
                              <button type="submit" class="btn blue"><?php echo $butoon;?></button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
			</div>
			<style>
 #map_canvas
 {
	 width:49% !important;
	 height:100px !important;
 }
 </style>
												</div>
	<?php include_once('includes/footer.php');?>