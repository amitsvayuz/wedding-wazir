<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN STYLE CUSTOMIZER -->
						<!--<div class="color-panel hidden-phone">
							<div class="color-mode-icons icon-color"></div>
							<div class="color-mode-icons icon-color-close"></div>
							<div class="color-mode">
								<p>THEME COLOR</p>
								<ul class="inline">
									<li class="color-black current color-default" data-style="default"></li>
									<li class="color-blue" data-style="blue"></li>
									<li class="color-brown" data-style="brown"></li>
									<li class="color-purple" data-style="purple"></li>
									<li class="color-white color-light" data-style="light"></li>
								</ul>
								<label class="hidden-phone">
								<input type="checkbox" class="header" checked value="" />
								<span class="color-mode-label">Fixed Header</span>
								</label>							
							</div>
						</div>-->
						<!-- END BEGIN STYLE CUSTOMIZER -->   	
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
						<h3 class="page-title">
							City	
                            <small>Manage City</small>							
							
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url()?>sp_manager/dashboard">Dashboard</a>
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="<?php echo base_url()?>General_setting/list_city">City</a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<?php  if(@$_REQUEST['set']):?>
								   <div class="alert alert-success">
									<button class="close" data-dismiss="alert"></button>
									<span><?php echo base64_decode(@$_REQUEST['set']);?>
								    </div>
									<?php endif;?>
									
									<div class="row-fluid"style="margin-bottom:2%">
									<div class="btn-group" >
										<a href="<?php echo base_url();?>General_setting/add_city"><button id="sample_editable_1_new" class="btn green">
										Add City <i class="icon-plus"></i>
										</button></a>
									</div>
									
								    </div>

			<!--------------------------------------new row7-------------------------------------->
			<div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>City Setting </h4>
                        <div class="tools">
                           
                        </div>
                     </div>
                    <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						
							<div class="portlet-body">
								
								<table class="table table-striped table-bordered table-hover" colspan="2" id="sample_1">
									<thead>
										<tr >
											
											<th class="hidden-480">City name</th>
											<th class="hidden-480">City Description</th>
											<th class="hidden-480">Action</th>
											
										</tr>
									</thead>
									<tbody>
									
									<?php 
											foreach($result as $row):
											
											
											?>
										
										<tr class="odd gradeX">
											
											<td><?php echo $row->name;
											?></td>
											
											<td class="center hidden-480"><?php echo $row->description;
											?></td>
											
											<td>	
			
										<a href="<?php echo base_url();?>General_setting/edit_city/<?php echo $row->id;?>"><i class="icon-edit"></i></a> | 
										<a onclick="confirm('Are you sure delete?');" href="<?php echo base_url();?>General_setting/delete_city/<?php echo $row->id;?>"><i class="icon-trash"></i></a>
									    </td>
										</tr>
									
									<?php endforeach;?>
									</tbody>
								</table>
				
			
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
                  </div>
                  <!-- END PORTLET-->
               </div>
            </div>
			</div>
			
	<?php include_once('includes/footer.php');?>