<?php include_once('includes/header.php');?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<?php include_once('includes/sidebar.php');?>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div id="portlet-config" class="modal hide">
				<div class="modal-header">
					<button data-dismiss="modal" class="close" type="button"></button>
					<h3>Widget Settings</h3>
				</div>
				<div class="modal-body">
					<p>Here will be a configuration form</p>
				</div>
			</div>
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
									
						<h3 class="page-title">
							Add Location
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url()?>sp_manager/dashboard">Dashboard</a>
								<i class="icon-angle-right"></i>
							</li>
							
							<li><a href="<?php echo base_url()?>General_setting/location">Location</a></li>
							<i class="icon-angle-right"></i>
							<li><a href="<?php echo base_url()?>General_setting/add_view">Add Location</a>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<?php 
				if($resultLoc):
				
				foreach($resultLoc as $rowloc):
			    $loc_id = $rowloc->loc_id;
				$city_id = $rowloc->city_id;
				$location = $rowloc->location;
				$loc_desc = $rowloc->loc_desc;
				$url =base_url().'General_setting/update_location/'.$loc_id;
				$button ='Update';
				endforeach;
				else:
				$loc_id = '';
				$city_id = '';
				$location = '';
				$loc_desc = '';
				$url =base_url().'General_setting/add_location';
				$button ='Save';
				endif;
				
				?>
		<!-- END PAGE -->
	
	<!-- END CONTAINER -->
	<div class="tab-pane" id="tab_1_4">
			<div class="row-fluid">
               <div class="span12">
			     <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Add Location</h4>
                        
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action='<?php echo $url;?>' method="post" class="form-horizontal">
                         
							<div class="control-group">
                              <label class="control-label">City :</label>
                               <div class="controls">
                                 <select class="m-wrap chosen-with-diselect span6" data-required="1"  name="locCity">
								 <?php foreach($resultCity as $rowPro):?>
								       <option   value=""></option>
                                       <option   <?php if($city_id==$rowPro->c_id): echo 'selected'; endif;?> value="<?php echo $rowPro->c_id;?>"><?php echo $rowPro->company_name;?></option>
                                <?php  endforeach;?> 
              
			                    
                                 </select>
								 <span class="help-inline" style="color:red;"><?php echo form_error('locCity');?></span>
                               </div>
                           </div>			
						   
						   <div class="control-group">
                              <label class="control-label"> Name :</label>
                               <div class="controls">
                                 <input type="text"  id="myPlaceTextBox" class="span6 m-wrap" placeholder="Location  Name" name="locName" value="<?php echo $location;?>"/>
								  <span class="help-inline" style="color:red;"><?php echo form_error('locName');?></span>
                                 <?php echo $map['html']; ?>
								
							   </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label">Description:</label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3"  placeholder="location Description" name="locDesc"><?php echo $loc_desc;?></textarea>
								 <span class="help-inline" style="color:red;"><?php echo form_error('locDesc');?></span>
                              </div>
                           </div>
                           <div class="form-actions">
                              <button type="submit" class="btn blue"><?php echo $button;?></button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
			</div>
			</div>
			<style>
			 #map_canvas
			 {
				 width:49% !important;
				 height:100px !important;
			 }
			</style>
												</div>
	<?php include_once('includes/footer.php');?>