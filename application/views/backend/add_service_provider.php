<?php include_once('includes/header.php');?>
   <!-- BEGIN CONTAINER -->
   <div class="page-container row-fluid">
    <?php include_once('includes/sidebar.php');?>
      <!-- BEGIN PAGE -->  
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <div id="portlet-config" class="modal hide">
            <div class="modal-header">
               <button data-dismiss="modal" class="close" type="button"></button>
               <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
               <p>Here will be a configuration form</p>
            </div>
         </div>
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->   
            <div class="row-fluid">
               <div class="span12">
                  <!-- BEGIN STYLE CUSTOMIZER -->
                  <!--<div class="color-panel hidden-phone">
                     <div class="color-mode-icons icon-color"></div>
                     <div class="color-mode-icons icon-color-close"></div>
                     <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                           <li class="color-black current color-default" data-style="default"></li>
                           <li class="color-blue" data-style="blue"></li>
                           <li class="color-brown" data-style="brown"></li>
                           <li class="color-purple" data-style="purple"></li>
                           <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                        <input type="checkbox" class="header" checked value="" />
                        <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                     </div>
                  </div>-->
                  <!-- END BEGIN STYLE CUSTOMIZER -->   
                  <h3 class="page-title">
                     Service
                   </h3>
                  <ul class="breadcrumb">
                     <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url();?>sp_manager/dashboard">Dashboard</a> 
                        <span class="icon-angle-right"></span>
                     </li>
                     <li>
                        <a href="<?php echo base_url();?>admin/create_user">Service</a>
                        <span class="icon-angle-right"></span>
                     </li>
                    
                  </ul>
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">
                <?php if($this->session->userdata('message')){?>
						  <div class="alert alert-success">
							<button class="close" data-dismiss="alert"></button>
						   <span style='color:green;'>
                                                <?php echo $this->session->userdata('message');?>            
                                                </span>
						  </div>
						<?php } ?>
                  <!-- BEGIN SAMPLE FORM PORTLET-->   
                  <div class="portlet box light-grey">
                     <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>Service Provider</h4>
                      
                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form id="register-form" action="<?php echo base_url();?>admin/create_user" method="post" class="form-horizontal">
						
						    <div class="control-group">
                              <label class="control-label"> Service :</label>
                              <div class="controls">
                                 <select class="span4 chosen" data-placeholder="Choose a Category" required id="role" name="role" tabindex="1">
									  <option value="none">Select Service</option>
	 <option  <?php if($role=='venue') {echo 'selected';}?> value="venue">Venue</option>
	 <option  value="planner" <?php if($role=='planner') {echo 'selected';}?>>Wedding</option>
	 <option <?php if($role=='decorator') {echo 'selected';}?> value="decorator">Decorater</option>
	 <option   value="photographer" <?php if($role=='photographer') {echo 'selected';}?>>Photographer</option>
                                 </select>
				            </div>
							<h6 style="color:#D51D59"><?php echo form_error('role'); ?></h6>
                           </div>
						   
						   <div class="control-group">
                              <label class="control-label">Type :</label>
                              <div class="controls">
                                 <label class="radio">
                                <input type="radio" name="optionsRadios1" id="option1" value="option1" />
								Company
                                 </label>
                                 <label class="radio">
                                   <input type="radio" name="optionsRadios1" id="option2" value="option2" checked />
									Freelancer
                                 </label>  
                              </div>
                           </div>
						   
                           <div class="control-group" id="comp" style="display:none">
                              <label class="control-label">Company:</label>
                              <div class="controls">
                                 <input type="text"  placeholder="Company Name" name="companyname" required class="span4 m-wrap" />
								 <h6 style="color:#D51D59"><?php echo form_error('companyname'); ?></h6>
                              </div>
                           </div>
                        
						  <div class="control-group">
                              <label class="control-label">Email  :</label>
                              <div class="controls">
                                    <input class="span4 m-wrap" id="email" type="email" placeholder="Email" required name="email"/>   
									<h6 style="color:#D51D59"><?php echo form_error('email'); ?></h6>									
                              </div>
                           </div>
						   
						   
						   
                         
                          
                          <div class="control-group">
                              <label class="control-label">Name :</label>
                              <div class="controls">
                                 <input class="span4 m-wrap" type="text" placeholder="Name" required name="username"/>    
									<h6 style="color:#D51D59"><?php echo form_error('username'); ?></h6>
                                 </div>
                           </div>
						   
						   
                           <div class="control-group">
                              <label class="control-label">Contact :</label>
                              <div class="controls">
                                 <input class="span4 m-wrap" type="text" id="password" placeholder="+91" required name="contact_no"/>
                                 <h6 style="color:#D51D59"><?php echo form_error('password'); ?></h6>
                              </div>
                           </div>
                    
                            <div class="control-group">
                              <label class="control-label"> Subscription To :</label>
                              <div class="controls">
                                 <select class="span4 chosen" data-placeholder="Choose a Category" required  name="recharge" tabindex="1">
	              <option value="none">Select Subscription Value</option>
                   <?php foreach($result as $recharge) { ?>
                    <option value="<?php echo $recharge->percentage;?>"><?php echo $recharge->setting_name;?></option>
	           <?php } ?>
                  </select>
	 </div>
		<h6 style="color:#D51D59"><?php echo form_error('role'); ?></h6>
         </div>
						   
                           <div class="form-actions">
                              <button type="submit" id="submit" class="btn blue">Submit</button>
                              <button type="button" class="btn">Cancel</button>
                           </div>
                        </form>
                        <!-- END FORM-->           
                     </div>
                  </div>
                  <!-- END SAMPLE FORM PORTLET-->
               </div>
            </div>
            <!-- END PAGE CONTENT-->         
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->  
   </div>
   <!-- END CONTAINER -->
  
<?php include_once('includes/footer.php');?>
  <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

 <script>
   // When the browser is ready...
  $(function() {
  
    // Setup form validation on the #register-form element
    $("#register-form").validate({
    
        // Specify the validation rules
        rules: {
            companyname: "required",
            username: "required",
		    email: {
                required: true,
                email: true
            },
            contact_no: {
                required: true,
                minlength: 10,
				number: true,
			    maxlength: 10,
            },
		},
        
        // Specify the validation error messages
        messages: {
            companyname: "Please enter company name",
            username: "Please enter your user name",
            password: {
		number: "Please enter only digits only",
                required: "Please provide a contact number",
                minlength: "Your contact number must be at least 10 digits long"
            },
            email: "Invalid email address",
		    
        },
       
    });
    
  });
  
  </script>
 
   
   
   
   
   </script>

  <script>
    jQuery(document).ready(function() {     
      $('#register-back-btn').click(function(){
		  window.location='<?php echo base_url();?>';
	  });
	  $('#option1').click(function(){
		 $('#comp').show();
	  });
	  $('#option2').click(function(){
		 $('#comp').hide();
	  });
    });
  </script>

	
	 <script>
    $(document).ready(function() { 
		$("#email").keyup(function(){	
		var email = $("#email").val();
		var dataString = 'email=' + email;
		var base_url = "<?php echo base_url();?>";
		
			$.ajax({
				type: 'POST',
				url: base_url + 'admin/email_exists',
				data: dataString,
				success: function(result){
				   if(result == 1){
					   alert('Email Id already exists');
				   }
				}});
			return false;
		 });
    });
  </script>