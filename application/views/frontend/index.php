<?php include('include/header.php');?>

    <!-- begin #home -->
    <div id="home" class="content bg home">
        <!-- begin content-bg -->
        <div class="content-bg parallax-window" data-parallax="scroll" data-image-src="<?php echo base_url();?>assets/frontend/img/13-Christian-Katie-Hands-Rydgren.jpg" data-speed="0.5"></div>
        <!-- end content-bg -->
        <!-- begin container -->
       <div class="container home-content"><br/><br/><br/>
            <h1>Welcome to Wedding wazir</h1>
            <h3>Search the best wedding vendors</h3>
           <!-- <a href="#about" data-click="scroll-to-target" class="btn btn-theme">Learn more</a>
            <a href="#contact" data-click="scroll-to-target" class="btn btn-outline">Contact Us</a><br>
            <br>
            You can <a href="javascript:;" class="subscribe">subscribe</a> to our newsletter-->
        </div>
        <!-- end container -->
    </div>
    <!-- end #home -->

    <!-- begin #about -->
    <div id="about" class="content" data-scrollview="true">
        <!-- begin container -->
        <div class="container" data-animation="true" data-animation-type="fadeInDown">
            <h2 class="content-title" data-animation="true" data-animation-type="fadeInDown">About Us</h2>
            <p class="content-desc" data-animation="true" data-animation-type="fadeInDown">
               <i>Our vision is to use technology and smart processes to structure the highly unorganized <br/>services market in india.</i>
            </p>
            <!-- begin row -->
            <div class="row">
             <h3>Our Story</h3>
                <!-- begin col-6 -->
                <div class="col-md-6 no-pad" data-animation="true" data-animation-type="fadeInLeft">
                    <!-- begin about -->
                    <div class="about">
                       
                        <p>
                           We are a team of seasoned, professional wedding planners from around the world who have joined forces to provide our brides with a more comprehensive and personalized wedding solution to their destination wedding planning. For too many brides interested in a destination wedding, their only resource until now has been to work with a travel agent selling them a location and an overworked onsite wedding coordinator at one of the myriad resorts that dot the landscape today.

                        </p>
                        <p>
                           Some brides are only interested in the destination and the wedding itself is less important. For them, an assembly line wedding works very well. But for our bride who has a very different vision for her wedding, one that's anything but cookie cutter and one size fits all, we're a perfect fit. 
                        </p>
                        <p>
                            Our bride is interested in every aspect of her wedding and she wants a partner by her side that's as invested in her wedding as she is. At Extraordinary Destination Weddings, it's all about the details. It might involve incorporating a treasured family heirloom into your wedding mix, or it might be as simple as adding an element of surprise to delight your g uests at some point during the evening. 
                        </p>
                    </div>
                    <!-- end about -->
                </div>
                <!-- end col-6 -->
                <!-- begin col-6 -->
                <div class="col-md-6" data-animation="true" data-animation-type="fadeInRight">
                   <!-- begin image -->
                    <p><img src="<?php echo base_url();?>assets/frontend/img/home-bg.jpg" class="img-responsive"/></p>
                    <!-- end image -->
                </div>
                <!-- end col-6 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end #about -->

    <!-- begin #venues -->
    <div id="quote" class="content bg" data-scrollview="true">
        <!-- begin content-bg -->
        <div class="content-bg parallax-window" data-parallax="scroll" data-image-src="<?php echo base_url();?>assets/frontend/img/slide_hero2.jpg" data-speed="0.5"></div>
        <!-- end content-bg -->
        <!-- begin container -->
        <div class="container" data-animation="true" data-animation-type="fadeIn">
            <!-- begin row -->
            <div class="row">
                <!-- begin col-12 -->
                <div class="col-md-12 quote post-footer">
                  <h2><b> FIND YOUR WEDDING VENUE</b></h2>
                  <h4 style="color:#fff">  <b>Check Availability & Pricing at the click of a button</b></h4>
                  <a href="#" class="label">BROWSE VENUES</a>
				 </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end #venues -->

    <!-- beign #service -->
    
    <!-- end #about -->

    <!-- beign #service -->
    <div id="portfolio" class="content" data-scrollview="true">
        <!-- begin container -->
        <div class="container" data-animation="true" data-animation-type="fadeInDown">
            <h2 class="content-title">Our Services</h2>
            <p class="content-desc">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum consectetur eros dolor,<br>
                sed bibendum turpis luctus eget
            </p>

            <!--<div class="btn-filter-group">
                <div class="btn btn-filter btn-active" data-filter="all">All</div>
                <div class="btn btn-filter" data-filter="1">Front End</div>
                <div class="btn btn-filter" data-filter="2">Programming</div>
                <div class="btn btn-filter" data-filter="3">Web Design</div>
                <div class="btn btn-filter" data-filter="4">Database Integration</div>
            </div>-->

            <div class="row row-space-10 filtr-container">
                <!-- begin col-4 -->
                <div class="filtr-item col-md-4 col-xs-6" data-category="1" data-sort="value">
                    <!-- begin portfolio -->
                    <div class="portfolio">
                        <div class="image">
                            <a href="#"><img src="<?php echo base_url();?>assets\frontend\img\photographer.jpg" alt="photographer"></a>
                        </div>
                        <div class="desc">
                            <span class="desc-title">POGRAPHER</span>
                            <span class="desc-text">Lorem ipsum dolor sit amet</span>
                        </div>
                    </div>
                    <!-- end portfolio -->
                </div>
                <!-- end col-4 -->
                <!-- begin col-4 -->
                <div class="filtr-item col-md-4 col-xs-6" data-category="2" data-sort="value">
                    <!-- begin portfolio -->
                    <div class="portfolio">
                        <div class="image">
                            <a href="#"><img src="<?php echo base_url();?>assets\frontend\img\venue2.jpg" alt="venue2"></a>
                        </div>
                        <div class="desc">
                            <span class="desc-title">WEDDING PLANNER</span>
                            <span class="desc-text">Lorem ipsum dolor sit amet</span>
                        </div>
                    </div>
                    <!-- end portfolio -->
                </div>
                <!-- end col-4 -->
                <!-- begin col-4 -->
                <div class="filtr-item col-md-4 col-xs-6" data-category="3" data-sort="value">
                    <!-- begin portfolio -->
                    <div class="portfolio">
                        <div class="image">
                            <a href="#"><img src="<?php echo base_url();?>assets\frontend\img\wplanner.jpg" alt="wplanner"></a>
                        </div>
                        <div class="desc">
                            <span class="desc-title">DECORATORS</span>
                            <span class="desc-text">Lorem ipsum dolor sit amet</span>
                        </div>
                    </div>
                    <!-- end portfolio -->
                </div>
           
                
                <!-- end col-4 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end #services -->

    <!--begin #team-->
    
     <div id="client" class="content bg" data-scrollview="true">

        <!-- begin content-bg -->

        <div class="content-bg">

            <img src="assets\frontend\img\client-bg.jpg" alt="">

        </div>

        <!-- end content-bg -->

        <!-- begin container -->

        <div class="container" data-animation="true" data-animation-type="fadeInUp">

            <h2 class="content-title">Happy Client Testimonials</h2>

            <!-- begin carousel -->

            <div id="testimonials" class="carousel testimonials slide" data-ride="carousel">

                <!-- begin carousel-inner -->

                <div class="carousel-inner text-center">

                    <!-- begin item -->

                    <div class="item active">

                        <blockquote>

                            <i class="fa fa-quote-left"></i>

                           Very good service. I will give 5 star rating on every single parameter. I wanted to book the venue in three hours and wedding wazir was super prompt and made it possible.

                            <i class="fa fa-quote-right"></i>

                        </blockquote>

                        <div class="name">Amit Shah, East Delhi</div>

                    </div>

                    <!-- end item -->

                    <!-- begin item -->

                    <div class="item">

                        <blockquote>

                            <i class="fa fa-quote-left"></i>

                            I am really happy and thankful for your excellent support and superb follow up. Because of you guys, I got a fantastic venue, orchestra, photographer, etc at a reasonable price for my daughters engagement. For any assistance in future, I will be more than happy to contact you guys. Thank you all!

                            <i class="fa fa-quote-right"></i>

                        </blockquote>

                        <div class="name">shalini Mathur, West Delhi</div>

                    </div>

                    <!-- end item -->

                    <!-- begin item -->

                    <div class="item">

                        <blockquote>

                            <i class="fa fa-quote-left"></i>

                            I must say you and your team had been a great help to me throughout the process of finding a suitable banquet for my wedding. And am really thankful to you for all the effort, time and patience you have put up in the whole process. You went out of the way,to stick to my search and demands as and when required. Thank you again.

                            <i class="fa fa-quote-right"></i>

                        </blockquote>

                        <div class="name">Swapnil Patil, South Delhi</div>

                    </div>

                    <!-- end item -->

                </div>

                <!-- end carousel-inner -->

                <!-- begin carousel-indicators -->

                <ol class="carousel-indicators">

                    <li data-target="#testimonials" data-slide-to="0" class="active"></li>

                    <li data-target="#testimonials" data-slide-to="1" class=""></li>

                    <li data-target="#testimonials" data-slide-to="2" class=""></li>

                </ol>

                <!-- end carousel-indicators -->

            </div>

            <!-- end carousel -->

        </div>

        <!-- end containter -->

    </div>
    
    <!-- end #pricing -->

    <!-- begin #contact -->
    <div id="contact" class="content" data-scrollview="true">
        <!-- begin container -->
        <div class="container">
            <h2 class="content-title">Have Any Questions? Contact Us</h2>
            <p class="content-desc">
               Want to say hello? Want to know more about us? Give us a call or drop us an email and we will get back to you as soon as we can.


            </p>
            <!-- begin row -->
            <div class="row">
                <!-- begin col-6 -->
                <div class="col-md-8 form-col" data-animation="true" data-animation-type="fadeInRight">
                    <form class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <label class="control-label">Name <span class="text-theme">*</span></label>
                                    <input type="text" class="form-control" required="">
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">Email <span class="text-theme">*</span></label>
                                    <input type="email" class="form-control" required="">
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">Website</label>
                                    <input type="url" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="control-label">Comment <span class="text-theme">*</span></label>
                                    <textarea class="form-control" rows="10" required=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-theme">Post Comment</button>
                                </div>
                            </div>
                        </form>
                </div>
                
                
                <div class="col-md-4" data-animation="true" data-animation-type="fadeInLeft">
                  <br/>
                   <p>
                        <strong>L-3, FF, Bhagat Singh Building,  <br>
                     Street# 1,  Vasant Kunj Road, Mahipalpur Ext,<br>
                        New Delhi,, India 110037<br>
                       Mob: (+91) 9811 949 412</strong><br>
                    </p>
                    <p>Email: 
                        
                        <a href="mailto:mail@emailaddress.com">Mukulmathur1779@gmail.com</a>
                    </p>
                </div>
                <!-- end col-6 -->
                <!-- begin col-6 -->
                
                <!-- end col-6 -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end #contact -->

    <?php include('include/footer.php');?>