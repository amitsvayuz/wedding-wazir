<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Wedding Wazir </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description">
    <meta content="" name="author">


    <!--  BEGIN CSS STYLE  -->
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/frontend/plugins/bootstrap/css\bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/frontend\plugins\font-awesome\css\font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/frontend/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/frontend/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/frontend/css/panel.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/frontend/css/style-responsive.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/frontend/css/theme/default.css" id="theme" rel="stylesheet">
    
    <link rel="icon" type="image/png" href="assets/frontend/img/favicon.png">
    
    
    
    <!--  END CSS STYLE  -->
</head>
<body data-spy="scroll" data-target="#header-navbar" data-offset="51">

<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- begin #header -->
    <div id="header" class="header navbar navbar-fixed-top">
        <!-- begin container -->
        <div class="container">
            <!-- begin navbar-header -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index-1.htm" class="navbar-brand">
                    <span class="brand-text">
                      <img s src="assets/img/logo.png" class="img-responsive"/>
                    </span>
                </a>
            </div>
            <!-- end navbar-header -->
            <!-- begin navbar-collapse -->
            <div class="collapse navbar-collapse" id="header-navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#home" data-click="scroll-to-target">HOME</a> </li>
                    <li><a href="#about" data-click="scroll-to-target">ABOUT</a></li>
                  <!--  <li><a href="#service" data-click="scroll-to-target">SERVICES</a></li>-->
                    
                    <li><a href="#portfolio" data-click="scroll-to-target">SERVICES</a></li>
                    <li><a href="#client" data-click="scroll-to-target">TESTIMONIAL</a></li>
                   
                    <li><a href="#contact" data-click="scroll-to-target">CONTACT</a></li>
                   
                </ul>
            </div>
            <!-- end navbar-collapse -->
        </div>
        <!-- end container -->
    </div>
    <!-- end #header -->