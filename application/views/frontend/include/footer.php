<!-- begin #footer -->
    <div id="footer" class="footer">

        <div class="container">

        <div class="col-md-5 text-left"> Copyright © 2017, Wedding Wazir. All Rights Reserved </div>

            <div class="col-md-4 social-list pull-left">

                <a href="#"><i class="fa fa-facebook fa-fw"></i></a>

                <a href="#"><i class="fa fa-instagram fa-fw"></i></a>

                <a href="#"><i class="fa fa-twitter fa-fw"></i></a>

                <a href="#"><i class="fa fa-google-plus fa-fw"></i></a>

                <a href="#"><i class="fa fa-dribbble fa-fw"></i></a>

            </div>            

            

            <div class="col-md-3 text-right"> Technology Partner <a href="http://www.vayuz.com/">VAYUZ</a> </div>

        </div>

    </div>
    <!-- end #footer -->

    <div class="alert alert-dismissible fade" role="alert" data-action="subscribe">
        <strong>Holy guacamole!</strong> Best check yo self, you're not looking too good.
    </div>

    <!-- begin theme-settings -->
    
    <!-- end theme-settings -->
</div>
<!-- end #page-container -->

<!-- BEGIN JS  -->
<script src="<?php echo base_url();?>assets\frontend\plugins\jquery\jquery-1.12.4.min.js"></script>
<script src="<?php echo base_url();?>assets\frontend\plugins\bootstrap\js\bootstrap.min.js"></script>
<!--[if lt IE 9]>
<script src="assets/crossbrowserjs/html5shiv.js"></script>
<script src="assets/crossbrowserjs/respond.min.js"></script>
<script src="assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url();?>assets\frontend\plugins\scrollMonitor\scrollMonitor.js"></script>
<script src="<?php echo base_url();?>assets\frontend\plugins\parallax\parallax.min.js"></script>
<script src="<?php echo base_url();?>assets\frontend\plugins\imagesloaded\imagesloaded.min.js"></script>
<script src="<?php echo base_url();?>assets\frontend\plugins\filterizr\dist\jquery.filterizr.min.js"></script>
<script src="<?php echo base_url();?>assets\frontend\js\apps.min.js"></script>
<!--  END JS  -->

</body>
</html>