<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data['title']="Sign In";
		$this->load->view('backend/login',$data);
	}
/****************Dashboard view****************/	
	public function dashboard()
	{
		$data['title']="Dashboard";
		$this->load->view('backend/index',$data);
	}
/****************Registration view****************/
	public function registration()
	{
		$data['title']="Sign Up";
		$this->load->view('backend/registration',$data);
	}
/****************Forgot password view****************/	
	public function forgetpassword()
	{
		$data['title']="Forget Password";
		$this->load->view('backend/forgetpassword',$data);
	}
	public function cms()
	{
		$data['title']="CMS";
		$this->load->view('backend/cms',$data);
	}
	/*****************Decorator starts here******************/
	public function decorator()
	{
		$data['title']="Forget Password";
		$this->load->view('backend/decorator',$data);
	}
	/*****************Decorator Ends here******************/
	
}
