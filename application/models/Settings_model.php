<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Settings_model extends CI_model
{
	public function inser_setting($idfr,$names,$descs)
	{
		$arrSetting=array(
	         'name'=>$names,
			 'description'=>$descs,
			'icon'=>'iicon',
			 'status'=>0,		           
	   );
	 switch($idfr){
		 case 1:
		 return $this->db->insert('ww_arrangement',$arrSetting);
		 break;
		  case 2:
		 return $this->db->insert('ww_photographer_service',$arrSetting);
		 break;
		  case 3:
		 return $this->db->insert('ww_photography_resolution',$arrSetting);
		 break;
		  case 4:
		 return $this->db->insert('ww_photography_sharing',$arrSetting);
		 break;
		  case 5:
		 return $this->db->insert('ww_photography_style',$arrSetting);
		 break;
		  case 6:
		 return $this->db->insert('ww_property_preference',$arrSetting);
		 break;
		  case 7:
		 return $this->db->insert('ww_venue_preference',$arrSetting);
		 break;
		  case 8:
		  $arrSetting=array(
	  'name'=>$names,
	  'description'=>$descs,
	  'status'=>0,	
	 );
		 return $this->db->insert('ww_event',$arrSetting);
		 break;
	}
}
	public function get_view($id){
		switch($id){
			case 1:
		 $q = $this->db->get("ww_venue_preference");
			 break;
			 case 2:
		 $q = $this->db->get("ww_arrangement");
			 break;
			 case 3:
		 $q = $this->db->get("ww_event");
			 break;
			 case 4:
		 $q = $this->db->get("ww_photographer_service");
			 break;
			 case 5:
		 $q = $this->db->get("ww_photography_resolution");
			 break;
			 case 6:
		 $q = $this->db->get("ww_photography_style");
			 break;
			 case 7:
		 $q = $this->db->get("ww_photography_sharing");
			 break;
			 case 8:
		 $q = $this->db->get("ww_property_preference");
			 break;
		}
		 if($q->num_rows() > 0)
				{
					return $q->result();
				}
			   return array();
	}

	public function delete_row($id,$idfr){
	  $this->db->where('id', $id);
	switch($idfr){
   	case 1:
	   return $this->db->delete('ww_venue_preference'); 
	   break;
	   case 2:
	   return $this->db->delete('ww_arrangement'); 
	   break;
	   case 3:
	   return $this->db->delete('ww_event'); 
	   break;
	   case 4:
	   return $this->db->delete('ww_photographer_service'); 
	   break;
	   case 5:
	   return $this->db->delete('ww_photography_resolution'); 
	   break;
	   case 6:
	   return $this->db->delete('ww_photography_style'); 
	   break;
	   case 7:
	   return $this->db->delete('ww_photography_sharing'); 
	   break;
	   case 8:
	   return $this->db->delete('ww_property_preference'); 
	   break;
		}
	}
	
	public function get_viewid($id,$idfr){
		$this->db->where('id', $id);
		switch($idfr){
			case 1:
			$q = $this->db->get("ww_venue_preference");
			 break;
			 case 2:
			$q = $this->db->get("ww_arrangement");
			 break;
			 case 3:
			$q = $this->db->get("ww_event");
			 break;
			 case 4:
			$q = $this->db->get("ww_photographer_service");
			 break;
			 case 5:
			$q = $this->db->get("ww_photography_resolution");
			 break;
			 case 6:
			$q = $this->db->get("ww_photography_style");
			 break;
			 case 7:
			$q = $this->db->get("ww_photography_sharing");
			 break;
			 case 8:
			$q = $this->db->get("ww_property_preference");
			 break;
		}
		return $q->result();
	}
	
public function inser_update($ide,$idfr,$names,$descs){
	$arrSetting=array(
		 'name'=>$names,
		 'description'=>$descs,
		 'icon'=>'iicon',
		 'status'=>0,	
		);
	$this->db->where('id', $ide);
	switch($idfr){
		case 1:
		 return $this->db->update('ww_venue_preference',$arrSetting);
		 break;
		 case 2:
		 return $this->db->update('ww_arrangement',$arrSetting);
		 break;
		 case 3:
		$arrSetting=array(
		'name'=>$names,
		'description'=>$descs,
		'status'=>0,	
		);
		 return $this->db->update('ww_event',$arrSetting);
		 break;
		  case 4:
		 return $this->db->update('ww_photographer_service',$arrSetting);
		 break;
		  case 5:
		 return $this->db->update('ww_photography_resolution',$arrSetting);
		 break;
		  case 6:
		 return $this->db->update('ww_photography_style',$arrSetting);
		 break;
		  case 7:
		 return $this->db->update('ww_photography_sharing',$arrSetting);
		 break;
		  case 8:
		 return $this->db->update('ww_property_preference',$arrSetting);
		 break;
		 }
   
	}
	
	public function get_city($id){
		$this->db->where('id', $id);
		$q = $this->db->get('city_setting');
		return $q->result();
    }
	
	 public function get_citya(){
		$q = $this->db->get('city_setting');
		return $q->result();
    }
	
	public function insert_city($namee,$desc){
    $arrcms=array(
		'name'=>$namee,
		'description'=>$desc,
	 );
    return $this->db->insert('city_setting',$arrcms);
	}
 
	public function update_city($id,$namee,$desc){
		$arrcms=array(
		'name'=>$namee,
		'description'=>$desc,
		 );
		$this->db->where('id', $id);
		return $this->db->update('city_setting', $arrcms);
	}
	
	public function delete1_city($id){
		$this->db->where('id', $id);
	   return $this->db->delete('city_setting'); 
	}
}
?>