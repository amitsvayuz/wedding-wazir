<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lead_Model extends CI_Model
{
   // Photographer by Ankit Singh
   function leads(){
	   $this->db->select('*');
	   $this->db->from('ww_lead_module');
	   $query = $this->db->get();
	   return $query->result();
   }
   
   function leads_details($id){
	   $this->db->select('*');
	   $this->db->where('lead_id',$id);
	   $this->db->from('ww_lead_module');
	   $query = $this->db->get();
	   return $query->row();
   }
   
    function leads_email_details($val){
	   $this->db->select('*');
	   $this->db->where('user_name',$val);
	   $this->db->from('ww_admin_login');
	   $query = $this->db->get();
	   return $query->row();
   }
   
    function leads_provider($type){
	   $this->db->select('*');
	   $this->db->where('role_type',$type);
	   $this->db->from('ww_admin_login');
	   $query = $this->db->get();
	   return $query->result();
   }
   
    function get_custom_leads($id){
	   $this->db->select('*');
	   $this->db->where('lead_id',$id);
	   $this->db->from('ww_lead_module');
	   $query = $this->db->get();
	   return $query->row();
   }
   
    function leads_delete($id){
	  $this->db->where('lead_id',$id);
	  $this->db->delete('ww_lead_module');
   }
   
    function add_custom_leads($leadArray){
	  $this->db->insert('ww_lead_module',$leadArray);
	  return $this->db->insert_id();
   }
   
}
?>
