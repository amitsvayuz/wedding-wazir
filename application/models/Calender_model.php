<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calender_Model extends CI_Model
{
   /*
     + Calendar Start Model
     + Open Event Managenet Function insert_slot();
   */

   function __construct()
    {
        // Call the Model constructor
        parent::__construct();

    }

	public function insert_slot($tableSlot,$arrSlot)
	{

		$result=$this->db->insert($tableSlot,$arrSlot);
		if($result==TRUE):
		$data = "Successful add slot";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
		redirect('calender/create_event?set='.$data);
		else:
		$data = "Something is wrong";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
		redirect('calender/create_event?set='.$data);
		endif;
	}

  public function insert_slot2($tableSlot,$arrSlot)
  {

    $result=$this->db->insert($tableSlot,$arrSlot);
    if($result==TRUE){
    $data = "success";}
    else{
    $data = "fail";
  }
    return $data;
  }

  public function update_slot2($tableSlot,$arrSlot,$id)
  {
    $this->db->where("slot_id",$id);
    $result=$this->db->update($tableSlot,$arrSlot);
    if($result==TRUE){
    $data = "success";}
    else{
    $data = "fail";
  }
    return $data;
  }


  public function update_slot_k($tableSlot,$arrSlot,$id)
  {
    $this->db->where("slot_id",$id);
    $result=$this->db->update($tableSlot,$arrSlot);
    if($result==TRUE){
    $data = "success";}
    else{
    $data = "fail";
  }
    return $data;
  }

	public function insert_schedule($tableSlot,$arrSchedule)
	{

		$result=$this->db->insert($tableSlot,$arrSchedule);
		if($result==TRUE):
		$data = "Successful add schedule";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
		redirect('calender/create_schedule');
		else:
		$data = "Something is wrong";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
		redirect('calender/create_schedule?set='.$data);
		endif;
	}

	public function update_schedule($tableSlot,$arrSchedule,$id)
	{
		$this->db->where('id',$id);
		$result=$this->db->update($tableSlot,$arrSchedule);
		if($result==TRUE):
		$data = "Successful update schedule";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
		redirect('calender/create_schedule');
		else:
		$data = "Something is wrong";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
		redirect('calender/create_schedule?set='.$data);
		endif;
	}

	public function delete_schedule($tableSlot,$id)
	{
		$this->db->where('id',$id);
		$result=$this->db->delete($tableSlot);
		if($result==TRUE):
		$data = "Successful delete schedule";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
		redirect('calender/create_schedule');
		else:
		$data = "Something is wrong";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
		redirect('calender/create_schedule?set='.$data);
		endif;
	}

	public function insert_special($tableSlot,$arrSchedule)
	{

		$result=$this->db->insert($tableSlot,$arrSchedule);
		if($result==TRUE):
		$data = "Successful add special day";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
		redirect('calender/create_special');
		else:
		$data = "Something is wrong";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
		redirect('calender/create_special');
		endif;
	}

	public function update_special($tableSlot,$arrSchedule,$id)
	{
		$this->db->where('id',$id);
		$result=$this->db->update($tableSlot,$arrSchedule);
		if($result==TRUE):
		$data = "Successful update special day";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
		redirect('calender/create_special');
		else:
		$data = "Something is wrong";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
		redirect('calender/create_special');
		endif;
	}

	public function delete_special($tableSlot,$id)
	{
		$this->db->where('id',$id);
		$result=$this->db->delete($tableSlot);
		if($result==TRUE):
		$data = "Successful delete special day";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
		redirect('calender/create_special');
		else:
		$data = "Something is wrong";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
		redirect('calender/create_special');
		endif;
	}

	public function insert_food($tableSlot,$arrSchedule)
	{

		$result=$this->db->insert($tableSlot,$arrSchedule);
		if($result==TRUE):
		$data = "Successful add food invite day";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
		redirect('calender/create_food');
		else:
		$data = "Something is wrong";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
		redirect('calender/create_food');
		endif;
	}



	public function update_food($tableSlot,$arrSchedule,$id)
	{
		$this->db->where('id',$id);
		$result=$this->db->update($tableSlot,$arrSchedule);
		if($result==TRUE):
		$data = "Successful update food invite day";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
		redirect('calender/create_food');
		else:
		$data = "Something is wrong";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
		redirect('calender/create_food');
		endif;
	}

	public function delete_food($tableSlot,$id)
	{
		$this->db->where('id',$id);
		$result=$this->db->delete($tableSlot);
		if($result==TRUE):
		$data = "Successful delete food invite day";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
		redirect('calender/create_food');
		else:
		$data = "Something is wrong";
		$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
		redirect('calender/create_food');
		endif;
	}

	public function check_date($tableSlot2,$start,$ftime,$ttime)
	{

        $where="SELECT * FROM `ww_cal_slot` WHERE (`fromTime` <= '$ftime' AND 'toTime' >= '$ftime') AND (`fromTime` <= '$ttime' AND 'toTime' >= '$ttime' ) AND `start` = '$start'";
		$query = $this->db->query($where);
		if($query->num_rows()):
		return 1;
		else:
		return 0;
		endif;
	}

	public function check_daterange($tableSlot1,$startd,$endd,$ftime,$ttime)
	{

		$where="SELECT * FROM `ww_cal_slot` WHERE (`fromTime` <= '$ftime' AND 'toTime' >= '$ftime') AND (`fromTime` <= '$ttime' AND 'toTime' >= '$ttime' ) AND `start` = '$start'";
		$query = $this->db->query($where);
		if($query->num_rows()):
		return 1;
		else:
		return 0;
		endif;
	}



	public function row_slot()
	{
		$this->db->where('type','EVT');
		$this->db->order_by("start", "desc");
		//$this->db->limit(3, 0);
		$query=$this->db->get('ww_cal_slot');
		if($query > 0):
		 return $query->result();
	    endif;
	}

	public function row_schedule()
	{
		$this->db->where('type','SCD');
		$this->db->order_by("start", "desc");
		//$this->db->limit(3, 0);
		$query=$this->db->get('ww_cal_slot');
		if($query > 0):
		 return $query->result();
	    endif;
	}

	public function row_food()
	{
		$this->db->where('type','FOD');
		$this->db->order_by("start", "desc");
		//$this->db->limit(3, 0);
		$query=$this->db->get('ww_cal_slot');
		if($query > 0):
		 return $query->result();
	    endif;
	}

	public function row_special()
	{
		$this->db->where('type','SPD');
		$this->db->order_by("start", "desc");
		//$this->db->limit(3, 0);
		$query=$this->db->get('ww_cal_slot');
		if($query > 0):
		 return $query->result();
	    endif;
	}

	public function get_slot($id)
	{
		$this->db->where("id",$id);
		$query=$this->db->get('ww_cal_slot');
		if($query > 0):
		 return $query->result();
	    endif;
	}

	public function get_goodById($id)
	{
		$this->db->where("id",$id);
		$query=$this->db->get('ww_cal_slot');
		if($query > 0):
		 return $query->result();
	    endif;
	}

    	public function get_special($id)
	{
		$this->db->where("id",$id);
		$query=$this->db->get('ww_cal_slot');
		if($query > 0):
		 return $query->result();
	    endif;
	}
	public function get_schedule($id)
	{
		$this->db->where("id",$id);
		$query=$this->db->get('ww_cal_slot');
		if($query > 0):
		 return $query->result();
	    endif;
	}

	public function update_slot($tableSlot,$arrSlot,$id)
	{
		$this->db->where("id",$id);
		$result=$this->db->update($tableSlot,$arrSlot);
		if($result==TRUE):
		$data = "Successful update slot";
		$data = base64_encode($data);
		redirect('calender/create_event?set='.$data);
		else:
		$data = "Something is wrong";
		$data = base64_encode($data);
		redirect('calender/create_event?set='.$data);
		endif;
	}


}
?>
