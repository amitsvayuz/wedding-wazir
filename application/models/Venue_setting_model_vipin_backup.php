<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Venue_setting_model extends CI_model
{
	public function get_venue($id){
		switch($id){
			case 1:
		 $q = $this->db->get("wedding_occasion_list");
			 break;
			 case 2:
		 $q = $this->db->get("nwedding_occasion_list");
			 break;
			 case 3:
		 $q = $this->db->get("nature_of_venue");
			 break;
			 case 4:
		 $q = $this->db->get("venue_type");
			 break;
			 case 5:
		 $q = $this->db->get("venue_type_other");
			 break;
			 case 6:
		 $q = $this->db->get("floor");
			 break;
			 case 7:
		 $q = $this->db->get("suite_type");
			 break;
			 case 8:
		 $q = $this->db->get("valet_facility");
			 break;
					 case 9:
		 $q = $this->db->get("seating_type");
			 break;
					 case 10:
		 $q = $this->db->get("other_service");
			 break;
		}
		 if($q->num_rows() > 0)
				{
					return $q->result();
				}
			   return array();
	}
	
	public function inser_venue($idfr,$names,$descs)
	{
		$arrSetting=array(
	    'name'=>$names,
	    'description'=>$descs,
        );
		 switch($idfr){
			 case 1:
			 return $this->db->insert('wedding_occasion_list',$arrSetting);
			 break;
			  case 2:
			 return $this->db->insert('nwedding_occasion_list',$arrSetting);
			 break;
			  case 3:
			 return $this->db->insert('nature_of_venue',$arrSetting);
			 break;
			  case 4:
			 return $this->db->insert('venue_type',$arrSetting);
			 break;
			  case 5:
			 return $this->db->insert('venue_type_other',$arrSetting);
			 break;
			  case 6:
			 return $this->db->insert('floor',$arrSetting);
			 break;
			  case 7:
			 return $this->db->insert('suite_type',$arrSetting);
			 break;
			  case 8:
			 return $this->db->insert('valet_facility',$arrSetting);
			 break;
									  case 9:
			 return $this->db->insert('seating_type',$arrSetting);
			 break;
									  case 10:
			 return $this->db->insert('other_service',$arrSetting);
			 break;
			 
		 }
	}
	
	public function delete_row($id,$idfr){
		$this->db->where('id', $id);
		switch($idfr){
			case 1:
	   return $this->db->delete('wedding_occasion_list'); 
	   break;
	   case 2:
	   return $this->db->delete('nwedding_occasion_list'); 
	   break;
	   case 3:
	   return $this->db->delete('nature_of_venue'); 
	   break;
	   case 4:
	   return $this->db->delete('venue_type'); 
	   break;
	   case 5:
	   return $this->db->delete('venue_type_other'); 
	   break;
	   case 6:
	   return $this->db->delete('floor'); 
	   break;
	   case 7:
	   return $this->db->delete('suite_type'); 
	   break;
	   case 8:
	   return $this->db->delete('valet_facility'); 
	   break;
	   case 9:
	   return $this->db->delete('seating_type'); 
	   break;
	   case 10:
	   return $this->db->delete('other_service'); 
	   break;
		
		
		}
	}

	public function get_venueid($id,$idfr){
	 
		$this->db->where('id', $id);
		switch($idfr){
			case 1:
		 $q = $this->db->get("wedding_occasion_list");
			 break;
			 case 2:
		 $q = $this->db->get("nwedding_occasion_list");
			 break;
			 case 3:
		 $q = $this->db->get("nature_of_venue");
			 break;
			 case 4:
		 $q = $this->db->get("venue_type");
			 break;
			 case 5:
		 $q = $this->db->get("venue_type_other");
			 break;
			 case 6:
		 $q = $this->db->get("floor");
			 break;
			 case 7:
		 $q = $this->db->get("suite_type");
			 break;
			 case 8:
		 $q = $this->db->get("valet_facility");
			 break;
				case 9:
		 $q = $this->db->get("seating_type");
			 break;
				  case 10:
		 $q = $this->db->get("other_service");
			 break;
		}
	 
		return $q->result();
	}

	public function venue_update($ide,$idfr,$names,$descs){
		$arrSetting=array(
		'name'=>$names,
		'description'=>$descs,					  
		);
				
	      $this->db->where('id', $ide);
		 switch($idfr){
		 case 1:
		 return $this->db->update('wedding_occasion_list',$arrSetting);
		 break;
		  case 2:
		 return $this->db->update('nwedding_occasion_list',$arrSetting);
		 break;
		  case 3:
		 return $this->db->update('nature_of_venue',$arrSetting);
		 break;
		  case 4:
		 return $this->db->update('venue_type',$arrSetting);
		 break;
		  case 5:
		 return $this->db->update('venue_type_other',$arrSetting);
		 break;
		  case 6:
		 return $this->db->update('floor',$arrSetting);
		 break;
		  case 7:
		 return $this->db->update('suite_type',$arrSetting);
		 break;
		  case 8:
		 return $this->db->update('valet_facility',$arrSetting);
		 break;
								  case 9:
		 return $this->db->update('seating_type',$arrSetting);
		 break;
								  case 10:
		 return $this->db->update('other_service',$arrSetting);
		 break;
		 
	 }

}

}
?>
