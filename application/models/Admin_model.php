<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Model extends CI_Model
{
   // Admin Registration for insert the data ww_admin_login

	public function registration($additional_data)
	{
		$this->db->insert('ww_admin_login',$additional_data);
		return 1;
	}


      // Check the email exit;
	public function emailexists($email)
	{
		$this->db->where('user_email', $email);
		$query = $this->db->get('ww_admin_login');
		if( $query->num_rows() > 0 ){ return 1; } else { return 0; }
	}


       // Add the Photographer for insert the data into ww;
	public function add_photographer($catData) {
		$this->db->insert('ww_admin_login',$catData);
		return $this->db->insert_id();
	}

	public function event_insert($event_array) {
		$result = $this->db->insert('ww_fullcalendar_dates',$event_array);
		return $this->db->insert_id();
	}

	public function list_photographer_services_delete($id) {
		$this->db->where('id', $id);
		$this->db->delete('ww_photographer_services');
	}

	public function listphotographer() {
		$this->db->select('*');
		$this->db->where('role_type',photographer);
		$this->db->from('ww_admin_login');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_names_role($role) {
		$this->db->select('*');
		$this->db->where('role_type',$role);
		$this->db->from('ww_admin_login');
		$query = $this->db->get();
		return $query->result();
	}


        public function select($role) {
		$this->db->select('*');
		$this->db->where('role_type',$role);
		$this->db->from('ww_admin_login');
		$query = $this->db->get();
		return $query->result();
	}

	public function list_photographer_title_images($id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('user_id',$id);
		$this->db->from('ww_sp_photograph');
		$query = $this->db->get();
		return $query->result();
	}

	public function list_photographer_images($id , $user_id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('user_id',$user_id);
		$this->db->where('id',$id);
		$this->db->from('ww_sp_photograph');
		$query = $this->db->get();
		return $query->result();
	}



	public function list_planner_images($id , $user_id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('user_id',$user_id);
		$this->db->where('id',$id);
		$this->db->from('ww_planner_portfolio');
		$query = $this->db->get();
		return $query->result();
	}

	public function list_decorator_images($id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('user_id',$id);
		$this->db->from('ww_decorator_portfolio');
		$query = $this->db->get();
		return $query->result();
	}

	public function list_decorator_admin_images($user_id , $id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('user_id',$user_id);
		$this->db->where('id',$id);
		$this->db->from('ww_decorator_portfolio');
		$query = $this->db->get();
		return $query->result();
	}

	public function list_photographer_videos($id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('user_id',$id);
		$this->db->from('ww_sp_videos');
		$query = $this->db->get();
		return $query->result();
	}

	public function list_photographer_services($id) {
		$this->db->select('*');
		$this->db->from('ww_photographer_services');
		$this->db->where('user_id', $id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_photographer_services($id) {
		$this->db->select('*');
		$this->db->from('ww_photographer_services');
		$this->db->where('user_id', $id);
		$query = $this->db->get();
		return $query->row();
	}

	public function get_decorator_services($id) {
		$this->db->select('*');
		$this->db->from('ww_decorator_services');
		$this->db->where('user_id', $id);
		$query = $this->db->get();
		return $query->row();
	}

	public function edit_listphotographer($id) {
		$this->db->select('*');
		$this->db->from('ww_photographer');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->row();
	}

	public function update_photographer($id , $catData) {
		$this->db->where('id',$id);
	    $this->db->update('ww_photographer',$catData); //echo $this->db->last_query();die;
	}

	public function update_photographer_services($user_id , $additionaldata) {
	    $this->db->where('user_id',$user_id);
	    $this->db->update('ww_photographer_services',$additionaldata);
	}

	public function update_decorator_services($user_id , $additionaldata) {
	    $this->db->where('user_id',$user_id);
	    $this->db->update('ww_decorator_services',$additionaldata);
	}


	public function authenticate($username,$password) {
		$this->db->select('*');
		$this->db->from('ww_admin_login');
		$this->db->where('user_email',$username);
		$this->db->where('password',$password);
		$query = $this->db->get();
		return $query->row();
	}

	public function add_photographer_services($additionaldata) {
		$this->db->insert('ww_photographer_services',$additionaldata);
		return $this->db->insert_id();
	}

	public function photographer_tags($additionaldata) {
		$this->db->insert('ww_sp_photograph',$additionaldata);
		return $this->db->insert_id();
	}

	public function get_photographer_tags($id) {
		$this->db->select('*');
		$this->db->where('user_id',$id);
		$this->db->order_by('date','desc');
		$query=$this->db->get('ww_sp_photograph');
		return $query->row();
	}

	public function add_photographertags($additionaldata,$id) {
		$this->db->where('id',$id);
		$this->db->update('ww_sp_photograph',$additionaldata);
	}

	public function add_video_tags($additionaldata) {
		$this->db->insert('ww_sp_videos',$additionaldata);
	}

	public function add_cinema_tags($additionaldata) {
		$this->db->insert('ww_sp_cinema',$additionaldata);
	}

	/********Select all Service Providers*************/

	public function select_all_serviceprovider()
	{
		$this->db->select('*');
		$this->db->from('ww_admin_login');
		$this->db->where('role_type', 'planner');
		$query = $this->db->get();
		return $query->result();
	}

	public function select_all_decorator()
	{
		$this->db->select('*');
		$this->db->from('ww_admin_login');
		$this->db->where('role_type', 'decorator');
		$query = $this->db->get();
		return $query->result();
	}

	public function select_all_venuemanager()
	{
		$this->db->select('*');
		$this->db->from('ww_admin_login');
		$this->db->where('role_type', 'venue');
		$query = $this->db->get();
		return $query->result();
	}

	public function select_single_data($table,$id)
	{
	  $this->db->select('*');
	  $this->db->from($table);
	  $this->db->where('id', $id);
	  $query = $this->db->get();
	  return $query->result();
	}

	// select select_single_property

        public function select_single_property($tableProperty,$id)
	{
	  $this->db->select('*');
	  $this->db->from($tableProperty);
	  $this->db->where('pro_id', $id);
	  $query = $this->db->get();
	  return $query->result();
	}


        // select select_single_belt

        public function select_single_belt($tableBelt,$id)
	{
	  $this->db->select('*');
	  $this->db->from($tableBelt);
	  $this->db->where('id', $id);
	  $query = $this->db->get();
	  return $query->result();
	}

        // select select_single_brand

        public function select_single_brand($tableBrand,$id)
	{
	  $this->db->select('*');
	  $this->db->from($tableBrand);
	  $this->db->where('id', $id);
	  $query = $this->db->get();
	  return $query->result();
	}

        // select select_single_venue

        public function select_single_venue($tableVenue,$id)
	{
	  $this->db->select('*');
	  $this->db->from($tableVenue);
	  $this->db->where('sp_id', $id);
	  $query = $this->db->get();
	  return $query->result();
	}



	public function select_photographer_services($id)
	{
	  $this->db->select('*');
	  $this->db->from('ww_photographer_services');
	  $this->db->where('user_id', $id);
	  $query = $this->db->get();
	  return $query->result();
	}

	public function select_planner_services($id)
	{
	  $this->db->select('*');
	  $this->db->from('ww_planner_services');
	  $this->db->where('user_id', $id);
	  $query = $this->db->get();
	  return $query->result();
	}

	public function update_data($table,$content,$where)
	{

		$this->db->where($where);
		$result = $this->db->update($table,$content);
		if(($result==1))
		{
		 return true;
		}
		else
		{
		return false;
		}
	}

	public function deletephotographer($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_admin_login');
	}

	public function deletevenue($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_admin_login');
	}

	public function deletedecorator($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_admin_login');
	}

	public function list_planner_services($id) {
		$this->db->select('*');
		$this->db->where('user_id', $id);
		$query = $this->db->get('ww_planner_services');
		return $query->row();
	}

	public function list_planner_arrangements() {
		$this->db->select('*');
		$query = $this->db->get('ww_planner_arrangements');
		return $query->result();
	}

	public function list_planner_events() {
		$this->db->select('*');
		$query = $this->db->get('ww_event');
		return $query->result();
	}

	public function add_planner_services($additionaldata) {
		$this->db->insert('ww_planner_services',$additionaldata);
		return $this->db->insert_id();
	}

	public function get_planner_services($id , $user_id) {
		$this->db->select('*');
		$this->db->where('user_id', $user_id);
		$this->db->where('id', $id);
		$query = $this->db->get('ww_planner_services');
		return $query->row();
	}

	public function update_planner_services($id , $user_id , $additionaldata) {
	    $this->db->where('id',$id);
	   // $this->db->where('user_id',$user_id);
	    $this->db->update('ww_planner_services',$additionaldata);
	}

	public function list_planner_services_delete($id) {
		$this->db->where('id', $id);
		$this->db->delete('ww_planner_services');
	}

	public function planner_tags($additionaldata) {
		$this->db->insert('ww_planner_portfolio',$additionaldata);
		return $this->db->insert_id();
	}

	public function get_planner_tags($id) {
		$this->db->select('*');
		$this->db->where('user_id',$id);
		$this->db->order_by('date','desc');
		$query=$this->db->get('ww_planner_portfolio');
		return $query->row();
	}

	public function add_planner_tags($additionaldata,$id) {
		$this->db->where('id',$id);
		$this->db->update('ww_planner_portfolio',$additionaldata);
	}

	public function get_password($email) {
		$this->db->select('*');
		$this->db->where('user_email', $email);
		$query = $this->db->get('ww_admin_login');
		return $query->row();
	}

	public function update_password($new_password , $email) {
		$this->db->where('user_email',$email);
		$this->db->update('ww_admin_login',$new_password);
		return 1;
	}

	public function update_user_status($user_status , $id) {
		$this->db->where('id',$id);
		$this->db->update('ww_admin_login',$user_status);
		return 1;
	}

	public function current_password($current_password , $user_id) {
		$this->db->where('id', $user_id);
		$this->db->where('password', $current_password);
		$query = $this->db->get('ww_admin_login');
		return $query->row();
	}

	public function change_password($passArray , $id) {
		$this->db->where('id',$id);
		$this->db->update('ww_admin_login',$passArray);
	}

	public function get_last_record($id) {
		$this->db->select('*');
		$this->db->where('user_id', $id);
		$this->db->order_by("date","desc");
		$query = $this->db->get('ww_sp_photograph');
		return $query->row();
	}

	public function delete_last_record($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_sp_photograph');
	}

	public function video_insert($additionaldata) {
		$this->db->insert('ww_sp_videos',$additionaldata);
		return $this->db->insert_id();
	}

	public function get_last_decorator_record($id) {
		$this->db->select('*');
		$this->db->where('user_id', $id);
		$this->db->order_by("date","desc");
		$query = $this->db->get('ww_decorator_portfolio');
		return $query->row();
	}

	public function get_last_planner_record($id) {
		$this->db->select('*');
		$this->db->where('user_id', $id);
		$this->db->order_by("date","desc");
		$query = $this->db->get('ww_planner_portfolio');
		return $query->row();
	}

	public function delete_last_planner_record($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_admin_login');
	}

	public function delete_last_decorator_record($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_decorator_portfolio');
	}

	public function decorator_tags($additionaldata) {
		$this->db->insert('ww_decorator_portfolio',$additionaldata);
		return $this->db->insert_id();
	}

	public function get_decorator_tags($id) {
		$this->db->select('*');
		$this->db->where('user_id',$id);
		$this->db->order_by('date','desc');
		$query=$this->db->get('ww_decorator_portfolio');
		return $query->row();
	}

	public function add_decorator_tags($additionaldata,$id) {
		$this->db->where('id',$id);
		$this->db->update('ww_decorator_portfolio',$additionaldata);
	}

	/*********Funtiona by Manoj*********/
	/********************Add Customer*****************/
	public function add_data($details,$table) {
		$this->db->insert($table,$details);
		return $this->db->insert_id();
	}
		public function select_all_customer()
	{
		$this->db->select('*');
		$this->db->from('ww_customer');
		//$this->db->where('sta', 'decorater');
		$query = $this->db->get();
		return $query->result();
	}
		public function select_recharge_history($id)
	{
		$this->db->select('*');
                $this->db->where('sp_id', $id);
		$this->db->from('ww_recharge_history');
		$query = $this->db->get();
		return $query->result();
	}
	public function select_single_customer($id)
	{
		$this->db->select('*');
		$this->db->from('ww_customer');
		$this->db->where('customer_id', $id);
		$query = $this->db->get();
		return $query->result();
	}
	public function select_all_sp()
	{
		$this->db->select('*');
		$this->db->from('ww_admin_login');
		//$this->db->where('role_type', 'planner');
		$query = $this->db->get();
		return $query->result();
	}
	public function select_all_rchrgsttng()
	{
		$this->db->select('*');
		$this->db->from('ww_recharge_setting');
		//$this->db->where('role_type', 'planner');
		$query = $this->db->get();
		return $query->result();
	}
		public function select_single_recharge_setting($id)
	{
		$this->db->select('*');
		$this->db->from('ww_recharge_setting');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->result();
	}
	public function select_all_cupon()
	{
		$this->db->select('*');
		$this->db->from('ww_cupons');

		$query = $this->db->get();
		return $query->result();
	}
	public function select_single_cupon($id)
	{
		$this->db->select('*');
		$this->db->from('ww_cupons');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function venue_photographer_tags($additionaldata) {
		$this->db->insert('ww_venue_photograph',$additionaldata);
		return $this->db->insert_id();
	}

	public function venue_photographertags($additionaldata,$id) {
		$this->db->where('id',$id);
		$this->db->update('ww_venue_photograph',$additionaldata);
	}

	public function get_venue_tags($id) {
		$this->db->select('*');
		$this->db->where('user_id',$id);
		$this->db->order_by('date','desc');
		$query=$this->db->get('ww_venue_photograph');
		return $query->row();
	}

	public function select_events($id) {
		$this->db->select('*');
		$this->db->where('sp_id', $id);
		$query = $this->db->get('ww_cal_slot');
		return $query->result();
	}

	public function delete_venue_last_record($id)
	{
	  $this->db->where('id', $id);
	  $this->db->delete('ww_venue_photograph');
	}

	// recharge history inserted;

	public function recharge_history($table,$rechargearr)
	{
		$result = $this->db->insert($table,$rechargearr);
		if($result == true):
		return 1;
		else :
		return 0;
		endif;
	}

	public function event_details($event_id)
	{
		$this->db->where('id', $event_id);
		$query = $this->db->get('ww_fullcalendar_dates');
		return $query->row();
	}

	public function event_delete($event_id)
	{
		$this->db->where('id', $event_id);
        $this->db->delete('ww_fullcalendar_dates');
	}

	public function activate_account($statusArray , $id)
	{
	    $this->db->where('id',$id);
		$this->db->update('ww_admin_login',$statusArray);
	}

	public function event_update_details($id , $additionaldata) {
	    $this->db->where('id',$id);
	    $this->db->update('ww_fullcalendar_dates',$additionaldata);
	}

	public function event_slots($events_slots,$start ,$user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('start', $start);
		$this->db->where('events_slots', $events_slots);
		$query = $this->db->get('ww_fullcalendar_dates');
		if( $query->num_rows() > 0 ){ return 1; } else { return 0; }
	}

	public function add_two_slots($additionaldata) {
		$this->db->insert('two_slots',$additionaldata);
		return $this->db->insert_id();
	}

	public function add_three_slots($additionaldata) {
		$this->db->insert('three_slots',$additionaldata);
		return $this->db->insert_id();
	}

	public function select_two_slots()
	{
		$this->db->select('*');
		$this->db->from('two_slots');
		$query = $this->db->get();
		return $query->row();
	}

		public function select_three_slots()
	{
		$this->db->select('*');
		$this->db->from('three_slots');
		$query = $this->db->get();
		return $query->row();
	}

	public function select_venues($id)
	{
		$this->db->select('v_name');
		$this->db->from('ww_venue');
		$this->db->where('sp_id',$id);
		$query = $this->db->get();
		return $query->result();
	}

	//Function By Vipin //
	public function list_venue_title_images($id) {
        $this->db->select('*');
        $this->db->where('user_id',$id);
        $this->db->from('ww_venue_photograph');
        $query = $this->db->get();
        return $query->result();
    }

	public function user($id) {
		$this->db->select('*');
		$this->db->where('id',$id);
		$this->db->from('ww_admin_login');
		$query = $this->db->get();
		return $query->result();
	}

	public function total_event_slots($start,$user_id) {
		$this->db->select('count(*) as rows');
		$this->db->where('id',$user_id);
		$this->db->where('start',$start);
		$this->db->from('ww_fullcalendar_dates');
		$query = $this->db->get();
		return $query->result();
	}
		/******Send_proposal Starts here*********/
	public function send_proposal($table,$proposalarray)
	{
		$result = $this->db->insert($table,$proposalarray);
		return true;
	}
		/******get_proposal Starts here*********/
	public function get_proposal($user_id)
	{
		$this->db->where('sp_id', $user_id);
		$result= $this->db->get('ww_customer_proposal');
		if($result->num_rows() > 0):
		return $result->result();
		endif;
	}

	public function add_decorator_services($additionaldata) {
		$this->db->insert('ww_decorator_services',$additionaldata);
		return $this->db->insert_id();
	}

	public function list_decorator_services_delete($id) {
		$this->db->where('id', $id);
		$this->db->delete('ww_decorator_services');
	}

	public function list_deco_services($id) {
		$this->db->select('*');
		$this->db->from('ww_decorator_services');
		$this->db->where('user_id', $id);
		$query = $this->db->get();
		return $query->row();
	}
        public function update_decoratorservices($id , $additionaldata) {
		$this->db->where('user_id',$id);
		$this->db->update('ww_decorator_services',$additionaldata);
	}
/********************Portfolio*******************/
public function list_planner_title_images($id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('sp_id',$id);
		$this->db->from('ww_portfolio_images');
		$query = $this->db->get();
		return $query->result();
	}
	public function list_album($id) {
		$this->db->select('*');
		//$this->db->where('role_type',photographer);
		$this->db->where('sp_id',$id);
		$this->db->from('ww_portfolio');
		$query = $this->db->get();
		return $query->result();
	}
		public function get_data_all($table)
	{
		$this->db->select('*');
		$this->db->from($table);
		//$this->db->where('sta', 'decorater');
		$query = $this->db->get();
		return $query->result();
	}
//******************************************************************************************************************************************************
	public function get_data($table,$where)
	{

  //  echo "in admin_model get_data<br>";
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();

  }
  
  public function get_slots($table,$where)
		{

			//  echo "in admin_model get_data<br>";
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$this->db->order_by("slot_id", "desc");
			$query = $this->db->get();
			return $query->result();

		}
		
	public function get_data_like($table,$like)
	{

	//  echo "in admin_model get_data<br>";
		$this->db->select('*');
		$this->db->from($table);
		$this->db->like($like);
		$query = $this->db->get();
		return $query->result();

	}
	

	public function get_data_distinct($d,$table,$where)
	{

		//$this->db->select('*');
		$this->db->distinct();
		$this->db->select($d);
		$this->db->from($table);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result();
	}
	
	
	
	public function get_inq($table,$where)
				{

					//  echo "in admin_model get_data<br>";
					$this->db->select('*');
					$this->db->from($table);
					$this->db->where($where);
					$this->db->order_by("id", "desc");
					$query = $this->db->get();
					return $query->result();

				}

       public function get_inq_asc($table,$where)
				{

				//  echo "in admin_model get_data<br>";
					$this->db->select('*');
					$this->db->from($table);
				        $this->db->where($where);
					$this->db->order_by("id", "asc");
					$query = $this->db->get();
					return $query->result();				

				}
	
	
	
}
?>
