<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Inquiry_model extends CI_model
{
		function __construct()
		{
		 parent::__construct();
		}
		/****************insert inquiry***********************/
		public function insert_inquiry($inquiryarray)
		{

			$result = $this->db->insert('ww_inquiry',$inquiryarray);
			if($result==TRUE):
			$data = "success";

			else:
			$data = "fail";
endif;
			return $data;
		}

		public function insert_inquiry_k($inquiryarray)
		{

			$result = $this->db->insert('ww_inquiry_k',$inquiryarray);
			if($result==TRUE):
			$data = "success";

			else:
			$data = "fail";
endif;
			return $data;
		}

		/****************update inquiry***********************/
		public function update_inquiry($inquiryarray,$id)
		{
			$this->db->where('inq_id',$id);
			$result = $this->db->update('ww_inquiry',$inquiryarray);
			if($result==TRUE):
			$data = "Inquiry update successfully";
			$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
			redirect('Inquiry');
			else:
			$data = "Something is wrong";
			$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
			redirect('Inquiry');
			endif;

		}
		public function update_inquiry2($inquiryarray,$id)
		{
			$this->db->where('inq_id',$id);
			$result = $this->db->update('ww_inquiry_k',$inquiryarray);
			if($result==TRUE):
			$data = "success";

			else:
			$data = "fail";
			endif;
return $data;
		}
		/****************delete inquiry***********************/
		public function delete_inquiry($id)
		{
			$this->db->where('inq_id',$id);
			$result = $this->db->delete('ww_inquiry');
			if($result==TRUE):
			$data = "Inquiry delete successfully";
			$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
			redirect('Inquiry');
			else:
			$data = "Something is wrong";
			$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
			redirect('Inquiry');
			endif;

		}
		/****************get inquiry***********************/
        public function get_inquiry($id)
		{
			$this->db->where('sp_id',$id);
			$result = $this->db->get('ww_inquiry');
			if($result):
			return $result->result();
			endif;

		}

		/****************single get inquiry***********************/
		public function get_single_inquiry($id)
		{
			$this->db->where('inq_id',$id);
			$result = $this->db->get('ww_inquiry');
			if($result):
			return $result->result();
			endif;
		}
		/****************active inquiry***********************/
		public function active_inquiry($id,$status)
		{
			$this->db->where('inq_id',$id);
			$arry = array('status'=>$status);
			$result = $this->db->update('ww_inquiry',$arry);
			if($result == TRUE):
			return TRUE;
			endif;
		}

}
?>
