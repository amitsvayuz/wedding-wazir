<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Setting_model extends CI_model
{
/****************get city***********************/	
		public function get_city()
		{
			$result= $this->db->get('city_setting');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
/****************fetch_city***********************/	
		public function fetch_city()
		{
			$result= $this->db->get('ww_city');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}

		
		/****************get cityById***********************/	
		public function get_cityById($id)
		{
                        $this->db->where('id', $id);
			$result= $this->db->get('city_setting');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		
		
		/****************get property***********************/	
		public function get_property()
		{
                        
			$result= $this->db->get('ww_admin_login');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		/****************add city***********************/	
		public function add_brand($tablename,$cityarray)
		{
			$result = $this->db->insert($tablename,$cityarray);
			if($result=true){
			$data = "Successful brand add";
			$data = base64_encode($data);
			redirect('General_setting/list_brand?set='.$data);
			}else{
			$data = "Something is wrong";
			$data = base64_encode($data);
			redirect('General_setting/list_brand?set='.$data);
			}
		}
		
		/****************update city***********************/	
		public function brand_update($tablename,$cityarray,$id)
		{
			$result = $this->db->update($tablename,$cityarray,"id=$id");
			if($result=true){
			$data = "Successful brand update";
			$data = base64_encode($data);
			redirect('General_setting/list_brand?set='.$data);
			}else{
			$data = "Something is wrong";
			$data = base64_encode($data);
			redirect('General_setting/list_brand?set='.$data);
			}
		}
		/****************delete city***********************/	
		public function delete_brand($id)
		{
	      $this->db->where('id', $id);
	      $result=$this->db->delete('city_setting'); 
		  if($result=true){
			$data = "Successful brand delete";
			$data = base64_encode($data);
			redirect('General_setting/list_brand?set='.$data);
			}else{
			$data = "Something is wrong";
			$data = base64_encode($data);
			redirect('General_setting/list_brand?set='.$data);
	       }
	   }
/****************INSERT COMPANY***********************/
		public function add_company($tablename,$companyarray)
		{
			    $result = $this->db->insert($tablename,$companyarray);
				if($result=true){
			$data = "Successful city add";
			$data = base64_encode($data);
			redirect('General_setting/list_city?set='.$data);
			}else{
			$data = "Something is wrong";
			$data = base64_encode($data);
			redirect('General_setting/list_city?set='.$data);
	   }
		}
		
		public function update_company($tablename,$companyarray,$id)
		{
			    $result = $this->db->update($tablename,$companyarray,"c_id=$id");
				if($result=true){
			$data = "Successful city update";
			$data = base64_encode($data);
			redirect('General_setting/list_city?set='.$data);
			}else{
			$data = "Something is wrong";
			$data = base64_encode($data);
			redirect('General_setting/list_city?set='.$data);
	   }
		}
		
		
/****************get company***********************/	
		public function get_company()
		{
			$result= $this->db->get('ww_company');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		public function get_companyById($id)
		{
			$this->db->where('c_id', $id);
			$result= $this->db->get('ww_company');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		
		public function delete_company($id)
		{
	      $this->db->where('c_id', $id);
	      $result=$this->db->delete('ww_company'); 
		  if($result=true){
			$data = "Successful city delete";
			$data = base64_encode($data);
			redirect('General_setting/list_city?set='.$data);
			}else{
			$data = "Something is wrong";
			$data = base64_encode($data);
			redirect('General_setting/list_city?set='.$data);
	       }
	   }
	   
	   public function get_beltById($id)
		{
			$this->db->where('id', $id);
			$result= $this->db->get('ww_venue_belt');
			if($result->num_rows() > 0):
			return $result->row();
			endif;
			
		}
		
		public function delete_belt($id)
		{
	      $this->db->where('id', $id);
	      $result=$this->db->delete('ww_venue_belt'); 
		  if($result=true)
		  {
			$data = "Successful belt delete";
			$data = base64_encode($data);
			redirect('General_setting/list_belt?set='.$data);
		  }
		  else
		 {
			$data = "Something is wrong";
			$data = base64_encode($data);
			redirect('General_setting/list_belt?set='.$data);
		 }
		}
		
		public function updateBelt($belt_Data,$id)
		{
			$this->db->where('id', $id);
			$result=$this->db->update('ww_venue_belt',$belt_Data);
			if($result=true)
			{
				$data = "Successful belt update";
				$data = base64_encode($data);
				redirect('General_setting/list_belt?set='.$data);
			}
			else
			{
				$data = "Something is wrong";
				$data = base64_encode($data);
				redirect('General_setting/list_belt?set='.$data);
		    }
		}
		
		public function get_venue_belt()
		{
			$result= $this->db->get('ww_venue_belt');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
		
		public function addBelt($addBelt)
		{
			$result=$this->db->insert('ww_venue_belt',$addBelt);
			if($result=true)
			{
				$data = "Successful belt add";
				$data = base64_encode($data);
				redirect('General_setting/list_belt?set='.$data);
			}
			else
			{
				$data = "Something is wrong";
				$data = base64_encode($data);
				redirect('General_setting/list_belt?set='.$data);
			
			}
		}
/****************add location***********************/
		public function add_location($tablename,$locarray)
		{
			
			$result=$this->db->insert($tablename,$locarray);
			if($result=true)
			{
				$data = "Location added successfully";
				$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
				redirect('General_setting/location');
			}
			else
			{
				$data = "Something is wrong";
				$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
				redirect('General_setting/location');
			
			}
		}
		
		/****************update model location***********************/
		public function update_location($tablename,$locarray,$id)
		{
			$this->db->where('loc_id',$id);
			$result=$this->db->update($tablename,$locarray);
			if($result=true)
			{
				$data = "Location updated successfully";
				$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
				redirect('General_setting/location');
			}
			else
			{
				$data = "Something is wrong";
				$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
				redirect('General_setting/location');
			
			}
		}
		
		/****************delete location***********************/
		public function delete_location($tablename,$id)
		{
			$this->db->where('loc_id',$id);
			$result=$this->db->delete($tablename);
			if($result=true)
			{
				$data = "Location deleted successfully";
				$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'success'));
				redirect('General_setting/location');
			}
			else
			{
				$data = "Something is wrong";
				$this->session->set_flashdata('message',array('set'=>$data , 'class'=>'danger'));
				redirect('General_setting/location');
			
			}
		}
		
		/****************get location***********************/
		public function get_location()
		{
			$result= $this->db->get('ww_location');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}
        
        /****************getById_location***********************/
		public function getById_location($id)
		{
			$this->db->where('loc_id',$id);
			$result= $this->db->get('ww_location');
			if($result->num_rows() > 0):
			return $result->result();
			endif;
			
		}	
		
}
?>
